package com.etihad.book.common.configuration;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <p>
 * Factory to get a <code>Configuration</code> instance for the requested URL to
 * the properties file. The <code>ConfigurationFactory</code> provides methods
 * to individually create a <code>Configuration</code> instance with automatic
 * refresh of properties in a specified interval.
 * </p>
 * 
 * 
 */
public class ConfigurationFactory {

	private static final String MESSAGE_PREFIX = "ConfigurationFactory::";

	private static final String NL = "\n";

	private static final String TAB = "\t";

	// default refresh delay in ms
	private static final long DEFAULT_REFRESH_DELAY = 600;

	private static final boolean DEFAULT_AUTOMATIC_REFRESH_ENABLED = true;

	private static Log cLog = LogFactory.getLog(ConfigurationFactory.class);

	private static ConfigurationFactory cInstance = new ConfigurationFactory();

	private long mRefreshDelay = DEFAULT_REFRESH_DELAY;

	private boolean mAutomaticRefreshEnabled = DEFAULT_AUTOMATIC_REFRESH_ENABLED;

	/**
	 * Returns the singleton <code>ConfigurationFactory</code> instance.
	 * 
	 * @return ConfigurationFactory - the singleton
	 *         <code>ConfigurationFactory</code> instance
	 */
	public static ConfigurationFactory getInstance() {
		if (cInstance == null) {
			cInstance = new ConfigurationFactory();
		}
		return cInstance;
	}

	/**
	 * Creates and returns a <code>Configuration</code> instance for the given
	 * URL to the properties file.
	 * 
	 * The API of the <code>Configuration</code> class can be viewed at <a href=
	 * "http://jakarta.apache.org/commons/configuration/apidocs_1.2/index.html">Apache
	 * Commons Configuration 1.2 </a>
	 * 
	 * @param pPropertiesUrl
	 *            the URL to the requested properties file to create a
	 *            <code>Configuration</code> instance for
	 * @param pRefreshDelay
	 *            the delay for checking the underlying properties file after
	 *            changes
	 * @param pAutomaticRefreshEnabled
	 *            if the configuration should be updated automatically if the
	 *            underlying properties file changed, set it to
	 *            <code>true</code> if automatic refresh should be active else
	 *            <code>false</code>
	 * @return Configuration - the created <code>Configuration</code> instance
	 *         for the given URL of the properties file
	 * @throws ConfigurationFactoryException
	 *             if an error occurs creating the <code>Configuration</code>
	 *             instance
	 */
	public Configuration getConfiguration(String pPropertiesUrl, long pRefreshDelay, boolean pAutomaticRefreshEnabled)
			throws ConfigurationFactoryException {

		if (cLog.isDebugEnabled()) {
			cLog.debug(MESSAGE_PREFIX + "getConfiguration() - start with URL=[" + pPropertiesUrl + "] refresh delay=["
					+ pRefreshDelay + "] and automatic refresh enabled=[" + pAutomaticRefreshEnabled + "]");
		}

		PropertiesConfiguration lPropertiesConfiguration = null;
		try {
			// create properties configuration by given URL
			lPropertiesConfiguration = new PropertiesConfiguration(pPropertiesUrl);
		} catch (ConfigurationException lException) {
			String lMessage = "could not create configuration for url=[" + pPropertiesUrl + "]";
			cLog.error(MESSAGE_PREFIX + "getConfiguration() - " + lMessage, lException);
			throw new ConfigurationFactoryException(lMessage, lException);
		}

		if (pAutomaticRefreshEnabled) {
			// create reload strategy to monitor property file changes
			FileChangedReloadingStrategy lReloadingStrategy = new FileChangedReloadingStrategy();
			// set configured delay before checking the file next time
			lReloadingStrategy.setRefreshDelay(pRefreshDelay);
			// set configuration to watch
			lReloadingStrategy.setConfiguration(lPropertiesConfiguration);

			// set reloading strategy to configuration
			lPropertiesConfiguration.setReloadingStrategy(lReloadingStrategy);

			if (cLog.isInfoEnabled()) {
				cLog.info(MESSAGE_PREFIX
						+ "getConfiguration() - automatic properties file check ACTIVE with refresh delay=["
						+ pRefreshDelay + "]");
			}
		} else {
			if (cLog.isInfoEnabled()) {
				cLog.info(MESSAGE_PREFIX + "getConfiguration() - automatic properties file check INACTIVE");
			}
		}

		if (cLog.isDebugEnabled()) {
			cLog.debug(MESSAGE_PREFIX + "getConfiguration() - end");
		}

		return lPropertiesConfiguration;
	}

	/**
	 * Creates and returns a <code>Configuration</code> instance for the given
	 * URL to the properties file.
	 * 
	 * The <code>Configuration</code> returned will be configured with the
	 * default delay from the <code>ConfigurationFactory</code> if the automatic
	 * refresh enabled flag is set to <code>true</code> in the
	 * <code>ConfigurationFactory</code>.
	 * 
	 * The API of the <code>Configuration</code> class can be viewed at <a href=
	 * "http://jakarta.apache.org/commons/configuration/apidocs_1.2/index.html">Apache
	 * Commons Configuration 1.2 </a>
	 * 
	 * @param pPropertiesUrl
	 *            the URL to the requested properties file to create a
	 *            <code>Configuration</code> instance for
	 * @return Configuration - the created <code>Configuration</code> instance
	 *         for the given URL of the properties file
	 * @throws ConfigurationFactoryException
	 *             if an error occurs creating the <code>Configuration</code>
	 *             instance
	 */
	public Configuration getConfiguration(String pPropertiesUrl) throws ConfigurationFactoryException {
		return getConfiguration(pPropertiesUrl, mRefreshDelay, mAutomaticRefreshEnabled);
	}

	/**
	 * Creates and returns a <code>Configuration</code> instance for the given
	 * URL to the properties file.
	 * 
	 * The <code>Configuration</code> returned will be configured with the given
	 * delay if the automatic refresh enabled flag is set to <code>true</code>
	 * in the <code>ConfigurationFactory</code>.
	 * 
	 * The API of the <code>Configuration</code> class can be viewed at <a href=
	 * "http://jakarta.apache.org/commons/configuration/apidocs_1.2/index.html">Apache
	 * Commons Configuration 1.2 </a>
	 * 
	 * @param pPropertiesUrl
	 *            the URL to the requested properties file to create a
	 *            <code>Configuration</code> instance for
	 * @param pRefreshDelay
	 *            the delay for checking the underlying properties file after
	 *            changes
	 * @return Configuration - the created <code>Configuration</code> instance
	 *         for the given URL of the properties file
	 * @throws ConfigurationFactoryException
	 *             if an error occurs creating the <code>Configuration</code>
	 *             instance
	 */
	public Configuration getConfiguration(String pPropertiesUrl, long pRefreshDelay)
			throws ConfigurationFactoryException {
		return getConfiguration(pPropertiesUrl, pRefreshDelay, mAutomaticRefreshEnabled);
	}

	/**
	 * Creates and returns a <code>Configuration</code> instance for the given
	 * URL to the properties file.
	 * 
	 * The <code>Configuration</code> returned will be configured with the delay
	 * configured in the <code>ConfigurationFactory</code> if the automatic
	 * refresh enabled flag is set to <code>true</code>.
	 * 
	 * The API of the <code>Configuration</code> class can be viewed at <a href=
	 * "http://jakarta.apache.org/commons/configuration/apidocs_1.2/index.html">Apache
	 * Commons Configuration 1.2 </a>
	 * 
	 * @param pPropertiesUrl
	 *            the URL to the requested properties file to create a
	 *            <code>Configuration</code> instance for
	 * @param pAutomaticRefreshEnabled
	 *            if the configuration should be updated automatically if the
	 *            underlying properties file changed, set it to
	 *            <code>true</code> if automatic refresh should be active else
	 *            <code>false</code>
	 * @return Configuration - the created <code>Configuration</code> instance
	 *         for the given URL of the properties file
	 * @throws ConfigurationFactoryException
	 *             if an error occurs creating the <code>Configuration</code>
	 *             instance
	 */
	public Configuration getConfiguration(String pPropertiesUrl, boolean pAutomaticRefreshEnabled)
			throws ConfigurationFactoryException {
		return getConfiguration(pPropertiesUrl, mRefreshDelay, pAutomaticRefreshEnabled);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder lSb = new StringBuilder();

		lSb.append("ConfigurationFactory[").append(NL);
		lSb.append(TAB).append("automaticRefreshEnabled=[").append(mAutomaticRefreshEnabled).append("]").append(NL);
		lSb.append(TAB).append("refreshDelay=[").append(mRefreshDelay).append("]").append(NL);
		lSb.append("]").append(NL);

		return lSb.toString();
	}
}
