package com.etihad.book.schema.common;

import java.io.Serializable;

/**
 * @author Danish
 */
public class GenericResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	protected Messages messages;

	/**
	 * @return the messages
	 */
	public Messages getMessages() {
		return messages;
	}

	/**
	 * @param messages
	 *            the messages to set
	 */
	public void setMessages(Messages messages) {
		this.messages = messages;
	}

}
