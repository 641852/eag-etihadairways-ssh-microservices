/**
 * 
 */
package com.etihad.book.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used for logging.This is logging using the LoggingAspect
 * class.By default logStackTrace is set to false which means it will only print
 * i/p o/p. When we enable logStacktrace = true it will print the whole
 * logStackTrace.
 * 
 *  @ Author Danish
 * 
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface EnableAutoLog {
	public boolean logStackTrace() default true;
}
