/**
 * 
 */
package com.etihad.book.schema.common;

import java.io.Serializable;

/**
 * 
 * @author Danish
 *
 */

public abstract class AbstractRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private SecurityContext securityContext;

	private RequestHeader requestHeader;

	/**
	 * @return the securityContext
	 */
	public SecurityContext getSecurityContext() {
		return securityContext;
	}

	/**
	 * @return the requestHeader
	 */
	public RequestHeader getRequestHeader() {
		return requestHeader;
	}

	/**
	 * @param securityContext
	 *            the securityContext to set
	 */
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}

	/**
	 * @param requestHeader
	 *            the requestHeader to set
	 */
	public void setRequestHeader(RequestHeader requestHeader) {
		this.requestHeader = requestHeader;
	}

}
