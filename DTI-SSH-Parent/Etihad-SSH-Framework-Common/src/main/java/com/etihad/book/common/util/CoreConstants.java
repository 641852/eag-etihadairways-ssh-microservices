package com.etihad.book.common.util;

/**
 * 
 * @author Danish
 *
 */
public class CoreConstants {

	public static final String NO_ERROR = "No Error";
	public static final String PROPERTIES_FILE_PATH = "config/dti-ssh.properties";
	
	
	private CoreConstants() {

	}

}
