package com.etihad.book.common.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.schema.common.Message;
import com.etihad.book.schema.common.Messages;

/**
 * 
 * @author Danish
 *
 */

@Component
public class Utils {

	private static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	private static final String GMT = "Etc/GMT";

	@Autowired
	private Config config;

	/**
	 * This method populate response header as per error codes. If the errorCodes is
	 * empty it will call populateSuccessResponse method.
	 * 
	 * @param errorCodes
	 *            the errorCodes
	 * @return the messages
	 */
	public Messages populateResponseHeader(List<String> errorCodes) {
		if (errorCodes.isEmpty()) {
			return populateSuccessResponse();
		}

		List<Message> messageList = new ArrayList<>();
		long count = 0;
		for (String errorCode : errorCodes) {
			count++;
			Message message = new Message();

			message.setCode(config.getProperty(errorCode + ".errorCode"));
			message.setText(config.getProperty(errorCode + ".errorMsg"));
			messageList.add(message);
		}
		Messages messages = new Messages();
		messages.setCode(1);
		messages.setCount(count);
		messages.setMessage(messageList);
		return messages;
	}

	public Messages populateCustomResponseHeader(String faultcode, String faultstring, String faultactor,
			String detail) {
		List<Message> messageList = new ArrayList<>();
		long count = 1;
		Message message = new Message();
		message.setCode(faultcode);
		message.setText("Fault: " + faultcode + " Detail: " + detail + " Fault String: " + faultstring + " Actor: "
				+ faultactor);
		messageList.add(message);
		Messages messages = new Messages();
		messages.setCode(1);
		messages.setCount(count);
		messages.setMessage(messageList);
		return messages;
	}

	/**
	 * This method is used to populate success response.
	 * 
	 * @return the messages
	 */
	private static Messages populateSuccessResponse() {
		Message message = new Message();
		message.setCode("0");
		message.setText("Success");
		List<Message> messageList = new ArrayList<>();
		messageList.add(message);
		Messages messages = new Messages();
		messages.setCode(0);
		messages.setCount(1);
		messages.setMessage(messageList);
		return messages;
	}

	

	/**
	 * 
	 * @param timeStr
	 * @param currentFormat
	 * @param requiredFormat
	 * @return
	 * @throws ParseException
	 */
	public String formattedTime(String timeStr, String currentFormat, String requiredFormat) throws ParseException {
		SimpleDateFormat currentSdf = new SimpleDateFormat("HHmm");
		Date shortTime = currentSdf.parse(timeStr);

		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(shortTime);
	}


	public static String formatDate(GregorianCalendar calendar, String... format) {

		SimpleDateFormat fmt = new SimpleDateFormat(format.length == 0 ? YYYY_MM_DD_HH_MM_SS : format[0]);
		fmt.setCalendar(calendar);
		return fmt.format(calendar.getTime());
	}

	public static String getGMTTime() {

		SimpleDateFormat dateFormat = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS);
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		return dateFormat.format(new Date());
	}

	public static String getGMTTime(GregorianCalendar calendar) {

		SimpleDateFormat dateFormat = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS);
		dateFormat.setCalendar(calendar);
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		return dateFormat.format(calendar.getTime());
	}

	public static String changeTimezoneFrom(String dateToChange, String format, String tzFrom, String tzTo) {
		tzFrom = tzFrom == null ? GMT : tzFrom;
		tzTo = tzTo == null ? GMT : tzTo;
		format = null == format ? YYYY_MM_DD_HH_MM_SS : format;
		DateTimeFormatter fromDTF = DateTimeFormatter.ofPattern(format).withZone(ZoneId.of(tzFrom));
		ZonedDateTime zdt = ZonedDateTime.parse(dateToChange, fromDTF);
		DateTimeFormatter toDTF = DateTimeFormatter.ofPattern(format).withZone(ZoneId.of(tzTo));
		return zdt.format(toDTF);
	}

	public static String changeTimezoneFrom(String dateToChange, String format, String tzFrom) {
		tzFrom = tzFrom == null ? "GMT" : tzFrom;
		String tzTo = "Etc/GMT";

		format = null == format ? "yyyy-MM-dd'T'HH:mm:ss"  : format;
		DateTimeFormatter fromDTF = DateTimeFormatter.ofPattern(format).withZone(ZoneId.of(tzFrom));
		ZonedDateTime zdt = ZonedDateTime.parse(dateToChange, fromDTF);
		DateTimeFormatter toDTF = DateTimeFormatter.ofPattern(format).withZone(ZoneId.of(tzTo));
		return zdt.format(toDTF);
	}

	public static String changeStringDateFormat(String dateToFormat, String fromFormat, String toFormat) {
		DateFormat originalFormat = new SimpleDateFormat(fromFormat, Locale.ENGLISH);
		DateFormat targetFormat = new SimpleDateFormat(toFormat);
		Date date;
		try {
			date = originalFormat.parse(dateToFormat);
		} catch (ParseException e) {
			return null;
		}
		return targetFormat.format(date);
	}

	public String formatXMLDate(XMLGregorianCalendar xc) {
		 DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm a z");
	     GregorianCalendar gCalendar = xc.toGregorianCalendar();
	     return df.format(gCalendar.getTime());
	}
	
	
}