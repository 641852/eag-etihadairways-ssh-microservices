package com.etihad.book.schema.common;

import java.io.Serializable;

/**
 * This class contains text and code which we get as response from the service.
 * 
 */
public class Message implements Serializable {

	private static final long serialVersionUID = 5557726419966195956L;
	private String text;
	private String code;

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param text
	 *            the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

}
