package com.etihad.book.schema.common;

import java.io.Serializable;

/**
 * 
 * @author Danish
 *
 */
public class RequestHeader implements Serializable {

	private static final long serialVersionUID = -1130064914593212602L;
	private String screenName;

	/**
	 * @return the screenName
	 */
	public String getScreenName() {
		return screenName;
	}

	/**
	 * @param screenName
	 *            the screenName to set
	 */
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

}
