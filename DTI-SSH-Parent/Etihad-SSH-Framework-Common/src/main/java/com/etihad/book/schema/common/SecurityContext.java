package com.etihad.book.schema.common;

import java.io.Serializable;

/**
 * @author Danish
 *
 */
public class SecurityContext implements Serializable {

	private static final long serialVersionUID = 3924712199955103454L;
	private String oAuth;
	private String correlationId;

	public String getoAuth() {
		return oAuth;
	}

	public void setoAuth(String oAuth) {
		this.oAuth = oAuth;
	}

	

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

}
