/**
 * 
 */
package com.etihad.book.common.logger;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.etihad.book.common.annotations.EnableAutoLog;
import com.etihad.book.common.exception.ApplicationException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This is the aspect class for logging.
 * @author Danish
 *
 */
@Aspect
@Component
public class LoggingAspect {

	private static final ObjectMapper mapper = new ObjectMapper();
	private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAspect.class);

	@Around(value = "@annotation(autoLog)")
	public Object around(ProceedingJoinPoint point, EnableAutoLog autoLog) throws Throwable {

		long start = System.currentTimeMillis();
		String methodName = MethodSignature.class.cast(point.getSignature()).getDeclaringTypeName() + "."
				+ MethodSignature.class.cast(point.getSignature()).getMethod().getName();
		String logId = "ApplicationLogger Thread-" + Thread.currentThread().getId();
		LOGGER.info(logId + " in : " + methodName + " with parameters");
		for (Object o : point.getArgs())
			LOGGER.info(mapper.writeValueAsString(o));
		Object result = null;
		try {
			result = point.proceed();
			LOGGER.info(logId + " out :" + mapper.writeValueAsString(result));
		} catch (ApplicationException e) {
			LOGGER.info(logId + " Caught Application Exception. Please see error log. ");
			LOGGER.error(logId + " Caught Application Exception ", e);
			throw e;
		} catch (Throwable t) {
			LOGGER.info(logId + " Caught Exception. Please see error log. ");
			if (autoLog.logStackTrace())
				LOGGER.error(logId + " Caught Exception ", t);
			else {
				LOGGER.error(logId + " Stack trace is suppressed");
				LOGGER.error(logId + " Caught Exception " + t.getMessage());
			}
			throw t;
		} finally {
			LOGGER.info(logId + " time taken :" + (System.currentTimeMillis() - start) + "ms");
			LOGGER.trace(logId + methodName + " time taken :" + (System.currentTimeMillis() - start) + "ms");
		}

		return result;
	}

}
