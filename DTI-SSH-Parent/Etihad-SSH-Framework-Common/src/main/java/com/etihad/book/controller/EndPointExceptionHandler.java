package com.etihad.book.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.etihad.book.schema.common.GenericResponse;


/**
 * 
 * @author Danish
 *
 */

@ControllerAdvice
public class EndPointExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private Utils util;
	@Autowired
	private Config config;

	@ExceptionHandler(Throwable.class)
	public ResponseEntity<GenericResponse> handleException(Throwable exception) {
		GenericResponse errorResponse = new GenericResponse();
		HttpStatus httpStatus;
		if (exception instanceof ApplicationException) {
			ApplicationException applicationException = (ApplicationException) exception;
			errorResponse.setMessages(applicationException.getMessages());
			httpStatus = HttpStatus.OK;
		} else {
			errorResponse.setMessages(util.populateCustomResponseHeader(config.getProperty("errorCode"), null, null,
					exception.getMessage()));
			httpStatus = HttpStatus.SERVICE_UNAVAILABLE;

		}
		return new ResponseEntity<>(errorResponse, httpStatus);
	}

	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		Map<String, String> responseBody = new HashMap<>();
		responseBody.put("path", request.getContextPath());
		responseBody.put("message", "The URL you have reached is not in service at this time (404).");
		return new ResponseEntity<>(responseBody, HttpStatus.NOT_FOUND);
	}
}
