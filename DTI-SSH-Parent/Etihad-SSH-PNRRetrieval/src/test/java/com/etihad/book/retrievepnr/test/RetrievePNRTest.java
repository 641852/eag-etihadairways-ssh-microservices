/**
 * 
 */
package com.etihad.book.retrievepnr.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.retrievepnr.controller.RetrievePNRController;
import com.etihad.book.retrievepnr.schema.RetrievePNRRequest;
import com.etihad.book.retrievepnr.schema.RetrievePNRResponse;
import com.etihad.book.retrievepnr.schema.SearchParameters;
import com.etihad.book.retrievepnr.service.RetrievePNRServiceImpl;
import com.etihad.book.schema.common.Message;
import com.etihad.book.schema.common.Messages;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author CTS
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RetrievePNRTest {
	
	private MockMvc mockMvc;

	@MockBean
	RetrievePNRServiceImpl retrievePNRService;

	@InjectMocks
	private RetrievePNRController retrievePNRController;

	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private Config config;

	@Before
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(retrievePNRController).build();

	}

	@Test
	public void testControllerForRetrievePNR() throws Exception {

		Mockito.when(retrievePNRService.retrievePNR(Mockito.anyObject()))
				.thenReturn(formTestResponseForRetrievePNR());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/service/v1/pnrretrieval")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(fromRetrievePNRRequest()))
				.accept(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content()
						.string(objectMapper.writeValueAsString(formTestResponseForRetrievePNR())))
				.andExpect(jsonPath("$.messages", notNullValue()))
				.andExpect(jsonPath("$.pnrdataAvailable", is(true)))
				.andExpect(jsonPath("$.messages.message[0].text", is("Success")))
				.andExpect(jsonPath("$.messages.message[0].code", is("0")));

	}
	
	private  RetrievePNRRequest fromRetrievePNRRequest() {
		
		final RetrievePNRRequest req = new RetrievePNRRequest(); 
		SearchParameters searchParams = new SearchParameters();
		searchParams.setLastName(config.getProperty("tripsearch.lastName"));
		searchParams.setPnr(config.getProperty("tripsearch.pnr"));
		searchParams.setBinarySecurityToken("Shared/IDL:IceSess\\/SessMgr:1\\.0.IDL/Common/!ICESMS\\/CERTG!ICESMSLB\\/CRT.LB!-3170940443955548277!1507542!0");
		req.setSearchParameters(searchParams);
		return req;
	}

	

	private RetrievePNRResponse formTestResponseForRetrievePNR() {
		
		final RetrievePNRResponse response = new RetrievePNRResponse();
		
		final Messages messages = new Messages();
		final List<Message> msg = new ArrayList<>();
		final Message message = new Message();
		message.setCode("0");
		message.setText("Success");
		msg.add(message);
		messages.setErrorMessages(msg);
		response.setMessages(messages);
		response.setPNRDataAvailable(true);
		return response;
	}
}