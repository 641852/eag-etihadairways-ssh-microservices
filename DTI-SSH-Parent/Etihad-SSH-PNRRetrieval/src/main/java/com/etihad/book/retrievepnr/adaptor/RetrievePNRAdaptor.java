/**
 * 
 */
package com.etihad.book.retrievepnr.adaptor;


import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.etihad.book.retrievepnr.constants.Constants;
import com.etihad.book.retrievepnr.schema.RetrievePNRRequest;
import com.etihad.book.retrievepnr.schema.RetrievePNRResponse;
import com.sabre.webservices.pnrbuilder.v1_15.PassengersPNRB;
import com.sabre.webservices.triprecord.MatchMode;
import com.sabre.webservices.triprecord.ObjectFactory;
import com.sabre.webservices.triprecord.StringWithMatchMode;
import com.sabre.webservices.triprecord.TripSearchRQ;
import com.sabre.webservices.triprecord.TripSearchRQ.ReadRequests.ReservationReadRequest;
import com.sabre.webservices.triprecord.TripSearchRQ.ReadRequests.ReservationReadRequest.LocatorCriteria;
import com.sabre.webservices.triprecord.TripSearchRQ.ReadRequests.ReservationReadRequest.LocatorCriteria.Locator;
import com.sabre.webservices.triprecord.TripSearchRQ.ReadRequests.ReservationReadRequest.NameCriteria;
import com.sabre.webservices.triprecord.TripSearchRQ.ReadRequests.ReservationReadRequest.NameCriteria.Name;
import com.sabre.webservices.triprecord.TripSearchRQ.ReadRequests.ReservationReadRequest.PosCriteria;
import com.sabre.webservices.triprecord.TripSearchRQ.ReadRequests.ReservationReadRequest.ReturnOptions;
import com.sabre.webservices.triprecord.TripSearchRQ.ReadRequests.ReservationReadRequest.TicketCriteria;
import com.sabre.webservices.triprecord.TripSearchRQ.ReadRequests.ReservationReadRequest.TicketCriteria.Ticket;
import com.sabre.webservices.triprecord.TripSearchRS;
import com.sabre.webservices.triprecord.TripSearchRS.ReservationsList;

/**
 * @author CTS
 *
 */
@Component
public class RetrievePNRAdaptor {
	
	@Autowired
	private Config config;
	
	@Autowired
	private Utils utils;
	
	private static final ObjectFactory factory = new ObjectFactory();

	/**
	 * This method is used to create SOAP request for PNR Retrieval
	 * @param request
	 * @param errorCodes
	 * @return
	 * @throws ApplicationException
	 */
	public TripSearchRQ createSOAPRequestForPNRRetrieval(RetrievePNRRequest request,List<String> errorCodes) throws ApplicationException{
		
		
		boolean errorFlag = false;
		TripSearchRQ requestBody = factory.createTripSearchRQ();
		requestBody.setVersion(config.getProperty("tripsearch.version"));
		TripSearchRQ.ReadRequests readRequests = new TripSearchRQ.ReadRequests();
		TripSearchRQ.ReadRequests.ReservationReadRequest resReadRequests = new ReservationReadRequest();
		TripSearchRQ.ReadRequests.ReservationReadRequest.NameCriteria nameCriteria = new NameCriteria();
		Name name = new TripSearchRQ.ReadRequests.ReservationReadRequest.NameCriteria.Name();
		StringWithMatchMode retrivePNRNameObj = new StringWithMatchMode();
		retrivePNRNameObj.setMatchMode(MatchMode.EXACT_NO_COMPRESS);
		retrivePNRNameObj.setValue(request.getSearchParameters().getLastName());
		
		name.setLastName(retrivePNRNameObj);
		nameCriteria.getName().add(name);
		
		if ( Constants.TICKET_LENGTH == request.getSearchParameters().getPnr().length()
					&& StringUtils.isNumeric(request.getSearchParameters().getPnr()) 
					&& ( Pattern.matches(Constants.PNR_REGEX, request.getSearchParameters().getPnr())
					|| Pattern.matches(Constants.PNR_REGEX_ANOTHER, request.getSearchParameters().getPnr()) ) ) 
		{
			TicketCriteria ticketCriteria = new TicketCriteria();
			Ticket ticket = new Ticket();
			StringWithMatchMode retrivePNRTicketObj = new StringWithMatchMode();
			retrivePNRTicketObj.setMatchMode(MatchMode.EXACT);
			retrivePNRTicketObj.setValue(request.getSearchParameters().getPnr());
			ticket.setTicketNumber(retrivePNRTicketObj);
			ticketCriteria.getTicket().add(ticket);
			resReadRequests.setTicketCriteria(ticketCriteria);
			errorFlag = true;
		}
		if ( Constants.PNR_LENGTH == request.getSearchParameters().getPnr().length()
				&& StringUtils.isAlphanumeric(request.getSearchParameters().getPnr()) ) 
		{
			LocatorCriteria locCriteria = new LocatorCriteria();
			Locator locator = new Locator();
			locator.setId(request.getSearchParameters().getPnr());
			locCriteria.getLocator().add(locator);
			resReadRequests.setLocatorCriteria(locCriteria);
			errorFlag = true;
		}
		if (!errorFlag) {
			errorCodes.add("pnrretrieval.104");
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		
		
		PosCriteria postCriteria = new PosCriteria();
		postCriteria.setAirlineCode(config.getProperty("sabre.organization"));
		resReadRequests.setPosCriteria(postCriteria);
		
		ReturnOptions retOptions = new ReturnOptions();
		retOptions.setViewName(config.getProperty("tripsearch.viewName"));
		retOptions.setResponseFormat(config.getProperty("tripsearch.responseFormat"));
		resReadRequests.setReturnOptions(retOptions);
		
		resReadRequests.setNameCriteria(nameCriteria);
		readRequests.setReservationReadRequest(resReadRequests);
		requestBody.setReadRequests(readRequests);
		return requestBody;
	}

	/**
	 * This method is used to create JSON Response for Retrieve PNR
	 * @param soapResponse
	 * @param errorCodes
	 * @param request
	 * @return
	 */
	public RetrievePNRResponse createJSONResponseForRetrievePNR(TripSearchRS soapResponse,
			List<String> errorCodes,RetrievePNRRequest request) {
		
		String success = soapResponse.getSuccess();
		RetrievePNRResponse response = new RetrievePNRResponse();
		ReservationsList resList = soapResponse.getReservationsList();
		int number = 0;
		boolean isPNRSuccess = false;
		if(null != resList)
		{
			number = resList.getNumberResults();
		}
		if(number == 0)
		{
			if(Constants.TICKET_LENGTH == request.getSearchParameters().getPnr().length() )
			{
				errorCodes.add("pnrretrieval.107");
			}
			else if(Constants.PNR_LENGTH == request.getSearchParameters().getPnr().length())
			{
				errorCodes.add("pnrretrieval.106");
			}
		}
		else if( StringUtils.isNotEmpty(success) && number > 0 
				&& (config.getProperty("tripsearch.success").equalsIgnoreCase(success)))
		{
			PassengersPNRB passengersPnrb = resList.getReservations().get(0).
					getReservation().
					get(0).
					getGetReservationRS()
					.getReservation().
					getPassengerReservation().
					getPassengers();
			
			String pnr = resList.getReservations().get(0).
					getReservation().
					get(0).
					getLocator();
			if(null != passengersPnrb.getCorporate() && StringUtils.isNotEmpty(passengersPnrb.getCorporate().getNameId()) )
			{
				errorCodes.add("pnrretrieval.105");
			}
			else if(StringUtils.isNotEmpty(pnr))
			{
				isPNRSuccess = true;
				response.setPnr(pnr);
			}
		}
		response.setPNRDataAvailable(isPNRSuccess);
		return response;
	}

}
