/**
 * 
 */
package com.etihad.book.retrievepnr.broker;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.Holder;

import org.ebxml.namespaces.messageheader.From;
import org.ebxml.namespaces.messageheader.MessageData;
import org.ebxml.namespaces.messageheader.MessageHeader;
import org.ebxml.namespaces.messageheader.PartyId;
import org.ebxml.namespaces.messageheader.Service;
import org.ebxml.namespaces.messageheader.To;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xmlsoap.schemas.ws._2002._12.secext.Security;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.sabre.webservices.triprecord.TripSearchRQ;
import com.sabre.webservices.triprecord.TripSearchRS;

import https.webservices_sabre_com.websvc.TripSearchPortType;

/**
 * @author CTS
 *
 */
@Component
public class RetrievePNRBroker {
	
	@Autowired
	private TripSearchPortType tripSearchProxy;
	
	@Autowired
	private Utils utils;
	
	@Autowired
	private Config config;

	/**
	 * This method is used to process PNR Request
	 * @param soapRequest
	 * @param token
	 * @return
	 * @throws ApplicationException
	 */
	public TripSearchRS processCreatePNRRequest(TripSearchRQ soapRequest, String token) throws ApplicationException {

		TripSearchRS soapResponse = null;
		List<String> errorCodes = new ArrayList<>();
		try {
		soapResponse = tripSearchProxy.tripSearchRQ(getMessageHeader(), getSecurityHeader(token),
				soapRequest);
		} catch(Exception e){
			errorCodes.add("pnrretrieval.102");
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		return soapResponse;
	}

	/**
	 * This method is used to form Message Header
	 * @return
	 */
	private Holder<MessageHeader> getMessageHeader() {

		MessageHeader header = new MessageHeader();
		header.setConversationId(config.getProperty("sabre.conversationId"));
		header.setAction(config.getProperty("pnrretrieval.sabre.action"));
		header.setCPAId(config.getProperty("sabre.cpaId"));
		header.setMessageData(getMessageData());

		header.setFrom(getFrom());
		header.setTo(getTo());

		Service service = new Service();
		service.setValue(config.getProperty("pnrretrieval.sabre.action"));
		header.setService(service);
		return new Holder<>(header);
	}

	/**
	 * This method is used to form Message Data
	 * @return
	 */
	private MessageData getMessageData() {

		MessageData messageData = new MessageData();
		messageData.setMessageId(config.getProperty("sabre.messageId"));
		messageData.setRefToMessageId("");
		messageData.setTimestamp("2018-11-27T09:58:31Z");

		return messageData;
	}

	/**
	 * This method is used to form From Part of Trip Search Header Request
	 * @return
	 */
	private From getFrom() {
		From from = new From();
		PartyId fromPartyId = new PartyId();
		fromPartyId.setValue(config.getProperty("sabre.fromParty"));
		from.getPartyId().add(fromPartyId);
		return from;
	}

	/**
	 * This method is used to form To Part of Trip Search Header Request
	 * @return
	 */
	private To getTo() {
		To to = new To();
		PartyId toPartyId = new PartyId();
		toPartyId.setValue(config.getProperty("sabre.toParty"));
		to.getPartyId().add(toPartyId);
		return to;
	}

	/**
	 * This method is used to form Security Header part of Trip Search Header Request
	 * @param token
	 * @return
	 */
	private Holder<Security> getSecurityHeader(String token) {

		Security security = new Security();
		security.setBinarySecurityToken(token);

		return new Holder<>(security);
	}


}
