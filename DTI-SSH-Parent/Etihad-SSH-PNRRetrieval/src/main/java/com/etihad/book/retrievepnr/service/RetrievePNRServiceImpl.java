/**
 * 
 */
package com.etihad.book.retrievepnr.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.etihad.book.retrievepnr.adaptor.RetrievePNRAdaptor;
import com.etihad.book.retrievepnr.broker.RetrievePNRBroker;
import com.etihad.book.retrievepnr.broker.RetrievePNRMockBroker;
import com.etihad.book.retrievepnr.schema.RetrievePNRRequest;
import com.etihad.book.retrievepnr.schema.RetrievePNRResponse;
import com.sabre.webservices.triprecord.TripSearchRQ;
import com.sabre.webservices.triprecord.TripSearchRS;

/**
 * @author CTS
 *
 */
@Service
public class RetrievePNRServiceImpl  implements RetrievePNRService{
	
	
	@Autowired
	private RetrievePNRBroker retrievePNRBroker;
	
	@Autowired
	private RetrievePNRMockBroker retrievePNRMockBroker;

	@Autowired
	private RetrievePNRAdaptor retrievePNRAdaptor;

	@Autowired
	private Utils utils;

	
	/**
	 * This method is used to process retrieve PNR
	 * @return
	 * @throws ApplicationException 
	 */
	public RetrievePNRResponse retrievePNR(RetrievePNRRequest request) throws ApplicationException  {

		RetrievePNRResponse response = null;
		TripSearchRQ soapRequest;
		TripSearchRS soapResponse;
		List<String> errorCodes = validateForFetchPNR(request);
		if (!errorCodes.isEmpty()) {
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		soapRequest = retrievePNRAdaptor.createSOAPRequestForPNRRetrieval(request,errorCodes);
		try {
			soapResponse = retrievePNRBroker.processCreatePNRRequest(soapRequest, request.getSearchParameters().getBinarySecurityToken());
			//soapResponse = retrievePNRMockBroker.processCreatePNRRequest();
		} catch (Exception e) {
			errorCodes.add("pnrretrieval.101");
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		response = retrievePNRAdaptor.createJSONResponseForRetrievePNR(soapResponse, errorCodes,request);
		response.setMessages(utils.populateResponseHeader(errorCodes));
		return response;

	}
	
	/**
	 * This method validates the PNR enquiry request
	 * @param request
	 * @return
	 */
	private List<String> validateForFetchPNR(RetrievePNRRequest request) {

		List errorCodes = new ArrayList(5);
		if (null == request || null == request.getSearchParameters())
			errorCodes.add("pnrretrieval.103");
		else 
		{
			if (StringUtils.isEmpty(request.getSearchParameters().getLastName()) ||
					StringUtils.isEmpty(request.getSearchParameters().getPnr()) )
			{
				errorCodes.add("pnrretrieval.104");
			}
		}
		return errorCodes;
	}

}
