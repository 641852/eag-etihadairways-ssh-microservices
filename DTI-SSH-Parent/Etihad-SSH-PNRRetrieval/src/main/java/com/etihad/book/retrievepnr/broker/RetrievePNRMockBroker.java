/**
 * 
 */
package com.etihad.book.retrievepnr.broker;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.sabre.webservices.triprecord.TripSearchRS;


/**
 * This is the implementation class of Retrieve PNR broker which calls the
 * service.
 * 
 * @author CTS
 * 
 */
@Component
public class RetrievePNRMockBroker {

	@Autowired
	private Utils utils;
	
	@Autowired
	private Config config;

	/**
	 * This method is used to create mock Response of Trip Search
	 * @return
	 * @throws ApplicationException
	 */
	public TripSearchRS processCreatePNRRequest() throws ApplicationException {

		TripSearchRS soapResponse = null;
		List<String> errorCodes = new ArrayList<>();
		JAXBContext context;
		Unmarshaller unmarshaller = null;
		try 
		{
			context = JAXBContext.newInstance(TripSearchRS.class);
			unmarshaller = context.createUnmarshaller();
			Resource resource = new ClassPathResource(config.getProperty("dummy.tripSearch.XML.location"));	
			soapResponse = (TripSearchRS) unmarshaller.unmarshal(resource.getFile());
		} 
		catch(Exception e){
			errorCodes.add("pnrretrieval.102");
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		return soapResponse;
	}


}