/**
 * 
 */
package com.etihad.book.retrievepnr.schema;

import com.etihad.book.schema.common.GenericResponse;

/**
 * @author CTS
 *
 */
public class RetrievePNRResponse extends GenericResponse {
	
	private boolean isPNRDataAvailable;
	private String pnr;

	/**
	 * @return the isPNRDataAvailable
	 */
	public boolean isPNRDataAvailable() {
		return isPNRDataAvailable;
	}

	/**
	 * @param isPNRDataAvailable the isPNRDataAvailable to set
	 */
	public void setPNRDataAvailable(boolean isPNRDataAvailable) {
		this.isPNRDataAvailable = isPNRDataAvailable;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

}
