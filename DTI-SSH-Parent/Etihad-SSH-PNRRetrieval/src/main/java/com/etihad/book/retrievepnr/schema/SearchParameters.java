/**
 * 
 */
package com.etihad.book.retrievepnr.schema;

import com.etihad.book.schema.common.AbstractRequest;

/**
 * Search Parameters class
 * @author 271102
 *
 */
public class SearchParameters extends AbstractRequest {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String lastName;
	String pnr;
	String binarySecurityToken;
	
	
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}
	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * 
	 * @return
	 */
	public String getBinarySecurityToken() {
		return binarySecurityToken;
	}
	
	/**
	 * 
	 * @param binarySecurityToken
	 */
	public void setBinarySecurityToken(String binarySecurityToken) {
		this.binarySecurityToken = binarySecurityToken;
	}


}
