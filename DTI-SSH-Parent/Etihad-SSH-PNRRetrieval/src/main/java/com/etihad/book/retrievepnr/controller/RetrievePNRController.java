/**
 * 
 */
package com.etihad.book.retrievepnr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.retrievepnr.schema.RetrievePNRRequest;
import com.etihad.book.retrievepnr.schema.RetrievePNRResponse;
import com.etihad.book.retrievepnr.service.RetrievePNRServiceImpl;



/**
 * @author CTS
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/service/v1")
public class RetrievePNRController {
	

	@Autowired
	private RetrievePNRServiceImpl retrievePNRservice;
	
	/**
	 * This method will be used to map the request to a specific URI and return the response 
	 * @return
	 * @throws ApplicationException 
	 */
	@CrossOrigin
	@RequestMapping(path="/pnrretrieval", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public RetrievePNRResponse retrievePNR(@RequestBody RetrievePNRRequest request) throws ApplicationException  {
		return retrievePNRservice.retrievePNR(request);

	}


}
