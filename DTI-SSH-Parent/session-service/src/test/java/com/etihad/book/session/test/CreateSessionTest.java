/**
 * 
 */
package com.etihad.book.session.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.session.domain.CreateSessionResponse;
import com.etihad.book.session.service.impl.SessionServiceImpl;

/**
 * @author Danish
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CreateSessionTest {

	@Autowired
	SessionServiceImpl sessionManagementService;

	@Test
	public void testCreateSession() throws ApplicationException {

		CreateSessionResponse response = sessionManagementService.createSession();

		long code = response.getMessages().getCode();
		long count = response.getMessages().getCount();
		boolean success = false;
		if (code == 0 && count == 1) {
			success = true;
		}
		assertEquals(true,success);

	}

	@Test
	public void testCreateSession_conversationId() throws ApplicationException {

		CreateSessionResponse response = sessionManagementService.createSession();
		String conversationId = response.getCreateSession().getConversationId();
		assertNotNull(conversationId);
	}
	
	@Test
	public void testCreateSession_securityToken() throws ApplicationException {

		CreateSessionResponse response = sessionManagementService.createSession();
		String securityToken = response.getCreateSession().getBinarySecurityToken();
		assertNotNull(securityToken);
	}
	
	
}
