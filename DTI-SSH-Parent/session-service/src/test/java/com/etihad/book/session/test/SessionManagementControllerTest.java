/**
 * 
 */
package com.etihad.book.session.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.etihad.book.session.controller.SessionManagementController;
import com.etihad.book.session.service.impl.SessionServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Danish
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SessionManagementControllerTest {

	private MockMvc mockMvc;

	@MockBean
	SessionServiceImpl createSessionService;

	@InjectMocks
	private SessionManagementController sessionManagementController;

	@Autowired
	private ObjectMapper objectMapper;

	@Before
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(sessionManagementController).build();

	}

	@Test
	public void testControllerForCreateSession() throws Exception {

		when(createSessionService.createSession()).thenReturn(TestSessionUtil.formTestResponseForCreateSession());

		mockMvc.perform(
				MockMvcRequestBuilders.get("/service/session/create-session").accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(jsonPath("$.*", Matchers.hasSize(2)))
				.andExpect(jsonPath("$.messages", notNullValue()))
				.andExpect(jsonPath("$.createSession.conversationId", is("659dd663-d7aa-4da6-b4e9-a513f0f6836e")))
				.andExpect(jsonPath("$.createSession.binarySecurityToken",
						is("Shared/IDL:IceSess\\/SessMgr:1\\.0.IDL/Common/!ICESMS\\/CERTG!ICESMSLB\\/CRT.LB!-3172377884791915381!1321454!0")))
				.andExpect(jsonPath("$.messages.message[0].text", is("No Error")))
				.andExpect(jsonPath("$.messages.message[0].code", is("0"))).andDo(print());
	}

	@Test
	public void testControllerForRefreshSession() throws Exception {

		Mockito.when(createSessionService.refreshSession(Mockito.anyObject()))
				.thenReturn(TestSessionUtil.formTestResponseForRefreshSession());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/service/session/refresh-session")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(TestSessionUtil.fromRefreshRequest()))
				.accept(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content()
						.string(objectMapper.writeValueAsString(TestSessionUtil.formTestResponseForRefreshSession())))
				.andExpect(jsonPath("$.messages", notNullValue()))
				.andExpect(jsonPath("$.refreshSession.refreshData", is("Are u there")))
				.andExpect(jsonPath("$.messages.message[0].text", is("No Error")))
				.andExpect(jsonPath("$.messages.message[0].code", is("0")));

	}

	@Test
	public void testControllerForCloseSession() throws Exception {

		when(createSessionService.closeSession(Mockito.anyObject()))
				.thenReturn(TestSessionUtil.formTestResponseForCloseSession());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/service/session/close-session")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(TestSessionUtil.fromCloseRequest()))
				.accept(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content()
						.string(objectMapper.writeValueAsString(TestSessionUtil.formTestResponseForCloseSession())))
				.andExpect(jsonPath("$.*", Matchers.hasSize(2))).andExpect(jsonPath("$.messages", notNullValue()))
				.andExpect(jsonPath("$.closeSession.status", is("Approved")))
				.andExpect(jsonPath("$.messages.message[0].text", is("No Error")))
				.andExpect(jsonPath("$.messages.message[0].code", is("0")));
	}
}
