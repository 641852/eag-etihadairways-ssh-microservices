/**
 * 
 */
package com.etihad.book.session.test;

import java.util.ArrayList;
import java.util.List;

import com.etihad.book.schema.common.Message;
import com.etihad.book.schema.common.Messages;
import com.etihad.book.session.domain.CloseSession;
import com.etihad.book.session.domain.CloseSessionRequest;
import com.etihad.book.session.domain.CloseSessionResponse;
import com.etihad.book.session.domain.CreateSession;
import com.etihad.book.session.domain.CreateSessionResponse;
import com.etihad.book.session.domain.RefreshSession;
import com.etihad.book.session.domain.RefreshSessionRequest;
import com.etihad.book.session.domain.RefreshSessionResponse;
import com.etihad.book.session.domain.TokenHolder;

/**
 * @author Danish
 *
 */
public class TestSessionUtil {

	public static CreateSessionResponse formTestResponseForCreateSession() {

		final CreateSessionResponse response = new CreateSessionResponse();
		final CreateSession createSession = new CreateSession();
		createSession.setConversationId("659dd663-d7aa-4da6-b4e9-a513f0f6836e");
		createSession.setBinarySecurityToken(
				"Shared/IDL:IceSess\\/SessMgr:1\\.0.IDL/Common/!ICESMS\\/CERTG!ICESMSLB\\/CRT.LB!-3172377884791915381!1321454!0");

		final Messages messages = new Messages();

		final List<Message> msg = new ArrayList<>();
		final Message message = new Message();
		message.setCode("0");
		message.setText("No Error");
		msg.add(message);
		messages.setErrorMessages(msg);
		response.setMessages(messages);
		response.setCreateSession(createSession);
		return response;

	}

	public static RefreshSessionRequest fromRefreshRequest() {

		final RefreshSessionRequest refreshSessionRequest = new RefreshSessionRequest();
		TokenHolder tokenHolder = new TokenHolder();
		tokenHolder.setSecurityToken(
				"Shared/IDL:IceSess\\/SessMgr:1\\.0.IDL/Common/!ICESMS\\/ACPCRTC!ICESMSLB\\/CRT.LB!-3171785576454677114!708738!0");
		refreshSessionRequest.setTokenHolder(tokenHolder);
		return refreshSessionRequest;
	}

	public static RefreshSessionResponse formTestResponseForRefreshSession() {

		final RefreshSessionResponse response = new RefreshSessionResponse();
		final RefreshSession refreshSession = new RefreshSession();
		refreshSession.setRefreshData("Are u there");

		final Messages messages = new Messages();

		final List<Message> msg = new ArrayList<>();
		final Message message = new Message();
		message.setCode("0");
		message.setText("No Error");
		msg.add(message);
		messages.setErrorMessages(msg);
		response.setMessages(messages);
		response.setRefreshSession(refreshSession);
		;
		return response;
	}

	public static RefreshSessionResponse formTestResponseForRefreshSession2() {

		final RefreshSessionResponse response = new RefreshSessionResponse();

		final Messages messages = new Messages();

		final List<Message> msg = new ArrayList<>();
		final Message message = new Message();
		message.setText(
				"Client received SOAP Fault from server: Invalid or Expired binary security token: Shared/IDL:IceSess\\/SessMgr:1\\.0.IDL/Common/!ICESMS\\/ACPCRTC!ICESMSLB\\/CRT.LB!-3171785576454677114!708738!0 Please see the server log to find more detail regarding exact cause of the failure.; nested exception is com.sun.xml.internal.ws.fault.ServerSOAPFaultException: Client received SOAP Fault from server: Invalid or Expired binary security token: Shared/IDL:IceSess\\/SessMgr:1\\.0.IDL/Common/!ICESMS\\/ACPCRTC!ICESMSLB\\/CRT.LB!-3171785576454677114!708738!0 Please see the server log to find more detail regarding exact cause of the failure..errorMsg");
		message.setCode(
				"Client received SOAP Fault from server: Invalid or Expired binary security token: Shared/IDL:IceSess\\/SessMgr:1\\.0.IDL/Common/!ICESMS\\/ACPCRTC!ICESMSLB\\/CRT.LB!-3171785576454677114!708738!0 Please see the server log to find more detail regarding exact cause of the failure.; nested exception is com.sun.xml.internal.ws.fault.ServerSOAPFaultException: Client received SOAP Fault from server: Invalid or Expired binary security token: Shared/IDL:IceSess\\/SessMgr:1\\.0.IDL/Common/!ICESMS\\/ACPCRTC!ICESMSLB\\/CRT.LB!-3171785576454677114!708738!0 Please see the server log to find more detail regarding exact cause of the failure..errorCode");
		msg.add(message);
		messages.setErrorMessages(msg);
		response.setMessages(messages);
		return response;
	}

	public static CloseSessionRequest fromCloseRequest() {

		final CloseSessionRequest closeSessionRequest = new CloseSessionRequest();
		TokenHolder tokenHolder = new TokenHolder();
		tokenHolder.setSecurityToken(
				"Shared/IDL:IceSess\\/SessMgr:1\\.0.IDL/Common/!ICESMS\\/CERTG!ICESMSLB\\/CRT.LB!-3172377884791915381!1321454!0");
		closeSessionRequest.setTokenHolder(tokenHolder);
		return closeSessionRequest;
	}

	public static CloseSessionResponse formTestResponseForCloseSession() {

		final CloseSessionResponse response = new CloseSessionResponse();
		final CloseSession closeSession = new CloseSession();
		closeSession.setStatus("Approved");

		final Messages messages = new Messages();

		final List<Message> msg = new ArrayList<>();
		final Message message = new Message();
		message.setCode("0");
		message.setText("No Error");
		msg.add(message);
		messages.setErrorMessages(msg);
		response.setMessages(messages);
		response.setCloseSession(closeSession);
		return response;
	}

}
