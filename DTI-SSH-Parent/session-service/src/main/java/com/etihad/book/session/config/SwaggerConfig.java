/**
 * 
 */
package com.etihad.book.session.config;


import static springfox.documentation.builders.PathSelectors.regex;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket api() throws FileNotFoundException, IOException, XmlPullParserException {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.etihad.book.session.controller"))
                .paths(regex("/service.*"))
                .build();
              //  .apiInfo(metaInfo());
    }

    /*private ApiInfo metaInfo() throws FileNotFoundException, IOException, XmlPullParserException {
    	MavenXpp3Reader reader = new MavenXpp3Reader();
        Model model = reader.read(new FileReader("pom.xml"));

        return new ApiInfo(
                "Session Service Api Documentation",
                "Documentation automatically generated",
                model.getParent().getVersion(),
                "",
                new Contact("DTI-SSH-Team", "http://www.etihad.com",
                        "dti-ssh@abc.com"),
                "",
                ""
        );
    }*/
}