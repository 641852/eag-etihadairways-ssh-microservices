/**
 * 
 */
package com.etihad.book.session.domain;

import com.etihad.book.schema.common.GenericResponse;

/**
 * @author Danish
 *
 */
public class RefreshSessionResponse extends GenericResponse {
	
	
	private static final long serialVersionUID = 1L;
	private RefreshSession refreshSession;

	public RefreshSession getRefreshSession() {
		return refreshSession;
	}

	public void setRefreshSession(RefreshSession refreshSession) {
		this.refreshSession = refreshSession;
	}

}
