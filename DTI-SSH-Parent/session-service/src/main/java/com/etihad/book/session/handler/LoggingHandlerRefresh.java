/**
 * 
 */
package com.etihad.book.session.handler;

import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 * @author Danish
 *
 */
public class LoggingHandlerRefresh implements SOAPHandler<SOAPMessageContext> {

	@Override
	public void close(MessageContext context) {
		//close
		
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		
		SOAPMessage soapMsg = context.getMessage();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		printOutBoundMsg(soapMsg,out);
		return true;
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		
		Boolean isRequest = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		SOAPMessage soapMsg = context.getMessage();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		if(isRequest) {
			printOutBoundMsg(soapMsg,out);
		}
		else{
			printInBoundMsg(soapMsg,out);
		}
		return true;
	}

	private void printOutBoundMsg(SOAPMessage soapMsg, ByteArrayOutputStream out) {
		try {
			soapMsg.writeTo(out);
        } catch (Exception e) {
        	//
        }
		
	}
	
	private void printInBoundMsg(SOAPMessage soapMsg, ByteArrayOutputStream out) {
		try {
			soapMsg.writeTo(out);
        } catch (Exception e) {
        	//
        }
		
	}

	@Override
	public Set<QName> getHeaders() {
		return Collections.emptySet();
	}

	

}
