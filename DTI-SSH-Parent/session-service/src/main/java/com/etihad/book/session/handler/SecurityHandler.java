/**
 * 
 */
package com.etihad.book.session.handler;

import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.etihad.book.session.util.SessionConstants;
import com.etihad.book.session.util.TokenUtil;

/**
 * @author Danish
 *
 */
public class SecurityHandler implements SOAPHandler<SOAPMessageContext> {


	@Override
	public boolean handleFault(SOAPMessageContext context) {
		SOAPMessage soapMsg = context.getMessage();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		printOutBoundMsg(soapMsg,out);
		return true;
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		Boolean isRequest = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		SOAPMessage soapMsg = context.getMessage();
		
		if (!isRequest) {
			printOutBoundMsg(soapMsg,out);
			SOAPEnvelope soapEnv = null;
			try {
				soapEnv = soapMsg.getSOAPPart().getEnvelope();
				SOAPHeader soapHeader = soapEnv.getHeader();
				NodeList node = soapHeader.getElementsByTagNameNS(SessionConstants.SECURITYNS, SessionConstants.SECURITYLOCALNAME);
				Node item = node.item(0).getChildNodes().item(0);
				NodeList nodeName = item.getChildNodes();
				String securityToken = nodeName.item(0).getNodeValue();
				TokenUtil.setToken(securityToken);

			} catch (SOAPException e) {
				//need to log
			}
		}
		else {
			printOutBoundMsg(soapMsg,out);
		}
		return true;
	}

	private void printOutBoundMsg(SOAPMessage soapMsg, ByteArrayOutputStream out) {
		try {
			soapMsg.writeTo(out);
        } catch (Exception e) {
        	//
        }
		
	}
	@Override
	public Set<QName> getHeaders() {
		return Collections.emptySet();
	}
	
	@Override
	public void close(MessageContext context) {
		//close
	}

}
