/**
 * 
 */
package com.etihad.book.session.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.opentravel.ota._2002._11.SessionCloseRQ;
import org.opentravel.ota._2002._11.SessionCloseRS;
import org.opentravel.ota._2002._11.SessionCreateRQ;
import org.opentravel.ota._2002._11.SessionCreateRS;
import org.opentravel.ota._2003._05.OTAPingRQ;
import org.opentravel.ota._2003._05.OTAPingRS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.etihad.book.session.adaptor.SessionManagementAdaptor;
import com.etihad.book.session.broker.SessionManagementBroker;
import com.etihad.book.session.domain.CloseSessionRequest;
import com.etihad.book.session.domain.CloseSessionResponse;
import com.etihad.book.session.domain.CreateSessionResponse;
import com.etihad.book.session.domain.RefreshSessionRequest;
import com.etihad.book.session.domain.RefreshSessionResponse;
import com.etihad.book.session.domain.TokenHolder;
import com.etihad.book.session.service.ISesionService;
import com.etihad.book.session.util.TokenUtil;

/**
 * @author Danish
 *
 */
@Service
public class SessionServiceImpl implements ISesionService  {

	@Autowired
	SessionManagementBroker broker;

	@Autowired
	SessionManagementAdaptor adaptor;

	@Autowired
	Utils utils;

	@Autowired
	Config config;

	public CreateSessionResponse createSession() throws ApplicationException  {

		CreateSessionResponse response;
		SessionCreateRQ soapRequest;
		SessionCreateRS soapResponse;
		List<String> errorCodes = new ArrayList<>();
		if (!errorCodes.isEmpty()) {
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		soapRequest = adaptor.createSOAPRequestForCreateSession();
		try {
			soapResponse = broker.processCreateSessionRequest(soapRequest);
		} catch (Exception e) {
			errorCodes.add("create-session.101");
			errorCodes.add(e.getMessage());
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		response = adaptor.createJSONResponseForCreateSession(soapResponse);
		response.setMessages(utils.populateResponseHeader(errorCodes));
		return response;

	}
	
	@Override
	public RefreshSessionResponse refreshSession(RefreshSessionRequest refreshSessionRequest) throws ApplicationException {
		RefreshSessionResponse response;
		OTAPingRQ refreshSoapRequest;
		OTAPingRS soapResponse;
		TokenHolder tokenHolder = refreshSessionRequest.getTokenHolder(); 
		List<String> errorCodes = validateSecurityToken(tokenHolder.getSecurityToken());
		if (!errorCodes.isEmpty()) {
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		TokenUtil.setToken(tokenHolder.getSecurityToken());
		refreshSoapRequest = adaptor.createSOAPRequestForRefreshSession();
		try {
			soapResponse = broker.processRefreshSessionRequest(refreshSoapRequest);
		} catch (Exception e) {
			errorCodes.add(e.getMessage());
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		response = adaptor.createJSONResponseForRefreshSession(soapResponse);
		response.setMessages(utils.populateResponseHeader(errorCodes));
		return response;
	}


	@Override
	public CloseSessionResponse closeSession(CloseSessionRequest closeSessionRequest) throws ApplicationException {
		
		CloseSessionResponse response;
		SessionCloseRQ closeSoapRequest;
		SessionCloseRS soapResponse;
		TokenHolder tokenHolder = closeSessionRequest.getTokenHolder(); 
		List<String> errorCodes = validateSecurityToken(tokenHolder.getSecurityToken());
		if (!errorCodes.isEmpty()) {
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		TokenUtil.setToken(tokenHolder.getSecurityToken());
		closeSoapRequest = adaptor.createSOAPRequestForCloseSession();
		try {
			soapResponse = broker.processCloseSessionRequest(closeSoapRequest);
		} catch (Exception e) {
			errorCodes.add(e.getMessage());
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		response = adaptor.createJSONResponseForCloseSession(soapResponse);
		response.setMessages(utils.populateResponseHeader(errorCodes));
		return response;
	}

	private List<String> validateSecurityToken(String securityToken) {
		
		List<String> errorCodes = new ArrayList<>();
		if(StringUtils.isEmpty(securityToken)){
			errorCodes.add("session.validate.security-token");
		}
		return errorCodes;
	}

}
