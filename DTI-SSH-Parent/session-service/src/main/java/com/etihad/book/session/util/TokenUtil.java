/**
 * 
 */
package com.etihad.book.session.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Danish
 *
 */
public class TokenUtil {
	
	private TokenUtil() {
		
	}

	private static Map<String,String> map = new HashMap<>();

	public static void setToken(String str) {
		map.put("security", str);
	}

	public static String getToken() {
		return map.get("security");
	}

}
