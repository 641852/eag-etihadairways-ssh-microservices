/**
 * 
 */
package com.etihad.book.session.broker;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.ws.Holder;

import org.apache.commons.lang.RandomStringUtils;
import org.ebxml.namespaces.messageheader.From;
import org.ebxml.namespaces.messageheader.MessageData;
import org.ebxml.namespaces.messageheader.MessageHeader;
import org.ebxml.namespaces.messageheader.PartyId;
import org.ebxml.namespaces.messageheader.Service;
import org.ebxml.namespaces.messageheader.To;
import org.opentravel.ota._2002._11.SessionCloseRQ;
import org.opentravel.ota._2002._11.SessionCloseRS;
import org.opentravel.ota._2002._11.SessionCreateRQ;
import org.opentravel.ota._2002._11.SessionCreateRS;
import org.opentravel.ota._2003._05.OTAPingRQ;
import org.opentravel.ota._2003._05.OTAPingRS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xmlsoap.schemas.ws._2002._12.secext.Security;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.etihad.book.session.util.SessionConstants;
import com.etihad.book.session.util.TokenUtil;

import https.webservices_sabre_com.websvc.OTAPingPortType;
import https.webservices_sabre_com.websvc.SessionClosePortType;
import https.webservices_sabre_com.websvc.SessionCreatePortType;

/**
 * @author Danish
 *
 */
@Component
public class SessionManagementBroker {

	@Autowired
	SessionCreatePortType sessionCreateProxy;
	
	@Autowired
	OTAPingPortType refreshSessionProxy;
	
	@Autowired
	SessionClosePortType closeSessionProxy;
	
	@Autowired
	Utils utils;
	
	@Autowired
	Config config;

	public SessionCreateRS processCreateSessionRequest(SessionCreateRQ soapRequest) throws ApplicationException {

		SessionCreateRS soapResponse = null;
		List<String> errorCodes = new ArrayList<>();
		try {
		soapResponse = sessionCreateProxy.sessionCreateRQ(getMessageHeaderForCreate(), getSecurityHeader(),
				soapRequest);
		} catch(Exception e){
			errorCodes.add("session.create-session.102");
			errorCodes.add(e.getMessage());
			errorCodes.add(e.getCause().toString());
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		return soapResponse;
	}
	
	 private String createConversationId() {

	        StringBuilder buffer = new StringBuilder(getTimestamp());
	        buffer.append("-");
	        buffer.append(longRandomHexString());
	        return buffer.toString();

	    }

	    private String getTimestamp() {

	        SimpleDateFormat sdf = new SimpleDateFormat(SessionConstants.TIMESTAMP);

	        return sdf.format(new Date());

	    }

	    private String longRandomHexString() {

	        return RandomStringUtils.randomAlphanumeric(8);
	    }

	private Holder<MessageHeader> getMessageHeaderForCreate() {

		MessageHeader header = new MessageHeader();
		header.setConversationId(createConversationId());
		header.setAction(config.getProperty(SessionConstants.ACTION_CREATE));
		header.setCPAId(config.getProperty(SessionConstants.CPAID));
		header.setMessageData(getMessageData());

		header.setFrom(getFrom());
		header.setTo(getTo());

		Service service = new Service();
		service.setValue(config.getProperty(SessionConstants.ACTION_CREATE));
		header.setService(service);
		return new Holder<>(header);
	}

	private MessageData getMessageData() {

		MessageData messageData = new MessageData();
		messageData.setMessageId(config.getProperty("sabre.messageId"));
		messageData.setRefToMessageId("");
		messageData.setTimeout(BigInteger.valueOf(600));
		messageData.setTimestamp("2018-11-27T09:58:31Z");

		return messageData;
	}

	private From getFrom() {
		From from = new From();
		PartyId fromPartyId = new PartyId();
		fromPartyId.setValue(config.getProperty(SessionConstants.FROMPARTY));
		from.getPartyId().add(fromPartyId);
		return from;
	}

	private To getTo() {
		To to = new To();
		PartyId toPartyId = new PartyId();
		toPartyId.setValue(config.getProperty(SessionConstants.TOPARTY));
		to.getPartyId().add(toPartyId);
		return to;
	}

	private Holder<Security> getSecurityHeader() {

		Security security = new Security();
		security.setBinarySecurityToken(TokenUtil.getToken());
		Security.UsernameToken usernameToken = new Security.UsernameToken();
		usernameToken.setDomain(config.getProperty(SessionConstants.DOMAIN));
		usernameToken.setOrganization(config.getProperty(SessionConstants.ORGANIZATION));
		usernameToken.setPassword(config.getProperty(SessionConstants.SABRE_SECRET));
		usernameToken.setUsername(config.getProperty(SessionConstants.USERNAME));
		security.setUsernameToken(usernameToken);
		return new Holder<>(security);
	}
	
	//refresh

	public OTAPingRS processRefreshSessionRequest(OTAPingRQ soapRequest) {
		
		OTAPingRS refreshSoapResponse;
		refreshSoapResponse =refreshSessionProxy.otaPingRQ(getMessageHeaderForRefresh(), getSecurityHeader(), soapRequest);
		return refreshSoapResponse;
	}

	private Holder<MessageHeader> getMessageHeaderForRefresh() {
		
		MessageHeader refreshMessageHeader = new MessageHeader();
		
		refreshMessageHeader.setConversationId(createConversationId());
		refreshMessageHeader.setAction("OTA_PingRQ");
		refreshMessageHeader.setCPAId(config.getProperty(SessionConstants.CPAID));
		refreshMessageHeader.setMessageData(getMessageData());

		refreshMessageHeader.setFrom(getFrom());
		refreshMessageHeader.setTo(getTo());

		Service service = new Service();
		service.setValue("OTA_PingRQ");
		refreshMessageHeader.setService(service);
		return new Holder<>(refreshMessageHeader);
	}

	
	public SessionCloseRS processCloseSessionRequest(SessionCloseRQ closeSoapRequest) {
		SessionCloseRS sessionCloseRS;
		sessionCloseRS = closeSessionProxy.sessionCloseRQ(getMessageHeaderForClose(), getSecurityHeader(), closeSoapRequest);
		return sessionCloseRS;
	}

	private Holder<MessageHeader> getMessageHeaderForClose() {
		MessageHeader closeMessageHeader = new MessageHeader();
		
		closeMessageHeader.setConversationId(createConversationId());
		closeMessageHeader.setAction("SessionCloseRQ");
		closeMessageHeader.setCPAId(config.getProperty(SessionConstants.CPAID));
		closeMessageHeader.setMessageData(getMessageData());

		closeMessageHeader.setFrom(getFrom());
		closeMessageHeader.setTo(getTo());

		Service service = new Service();
		service.setValue("SessionCloseRQ");
		closeMessageHeader.setService(service);
		return new Holder<>(closeMessageHeader);
	}

}
