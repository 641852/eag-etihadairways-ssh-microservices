/**
 * 
 */
package com.etihad.book.session.domain;

import java.io.Serializable;

/**
 * @author Danish
 *
 */
public class TokenHolder implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String securityToken;

	public String getSecurityToken() {
		return securityToken;
	}

	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}

}
