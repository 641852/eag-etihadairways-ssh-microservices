/**
 * 
 */
package com.etihad.book.session.domain;

/**
 * @author Danish
 *
 */
public class RefreshSession {

	private Object refreshData;

	public Object getRefreshData() {
		return refreshData;
	}

	public void setRefreshData(Object refreshData) {
		this.refreshData = refreshData;
	}

}
