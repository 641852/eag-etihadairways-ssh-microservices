
package com.sabre.webservices.pnrbuilder.v1_15;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import com.sabre.services.res.or.v1_8.OpenReservationElementType;


/**
 * <p>Java class for Results.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Results.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Request" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="fragment" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>base64Binary">
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="UpdateResult" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Item" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AncillaryServicePricing" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="OriginalBasePrice" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryPrice.PNRB" minOccurs="0"/>
 *                                       &lt;element name="EquivalentPrice" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryPrice.PNRB" minOccurs="0"/>
 *                                       &lt;element name="Taxes" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="Tax" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryTax.PNRB" maxOccurs="99" minOccurs="0"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="TTLPrice" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryPrice.PNRB"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="OpenReservationElement" type="{http://services.sabre.com/res/or/v1_8}OpenReservationElementType" minOccurs="0"/>
 *                             &lt;element name="AirSegment" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AirType" minOccurs="0"/>
 *                           &lt;/sequence>
 *                           &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_15}UniqueIdentifier.PNRB" />
 *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_15}OperationType.PNRB" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="UpdateId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Results.PNRB", propOrder = {
    "request",
    "updateResult"
})
public class ResultsPNRB {

    @XmlElement(name = "Request")
    protected ResultsPNRB.Request request;
    @XmlElement(name = "UpdateResult")
    protected List<ResultsPNRB.UpdateResult> updateResult;

    /**
     * Gets the value of the request property.
     * 
     * @return
     *     possible object is
     *     {@link ResultsPNRB.Request }
     *     
     */
    public ResultsPNRB.Request getRequest() {
        return request;
    }

    /**
     * Sets the value of the request property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultsPNRB.Request }
     *     
     */
    public void setRequest(ResultsPNRB.Request value) {
        this.request = value;
    }

    /**
     * Gets the value of the updateResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updateResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdateResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResultsPNRB.UpdateResult }
     * 
     * 
     */
    public List<ResultsPNRB.UpdateResult> getUpdateResult() {
        if (updateResult == null) {
            updateResult = new ArrayList<ResultsPNRB.UpdateResult>();
        }
        return this.updateResult;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="fragment" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>base64Binary">
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fragment"
    })
    public static class Request {

        @XmlElement(required = true)
        protected List<ResultsPNRB.Request.Fragment> fragment;

        /**
         * Gets the value of the fragment property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fragment property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFragment().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ResultsPNRB.Request.Fragment }
         * 
         * 
         */
        public List<ResultsPNRB.Request.Fragment> getFragment() {
            if (fragment == null) {
                fragment = new ArrayList<ResultsPNRB.Request.Fragment>();
            }
            return this.fragment;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>base64Binary">
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class Fragment {

            @XmlValue
            protected byte[] value;

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     byte[]
             */
            public byte[] getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     byte[]
             */
            public void setValue(byte[] value) {
                this.value = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Item" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AncillaryServicePricing" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="OriginalBasePrice" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryPrice.PNRB" minOccurs="0"/>
     *                             &lt;element name="EquivalentPrice" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryPrice.PNRB" minOccurs="0"/>
     *                             &lt;element name="Taxes" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="Tax" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryTax.PNRB" maxOccurs="99" minOccurs="0"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="TTLPrice" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryPrice.PNRB"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="OpenReservationElement" type="{http://services.sabre.com/res/or/v1_8}OpenReservationElementType" minOccurs="0"/>
     *                   &lt;element name="AirSegment" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AirType" minOccurs="0"/>
     *                 &lt;/sequence>
     *                 &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_15}UniqueIdentifier.PNRB" />
     *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_15}OperationType.PNRB" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attribute name="UpdateId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class UpdateResult {

        @XmlElement(name = "Item")
        protected List<ResultsPNRB.UpdateResult.Item> item;
        @XmlAttribute(name = "UpdateId")
        protected String updateId;
        @XmlAttribute(name = "Status")
        protected String status;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ResultsPNRB.UpdateResult.Item }
         * 
         * 
         */
        public List<ResultsPNRB.UpdateResult.Item> getItem() {
            if (item == null) {
                item = new ArrayList<ResultsPNRB.UpdateResult.Item>();
            }
            return this.item;
        }

        /**
         * Gets the value of the updateId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUpdateId() {
            return updateId;
        }

        /**
         * Sets the value of the updateId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUpdateId(String value) {
            this.updateId = value;
        }

        /**
         * Gets the value of the status property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AncillaryServicePricing" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="OriginalBasePrice" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryPrice.PNRB" minOccurs="0"/>
         *                   &lt;element name="EquivalentPrice" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryPrice.PNRB" minOccurs="0"/>
         *                   &lt;element name="Taxes" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="Tax" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryTax.PNRB" maxOccurs="99" minOccurs="0"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="TTLPrice" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryPrice.PNRB"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="OpenReservationElement" type="{http://services.sabre.com/res/or/v1_8}OpenReservationElementType" minOccurs="0"/>
         *         &lt;element name="AirSegment" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AirType" minOccurs="0"/>
         *       &lt;/sequence>
         *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_15}UniqueIdentifier.PNRB" />
         *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_15}OperationType.PNRB" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ancillaryServicePricing",
            "openReservationElement",
            "airSegment"
        })
        public static class Item {

            @XmlElement(name = "AncillaryServicePricing")
            protected ResultsPNRB.UpdateResult.Item.AncillaryServicePricing ancillaryServicePricing;
            @XmlElement(name = "OpenReservationElement")
            protected OpenReservationElementType openReservationElement;
            @XmlElement(name = "AirSegment")
            protected AirType airSegment;
            @XmlAttribute(name = "id")
            protected String id;
            @XmlAttribute(name = "type")
            protected String type;
            @XmlAttribute(name = "op")
            protected OperationTypePNRB op;

            /**
             * Gets the value of the ancillaryServicePricing property.
             * 
             * @return
             *     possible object is
             *     {@link ResultsPNRB.UpdateResult.Item.AncillaryServicePricing }
             *     
             */
            public ResultsPNRB.UpdateResult.Item.AncillaryServicePricing getAncillaryServicePricing() {
                return ancillaryServicePricing;
            }

            /**
             * Sets the value of the ancillaryServicePricing property.
             * 
             * @param value
             *     allowed object is
             *     {@link ResultsPNRB.UpdateResult.Item.AncillaryServicePricing }
             *     
             */
            public void setAncillaryServicePricing(ResultsPNRB.UpdateResult.Item.AncillaryServicePricing value) {
                this.ancillaryServicePricing = value;
            }

            /**
             * Gets the value of the openReservationElement property.
             * 
             * @return
             *     possible object is
             *     {@link OpenReservationElementType }
             *     
             */
            public OpenReservationElementType getOpenReservationElement() {
                return openReservationElement;
            }

            /**
             * Sets the value of the openReservationElement property.
             * 
             * @param value
             *     allowed object is
             *     {@link OpenReservationElementType }
             *     
             */
            public void setOpenReservationElement(OpenReservationElementType value) {
                this.openReservationElement = value;
            }

            /**
             * Gets the value of the airSegment property.
             * 
             * @return
             *     possible object is
             *     {@link AirType }
             *     
             */
            public AirType getAirSegment() {
                return airSegment;
            }

            /**
             * Sets the value of the airSegment property.
             * 
             * @param value
             *     allowed object is
             *     {@link AirType }
             *     
             */
            public void setAirSegment(AirType value) {
                this.airSegment = value;
            }

            /**
             * Gets the value of the id property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getId() {
                return id;
            }

            /**
             * Sets the value of the id property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setId(String value) {
                this.id = value;
            }

            /**
             * Gets the value of the type property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getType() {
                return type;
            }

            /**
             * Sets the value of the type property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setType(String value) {
                this.type = value;
            }

            /**
             * Gets the value of the op property.
             * 
             * @return
             *     possible object is
             *     {@link OperationTypePNRB }
             *     
             */
            public OperationTypePNRB getOp() {
                return op;
            }

            /**
             * Sets the value of the op property.
             * 
             * @param value
             *     allowed object is
             *     {@link OperationTypePNRB }
             *     
             */
            public void setOp(OperationTypePNRB value) {
                this.op = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="OriginalBasePrice" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryPrice.PNRB" minOccurs="0"/>
             *         &lt;element name="EquivalentPrice" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryPrice.PNRB" minOccurs="0"/>
             *         &lt;element name="Taxes" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="Tax" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryTax.PNRB" maxOccurs="99" minOccurs="0"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="TTLPrice" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryPrice.PNRB"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "originalBasePrice",
                "equivalentPrice",
                "taxes",
                "ttlPrice"
            })
            public static class AncillaryServicePricing {

                @XmlElement(name = "OriginalBasePrice")
                protected AncillaryPricePNRB originalBasePrice;
                @XmlElement(name = "EquivalentPrice")
                protected AncillaryPricePNRB equivalentPrice;
                @XmlElement(name = "Taxes")
                protected ResultsPNRB.UpdateResult.Item.AncillaryServicePricing.Taxes taxes;
                @XmlElement(name = "TTLPrice", required = true)
                protected AncillaryPricePNRB ttlPrice;

                /**
                 * Gets the value of the originalBasePrice property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link AncillaryPricePNRB }
                 *     
                 */
                public AncillaryPricePNRB getOriginalBasePrice() {
                    return originalBasePrice;
                }

                /**
                 * Sets the value of the originalBasePrice property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AncillaryPricePNRB }
                 *     
                 */
                public void setOriginalBasePrice(AncillaryPricePNRB value) {
                    this.originalBasePrice = value;
                }

                /**
                 * Gets the value of the equivalentPrice property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link AncillaryPricePNRB }
                 *     
                 */
                public AncillaryPricePNRB getEquivalentPrice() {
                    return equivalentPrice;
                }

                /**
                 * Sets the value of the equivalentPrice property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AncillaryPricePNRB }
                 *     
                 */
                public void setEquivalentPrice(AncillaryPricePNRB value) {
                    this.equivalentPrice = value;
                }

                /**
                 * Gets the value of the taxes property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ResultsPNRB.UpdateResult.Item.AncillaryServicePricing.Taxes }
                 *     
                 */
                public ResultsPNRB.UpdateResult.Item.AncillaryServicePricing.Taxes getTaxes() {
                    return taxes;
                }

                /**
                 * Sets the value of the taxes property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ResultsPNRB.UpdateResult.Item.AncillaryServicePricing.Taxes }
                 *     
                 */
                public void setTaxes(ResultsPNRB.UpdateResult.Item.AncillaryServicePricing.Taxes value) {
                    this.taxes = value;
                }

                /**
                 * Gets the value of the ttlPrice property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link AncillaryPricePNRB }
                 *     
                 */
                public AncillaryPricePNRB getTTLPrice() {
                    return ttlPrice;
                }

                /**
                 * Sets the value of the ttlPrice property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AncillaryPricePNRB }
                 *     
                 */
                public void setTTLPrice(AncillaryPricePNRB value) {
                    this.ttlPrice = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="Tax" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AncillaryTax.PNRB" maxOccurs="99" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "tax"
                })
                public static class Taxes {

                    @XmlElement(name = "Tax")
                    protected List<AncillaryTaxPNRB> tax;

                    /**
                     * Gets the value of the tax property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the tax property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getTax().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link AncillaryTaxPNRB }
                     * 
                     * 
                     */
                    public List<AncillaryTaxPNRB> getTax() {
                        if (tax == null) {
                            tax = new ArrayList<AncillaryTaxPNRB>();
                        }
                        return this.tax;
                    }

                }

            }

        }

    }

}
