
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AncillaryServiceEMDNumberUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AncillaryServiceEMDNumberUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EMDNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="EMDCoupon" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AncillaryServiceEMDNumberUpdate.PNRB", propOrder = {
    "emdNumber",
    "emdCoupon"
})
public class AncillaryServiceEMDNumberUpdatePNRB {

    @XmlElement(name = "EMDNumber")
    protected String emdNumber;
    @XmlElement(name = "EMDCoupon")
    protected String emdCoupon;

    /**
     * Gets the value of the emdNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMDNumber() {
        return emdNumber;
    }

    /**
     * Sets the value of the emdNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMDNumber(String value) {
        this.emdNumber = value;
    }

    /**
     * Gets the value of the emdCoupon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMDCoupon() {
        return emdCoupon;
    }

    /**
     * Sets the value of the emdCoupon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMDCoupon(String value) {
        this.emdCoupon = value;
    }

}
