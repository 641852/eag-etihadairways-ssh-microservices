
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BSGReservation.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BSGReservation.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Segments" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AirSegmentSellFromBSGInventory.PNRB" minOccurs="0"/>
 *         &lt;element name="TicketingInfo" type="{http://webservices.sabre.com/pnrbuilder/v1_15}TicketingTimeLimit.PNRB" minOccurs="0"/>
 *         &lt;element name="AssociatedReferenceInformation" type="{http://webservices.sabre.com/pnrbuilder/v1_15}BSGAssociatedReferenceInformation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BSGReservation.PNRB", propOrder = {
    "segments",
    "ticketingInfo",
    "associatedReferenceInformation"
})
public class BSGReservationPNRB {

    @XmlElement(name = "Segments")
    protected AirSegmentSellFromBSGInventoryPNRB segments;
    @XmlElement(name = "TicketingInfo")
    protected TicketingTimeLimitPNRB ticketingInfo;
    @XmlElement(name = "AssociatedReferenceInformation")
    protected BSGAssociatedReferenceInformation associatedReferenceInformation;

    /**
     * Gets the value of the segments property.
     * 
     * @return
     *     possible object is
     *     {@link AirSegmentSellFromBSGInventoryPNRB }
     *     
     */
    public AirSegmentSellFromBSGInventoryPNRB getSegments() {
        return segments;
    }

    /**
     * Sets the value of the segments property.
     * 
     * @param value
     *     allowed object is
     *     {@link AirSegmentSellFromBSGInventoryPNRB }
     *     
     */
    public void setSegments(AirSegmentSellFromBSGInventoryPNRB value) {
        this.segments = value;
    }

    /**
     * Gets the value of the ticketingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TicketingTimeLimitPNRB }
     *     
     */
    public TicketingTimeLimitPNRB getTicketingInfo() {
        return ticketingInfo;
    }

    /**
     * Sets the value of the ticketingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TicketingTimeLimitPNRB }
     *     
     */
    public void setTicketingInfo(TicketingTimeLimitPNRB value) {
        this.ticketingInfo = value;
    }

    /**
     * Gets the value of the associatedReferenceInformation property.
     * 
     * @return
     *     possible object is
     *     {@link BSGAssociatedReferenceInformation }
     *     
     */
    public BSGAssociatedReferenceInformation getAssociatedReferenceInformation() {
        return associatedReferenceInformation;
    }

    /**
     * Sets the value of the associatedReferenceInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link BSGAssociatedReferenceInformation }
     *     
     */
    public void setAssociatedReferenceInformation(BSGAssociatedReferenceInformation value) {
        this.associatedReferenceInformation = value;
    }

}
