
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * * add future queue placement in remarks field
 * 
 *                 5Q-(PCC)(queue #)/(prefatory instruction number)
 *                 5Q-B4T03AUG55/11
 * 
 *                 NOTE: max of 14 5Q- entries in one PNR. Cannot enter
 *                 remarks
 *                 beginning with 5QQ.
 *             
 * 
 * <p>Java class for QueueRemark.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueueRemark.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PseudoCityCode" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="QueueNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Numeric0to99999" minOccurs="0"/>
 *         &lt;element name="FutureDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="InstructionNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Numeric0to99999" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="index" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Numeric0to99999" />
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_15}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_15}OperationType.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueueRemark.PNRB", propOrder = {
    "pseudoCityCode",
    "queueNumber",
    "futureDate",
    "instructionNumber"
})
public class QueueRemarkPNRB {

    @XmlElement(name = "PseudoCityCode")
    protected String pseudoCityCode;
    @XmlElement(name = "QueueNumber")
    protected Integer queueNumber;
    @XmlElement(name = "FutureDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar futureDate;
    @XmlElement(name = "InstructionNumber")
    protected Integer instructionNumber;
    @XmlAttribute(name = "index")
    protected Integer index;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;

    /**
     * Gets the value of the pseudoCityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPseudoCityCode() {
        return pseudoCityCode;
    }

    /**
     * Sets the value of the pseudoCityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPseudoCityCode(String value) {
        this.pseudoCityCode = value;
    }

    /**
     * Gets the value of the queueNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQueueNumber() {
        return queueNumber;
    }

    /**
     * Sets the value of the queueNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQueueNumber(Integer value) {
        this.queueNumber = value;
    }

    /**
     * Gets the value of the futureDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFutureDate() {
        return futureDate;
    }

    /**
     * Sets the value of the futureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFutureDate(XMLGregorianCalendar value) {
        this.futureDate = value;
    }

    /**
     * Gets the value of the instructionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInstructionNumber() {
        return instructionNumber;
    }

    /**
     * Sets the value of the instructionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInstructionNumber(Integer value) {
        this.instructionNumber = value;
    }

    /**
     * Gets the value of the index property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIndex() {
        return index;
    }

    /**
     * Sets the value of the index property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIndex(Integer value) {
        this.index = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }

}
