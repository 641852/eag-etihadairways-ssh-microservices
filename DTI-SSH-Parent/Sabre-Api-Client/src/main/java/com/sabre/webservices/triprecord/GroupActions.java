
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GroupActions.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GroupActions">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DeleteAll"/>
 *     &lt;enumeration value="ReplaceAll"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GroupActions")
@XmlEnum
public enum GroupActions {

    @XmlEnumValue("DeleteAll")
    DELETE_ALL("DeleteAll"),
    @XmlEnumValue("ReplaceAll")
    REPLACE_ALL("ReplaceAll");
    private final String value;

    GroupActions(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupActions fromValue(String v) {
        for (GroupActions c: GroupActions.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
