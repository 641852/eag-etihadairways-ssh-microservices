
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ODExceptionTypeIndicatorsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ODExceptionTypeIndicatorsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="forcedSell" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="schedChg" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="forcedPartialCancel" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="ttyRejectAgent" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="ttyTransaction" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ODExceptionTypeIndicatorsType")
public class ODExceptionTypeIndicatorsType {

    @XmlAttribute(name = "forcedSell")
    protected Boolean forcedSell;
    @XmlAttribute(name = "schedChg")
    protected Boolean schedChg;
    @XmlAttribute(name = "forcedPartialCancel")
    protected Boolean forcedPartialCancel;
    @XmlAttribute(name = "ttyRejectAgent")
    protected Boolean ttyRejectAgent;
    @XmlAttribute(name = "ttyTransaction")
    protected Boolean ttyTransaction;

    /**
     * Gets the value of the forcedSell property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isForcedSell() {
        if (forcedSell == null) {
            return false;
        } else {
            return forcedSell;
        }
    }

    /**
     * Sets the value of the forcedSell property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForcedSell(Boolean value) {
        this.forcedSell = value;
    }

    /**
     * Gets the value of the schedChg property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isSchedChg() {
        if (schedChg == null) {
            return false;
        } else {
            return schedChg;
        }
    }

    /**
     * Sets the value of the schedChg property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSchedChg(Boolean value) {
        this.schedChg = value;
    }

    /**
     * Gets the value of the forcedPartialCancel property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isForcedPartialCancel() {
        if (forcedPartialCancel == null) {
            return false;
        } else {
            return forcedPartialCancel;
        }
    }

    /**
     * Sets the value of the forcedPartialCancel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForcedPartialCancel(Boolean value) {
        this.forcedPartialCancel = value;
    }

    /**
     * Gets the value of the ttyRejectAgent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isTtyRejectAgent() {
        if (ttyRejectAgent == null) {
            return false;
        } else {
            return ttyRejectAgent;
        }
    }

    /**
     * Sets the value of the ttyRejectAgent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTtyRejectAgent(Boolean value) {
        this.ttyRejectAgent = value;
    }

    /**
     * Gets the value of the ttyTransaction property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isTtyTransaction() {
        if (ttyTransaction == null) {
            return false;
        } else {
            return ttyTransaction;
        }
    }

    /**
     * Sets the value of the ttyTransaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTtyTransaction(Boolean value) {
        this.ttyTransaction = value;
    }

}
