
package com.sabre.webservices.pnrbuilder.v1_15;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * List of Fqtv Upgrade requests associated to
 * 				passenger and air.
 * 
 * <p>Java class for FqtvUpgradeRequests complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FqtvUpgradeRequests">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FqtvUpgradeRequest" type="{http://webservices.sabre.com/pnrbuilder/v1_15}FqtvUpgradeRequest" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FqtvUpgradeRequests", propOrder = {
    "fqtvUpgradeRequest"
})
public class FqtvUpgradeRequests {

    @XmlElement(name = "FqtvUpgradeRequest")
    protected List<FqtvUpgradeRequest> fqtvUpgradeRequest;

    /**
     * Gets the value of the fqtvUpgradeRequest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fqtvUpgradeRequest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFqtvUpgradeRequest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FqtvUpgradeRequest }
     * 
     * 
     */
    public List<FqtvUpgradeRequest> getFqtvUpgradeRequest() {
        if (fqtvUpgradeRequest == null) {
            fqtvUpgradeRequest = new ArrayList<FqtvUpgradeRequest>();
        }
        return this.fqtvUpgradeRequest;
    }

}
