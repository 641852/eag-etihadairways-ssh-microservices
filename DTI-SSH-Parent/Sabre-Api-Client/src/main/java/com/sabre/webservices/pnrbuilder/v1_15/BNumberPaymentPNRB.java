
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BNumberPayment.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BNumberPayment.PNRB">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservices.sabre.com/pnrbuilder/v1_15}Payment.PNRB">
 *       &lt;sequence>
 *         &lt;element name="FormOfPaymentInfo" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="BNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BNumberPayment.PNRB", propOrder = {
    "formOfPaymentInfo",
    "bNumber"
})
public class BNumberPaymentPNRB
    extends PaymentPNRB
{

    @XmlElement(name = "FormOfPaymentInfo")
    protected String formOfPaymentInfo;
    @XmlElement(name = "BNumber")
    protected String bNumber;

    /**
     * Gets the value of the formOfPaymentInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormOfPaymentInfo() {
        return formOfPaymentInfo;
    }

    /**
     * Sets the value of the formOfPaymentInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormOfPaymentInfo(String value) {
        this.formOfPaymentInfo = value;
    }

    /**
     * Gets the value of the bNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBNumber() {
        return bNumber;
    }

    /**
     * Sets the value of the bNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBNumber(String value) {
        this.bNumber = value;
    }

}
