
package com.sabre.webservices.pnrbuilder.v1_15;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountingLines.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountingLines.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountingLine" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AccountingLine.PNRB" maxOccurs="99" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountingLines.PNRB", propOrder = {
    "accountingLine"
})
public class AccountingLinesPNRB {

    @XmlElement(name = "AccountingLine")
    protected List<AccountingLinePNRB> accountingLine;

    /**
     * Gets the value of the accountingLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountingLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountingLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountingLinePNRB }
     * 
     * 
     */
    public List<AccountingLinePNRB> getAccountingLine() {
        if (accountingLine == null) {
            accountingLine = new ArrayList<AccountingLinePNRB>();
        }
        return this.accountingLine;
    }

}
