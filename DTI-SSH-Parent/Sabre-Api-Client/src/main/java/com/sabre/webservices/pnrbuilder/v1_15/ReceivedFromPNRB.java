
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReceivedFrom.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReceivedFrom.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Name" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="AgentName" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="TourWholesalerPCC" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="NewControllingPCC" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="fromPassenger" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceivedFrom.PNRB", propOrder = {
    "name",
    "agentName",
    "tourWholesalerPCC",
    "newControllingPCC"
})
public class ReceivedFromPNRB {

    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "AgentName")
    protected String agentName;
    @XmlElement(name = "TourWholesalerPCC")
    protected String tourWholesalerPCC;
    @XmlElement(name = "NewControllingPCC")
    protected String newControllingPCC;
    @XmlAttribute(name = "fromPassenger")
    protected Boolean fromPassenger;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the agentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentName() {
        return agentName;
    }

    /**
     * Sets the value of the agentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentName(String value) {
        this.agentName = value;
    }

    /**
     * Gets the value of the tourWholesalerPCC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTourWholesalerPCC() {
        return tourWholesalerPCC;
    }

    /**
     * Sets the value of the tourWholesalerPCC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTourWholesalerPCC(String value) {
        this.tourWholesalerPCC = value;
    }

    /**
     * Gets the value of the newControllingPCC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewControllingPCC() {
        return newControllingPCC;
    }

    /**
     * Sets the value of the newControllingPCC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewControllingPCC(String value) {
        this.newControllingPCC = value;
    }

    /**
     * Gets the value of the fromPassenger property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFromPassenger() {
        return fromPassenger;
    }

    /**
     * Sets the value of the fromPassenger property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFromPassenger(Boolean value) {
        this.fromPassenger = value;
    }

}
