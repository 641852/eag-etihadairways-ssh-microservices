
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RemarkType.PNRB.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RemarkType.PNRB">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="HS"/>
 *     &lt;enumeration value="HD"/>
 *     &lt;enumeration value="REG"/>
 *     &lt;enumeration value="QQ"/>
 *     &lt;enumeration value="CLIADR"/>
 *     &lt;enumeration value="DELADR"/>
 *     &lt;enumeration value="INVOICE"/>
 *     &lt;enumeration value="ITINERARY"/>
 *     &lt;enumeration value="INTERFACE"/>
 *     &lt;enumeration value="FILLER"/>
 *     &lt;enumeration value="CODED"/>
 *     &lt;enumeration value="PRTONTKT"/>
 *     &lt;enumeration value="INVSEGASSOC"/>
 *     &lt;enumeration value="ITINSEGASSOC"/>
 *     &lt;enumeration value="FOP"/>
 *     &lt;enumeration value="CORPORATE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RemarkType.PNRB")
@XmlEnum
public enum RemarkTypePNRB {

    HS,
    HD,
    REG,
    QQ,
    CLIADR,
    DELADR,
    INVOICE,
    ITINERARY,
    INTERFACE,
    FILLER,
    CODED,
    PRTONTKT,
    INVSEGASSOC,
    ITINSEGASSOC,
    FOP,
    CORPORATE;

    public String value() {
        return name();
    }

    public static RemarkTypePNRB fromValue(String v) {
        return valueOf(v);
    }

}
