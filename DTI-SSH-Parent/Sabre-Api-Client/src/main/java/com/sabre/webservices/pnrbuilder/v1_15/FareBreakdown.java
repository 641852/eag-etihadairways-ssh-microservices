
package com.sabre.webservices.pnrbuilder.v1_15;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FareBreakdown complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FareBreakdown">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservices.sabre.com/pnrbuilder/v1_15}PassengerQuantity">
 *       &lt;sequence>
 *         &lt;element name="FareBasisCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FareCalc" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FlightSegment" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence minOccurs="0">
 *                   &lt;element name="DepartureDateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FlightNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_15}FlightNumber" minOccurs="0"/>
 *                   &lt;element name="ActionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FlightType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AirPort" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Airport" minOccurs="0"/>
 *                   &lt;element name="OperatingAirline" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AirlineCode" minOccurs="0"/>
 *                   &lt;element name="FareBasisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ValidityDates" type="{http://webservices.sabre.com/pnrbuilder/v1_15}ValidityDates" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="RPH" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PassengerTypeRequested" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PassengerTypePriced" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FareBreakdown", propOrder = {
    "fareBasisCode",
    "fareCalc",
    "flightSegment",
    "passengerTypeRequested",
    "passengerTypePriced"
})
public class FareBreakdown
    extends PassengerQuantity
{

    @XmlElement(name = "FareBasisCode")
    protected List<String> fareBasisCode;
    @XmlElement(name = "FareCalc")
    protected List<String> fareCalc;
    @XmlElement(name = "FlightSegment")
    protected List<FareBreakdown.FlightSegment> flightSegment;
    @XmlElement(name = "PassengerTypeRequested")
    protected FareBreakdown.PassengerTypeRequested passengerTypeRequested;
    @XmlElement(name = "PassengerTypePriced")
    protected FareBreakdown.PassengerTypePriced passengerTypePriced;

    /**
     * Gets the value of the fareBasisCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareBasisCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareBasisCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFareBasisCode() {
        if (fareBasisCode == null) {
            fareBasisCode = new ArrayList<String>();
        }
        return this.fareBasisCode;
    }

    /**
     * Gets the value of the fareCalc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareCalc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareCalc().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFareCalc() {
        if (fareCalc == null) {
            fareCalc = new ArrayList<String>();
        }
        return this.fareCalc;
    }

    /**
     * Gets the value of the flightSegment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flightSegment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlightSegment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FareBreakdown.FlightSegment }
     * 
     * 
     */
    public List<FareBreakdown.FlightSegment> getFlightSegment() {
        if (flightSegment == null) {
            flightSegment = new ArrayList<FareBreakdown.FlightSegment>();
        }
        return this.flightSegment;
    }

    /**
     * Gets the value of the passengerTypeRequested property.
     * 
     * @return
     *     possible object is
     *     {@link FareBreakdown.PassengerTypeRequested }
     *     
     */
    public FareBreakdown.PassengerTypeRequested getPassengerTypeRequested() {
        return passengerTypeRequested;
    }

    /**
     * Sets the value of the passengerTypeRequested property.
     * 
     * @param value
     *     allowed object is
     *     {@link FareBreakdown.PassengerTypeRequested }
     *     
     */
    public void setPassengerTypeRequested(FareBreakdown.PassengerTypeRequested value) {
        this.passengerTypeRequested = value;
    }

    /**
     * Gets the value of the passengerTypePriced property.
     * 
     * @return
     *     possible object is
     *     {@link FareBreakdown.PassengerTypePriced }
     *     
     */
    public FareBreakdown.PassengerTypePriced getPassengerTypePriced() {
        return passengerTypePriced;
    }

    /**
     * Sets the value of the passengerTypePriced property.
     * 
     * @param value
     *     allowed object is
     *     {@link FareBreakdown.PassengerTypePriced }
     *     
     */
    public void setPassengerTypePriced(FareBreakdown.PassengerTypePriced value) {
        this.passengerTypePriced = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence minOccurs="0">
     *         &lt;element name="DepartureDateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FlightNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_15}FlightNumber" minOccurs="0"/>
     *         &lt;element name="ActionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FlightType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AirPort" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Airport" minOccurs="0"/>
     *         &lt;element name="OperatingAirline" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AirlineCode" minOccurs="0"/>
     *         &lt;element name="FareBasisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ValidityDates" type="{http://webservices.sabre.com/pnrbuilder/v1_15}ValidityDates" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="RPH" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "departureDateTime",
        "resBookDesigCode",
        "flightNumber",
        "actionCode",
        "flightType",
        "airPort",
        "operatingAirline",
        "fareBasisCode",
        "validityDates"
    })
    public static class FlightSegment {

        @XmlElement(name = "DepartureDateTime")
        protected String departureDateTime;
        @XmlElement(name = "ResBookDesigCode")
        protected String resBookDesigCode;
        @XmlElement(name = "FlightNumber")
        protected String flightNumber;
        @XmlElement(name = "ActionCode")
        protected String actionCode;
        @XmlElement(name = "FlightType")
        protected String flightType;
        @XmlElement(name = "AirPort")
        protected Airport airPort;
        @XmlElement(name = "OperatingAirline")
        protected String operatingAirline;
        @XmlElement(name = "FareBasisCode")
        protected String fareBasisCode;
        @XmlElement(name = "ValidityDates")
        protected ValidityDates validityDates;
        @XmlAttribute(name = "RPH")
        @XmlSchemaType(name = "anySimpleType")
        protected String rph;

        /**
         * Gets the value of the departureDateTime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDepartureDateTime() {
            return departureDateTime;
        }

        /**
         * Sets the value of the departureDateTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDepartureDateTime(String value) {
            this.departureDateTime = value;
        }

        /**
         * Gets the value of the resBookDesigCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResBookDesigCode() {
            return resBookDesigCode;
        }

        /**
         * Sets the value of the resBookDesigCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResBookDesigCode(String value) {
            this.resBookDesigCode = value;
        }

        /**
         * Gets the value of the flightNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlightNumber() {
            return flightNumber;
        }

        /**
         * Sets the value of the flightNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlightNumber(String value) {
            this.flightNumber = value;
        }

        /**
         * Gets the value of the actionCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActionCode() {
            return actionCode;
        }

        /**
         * Sets the value of the actionCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActionCode(String value) {
            this.actionCode = value;
        }

        /**
         * Gets the value of the flightType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlightType() {
            return flightType;
        }

        /**
         * Sets the value of the flightType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlightType(String value) {
            this.flightType = value;
        }

        /**
         * Gets the value of the airPort property.
         * 
         * @return
         *     possible object is
         *     {@link Airport }
         *     
         */
        public Airport getAirPort() {
            return airPort;
        }

        /**
         * Sets the value of the airPort property.
         * 
         * @param value
         *     allowed object is
         *     {@link Airport }
         *     
         */
        public void setAirPort(Airport value) {
            this.airPort = value;
        }

        /**
         * Gets the value of the operatingAirline property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOperatingAirline() {
            return operatingAirline;
        }

        /**
         * Sets the value of the operatingAirline property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOperatingAirline(String value) {
            this.operatingAirline = value;
        }

        /**
         * Gets the value of the fareBasisCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFareBasisCode() {
            return fareBasisCode;
        }

        /**
         * Sets the value of the fareBasisCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFareBasisCode(String value) {
            this.fareBasisCode = value;
        }

        /**
         * Gets the value of the validityDates property.
         * 
         * @return
         *     possible object is
         *     {@link ValidityDates }
         *     
         */
        public ValidityDates getValidityDates() {
            return validityDates;
        }

        /**
         * Sets the value of the validityDates property.
         * 
         * @param value
         *     allowed object is
         *     {@link ValidityDates }
         *     
         */
        public void setValidityDates(ValidityDates value) {
            this.validityDates = value;
        }

        /**
         * Gets the value of the rph property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Sets the value of the rph property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class PassengerTypePriced {

        @XmlAttribute(name = "Code")
        protected String code;

        /**
         * Gets the value of the code property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class PassengerTypeRequested {

        @XmlAttribute(name = "Code")
        protected String code;

        /**
         * Gets the value of the code property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

    }

}
