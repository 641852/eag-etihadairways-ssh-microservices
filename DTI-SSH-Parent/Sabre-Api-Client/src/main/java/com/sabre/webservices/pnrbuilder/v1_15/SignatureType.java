
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SignatureType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SignatureType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HistoryTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DutyCode" type="{http://webservices.sabre.com/pnrbuilder/v1_15}DutyCodeList" minOccurs="0"/>
 *         &lt;element name="AgentSine" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AgentSineType" minOccurs="0"/>
 *         &lt;element name="HomePCC" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="AgencyPCC" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="OAC" type="{http://webservices.sabre.com/pnrbuilder/v1_15}OACType" minOccurs="0"/>
 *         &lt;element name="HistoryFrom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="SequenceNbr" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SignatureType", propOrder = {
    "historyTimestamp",
    "dutyCode",
    "agentSine",
    "homePCC",
    "agencyPCC",
    "oac",
    "historyFrom"
})
public class SignatureType {

    @XmlElement(name = "HistoryTimestamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar historyTimestamp;
    @XmlElement(name = "DutyCode")
    protected String dutyCode;
    @XmlElement(name = "AgentSine")
    protected String agentSine;
    @XmlElement(name = "HomePCC")
    protected String homePCC;
    @XmlElement(name = "AgencyPCC")
    protected String agencyPCC;
    @XmlElement(name = "OAC")
    protected OACType oac;
    @XmlElement(name = "HistoryFrom")
    protected String historyFrom;
    @XmlAttribute(name = "SequenceNbr")
    protected Integer sequenceNbr;

    /**
     * Gets the value of the historyTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHistoryTimestamp() {
        return historyTimestamp;
    }

    /**
     * Sets the value of the historyTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHistoryTimestamp(XMLGregorianCalendar value) {
        this.historyTimestamp = value;
    }

    /**
     * Gets the value of the dutyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDutyCode() {
        return dutyCode;
    }

    /**
     * Sets the value of the dutyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDutyCode(String value) {
        this.dutyCode = value;
    }

    /**
     * Gets the value of the agentSine property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentSine() {
        return agentSine;
    }

    /**
     * Sets the value of the agentSine property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentSine(String value) {
        this.agentSine = value;
    }

    /**
     * Gets the value of the homePCC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHomePCC() {
        return homePCC;
    }

    /**
     * Sets the value of the homePCC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHomePCC(String value) {
        this.homePCC = value;
    }

    /**
     * Gets the value of the agencyPCC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyPCC() {
        return agencyPCC;
    }

    /**
     * Sets the value of the agencyPCC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyPCC(String value) {
        this.agencyPCC = value;
    }

    /**
     * Gets the value of the oac property.
     * 
     * @return
     *     possible object is
     *     {@link OACType }
     *     
     */
    public OACType getOAC() {
        return oac;
    }

    /**
     * Sets the value of the oac property.
     * 
     * @param value
     *     allowed object is
     *     {@link OACType }
     *     
     */
    public void setOAC(OACType value) {
        this.oac = value;
    }

    /**
     * Gets the value of the historyFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHistoryFrom() {
        return historyFrom;
    }

    /**
     * Sets the value of the historyFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHistoryFrom(String value) {
        this.historyFrom = value;
    }

    /**
     * Gets the value of the sequenceNbr property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSequenceNbr() {
        return sequenceNbr;
    }

    /**
     * Sets the value of the sequenceNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSequenceNbr(Integer value) {
        this.sequenceNbr = value;
    }

}
