
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReservationCompletionCriteria.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReservationCompletionCriteria.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SendEmail" type="{http://webservices.sabre.com/pnrbuilder/v1_15}SendEmail.PNRB" minOccurs="0"/>
 *         &lt;element name="Transaction" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Transaction.PNRB" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="returnReservation" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReservationCompletionCriteria.PNRB", propOrder = {
    "sendEmail",
    "transaction"
})
public class ReservationCompletionCriteriaPNRB {

    @XmlElement(name = "SendEmail")
    protected SendEmailPNRB sendEmail;
    @XmlElement(name = "Transaction")
    protected TransactionPNRB transaction;
    @XmlAttribute(name = "returnReservation")
    protected Boolean returnReservation;

    /**
     * Gets the value of the sendEmail property.
     * 
     * @return
     *     possible object is
     *     {@link SendEmailPNRB }
     *     
     */
    public SendEmailPNRB getSendEmail() {
        return sendEmail;
    }

    /**
     * Sets the value of the sendEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendEmailPNRB }
     *     
     */
    public void setSendEmail(SendEmailPNRB value) {
        this.sendEmail = value;
    }

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionPNRB }
     *     
     */
    public TransactionPNRB getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionPNRB }
     *     
     */
    public void setTransaction(TransactionPNRB value) {
        this.transaction = value;
    }

    /**
     * Gets the value of the returnReservation property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReturnReservation() {
        return returnReservation;
    }

    /**
     * Sets the value of the returnReservation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnReservation(Boolean value) {
        this.returnReservation = value;
    }

}
