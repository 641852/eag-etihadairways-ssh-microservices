
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchCriterionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchCriterionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="Field" use="required" type="{http://webservices.sabre.com/triprecord}GenericFieldType" />
 *       &lt;attribute name="Value" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;minLength value="1"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="MatchingOperator" type="{http://webservices.sabre.com/triprecord}MatchingOperatorType" default="EQUALS" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriterionType")
public class SearchCriterionType {

    @XmlAttribute(name = "Field", required = true)
    protected GenericFieldType field;
    @XmlAttribute(name = "Value", required = true)
    protected String value;
    @XmlAttribute(name = "MatchingOperator")
    protected MatchingOperatorType matchingOperator;

    /**
     * Gets the value of the field property.
     * 
     * @return
     *     possible object is
     *     {@link GenericFieldType }
     *     
     */
    public GenericFieldType getField() {
        return field;
    }

    /**
     * Sets the value of the field property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericFieldType }
     *     
     */
    public void setField(GenericFieldType value) {
        this.field = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the matchingOperator property.
     * 
     * @return
     *     possible object is
     *     {@link MatchingOperatorType }
     *     
     */
    public MatchingOperatorType getMatchingOperator() {
        if (matchingOperator == null) {
            return MatchingOperatorType.EQUALS;
        } else {
            return matchingOperator;
        }
    }

    /**
     * Sets the value of the matchingOperator property.
     * 
     * @param value
     *     allowed object is
     *     {@link MatchingOperatorType }
     *     
     */
    public void setMatchingOperator(MatchingOperatorType value) {
        this.matchingOperator = value;
    }

}
