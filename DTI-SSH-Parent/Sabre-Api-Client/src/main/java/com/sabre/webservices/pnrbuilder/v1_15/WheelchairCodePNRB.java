
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WheelchairCode.PNRB.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="WheelchairCode.PNRB">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="WCHR"/>
 *     &lt;enumeration value="WCHS"/>
 *     &lt;enumeration value="WCHC"/>
 *     &lt;enumeration value="WCBD"/>
 *     &lt;enumeration value="WCBW"/>
 *     &lt;enumeration value="WCMP"/>
 *     &lt;enumeration value="WCOB"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "WheelchairCode.PNRB")
@XmlEnum
public enum WheelchairCodePNRB {

    WCHR,
    WCHS,
    WCHC,
    WCBD,
    WCBW,
    WCMP,
    WCOB;

    public String value() {
        return name();
    }

    public static WheelchairCodePNRB fromValue(String v) {
        return valueOf(v);
    }

}
