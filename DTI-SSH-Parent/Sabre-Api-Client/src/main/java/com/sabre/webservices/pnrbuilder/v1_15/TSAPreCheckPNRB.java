
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TSAPreCheck.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TSAPreCheck.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodeWordLevel" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TSAPreCheck.PNRB", propOrder = {
    "codeWordLevel"
})
public class TSAPreCheckPNRB {

    @XmlElement(name = "CodeWordLevel")
    protected String codeWordLevel;

    /**
     * Gets the value of the codeWordLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeWordLevel() {
        return codeWordLevel;
    }

    /**
     * Sets the value of the codeWordLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeWordLevel(String value) {
        this.codeWordLevel = value;
    }

}
