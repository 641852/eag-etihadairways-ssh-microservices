
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Formats for FOP:
 * 
 *                 * Form of payment B Number - BSP Netherlands only
 * 
 *                 5-(FOP entry).B(number) 5-CHEQUE.B12345
 * 
 *                 * add cash FOP
 * 
 *                 5-CASH
 * 
 *                 * add check FOP
 * 
 *                 5-CHECK 5-CK 5-CHEQUE
 * 
 *                 * add credit card FOP that generates an approval
 * 
 *                 5-*(credit card code)(credit card number)‡(2-digit month
 *                 of exp)(2-digit year of exp) 5-*AX1234567890123456‡XX/XX
 * 
 *                 * suppress credit card number on invoice with form of
 *                 payment in remarKs
 * 
 *                 5-*(credit card code)(credit card #)‡(2-digit month of
 *                 exp)/(2-digit year of ex)-XN
 *                 5-*AX1234567890123456‡XX/XX-XN
 * 
 *                 * add credit card form of payment that does not generate
 *                 an approval
 * 
 *                 5-(credit card code)(credit card #)‡(2-digit month of
 *                 exp)/(2-digit year of exp) 5-AX1234567890123456‡XX/XX
 * 
 *                 * add credit card with extended payment
 * 
 *                 5-*(credit card code)(credit card number)‡(2-digit month
 *                 of exp)/(2-digit year of exp)*(extendede payment entry
 *                 if applicable)
 * 
 *                 5-*AX1234567890123456‡XX/XX*E
 * 
 *                 * add credit card with card security code
 * 
 *                 5-*(credit card code)(credit card number)‡(2-digit month
 *                 of exp)/(2-digit year of exp)(*extended payment entry if
 *                 applicable)‡CSC/(2 letter airline code)
 *                 5-*AX1234567890123456‡XX/XX‡1234/AL
 * 
 *                 * add GTR
 * 
 *                 5-GR‡(GTR entry) 5-GR‡K19876543002
 * 
 *                 * add GTR when using it as a form of payment for travel
 *                 on Delta airlines and you issue the ticket
 *                 electronically
 * 
 *                 5-GR(one letter followed by up to seven numbers)
 *                 5-GRD9999999
 * 
 *                 * delete form of payment
 * 
 *                 5(line number)¤ 51¤
 * 
 *                 * modify FOP
 * 
 *                 5(line #)¤-(new FOP entry) 51¤-*AX1234567890123456‡XX/XX
 *             
 * 
 * <p>Java class for CreditCardPayment.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditCardPayment.PNRB">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservices.sabre.com/pnrbuilder/v1_15}Payment.PNRB">
 *       &lt;sequence>
 *         &lt;element name="CreditCardNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_15}PaymentCardNumber" minOccurs="0"/>
 *         &lt;element name="CreditCardCode" type="{http://webservices.sabre.com/pnrbuilder/v1_15}PaymentCardCode" minOccurs="0"/>
 *         &lt;element name="ExpirationMonth" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="ExpirationYear" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="CSC" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="AirlineCode" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AirlineCode.2to4Char" minOccurs="0"/>
 *         &lt;element name="RequestApprovalCode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MonthsExtendedPayments" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Numeric0to99999" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardPayment.PNRB", propOrder = {
    "creditCardNumber",
    "creditCardCode",
    "expirationMonth",
    "expirationYear",
    "csc",
    "airlineCode",
    "requestApprovalCode",
    "monthsExtendedPayments"
})
public class CreditCardPaymentPNRB
    extends PaymentPNRB
{

    @XmlElement(name = "CreditCardNumber")
    protected String creditCardNumber;
    @XmlElement(name = "CreditCardCode")
    protected String creditCardCode;
    @XmlElement(name = "ExpirationMonth")
    protected String expirationMonth;
    @XmlElement(name = "ExpirationYear")
    protected String expirationYear;
    @XmlElement(name = "CSC")
    protected String csc;
    @XmlElement(name = "AirlineCode")
    protected String airlineCode;
    @XmlElement(name = "RequestApprovalCode")
    protected Boolean requestApprovalCode;
    @XmlElement(name = "MonthsExtendedPayments")
    protected Integer monthsExtendedPayments;

    /**
     * Gets the value of the creditCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    /**
     * Sets the value of the creditCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCardNumber(String value) {
        this.creditCardNumber = value;
    }

    /**
     * Gets the value of the creditCardCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCardCode() {
        return creditCardCode;
    }

    /**
     * Sets the value of the creditCardCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCardCode(String value) {
        this.creditCardCode = value;
    }

    /**
     * Gets the value of the expirationMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpirationMonth() {
        return expirationMonth;
    }

    /**
     * Sets the value of the expirationMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpirationMonth(String value) {
        this.expirationMonth = value;
    }

    /**
     * Gets the value of the expirationYear property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpirationYear() {
        return expirationYear;
    }

    /**
     * Sets the value of the expirationYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpirationYear(String value) {
        this.expirationYear = value;
    }

    /**
     * Gets the value of the csc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCSC() {
        return csc;
    }

    /**
     * Sets the value of the csc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCSC(String value) {
        this.csc = value;
    }

    /**
     * Gets the value of the airlineCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirlineCode() {
        return airlineCode;
    }

    /**
     * Sets the value of the airlineCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirlineCode(String value) {
        this.airlineCode = value;
    }

    /**
     * Gets the value of the requestApprovalCode property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequestApprovalCode() {
        return requestApprovalCode;
    }

    /**
     * Sets the value of the requestApprovalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequestApprovalCode(Boolean value) {
        this.requestApprovalCode = value;
    }

    /**
     * Gets the value of the monthsExtendedPayments property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMonthsExtendedPayments() {
        return monthsExtendedPayments;
    }

    /**
     * Sets the value of the monthsExtendedPayments property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMonthsExtendedPayments(Integer value) {
        this.monthsExtendedPayments = value;
    }

}
