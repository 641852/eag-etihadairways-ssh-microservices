
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CurrencyCodeList.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CurrencyCodeList">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="ADP"/>
 *     &lt;enumeration value="AED"/>
 *     &lt;enumeration value="AFA"/>
 *     &lt;enumeration value="AFN"/>
 *     &lt;enumeration value="ALL"/>
 *     &lt;enumeration value="AMD"/>
 *     &lt;enumeration value="ANG"/>
 *     &lt;enumeration value="AOA"/>
 *     &lt;enumeration value="AON"/>
 *     &lt;enumeration value="AOR"/>
 *     &lt;enumeration value="ARS"/>
 *     &lt;enumeration value="ATS"/>
 *     &lt;enumeration value="AUD"/>
 *     &lt;enumeration value="AWG"/>
 *     &lt;enumeration value="AZM"/>
 *     &lt;enumeration value="AZN"/>
 *     &lt;enumeration value="BAM"/>
 *     &lt;enumeration value="BBD"/>
 *     &lt;enumeration value="BDT"/>
 *     &lt;enumeration value="BEF"/>
 *     &lt;enumeration value="BGL"/>
 *     &lt;enumeration value="BGN"/>
 *     &lt;enumeration value="BHD"/>
 *     &lt;enumeration value="BIF"/>
 *     &lt;enumeration value="BMD"/>
 *     &lt;enumeration value="BND"/>
 *     &lt;enumeration value="BOB"/>
 *     &lt;enumeration value="BOV"/>
 *     &lt;enumeration value="BRL"/>
 *     &lt;enumeration value="BSD"/>
 *     &lt;enumeration value="BTN"/>
 *     &lt;enumeration value="BWP"/>
 *     &lt;enumeration value="BYR"/>
 *     &lt;enumeration value="BZD"/>
 *     &lt;enumeration value="CAD"/>
 *     &lt;enumeration value="CDF"/>
 *     &lt;enumeration value="CHE"/>
 *     &lt;enumeration value="CHF"/>
 *     &lt;enumeration value="CHW"/>
 *     &lt;enumeration value="CLF"/>
 *     &lt;enumeration value="CLP"/>
 *     &lt;enumeration value="CNY"/>
 *     &lt;enumeration value="COP"/>
 *     &lt;enumeration value="COU"/>
 *     &lt;enumeration value="CRC"/>
 *     &lt;enumeration value="CUP"/>
 *     &lt;enumeration value="CUC"/>
 *     &lt;enumeration value="CVE"/>
 *     &lt;enumeration value="CYP"/>
 *     &lt;enumeration value="CZK"/>
 *     &lt;enumeration value="DEM"/>
 *     &lt;enumeration value="DJF"/>
 *     &lt;enumeration value="DKK"/>
 *     &lt;enumeration value="DOP"/>
 *     &lt;enumeration value="DZD"/>
 *     &lt;enumeration value="ECS"/>
 *     &lt;enumeration value="ECV"/>
 *     &lt;enumeration value="EEK"/>
 *     &lt;enumeration value="EGP"/>
 *     &lt;enumeration value="ERN"/>
 *     &lt;enumeration value="ESP"/>
 *     &lt;enumeration value="ETB"/>
 *     &lt;enumeration value="EUR"/>
 *     &lt;enumeration value="FIM"/>
 *     &lt;enumeration value="FJD"/>
 *     &lt;enumeration value="FKP"/>
 *     &lt;enumeration value="FRF"/>
 *     &lt;enumeration value="GBP"/>
 *     &lt;enumeration value="GEL"/>
 *     &lt;enumeration value="GHC"/>
 *     &lt;enumeration value="GHS"/>
 *     &lt;enumeration value="GIP"/>
 *     &lt;enumeration value="GMD"/>
 *     &lt;enumeration value="GNF"/>
 *     &lt;enumeration value="GRD"/>
 *     &lt;enumeration value="GTQ"/>
 *     &lt;enumeration value="GWP"/>
 *     &lt;enumeration value="GYD"/>
 *     &lt;enumeration value="HKD"/>
 *     &lt;enumeration value="HNL"/>
 *     &lt;enumeration value="HRK"/>
 *     &lt;enumeration value="HTG"/>
 *     &lt;enumeration value="HUF"/>
 *     &lt;enumeration value="IDR"/>
 *     &lt;enumeration value="IEP"/>
 *     &lt;enumeration value="ILS"/>
 *     &lt;enumeration value="INR"/>
 *     &lt;enumeration value="IQD"/>
 *     &lt;enumeration value="IRR"/>
 *     &lt;enumeration value="ISK"/>
 *     &lt;enumeration value="ITL"/>
 *     &lt;enumeration value="JMD"/>
 *     &lt;enumeration value="JOD"/>
 *     &lt;enumeration value="JPY"/>
 *     &lt;enumeration value="KES"/>
 *     &lt;enumeration value="KGS"/>
 *     &lt;enumeration value="KHR"/>
 *     &lt;enumeration value="KMF"/>
 *     &lt;enumeration value="KPW"/>
 *     &lt;enumeration value="KRW"/>
 *     &lt;enumeration value="KWD"/>
 *     &lt;enumeration value="KYD"/>
 *     &lt;enumeration value="KZT"/>
 *     &lt;enumeration value="LAK"/>
 *     &lt;enumeration value="LBP"/>
 *     &lt;enumeration value="LKR"/>
 *     &lt;enumeration value="LRD"/>
 *     &lt;enumeration value="LSL"/>
 *     &lt;enumeration value="LTL"/>
 *     &lt;enumeration value="LUF"/>
 *     &lt;enumeration value="LVL"/>
 *     &lt;enumeration value="LYD"/>
 *     &lt;enumeration value="MAD"/>
 *     &lt;enumeration value="MDL"/>
 *     &lt;enumeration value="MGA"/>
 *     &lt;enumeration value="MGF"/>
 *     &lt;enumeration value="MKD"/>
 *     &lt;enumeration value="MMK"/>
 *     &lt;enumeration value="MNT"/>
 *     &lt;enumeration value="MOP"/>
 *     &lt;enumeration value="MRO"/>
 *     &lt;enumeration value="MTL"/>
 *     &lt;enumeration value="MUR"/>
 *     &lt;enumeration value="MVR"/>
 *     &lt;enumeration value="MWK"/>
 *     &lt;enumeration value="MXN"/>
 *     &lt;enumeration value="MXV"/>
 *     &lt;enumeration value="MYR"/>
 *     &lt;enumeration value="MZM"/>
 *     &lt;enumeration value="MZN"/>
 *     &lt;enumeration value="NAD"/>
 *     &lt;enumeration value="NGN"/>
 *     &lt;enumeration value="NIO"/>
 *     &lt;enumeration value="NLG"/>
 *     &lt;enumeration value="NOK"/>
 *     &lt;enumeration value="NPR"/>
 *     &lt;enumeration value="NZD"/>
 *     &lt;enumeration value="OMR"/>
 *     &lt;enumeration value="PAB"/>
 *     &lt;enumeration value="PEN"/>
 *     &lt;enumeration value="PGK"/>
 *     &lt;enumeration value="PHP"/>
 *     &lt;enumeration value="PKR"/>
 *     &lt;enumeration value="PLN"/>
 *     &lt;enumeration value="PTE"/>
 *     &lt;enumeration value="PYG"/>
 *     &lt;enumeration value="QAR"/>
 *     &lt;enumeration value="ROL"/>
 *     &lt;enumeration value="RON"/>
 *     &lt;enumeration value="RSD"/>
 *     &lt;enumeration value="RUB"/>
 *     &lt;enumeration value="RUR"/>
 *     &lt;enumeration value="RWF"/>
 *     &lt;enumeration value="SAR"/>
 *     &lt;enumeration value="SBD"/>
 *     &lt;enumeration value="SCR"/>
 *     &lt;enumeration value="SDD"/>
 *     &lt;enumeration value="SDG"/>
 *     &lt;enumeration value="SEK"/>
 *     &lt;enumeration value="SGD"/>
 *     &lt;enumeration value="SHP"/>
 *     &lt;enumeration value="SIT"/>
 *     &lt;enumeration value="SKK"/>
 *     &lt;enumeration value="SLL"/>
 *     &lt;enumeration value="SOS"/>
 *     &lt;enumeration value="SRD"/>
 *     &lt;enumeration value="SRG"/>
 *     &lt;enumeration value="SSP"/>
 *     &lt;enumeration value="STD"/>
 *     &lt;enumeration value="SVC"/>
 *     &lt;enumeration value="SYP"/>
 *     &lt;enumeration value="SZL"/>
 *     &lt;enumeration value="THB"/>
 *     &lt;enumeration value="TJR"/>
 *     &lt;enumeration value="TJS"/>
 *     &lt;enumeration value="TMM"/>
 *     &lt;enumeration value="TMT"/>
 *     &lt;enumeration value="TND"/>
 *     &lt;enumeration value="TOP"/>
 *     &lt;enumeration value="TPE"/>
 *     &lt;enumeration value="TRL"/>
 *     &lt;enumeration value="TRY"/>
 *     &lt;enumeration value="TTD"/>
 *     &lt;enumeration value="TWD"/>
 *     &lt;enumeration value="TZS"/>
 *     &lt;enumeration value="UAH"/>
 *     &lt;enumeration value="UGX"/>
 *     &lt;enumeration value="USD"/>
 *     &lt;enumeration value="USN"/>
 *     &lt;enumeration value="USS"/>
 *     &lt;enumeration value="UYI"/>
 *     &lt;enumeration value="UYU"/>
 *     &lt;enumeration value="UZS"/>
 *     &lt;enumeration value="VEB"/>
 *     &lt;enumeration value="VEF"/>
 *     &lt;enumeration value="VND"/>
 *     &lt;enumeration value="VUV"/>
 *     &lt;enumeration value="WST"/>
 *     &lt;enumeration value="XAF"/>
 *     &lt;enumeration value="XAG"/>
 *     &lt;enumeration value="XAU"/>
 *     &lt;enumeration value="XBA"/>
 *     &lt;enumeration value="XBB"/>
 *     &lt;enumeration value="XBC"/>
 *     &lt;enumeration value="XBD"/>
 *     &lt;enumeration value="XCD"/>
 *     &lt;enumeration value="XDR"/>
 *     &lt;enumeration value="XFO"/>
 *     &lt;enumeration value="XFU"/>
 *     &lt;enumeration value="XOF"/>
 *     &lt;enumeration value="XPD"/>
 *     &lt;enumeration value="XPF"/>
 *     &lt;enumeration value="XPT"/>
 *     &lt;enumeration value="XSU"/>
 *     &lt;enumeration value="XTS"/>
 *     &lt;enumeration value="XUA"/>
 *     &lt;enumeration value="XXX"/>
 *     &lt;enumeration value="YER"/>
 *     &lt;enumeration value="YUM"/>
 *     &lt;enumeration value="ZAL"/>
 *     &lt;enumeration value="ZAR"/>
 *     &lt;enumeration value="ZMK"/>
 *     &lt;enumeration value="ZMW"/>
 *     &lt;enumeration value="ZRN"/>
 *     &lt;enumeration value="ZWD"/>
 *     &lt;enumeration value="ZWL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CurrencyCodeList")
@XmlEnum
public enum CurrencyCodeList {


    /**
     * Andorran Peseta: Andorra
     * 
     */
    ADP,

    /**
     * UAE Dirham: United Arab Emirates
     * 
     */
    AED,

    /**
     * Afghani: Afghanistan; replaced in 2003 by AFN
     * 
     */
    AFA,

    /**
     * Afghani: Afghanistan; 
     * 
     */
    AFN,

    /**
     * Lek: Albania
     * 
     */
    ALL,

    /**
     * Armenian Dram: Armenia
     * 
     */
    AMD,

    /**
     * Antillian Guilder: Netherlands Antilles
     * 
     */
    ANG,

    /**
     * Angolan Kwanza: Angola
     * 
     */
    AOA,

    /**
     * New Kwanza: Angola; used until 1995, now AOA
     * 
     */
    AON,

    /**
     * Kwanza Reajustado: Angola; used until 1995, now AON
     * 
     */
    AOR,

    /**
     * Argentine Peso: Argentina
     * 
     */
    ARS,

    /**
     * Schilling: Austria; replaced by Euro in 2002
     * 
     */
    ATS,

    /**
     * Australian Dollar: Australia, Christmas Island, Cocos (Keeling) Islands, Heard
     *                         and Mcdonald Islands, Kiribati, Nauru, Norfolk Island, Tuvalu
     *                     
     * 
     */
    AUD,

    /**
     * Aruban Guilder: Aruba
     * 
     */
    AWG,

    /**
     * Azerbaijanian Manat: Azerbaijan; replaced by AZN in 2006
     * 
     */
    AZM,

    /**
     * Azerbaijani Manat: Azerbaijan
     * 
     */
    AZN,

    /**
     * Convertible Marks: Bosnia and Herzegovina
     * 
     */
    BAM,

    /**
     * Barbados Dollar: Barbados
     * 
     */
    BBD,

    /**
     * Taka: Bangladesh
     * 
     */
    BDT,

    /**
     * Belgian Franc: Belgium; replaced by Euro in 2002
     * 
     */
    BEF,

    /**
     * Lev: Bulgaria; replaced by BGN in 1999
     * 
     */
    BGL,

    /**
     * Bulgarian LEV: Bulgaria
     * 
     */
    BGN,

    /**
     * Bahraini Dinar: Bahrain
     * 
     */
    BHD,

    /**
     * Burundi Franc: Burundi
     * 
     */
    BIF,

    /**
     * Bermudian Dollar: Bermuda
     * 
     */
    BMD,

    /**
     * Brunei Dollar: Brunei Darussalam
     * 
     */
    BND,

    /**
     * Bolivian Boliviano: Bolivia
     * 
     */
    BOB,

    /**
     * Bolivian MVDOL: Bolivia (not in circulation)
     * 
     */
    BOV,

    /**
     * Brazilian Real: Brazil
     * 
     */
    BRL,

    /**
     * Bahamian Dollar: Bahamas
     * 
     */
    BSD,

    /**
     * Ngultrum: Bhutan
     * 
     */
    BTN,

    /**
     * Pula: Botswana
     * 
     */
    BWP,

    /**
     * Belarussian Ruble: Belarus
     * 
     */
    BYR,

    /**
     * Belize Dollar: Belize
     * 
     */
    BZD,

    /**
     * Canadian Dollar: Canada
     * 
     */
    CAD,

    /**
     * Franc Congolais: Congo, The Democratic Republic Of
     * 
     */
    CDF,

    /**
     * WIR Euro(complementary currency): Switzerland
     * 
     */
    CHE,

    /**
     * Swiss Franc: Liechtenstein, Switzerland
     * 
     */
    CHF,

    /**
     * WIR Franc (complementary currency): Switzerland
     * 
     */
    CHW,

    /**
     * Unidades de fomento: Chile
     * 
     */
    CLF,

    /**
     * Chilean Peso: Chile
     * 
     */
    CLP,

    /**
     * Yuan Renminbi: China
     * 
     */
    CNY,

    /**
     * Colombian Peso: Colombia
     * 
     */
    COP,

    /**
     * Unidad de Valor Real: Colombia
     * 
     */
    COU,

    /**
     * Costa Rican Colon: Costa Rica
     * 
     */
    CRC,

    /**
     * Cuban Peso: Cuba
     * 
     */
    CUP,

    /**
     * Cuban Convertible Peso: Cuba
     * 
     */
    CUC,

    /**
     * Cape Verde Escudo: Cape Verde
     * 
     */
    CVE,

    /**
     * Cyprus Pound: Cyprus; replaced by Euro in 2007
     * 
     */
    CYP,

    /**
     * Czech Koruna: Czech Republic
     * 
     */
    CZK,

    /**
     * Deutsche Mark: Germany; replaced by Euro in 2002
     * 
     */
    DEM,

    /**
     * Djibouti Franc: Djibouti
     * 
     */
    DJF,

    /**
     * Danish Krone: Denmark, Faeroe Islands, Greenland
     * 
     */
    DKK,

    /**
     * Dominican Peso: Dominican Republic
     * 
     */
    DOP,

    /**
     * Algerian Dinar: Algeria
     * 
     */
    DZD,

    /**
     * Sucre: Ecuador; ; replaced by US Dollar in 2000
     * 
     */
    ECS,

    /**
     * Unidad de Valor Constante (UVC): Ecuador; not used since 2000
     * 
     */
    ECV,

    /**
     * Kroon: Estonia; replaced by Euro in 2011
     * 
     */
    EEK,

    /**
     * Egyptian Pound: Egypt
     * 
     */
    EGP,

    /**
     * Nakfa: Eritrea
     * 
     */
    ERN,

    /**
     * Spanish Peseta: Andorra, Spain; replaced by Euro in 2002
     * 
     */
    ESP,

    /**
     * Ethiopian Birr: Ethiopia
     * 
     */
    ETB,

    /**
     * Euro:
     * 
     */
    EUR,

    /**
     * Markka: Finland; replaced by Euro in 2002
     * 
     */
    FIM,

    /**
     * Fiji Dollar: Fiji
     * 
     */
    FJD,

    /**
     * Pound: Falkland Islands (Malvinas)
     * 
     */
    FKP,

    /**
     * French Franc: Andorra, France, French Guiana, French Southern Territories,
     *                         Guadeloupe, Martinique, Monaco, Reunion, St Pierre and Miquelon; replaced by Euro in 1999
     * 
     */
    FRF,

    /**
     * Pound Sterling: United Kingdom
     * 
     */
    GBP,

    /**
     * Lari: Georgia
     * 
     */
    GEL,

    /**
     * Cedi: Ghana; replaced in 2007 by Ghanaian New Cedi GHS
     * 
     */
    GHC,

    /**
     * Cedi: Ghana
     * 
     */
    GHS,

    /**
     * Gibraltar Pound: Gibraltar
     * 
     */
    GIP,

    /**
     * Dalasi: Gambia
     * 
     */
    GMD,

    /**
     * Guinea Franc: Guinea
     * 
     */
    GNF,

    /**
     * Drachma: Greece; replaced by Euro in 2002
     * 
     */
    GRD,

    /**
     * Quetzal: Guatemala
     * 
     */
    GTQ,

    /**
     * Guinea-Bissau Peso: Guinea-Bissau; replaced by CFA franc in 1997
     * 
     */
    GWP,

    /**
     * Guyana Dollar: Guyana
     * 
     */
    GYD,

    /**
     * Hong Kong Dollar: Hong Kong
     * 
     */
    HKD,

    /**
     * Lempira: Honduras
     * 
     */
    HNL,

    /**
     * Kuna: Croatia
     * 
     */
    HRK,

    /**
     * Gourde: Haiti
     * 
     */
    HTG,

    /**
     * Forint: Hungary
     * 
     */
    HUF,

    /**
     * Rupiah: East Timor, Indonesia
     * 
     */
    IDR,

    /**
     * Irish Pound: Ireland; replaced by Euro in 2009
     * 
     */
    IEP,

    /**
     * New Israeli Sheqel: Israel
     * 
     */
    ILS,

    /**
     * Indian Rupee: Bhutan, India
     * 
     */
    INR,

    /**
     * Iraqi Dinar: Iraq
     * 
     */
    IQD,

    /**
     * Iranian Rial: Iran (Islamic Republic Of)
     * 
     */
    IRR,

    /**
     * Iceland Krona: Iceland
     * 
     */
    ISK,

    /**
     * Italian Lira: Italy, San Marino, Vatican City State (Holy See); replaced by Euro in 1999
     *                     
     * 
     */
    ITL,

    /**
     * Jamaican Dollar: Jamaica
     * 
     */
    JMD,

    /**
     * Jordanian Dinar: Jordan
     * 
     */
    JOD,

    /**
     * Yen: Japan
     * 
     */
    JPY,

    /**
     * Kenyan Shilling: Kenya
     * 
     */
    KES,

    /**
     * Som: Kyrgyzstan
     * 
     */
    KGS,

    /**
     * Riel: Cambodia
     * 
     */
    KHR,

    /**
     * Comoro Franc: Comoros
     * 
     */
    KMF,

    /**
     * North Korean Won: Korea, Democratic People's Republic Of
     * 
     */
    KPW,

    /**
     * Won: Korea, Republic Of
     * 
     */
    KRW,

    /**
     * Kuwaiti Dinar: Kuwait
     * 
     */
    KWD,

    /**
     * Cayman Islands Dollar: Cayman Islands
     * 
     */
    KYD,

    /**
     * Tenge: Kazakhstan
     * 
     */
    KZT,

    /**
     * Kip: Lao People's Democratic Republic
     * 
     */
    LAK,

    /**
     * Lebanese Pound: Lebanon
     * 
     */
    LBP,

    /**
     * Sri Lanka Rupee: Sri Lanka
     * 
     */
    LKR,

    /**
     * Liberian Dollar: Liberia
     * 
     */
    LRD,

    /**
     * Loti: Lesotho
     * 
     */
    LSL,

    /**
     * Lithuanian Litas: Lithuania; replaced by Euro in 2015
     * 
     */
    LTL,

    /**
     * Luxembourg Franc: Luxembourg; replaced by Euro in 2002
     * 
     */
    LUF,

    /**
     * Latvian Lats: Latvia; replaced by Euro in 2014
     * 
     */
    LVL,

    /**
     * Libyan Dinar: Libyan Arab Jamahiriya
     * 
     */
    LYD,

    /**
     * Moroccan Dirham: Morocco, Western Sahara
     * 
     */
    MAD,

    /**
     * Moldovan Leu: Moldova, Republic Of
     * 
     */
    MDL,

    /**
     * Malagasy Ariary: Madagascar
     * 
     */
    MGA,

    /**
     * Malagasy Franc: Madagascar; replaced by Ariary in 2005
     * 
     */
    MGF,

    /**
     * Denar: Macedonia, The Former Yugoslav Republic Of
     * 
     */
    MKD,

    /**
     * Kyat: Myanmar
     * 
     */
    MMK,

    /**
     * Tugrik: Mongolia
     * 
     */
    MNT,

    /**
     * Pataca: Macau
     * 
     */
    MOP,

    /**
     * Ouguiya: Mauritania
     * 
     */
    MRO,

    /**
     * Maltese Lira: Malta; replaced by Euro in 2008
     * 
     */
    MTL,

    /**
     * Mauritius Rupee: Mauritius
     * 
     */
    MUR,

    /**
     * Rufiyaa: Maldives
     * 
     */
    MVR,

    /**
     * Kwacha: Malawi
     * 
     */
    MWK,

    /**
     * Mexican Peso: Mexico
     * 
     */
    MXN,

    /**
     * Mexican Unidad de Inversion (UDI): Mexico
     * 
     */
    MXV,

    /**
     * Malaysian Ringgit: Malaysia
     * 
     */
    MYR,

    /**
     * Metical: Mozambique; replaced by MZN in 2012
     * 
     */
    MZM,

    /**
     * Metical: Mozambique
     * 
     */
    MZN,

    /**
     * Namibia Dollar: Namibia
     * 
     */
    NAD,

    /**
     * Naira: Nigeria
     * 
     */
    NGN,

    /**
     * Cordoba Oro: Nicaragua
     * 
     */
    NIO,

    /**
     * Netherlands Guilder: Netherlands; replaced by Euro in 2002
     * 
     */
    NLG,

    /**
     * Norwegian Krone: Bouvet Island, Norway, Svalbard and Jan Mayen Islands
     *                     
     * 
     */
    NOK,

    /**
     * Nepalese Rupee: Nepal
     * 
     */
    NPR,

    /**
     * New Zealand Dollar: Cook Islands, New Zealand, Niue, Pitcairn, Tokelau
     *                     
     * 
     */
    NZD,

    /**
     * Rial Omani: Oman
     * 
     */
    OMR,

    /**
     * Balboa: Panama
     * 
     */
    PAB,

    /**
     * Nuevo Sol: Peru
     * 
     */
    PEN,

    /**
     * Kina: Papua New Guinea
     * 
     */
    PGK,

    /**
     * Philippine Peso: Philippines
     * 
     */
    PHP,

    /**
     * Pakistan Rupee: Pakistan
     * 
     */
    PKR,

    /**
     * Zloty: Poland
     * 
     */
    PLN,

    /**
     * Portuguese Escudo: Portugal; replaced by Euro in 2002
     * 
     */
    PTE,

    /**
     * Guarani: Paraguay
     * 
     */
    PYG,

    /**
     * Qatari Rial: Qatar
     * 
     */
    QAR,

    /**
     * Leu: Romania; replaced by RON in 2005
     * 
     */
    ROL,

    /**
     * Leu: Romania
     * 
     */
    RON,

    /**
     * Serbian Dinar: Serbia
     * 
     */
    RSD,

    /**
     * Russian Ruble: Russian Federation
     * 
     */
    RUB,

    /**
     * Russian Ruble: Russian Federation; replaced by RUB in 1998, but the code is still in use
     * 
     */
    RUR,

    /**
     * Rwanda Franc: Rwanda
     * 
     */
    RWF,

    /**
     * Saudi Riyal: Saudi Arabia
     * 
     */
    SAR,

    /**
     * Solomon Islands Dollar: Solomon Islands
     * 
     */
    SBD,

    /**
     * Seychelles Rupee: Seychelles
     * 
     */
    SCR,

    /**
     * Sudanese Dinar: Sudan; replased by Sudanese Pound (SDG) in 2007
     * 
     */
    SDD,

    /**
     * Sudanese Pound: Sudan
     * 
     */
    SDG,

    /**
     * Swedish Krona: Sweden
     * 
     */
    SEK,

    /**
     * Singapore Dollar: Singapore
     * 
     */
    SGD,

    /**
     * St Helena Pound: St Helena
     * 
     */
    SHP,

    /**
     * Tolar: Slovenia; replaced by Euro in 2007
     * 
     */
    SIT,

    /**
     * Slovak Koruna: Slovakia; replaced by Euro in 2009
     * 
     */
    SKK,

    /**
     * Leone: Sierra Leone
     * 
     */
    SLL,

    /**
     * Somali Shilling: Somalia
     * 
     */
    SOS,

    /**
     * Surinam Dollar: Suriname
     * 
     */
    SRD,

    /**
     * Surinam Guilder: Suriname; replaced by Suriname Guilder in 2004
     * 
     */
    SRG,

    /**
     * South Sudanese Pound; South Sudan
     * 
     */
    SSP,

    /**
     * Dobra: Sao Tome and Principe
     * 
     */
    STD,

    /**
     * El Salvador Colon: El Salvador
     * 
     */
    SVC,

    /**
     * Syrian Pound: Syrian Arab Republic
     * 
     */
    SYP,

    /**
     * Lilangeni: Swaziland
     * 
     */
    SZL,

    /**
     * Baht: Thailand
     * 
     */
    THB,

    /**
     * Tajik Ruble (old): Tajikistan (Old); replaced by Somoni (TJS) in 2000
     * 
     */
    TJR,

    /**
     * Somoni: Tajikistan
     * 
     */
    TJS,

    /**
     * Manat: Turkmenistan; replaced by TMT in 2009
     * 
     */
    TMM,

    /**
     * Manat: Turkmenistan
     * 
     */
    TMT,

    /**
     * Tunisian Dinar: Tunisia
     * 
     */
    TND,

    /**
     * Pa'anga: Tonga
     * 
     */
    TOP,

    /**
     * Timor Escudo: East Timor; replaced by USD
     * 
     */
    TPE,

    /**
     * Turkish Lira: Turkey; replaced by TRY in 2005
     * 
     */
    TRL,

    /**
     * Turkish Lira: Turkey
     * 
     */
    TRY,

    /**
     * Trinidad and Tobago Dollar: Trinidad and Tobago
     * 
     */
    TTD,

    /**
     * New Taiwan Dollar: Taiwan, Province Of China
     * 
     */
    TWD,

    /**
     * Tanzanian Shilling: Tanzania, United Republic Of
     * 
     */
    TZS,

    /**
     * Hryvnia: Ukraine
     * 
     */
    UAH,

    /**
     * Uganda Shilling: Uganda
     * 
     */
    UGX,

    /**
     * US Dollar: American Samoa, British Indian Ocean Territory,, Guam, Haiti, Marshall
     *                         Islands, Micronesia, Northern Mariana Islands, Palau, Panama, Puerto Rico, Turks and Caicos
     *                         Islands, United States, United States Minor Outlaying Islands, Virgin Islands (British), Virgin
     *                         Islands (U.S.)
     *                     
     * 
     */
    USD,

    /**
     * (Next day): United States
     * 
     */
    USN,

    /**
     * (Same day): United States
     * 
     */
    USS,

    /**
     * Uruguay Peso en Unidades Indexadas (URUIURUI) (funds code): Uruguay
     * 
     */
    UYI,

    /**
     * Peso Uruguayo: Uruguay
     * 
     */
    UYU,

    /**
     * Uzbekistan Sum: Uzbekistan
     * 
     */
    UZS,

    /**
     * Bolivar: Venezuela
     * 
     */
    VEB,

    /**
     * Bolivar: Venezuela
     * 
     */
    VEF,

    /**
     * Dong: Vietnam
     * 
     */
    VND,

    /**
     * Vatu: Vanuatu
     * 
     */
    VUV,

    /**
     * Tala: Samoa
     * 
     */
    WST,

    /**
     * CFA Franc BEAC: Cameroon, Central African Republic, Chad, Congo, Equatorial
     *                         Guinea, Gabon
     *                     
     * 
     */
    XAF,

    /**
     * Silver: Bond Markets Units
     * 
     */
    XAG,

    /**
     * Gold Bond Markets Units:
     * 
     */
    XAU,

    /**
     * European Composite Unit (EURCO): Bond Markets Units
     * 
     */
    XBA,

    /**
     * European Monetary Unit (E.M.U.-6): Bond Markets Units
     * 
     */
    XBB,

    /**
     * European Unit of Account 9 (E.U.A.- 9): Bond Markets Units
     * 
     */
    XBC,

    /**
     * European Unit of Account 17 (E.U.A.- 17): Bond Markets Units
     * 
     */
    XBD,

    /**
     * East Caribbean Dollar: Anguilla, Antigua and Barbuda, Dominica, Grenada,
     *                         Montserrat, St Kitts - Nevis, Saint Lucia, Saint Vincent and The Grenadines
     *                     
     * 
     */
    XCD,

    /**
     * SDR: International Monetary Fund (Imf)
     * 
     */
    XDR,

    /**
     * Gold-Franc: Special settlement currencies; replaced by Special Drawing Rights (XDR) in 2003
     * 
     */
    XFO,

    /**
     * UIC-Franc: Special settlement currencies; replaced by EURO in 2013
     * 
     */
    XFU,

    /**
     * CFA Franc BCEAO: Benin, Burkina Faso, Cote D'ivoire, Guinea-Bissau, Mali, Niger,
     *                         Senegal, Togo
     *                     
     * 
     */
    XOF,

    /**
     * Palladium: Bond Markets Units
     * 
     */
    XPD,

    /**
     * CFP Franc: French Polynesia, New Caledonia, Wallis and Futuna Islands
     *                     
     * 
     */
    XPF,

    /**
     * Platinum: Bond Markets Units
     * 
     */
    XPT,

    /**
     * SUCRE: Unified System for Regional Compensation (SUCRE)[
     * 
     */
    XSU,

    /**
     * Codes specifically reserved for testing purposes:
     * 
     */
    XTS,

    /**
     * ADB Unit of Account: African Development Bank[
     * 
     */
    XUA,

    /**
     * The codes assigned for transactions where no currency is involved are::
     *                     
     * 
     */
    XXX,

    /**
     * Yemeni Rial: Yemen
     * 
     */
    YER,

    /**
     * New Dinar: Yugoslavia; 
     * 
     */
    YUM,

    /**
     * (financial Rand): Lesotho; not used since 1995ZM
     * 
     */
    ZAL,

    /**
     * Rand: Lesotho, Namibia, South Africa
     * 
     */
    ZAR,

    /**
     * Kwacha: Zambia; replaced by ZMW in 2012
     * 
     */
    ZMK,

    /**
     * Kwacha: Zambia
     * 
     */
    ZMW,

    /**
     * New Zaire: Zaire; replaced by Congolese Franc in 1998
     * 
     */
    ZRN,

    /**
     * Zimbabwe Dollar: Zimbabwe; replaced by Zimbabwean Dollar in 2009
     * 
     */
    ZWD,

    /**
     * Zimbabwean Dollar: Zimbabwe
     * 
     */
    ZWL;

    public String value() {
        return name();
    }

    public static CurrencyCodeList fromValue(String v) {
        return valueOf(v);
    }

}
