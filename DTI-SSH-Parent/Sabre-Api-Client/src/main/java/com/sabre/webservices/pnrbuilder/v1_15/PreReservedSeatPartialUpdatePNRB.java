
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PreReservedSeatPartialUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PreReservedSeatPartialUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ActionCodeUpdate">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CurrentActioncode" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString"/>
 *                   &lt;element name="PreviousActionCode" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_15}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_15}OperationType.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PreReservedSeatPartialUpdate.PNRB", propOrder = {
    "actionCodeUpdate"
})
public class PreReservedSeatPartialUpdatePNRB {

    @XmlElement(name = "ActionCodeUpdate", required = true)
    protected PreReservedSeatPartialUpdatePNRB.ActionCodeUpdate actionCodeUpdate;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;

    /**
     * Gets the value of the actionCodeUpdate property.
     * 
     * @return
     *     possible object is
     *     {@link PreReservedSeatPartialUpdatePNRB.ActionCodeUpdate }
     *     
     */
    public PreReservedSeatPartialUpdatePNRB.ActionCodeUpdate getActionCodeUpdate() {
        return actionCodeUpdate;
    }

    /**
     * Sets the value of the actionCodeUpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreReservedSeatPartialUpdatePNRB.ActionCodeUpdate }
     *     
     */
    public void setActionCodeUpdate(PreReservedSeatPartialUpdatePNRB.ActionCodeUpdate value) {
        this.actionCodeUpdate = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CurrentActioncode" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString"/>
     *         &lt;element name="PreviousActionCode" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "currentActioncode",
        "previousActionCode"
    })
    public static class ActionCodeUpdate {

        @XmlElement(name = "CurrentActioncode", required = true)
        protected String currentActioncode;
        @XmlElement(name = "PreviousActionCode", required = true)
        protected String previousActionCode;

        /**
         * Gets the value of the currentActioncode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrentActioncode() {
            return currentActioncode;
        }

        /**
         * Sets the value of the currentActioncode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrentActioncode(String value) {
            this.currentActioncode = value;
        }

        /**
         * Gets the value of the previousActionCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPreviousActionCode() {
            return previousActionCode;
        }

        /**
         * Sets the value of the previousActionCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPreviousActionCode(String value) {
            this.previousActionCode = value;
        }

    }

}
