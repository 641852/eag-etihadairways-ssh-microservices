
package com.sabre.webservices.pnrbuilder.v1_15;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ItineraryPricing.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItineraryPricing.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FuturePriceInfo" type="{http://webservices.sabre.com/pnrbuilder/v1_15}FuturePriceInfo.PNRB" maxOccurs="99" minOccurs="0"/>
 *         &lt;element name="PricedItinerary" type="{http://webservices.sabre.com/pnrbuilder/v1_15}PricedItinerary.PNRB" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="numberInParty" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Numeric0to99999" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItineraryPricing.PNRB", propOrder = {
    "futurePriceInfo",
    "pricedItinerary"
})
public class ItineraryPricingPNRB {

    @XmlElement(name = "FuturePriceInfo")
    protected List<FuturePriceInfoPNRB> futurePriceInfo;
    @XmlElement(name = "PricedItinerary")
    protected List<PricedItineraryPNRB> pricedItinerary;
    @XmlAttribute(name = "numberInParty")
    protected Integer numberInParty;

    /**
     * Gets the value of the futurePriceInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the futurePriceInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFuturePriceInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FuturePriceInfoPNRB }
     * 
     * 
     */
    public List<FuturePriceInfoPNRB> getFuturePriceInfo() {
        if (futurePriceInfo == null) {
            futurePriceInfo = new ArrayList<FuturePriceInfoPNRB>();
        }
        return this.futurePriceInfo;
    }

    /**
     * Gets the value of the pricedItinerary property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pricedItinerary property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPricedItinerary().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PricedItineraryPNRB }
     * 
     * 
     */
    public List<PricedItineraryPNRB> getPricedItinerary() {
        if (pricedItinerary == null) {
            pricedItinerary = new ArrayList<PricedItineraryPNRB>();
        }
        return this.pricedItinerary;
    }

    /**
     * Gets the value of the numberInParty property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberInParty() {
        return numberInParty;
    }

    /**
     * Sets the value of the numberInParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberInParty(Integer value) {
        this.numberInParty = value;
    }

}
