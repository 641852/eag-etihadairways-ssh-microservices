
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScoreType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ScoreType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="TDW"/>
 *     &lt;enumeration value="AL"/>
 *     &lt;enumeration value="MTO"/>
 *     &lt;enumeration value="CE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ScoreType")
@XmlEnum
public enum ScoreType {

    TDW,
    AL,
    MTO,
    CE;

    public String value() {
        return name();
    }

    public static ScoreType fromValue(String v) {
        return valueOf(v);
    }

}
