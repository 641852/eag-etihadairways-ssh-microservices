
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PreviousCriteriaType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PreviousCriteriaType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="LastRecordLocator" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PreviousCriteriaType")
public class PreviousCriteriaType {

    @XmlAttribute(name = "LastRecordLocator", required = true)
    protected String lastRecordLocator;

    /**
     * Gets the value of the lastRecordLocator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastRecordLocator() {
        return lastRecordLocator;
    }

    /**
     * Sets the value of the lastRecordLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastRecordLocator(String value) {
        this.lastRecordLocator = value;
    }

}
