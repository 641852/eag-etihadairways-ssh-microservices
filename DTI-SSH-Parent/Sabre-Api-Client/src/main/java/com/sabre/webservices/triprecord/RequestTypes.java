
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RequestTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RequestTypes">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="Both"/>
 *     &lt;enumeration value="Current"/>
 *     &lt;enumeration value="Past"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RequestTypes")
@XmlEnum
public enum RequestTypes {

    @XmlEnumValue("Both")
    BOTH("Both"),
    @XmlEnumValue("Current")
    CURRENT("Current"),
    @XmlEnumValue("Past")
    PAST("Past");
    private final String value;

    RequestTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RequestTypes fromValue(String v) {
        for (RequestTypes c: RequestTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
