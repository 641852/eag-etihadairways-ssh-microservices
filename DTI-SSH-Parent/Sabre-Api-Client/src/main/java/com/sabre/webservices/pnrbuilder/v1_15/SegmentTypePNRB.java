
package com.sabre.webservices.pnrbuilder.v1_15;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.sabre.services.res.or.v1_8.ProductType;


/**
 * <p>Java class for SegmentType.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SegmentType.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Poc" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Poc" minOccurs="0"/>
 *         &lt;element name="Air" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://webservices.sabre.com/pnrbuilder/v1_15}AirType">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Vehicle" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Vehicle" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Product" type="{http://services.sabre.com/res/or/v1_8}ProductType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Hotel" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Hotel" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Open" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Open" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Arunk" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Arunk" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="General" type="{http://webservices.sabre.com/pnrbuilder/v1_15}GeneralSegmentType.PNRB" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Segment" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;choice>
 *                     &lt;element name="Air" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;extension base="{http://webservices.sabre.com/pnrbuilder/v1_15}AirType">
 *                           &lt;/extension>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="Vehicle" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Vehicle" minOccurs="0"/>
 *                     &lt;element name="Hotel" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Hotel" minOccurs="0"/>
 *                     &lt;element name="Open" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Open" minOccurs="0"/>
 *                     &lt;element name="Arunk" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Arunk" minOccurs="0"/>
 *                     &lt;element name="General" type="{http://webservices.sabre.com/pnrbuilder/v1_15}GeneralSegmentType.PNRB" minOccurs="0"/>
 *                   &lt;/choice>
 *                   &lt;element name="Product" type="{http://services.sabre.com/res/or/v1_8}ProductType" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="sequence" type="{http://www.w3.org/2001/XMLSchema}short" />
 *                 &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_15}UniqueIdentifier.PNRB" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SegmentType.PNRB", propOrder = {
    "poc",
    "air",
    "vehicle",
    "product",
    "hotel",
    "open",
    "arunk",
    "general",
    "segment"
})
public class SegmentTypePNRB {

    @XmlElement(name = "Poc")
    protected Poc poc;
    @XmlElement(name = "Air")
    protected List<SegmentTypePNRB.Air> air;
    @XmlElement(name = "Vehicle")
    protected List<Vehicle> vehicle;
    @XmlElement(name = "Product")
    protected List<ProductType> product;
    @XmlElement(name = "Hotel")
    protected List<Hotel> hotel;
    @XmlElement(name = "Open")
    protected List<Open> open;
    @XmlElement(name = "Arunk")
    protected List<Arunk> arunk;
    @XmlElement(name = "General")
    protected List<GeneralSegmentTypePNRB> general;
    @XmlElement(name = "Segment")
    protected List<SegmentTypePNRB.Segment> segment;

    /**
     * Gets the value of the poc property.
     * 
     * @return
     *     possible object is
     *     {@link Poc }
     *     
     */
    public Poc getPoc() {
        return poc;
    }

    /**
     * Sets the value of the poc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Poc }
     *     
     */
    public void setPoc(Poc value) {
        this.poc = value;
    }

    /**
     * Gets the value of the air property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the air property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAir().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SegmentTypePNRB.Air }
     * 
     * 
     */
    public List<SegmentTypePNRB.Air> getAir() {
        if (air == null) {
            air = new ArrayList<SegmentTypePNRB.Air>();
        }
        return this.air;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vehicle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVehicle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Vehicle }
     * 
     * 
     */
    public List<Vehicle> getVehicle() {
        if (vehicle == null) {
            vehicle = new ArrayList<Vehicle>();
        }
        return this.vehicle;
    }

    /**
     * Gets the value of the product property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the product property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProduct().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductType }
     * 
     * 
     */
    public List<ProductType> getProduct() {
        if (product == null) {
            product = new ArrayList<ProductType>();
        }
        return this.product;
    }

    /**
     * Gets the value of the hotel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hotel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHotel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Hotel }
     * 
     * 
     */
    public List<Hotel> getHotel() {
        if (hotel == null) {
            hotel = new ArrayList<Hotel>();
        }
        return this.hotel;
    }

    /**
     * Gets the value of the open property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the open property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOpen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Open }
     * 
     * 
     */
    public List<Open> getOpen() {
        if (open == null) {
            open = new ArrayList<Open>();
        }
        return this.open;
    }

    /**
     * Gets the value of the arunk property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the arunk property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArunk().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Arunk }
     * 
     * 
     */
    public List<Arunk> getArunk() {
        if (arunk == null) {
            arunk = new ArrayList<Arunk>();
        }
        return this.arunk;
    }

    /**
     * Gets the value of the general property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the general property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGeneral().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GeneralSegmentTypePNRB }
     * 
     * 
     */
    public List<GeneralSegmentTypePNRB> getGeneral() {
        if (general == null) {
            general = new ArrayList<GeneralSegmentTypePNRB>();
        }
        return this.general;
    }

    /**
     * Gets the value of the segment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the segment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSegment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SegmentTypePNRB.Segment }
     * 
     * 
     */
    public List<SegmentTypePNRB.Segment> getSegment() {
        if (segment == null) {
            segment = new ArrayList<SegmentTypePNRB.Segment>();
        }
        return this.segment;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://webservices.sabre.com/pnrbuilder/v1_15}AirType">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Air
        extends AirType
    {


    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;choice>
     *           &lt;element name="Air" minOccurs="0">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;extension base="{http://webservices.sabre.com/pnrbuilder/v1_15}AirType">
     *                 &lt;/extension>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *           &lt;element name="Vehicle" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Vehicle" minOccurs="0"/>
     *           &lt;element name="Hotel" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Hotel" minOccurs="0"/>
     *           &lt;element name="Open" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Open" minOccurs="0"/>
     *           &lt;element name="Arunk" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Arunk" minOccurs="0"/>
     *           &lt;element name="General" type="{http://webservices.sabre.com/pnrbuilder/v1_15}GeneralSegmentType.PNRB" minOccurs="0"/>
     *         &lt;/choice>
     *         &lt;element name="Product" type="{http://services.sabre.com/res/or/v1_8}ProductType" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="sequence" type="{http://www.w3.org/2001/XMLSchema}short" />
     *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_15}UniqueIdentifier.PNRB" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "air",
        "vehicle",
        "hotel",
        "open",
        "arunk",
        "general",
        "product"
    })
    public static class Segment {

        @XmlElement(name = "Air")
        protected SegmentTypePNRB.Segment.Air air;
        @XmlElement(name = "Vehicle")
        protected Vehicle vehicle;
        @XmlElement(name = "Hotel")
        protected Hotel hotel;
        @XmlElement(name = "Open")
        protected Open open;
        @XmlElement(name = "Arunk")
        protected Arunk arunk;
        @XmlElement(name = "General")
        protected GeneralSegmentTypePNRB general;
        @XmlElement(name = "Product")
        protected ProductType product;
        @XmlAttribute(name = "sequence")
        protected Short sequence;
        @XmlAttribute(name = "id")
        protected String id;

        /**
         * Gets the value of the air property.
         * 
         * @return
         *     possible object is
         *     {@link SegmentTypePNRB.Segment.Air }
         *     
         */
        public SegmentTypePNRB.Segment.Air getAir() {
            return air;
        }

        /**
         * Sets the value of the air property.
         * 
         * @param value
         *     allowed object is
         *     {@link SegmentTypePNRB.Segment.Air }
         *     
         */
        public void setAir(SegmentTypePNRB.Segment.Air value) {
            this.air = value;
        }

        /**
         * Gets the value of the vehicle property.
         * 
         * @return
         *     possible object is
         *     {@link Vehicle }
         *     
         */
        public Vehicle getVehicle() {
            return vehicle;
        }

        /**
         * Sets the value of the vehicle property.
         * 
         * @param value
         *     allowed object is
         *     {@link Vehicle }
         *     
         */
        public void setVehicle(Vehicle value) {
            this.vehicle = value;
        }

        /**
         * Gets the value of the hotel property.
         * 
         * @return
         *     possible object is
         *     {@link Hotel }
         *     
         */
        public Hotel getHotel() {
            return hotel;
        }

        /**
         * Sets the value of the hotel property.
         * 
         * @param value
         *     allowed object is
         *     {@link Hotel }
         *     
         */
        public void setHotel(Hotel value) {
            this.hotel = value;
        }

        /**
         * Gets the value of the open property.
         * 
         * @return
         *     possible object is
         *     {@link Open }
         *     
         */
        public Open getOpen() {
            return open;
        }

        /**
         * Sets the value of the open property.
         * 
         * @param value
         *     allowed object is
         *     {@link Open }
         *     
         */
        public void setOpen(Open value) {
            this.open = value;
        }

        /**
         * Gets the value of the arunk property.
         * 
         * @return
         *     possible object is
         *     {@link Arunk }
         *     
         */
        public Arunk getArunk() {
            return arunk;
        }

        /**
         * Sets the value of the arunk property.
         * 
         * @param value
         *     allowed object is
         *     {@link Arunk }
         *     
         */
        public void setArunk(Arunk value) {
            this.arunk = value;
        }

        /**
         * Gets the value of the general property.
         * 
         * @return
         *     possible object is
         *     {@link GeneralSegmentTypePNRB }
         *     
         */
        public GeneralSegmentTypePNRB getGeneral() {
            return general;
        }

        /**
         * Sets the value of the general property.
         * 
         * @param value
         *     allowed object is
         *     {@link GeneralSegmentTypePNRB }
         *     
         */
        public void setGeneral(GeneralSegmentTypePNRB value) {
            this.general = value;
        }

        /**
         * Gets the value of the product property.
         * 
         * @return
         *     possible object is
         *     {@link ProductType }
         *     
         */
        public ProductType getProduct() {
            return product;
        }

        /**
         * Sets the value of the product property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProductType }
         *     
         */
        public void setProduct(ProductType value) {
            this.product = value;
        }

        /**
         * Gets the value of the sequence property.
         * 
         * @return
         *     possible object is
         *     {@link Short }
         *     
         */
        public Short getSequence() {
            return sequence;
        }

        /**
         * Sets the value of the sequence property.
         * 
         * @param value
         *     allowed object is
         *     {@link Short }
         *     
         */
        public void setSequence(Short value) {
            this.sequence = value;
        }

        /**
         * Gets the value of the id property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getId() {
            return id;
        }

        /**
         * Sets the value of the id property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setId(String value) {
            this.id = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://webservices.sabre.com/pnrbuilder/v1_15}AirType">
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Air
            extends AirType
        {


        }

    }

}
