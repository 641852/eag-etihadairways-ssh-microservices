
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * Used to update confirmation for air segment
 * 
 * <p>Java class for ConfirmationUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConfirmationUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="RecordLocator" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="SegmentAccess" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="AirlineCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConfirmationUpdate.PNRB")
public class ConfirmationUpdatePNRB {

    @XmlAttribute(name = "RecordLocator")
    protected String recordLocator;
    @XmlAttribute(name = "SegmentAccess")
    protected String segmentAccess;
    @XmlAttribute(name = "AirlineCode")
    protected String airlineCode;

    /**
     * Gets the value of the recordLocator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordLocator() {
        return recordLocator;
    }

    /**
     * Sets the value of the recordLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordLocator(String value) {
        this.recordLocator = value;
    }

    /**
     * Gets the value of the segmentAccess property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmentAccess() {
        return segmentAccess;
    }

    /**
     * Sets the value of the segmentAccess property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmentAccess(String value) {
        this.segmentAccess = value;
    }

    /**
     * Gets the value of the airlineCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirlineCode() {
        return airlineCode;
    }

    /**
     * Sets the value of the airlineCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirlineCode(String value) {
        this.airlineCode = value;
    }

}
