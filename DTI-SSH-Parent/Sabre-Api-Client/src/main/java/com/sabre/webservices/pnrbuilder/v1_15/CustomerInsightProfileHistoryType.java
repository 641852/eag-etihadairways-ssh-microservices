
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerInsightProfileHistoryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerInsightProfileHistoryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HistoryAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfileTag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfileID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerInsightProfileHistoryType", propOrder = {
    "historyAction",
    "profileTag",
    "profileID"
})
public class CustomerInsightProfileHistoryType {

    @XmlElement(name = "HistoryAction")
    protected String historyAction;
    @XmlElement(name = "ProfileTag")
    protected String profileTag;
    @XmlElement(name = "ProfileID")
    protected String profileID;

    /**
     * Gets the value of the historyAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHistoryAction() {
        return historyAction;
    }

    /**
     * Sets the value of the historyAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHistoryAction(String value) {
        this.historyAction = value;
    }

    /**
     * Gets the value of the profileTag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileTag() {
        return profileTag;
    }

    /**
     * Sets the value of the profileTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileTag(String value) {
        this.profileTag = value;
    }

    /**
     * Gets the value of the profileID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileID() {
        return profileID;
    }

    /**
     * Sets the value of the profileID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileID(String value) {
        this.profileID = value;
    }

}
