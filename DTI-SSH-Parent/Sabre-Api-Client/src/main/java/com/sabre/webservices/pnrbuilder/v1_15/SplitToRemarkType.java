
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SplitToRemarkType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SplitToRemarkType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservices.sabre.com/pnrbuilder/v1_15}BaseRemarkType">
 *       &lt;sequence>
 *         &lt;element name="OriginalNumberOfPax" type="{http://webservices.sabre.com/pnrbuilder/v1_15}DividePaxNumberType" minOccurs="0"/>
 *         &lt;element name="CurrentNumberOfPax" type="{http://webservices.sabre.com/pnrbuilder/v1_15}DividePaxNumberType" minOccurs="0"/>
 *         &lt;element name="CurrentPassengerNames" type="{http://webservices.sabre.com/pnrbuilder/v1_15}DividePassengerNameType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SplitToRemarkType", propOrder = {
    "originalNumberOfPax",
    "currentNumberOfPax",
    "currentPassengerNames"
})
public class SplitToRemarkType
    extends BaseRemarkType
{

    @XmlElement(name = "OriginalNumberOfPax")
    protected String originalNumberOfPax;
    @XmlElement(name = "CurrentNumberOfPax")
    protected String currentNumberOfPax;
    @XmlElement(name = "CurrentPassengerNames")
    protected String currentPassengerNames;

    /**
     * Gets the value of the originalNumberOfPax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalNumberOfPax() {
        return originalNumberOfPax;
    }

    /**
     * Sets the value of the originalNumberOfPax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalNumberOfPax(String value) {
        this.originalNumberOfPax = value;
    }

    /**
     * Gets the value of the currentNumberOfPax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentNumberOfPax() {
        return currentNumberOfPax;
    }

    /**
     * Sets the value of the currentNumberOfPax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentNumberOfPax(String value) {
        this.currentNumberOfPax = value;
    }

    /**
     * Gets the value of the currentPassengerNames property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentPassengerNames() {
        return currentPassengerNames;
    }

    /**
     * Sets the value of the currentPassengerNames property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentPassengerNames(String value) {
        this.currentPassengerNames = value;
    }

}
