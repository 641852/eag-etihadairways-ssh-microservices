
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ODFareTypeIndicatorsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ODFareTypeIndicatorsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="regular" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="sponsor" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="default" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="sponsorDefault" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="hostDefault" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ODFareTypeIndicatorsType")
public class ODFareTypeIndicatorsType {

    @XmlAttribute(name = "regular")
    protected Boolean regular;
    @XmlAttribute(name = "sponsor")
    protected Boolean sponsor;
    @XmlAttribute(name = "default")
    protected Boolean _default;
    @XmlAttribute(name = "sponsorDefault")
    protected Boolean sponsorDefault;
    @XmlAttribute(name = "hostDefault")
    protected Boolean hostDefault;

    /**
     * Gets the value of the regular property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isRegular() {
        if (regular == null) {
            return false;
        } else {
            return regular;
        }
    }

    /**
     * Sets the value of the regular property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRegular(Boolean value) {
        this.regular = value;
    }

    /**
     * Gets the value of the sponsor property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isSponsor() {
        if (sponsor == null) {
            return false;
        } else {
            return sponsor;
        }
    }

    /**
     * Sets the value of the sponsor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSponsor(Boolean value) {
        this.sponsor = value;
    }

    /**
     * Gets the value of the default property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isDefault() {
        if (_default == null) {
            return false;
        } else {
            return _default;
        }
    }

    /**
     * Sets the value of the default property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefault(Boolean value) {
        this._default = value;
    }

    /**
     * Gets the value of the sponsorDefault property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isSponsorDefault() {
        if (sponsorDefault == null) {
            return false;
        } else {
            return sponsorDefault;
        }
    }

    /**
     * Sets the value of the sponsorDefault property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSponsorDefault(Boolean value) {
        this.sponsorDefault = value;
    }

    /**
     * Gets the value of the hostDefault property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isHostDefault() {
        if (hostDefault == null) {
            return false;
        } else {
            return hostDefault;
        }
    }

    /**
     * Sets the value of the hostDefault property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHostDefault(Boolean value) {
        this.hostDefault = value;
    }

}
