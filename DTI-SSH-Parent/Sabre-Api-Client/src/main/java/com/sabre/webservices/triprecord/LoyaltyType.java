
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoyaltyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LoyaltyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Supplier" type="{http://webservices.sabre.com/triprecord}SupplierType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{http://webservices.sabre.com/triprecord}UpdateAttributes"/>
 *       &lt;attribute name="MembershipID" type="{http://webservices.sabre.com/triprecord}string1to32" />
 *       &lt;attribute name="ProgramType" type="{http://webservices.sabre.com/triprecord}string1to10" />
 *       &lt;attribute name="ProfileStoredItem" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoyaltyType", propOrder = {
    "supplier"
})
public class LoyaltyType {

    @XmlElement(name = "Supplier")
    protected SupplierType supplier;
    @XmlAttribute(name = "MembershipID")
    protected String membershipID;
    @XmlAttribute(name = "ProgramType")
    protected String programType;
    @XmlAttribute(name = "ProfileStoredItem")
    protected Boolean profileStoredItem;
    @XmlAttribute(name = "SequenceNumber")
    protected Integer sequenceNumber;
    @XmlAttribute(name = "Action")
    protected UpdateActions action;

    /**
     * Gets the value of the supplier property.
     * 
     * @return
     *     possible object is
     *     {@link SupplierType }
     *     
     */
    public SupplierType getSupplier() {
        return supplier;
    }

    /**
     * Sets the value of the supplier property.
     * 
     * @param value
     *     allowed object is
     *     {@link SupplierType }
     *     
     */
    public void setSupplier(SupplierType value) {
        this.supplier = value;
    }

    /**
     * Gets the value of the membershipID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMembershipID() {
        return membershipID;
    }

    /**
     * Sets the value of the membershipID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMembershipID(String value) {
        this.membershipID = value;
    }

    /**
     * Gets the value of the programType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramType() {
        return programType;
    }

    /**
     * Sets the value of the programType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramType(String value) {
        this.programType = value;
    }

    /**
     * Gets the value of the profileStoredItem property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProfileStoredItem() {
        if (profileStoredItem == null) {
            return false;
        } else {
            return profileStoredItem;
        }
    }

    /**
     * Sets the value of the profileStoredItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProfileStoredItem(Boolean value) {
        this.profileStoredItem = value;
    }

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSequenceNumber(Integer value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateActions }
     *     
     */
    public UpdateActions getAction() {
        if (action == null) {
            return UpdateActions.NONE;
        } else {
            return action;
        }
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateActions }
     *     
     */
    public void setAction(UpdateActions value) {
        this.action = value;
    }

}
