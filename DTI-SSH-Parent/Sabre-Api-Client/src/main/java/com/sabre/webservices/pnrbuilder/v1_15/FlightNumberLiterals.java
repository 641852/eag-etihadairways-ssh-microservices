
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for _flightNumberLiterals.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="_flightNumberLiterals">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OPEN"/>
 *     &lt;enumeration value="ARNK"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "_flightNumberLiterals")
@XmlEnum
public enum FlightNumberLiterals {

    OPEN,
    ARNK;

    public String value() {
        return name();
    }

    public static FlightNumberLiterals fromValue(String v) {
        return valueOf(v);
    }

}
