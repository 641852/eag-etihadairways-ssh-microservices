
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DKNumberHistoryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DKNumberHistoryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HistoryAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DKNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DKNumberHistoryType", propOrder = {
    "historyAction",
    "dkNumber"
})
public class DKNumberHistoryType {

    @XmlElement(name = "HistoryAction")
    protected String historyAction;
    @XmlElement(name = "DKNumber")
    protected String dkNumber;

    /**
     * Gets the value of the historyAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHistoryAction() {
        return historyAction;
    }

    /**
     * Sets the value of the historyAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHistoryAction(String value) {
        this.historyAction = value;
    }

    /**
     * Gets the value of the dkNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDKNumber() {
        return dkNumber;
    }

    /**
     * Sets the value of the dkNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDKNumber(String value) {
        this.dkNumber = value;
    }

}
