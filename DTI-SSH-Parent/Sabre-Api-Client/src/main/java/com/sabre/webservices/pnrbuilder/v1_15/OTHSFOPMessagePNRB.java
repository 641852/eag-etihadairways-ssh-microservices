
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 *  3 command process:
 * 
 *                 #1 - Send form of payment
 * 
 *                 format 3OTHS(opt seg #)/(credit card code)/(credit card
 *                 #)EXP (2 # exp month) (last 2 #'s exp year)
 *                 3OTHS1/AX11428755558239001EXP 12 07
 * 
 *                 #2 - send cardholder name as shown on the card
 * 
 *                 format 3OTHS(opt seg #)/CH (card holder name) 3OTHS/CH
 *                 ALLAN JACKSON
 * 
 *                 #3 - send free text remarks that you have read contract
 *                 of carriage information to passenger
 * 
 *                 format 3OTHS(opt seg #)/(free text) 3OTHS/PAX AGREES TO
 *                 OPERATOR PARTICIPANT CONTRACT
 *             
 * 
 * <p>Java class for OTHSFOPMessage.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OTHSFOPMessage.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreditCardCode" type="{http://webservices.sabre.com/pnrbuilder/v1_15}PaymentCardCode" minOccurs="0"/>
 *         &lt;element name="CreditCardNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_15}PaymentCardNumber" minOccurs="0"/>
 *         &lt;element name="ExpirationMonth" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="ExpirationYear" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="CardHolderName" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="Comment" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_15}OperationType.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OTHSFOPMessage.PNRB", propOrder = {
    "creditCardCode",
    "creditCardNumber",
    "expirationMonth",
    "expirationYear",
    "cardHolderName",
    "comment"
})
public class OTHSFOPMessagePNRB {

    @XmlElement(name = "CreditCardCode")
    protected String creditCardCode;
    @XmlElement(name = "CreditCardNumber")
    protected String creditCardNumber;
    @XmlElement(name = "ExpirationMonth")
    protected String expirationMonth;
    @XmlElement(name = "ExpirationYear")
    protected String expirationYear;
    @XmlElement(name = "CardHolderName")
    protected String cardHolderName;
    @XmlElement(name = "Comment")
    protected String comment;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;

    /**
     * Gets the value of the creditCardCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCardCode() {
        return creditCardCode;
    }

    /**
     * Sets the value of the creditCardCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCardCode(String value) {
        this.creditCardCode = value;
    }

    /**
     * Gets the value of the creditCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    /**
     * Sets the value of the creditCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCardNumber(String value) {
        this.creditCardNumber = value;
    }

    /**
     * Gets the value of the expirationMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpirationMonth() {
        return expirationMonth;
    }

    /**
     * Sets the value of the expirationMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpirationMonth(String value) {
        this.expirationMonth = value;
    }

    /**
     * Gets the value of the expirationYear property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpirationYear() {
        return expirationYear;
    }

    /**
     * Sets the value of the expirationYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpirationYear(String value) {
        this.expirationYear = value;
    }

    /**
     * Gets the value of the cardHolderName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardHolderName() {
        return cardHolderName;
    }

    /**
     * Sets the value of the cardHolderName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardHolderName(String value) {
        this.cardHolderName = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }

}
