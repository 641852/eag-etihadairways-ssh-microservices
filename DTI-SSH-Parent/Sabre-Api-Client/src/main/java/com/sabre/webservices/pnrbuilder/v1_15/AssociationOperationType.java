
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssociationOperationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AssociationOperationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ADD"/>
 *     &lt;enumeration value="UPDATE"/>
 *     &lt;enumeration value="DELETE"/>
 *     &lt;enumeration value="KEEP"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AssociationOperationType")
@XmlEnum
public enum AssociationOperationType {

    ADD,
    UPDATE,
    DELETE,
    KEEP;

    public String value() {
        return name();
    }

    public static AssociationOperationType fromValue(String v) {
        return valueOf(v);
    }

}
