
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssociationChildReferenceType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AssociationChildReferenceType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Index"/>
 *     &lt;enumeration value="UkyId"/>
 *     &lt;enumeration value="New"/>
 *     &lt;enumeration value="Type"/>
 *     &lt;enumeration value="Static"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AssociationChildReferenceType")
@XmlEnum
public enum AssociationChildReferenceType {

    @XmlEnumValue("Index")
    INDEX("Index"),
    @XmlEnumValue("UkyId")
    UKY_ID("UkyId"),
    @XmlEnumValue("New")
    NEW("New"),
    @XmlEnumValue("Type")
    TYPE("Type"),
    @XmlEnumValue("Static")
    STATIC("Static");
    private final String value;

    AssociationChildReferenceType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AssociationChildReferenceType fromValue(String v) {
        for (AssociationChildReferenceType c: AssociationChildReferenceType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
