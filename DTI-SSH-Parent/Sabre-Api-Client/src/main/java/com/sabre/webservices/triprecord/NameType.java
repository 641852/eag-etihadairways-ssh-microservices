
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NameType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="NameType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ADULT"/>
 *     &lt;enumeration value="CORP"/>
 *     &lt;enumeration value="ZNAME"/>
 *     &lt;enumeration value="GROUP"/>
 *     &lt;enumeration value="INFANT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "NameType")
@XmlEnum
public enum NameType {

    ADULT,
    CORP,
    ZNAME,
    GROUP,
    INFANT;

    public String value() {
        return name();
    }

    public static NameType fromValue(String v) {
        return valueOf(v);
    }

}
