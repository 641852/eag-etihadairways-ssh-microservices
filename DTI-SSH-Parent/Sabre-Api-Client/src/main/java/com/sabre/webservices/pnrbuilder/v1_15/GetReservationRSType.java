
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for GetReservationRSType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetReservationRSType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Warnings" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Warnings.PNRB" minOccurs="0"/>
 *         &lt;element name="Errors" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Errors.PNRB" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="Reservation" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Reservation.PNRB" minOccurs="0"/>
 *           &lt;element name="Content" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;choice>
 *                     &lt;any processContents='lax'/>
 *                   &lt;/choice>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *         &lt;element name="VCR" type="{http://webservices.sabre.com/pnrbuilder/v1_15}VCR" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Version" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetReservationRSType", propOrder = {
    "warnings",
    "errors",
    "reservation",
    "content",
    "vcr"
})
@XmlSeeAlso({
    GetReservationRS.class
})
public class GetReservationRSType {

    @XmlElement(name = "Warnings")
    protected WarningsPNRB warnings;
    @XmlElement(name = "Errors")
    protected ErrorsPNRB errors;
    @XmlElement(name = "Reservation")
    protected ReservationPNRB reservation;
    @XmlElement(name = "Content")
    protected GetReservationRSType.Content content;
    @XmlElement(name = "VCR")
    protected VCR vcr;
    @XmlAttribute(name = "Version")
    protected String version;

    /**
     * Gets the value of the warnings property.
     * 
     * @return
     *     possible object is
     *     {@link WarningsPNRB }
     *     
     */
    public WarningsPNRB getWarnings() {
        return warnings;
    }

    /**
     * Sets the value of the warnings property.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsPNRB }
     *     
     */
    public void setWarnings(WarningsPNRB value) {
        this.warnings = value;
    }

    /**
     * Gets the value of the errors property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsPNRB }
     *     
     */
    public ErrorsPNRB getErrors() {
        return errors;
    }

    /**
     * Sets the value of the errors property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsPNRB }
     *     
     */
    public void setErrors(ErrorsPNRB value) {
        this.errors = value;
    }

    /**
     * Gets the value of the reservation property.
     * 
     * @return
     *     possible object is
     *     {@link ReservationPNRB }
     *     
     */
    public ReservationPNRB getReservation() {
        return reservation;
    }

    /**
     * Sets the value of the reservation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReservationPNRB }
     *     
     */
    public void setReservation(ReservationPNRB value) {
        this.reservation = value;
    }

    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link GetReservationRSType.Content }
     *     
     */
    public GetReservationRSType.Content getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetReservationRSType.Content }
     *     
     */
    public void setContent(GetReservationRSType.Content value) {
        this.content = value;
    }

    /**
     * Gets the value of the vcr property.
     * 
     * @return
     *     possible object is
     *     {@link VCR }
     *     
     */
    public VCR getVCR() {
        return vcr;
    }

    /**
     * Sets the value of the vcr property.
     * 
     * @param value
     *     allowed object is
     *     {@link VCR }
     *     
     */
    public void setVCR(VCR value) {
        this.vcr = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;any processContents='lax'/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "any"
    })
    public static class Content {

        @XmlAnyElement(lax = true)
        protected Object any;

        /**
         * Gets the value of the any property.
         * 
         * @return
         *     possible object is
         *     {@link Element }
         *     {@link Object }
         *     
         */
        public Object getAny() {
            return any;
        }

        /**
         * Sets the value of the any property.
         * 
         * @param value
         *     allowed object is
         *     {@link Element }
         *     {@link Object }
         *     
         */
        public void setAny(Object value) {
            this.any = value;
        }

    }

}
