
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FrequentFlyerTSAPreCheckUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FrequentFlyerTSAPreCheckUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NameAssociationList" type="{http://webservices.sabre.com/pnrbuilder/v1_15}NameAssociationList.PNRB" minOccurs="0"/>
 *         &lt;element name="SegmentAssociationList" type="{http://webservices.sabre.com/pnrbuilder/v1_15}SegmentAssociationList.PNRB" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="Loyalty" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Loyalty.PNRB" minOccurs="0"/>
 *           &lt;element name="TSAPreCheck" type="{http://webservices.sabre.com/pnrbuilder/v1_15}TSAPreCheck.PNRB" minOccurs="0"/>
 *           &lt;element name="PassengerMetaData" type="{http://webservices.sabre.com/pnrbuilder/v1_15}PassengerMetaData.PNRB" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_15}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_15}OperationType.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FrequentFlyerTSAPreCheckUpdate.PNRB", propOrder = {
    "nameAssociationList",
    "segmentAssociationList",
    "loyalty",
    "tsaPreCheck",
    "passengerMetaData"
})
public class FrequentFlyerTSAPreCheckUpdatePNRB {

    @XmlElement(name = "NameAssociationList")
    protected NameAssociationListPNRB nameAssociationList;
    @XmlElement(name = "SegmentAssociationList")
    protected SegmentAssociationListPNRB segmentAssociationList;
    @XmlElement(name = "Loyalty")
    protected LoyaltyPNRB loyalty;
    @XmlElement(name = "TSAPreCheck")
    protected TSAPreCheckPNRB tsaPreCheck;
    @XmlElement(name = "PassengerMetaData")
    protected PassengerMetaDataPNRB passengerMetaData;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;

    /**
     * Gets the value of the nameAssociationList property.
     * 
     * @return
     *     possible object is
     *     {@link NameAssociationListPNRB }
     *     
     */
    public NameAssociationListPNRB getNameAssociationList() {
        return nameAssociationList;
    }

    /**
     * Sets the value of the nameAssociationList property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameAssociationListPNRB }
     *     
     */
    public void setNameAssociationList(NameAssociationListPNRB value) {
        this.nameAssociationList = value;
    }

    /**
     * Gets the value of the segmentAssociationList property.
     * 
     * @return
     *     possible object is
     *     {@link SegmentAssociationListPNRB }
     *     
     */
    public SegmentAssociationListPNRB getSegmentAssociationList() {
        return segmentAssociationList;
    }

    /**
     * Sets the value of the segmentAssociationList property.
     * 
     * @param value
     *     allowed object is
     *     {@link SegmentAssociationListPNRB }
     *     
     */
    public void setSegmentAssociationList(SegmentAssociationListPNRB value) {
        this.segmentAssociationList = value;
    }

    /**
     * Gets the value of the loyalty property.
     * 
     * @return
     *     possible object is
     *     {@link LoyaltyPNRB }
     *     
     */
    public LoyaltyPNRB getLoyalty() {
        return loyalty;
    }

    /**
     * Sets the value of the loyalty property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoyaltyPNRB }
     *     
     */
    public void setLoyalty(LoyaltyPNRB value) {
        this.loyalty = value;
    }

    /**
     * Gets the value of the tsaPreCheck property.
     * 
     * @return
     *     possible object is
     *     {@link TSAPreCheckPNRB }
     *     
     */
    public TSAPreCheckPNRB getTSAPreCheck() {
        return tsaPreCheck;
    }

    /**
     * Sets the value of the tsaPreCheck property.
     * 
     * @param value
     *     allowed object is
     *     {@link TSAPreCheckPNRB }
     *     
     */
    public void setTSAPreCheck(TSAPreCheckPNRB value) {
        this.tsaPreCheck = value;
    }

    /**
     * Gets the value of the passengerMetaData property.
     * 
     * @return
     *     possible object is
     *     {@link PassengerMetaDataPNRB }
     *     
     */
    public PassengerMetaDataPNRB getPassengerMetaData() {
        return passengerMetaData;
    }

    /**
     * Sets the value of the passengerMetaData property.
     * 
     * @param value
     *     allowed object is
     *     {@link PassengerMetaDataPNRB }
     *     
     */
    public void setPassengerMetaData(PassengerMetaDataPNRB value) {
        this.passengerMetaData = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }

}
