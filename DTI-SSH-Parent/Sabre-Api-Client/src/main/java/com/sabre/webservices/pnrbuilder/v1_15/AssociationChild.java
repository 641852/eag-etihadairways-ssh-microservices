
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssociationChild complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AssociationChild">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="RemarkType" type="{http://webservices.sabre.com/pnrbuilder/v1_15}RemarkType.PNRB" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="Reference" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="TempReference" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ReferenceType" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AssociationChildReferenceType" />
 *       &lt;attribute name="Type" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AssociationChildElementType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssociationChild", propOrder = {
    "remarkType"
})
public class AssociationChild {

    @XmlElement(name = "RemarkType")
    protected RemarkTypePNRB remarkType;
    @XmlAttribute(name = "Reference")
    protected String reference;
    @XmlAttribute(name = "TempReference")
    protected String tempReference;
    @XmlAttribute(name = "ReferenceType")
    protected AssociationChildReferenceType referenceType;
    @XmlAttribute(name = "Type")
    protected AssociationChildElementType type;

    /**
     * Gets the value of the remarkType property.
     * 
     * @return
     *     possible object is
     *     {@link RemarkTypePNRB }
     *     
     */
    public RemarkTypePNRB getRemarkType() {
        return remarkType;
    }

    /**
     * Sets the value of the remarkType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RemarkTypePNRB }
     *     
     */
    public void setRemarkType(RemarkTypePNRB value) {
        this.remarkType = value;
    }

    /**
     * Gets the value of the reference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReference() {
        return reference;
    }

    /**
     * Sets the value of the reference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReference(String value) {
        this.reference = value;
    }

    /**
     * Gets the value of the tempReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTempReference() {
        return tempReference;
    }

    /**
     * Sets the value of the tempReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTempReference(String value) {
        this.tempReference = value;
    }

    /**
     * Gets the value of the referenceType property.
     * 
     * @return
     *     possible object is
     *     {@link AssociationChildReferenceType }
     *     
     */
    public AssociationChildReferenceType getReferenceType() {
        return referenceType;
    }

    /**
     * Sets the value of the referenceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssociationChildReferenceType }
     *     
     */
    public void setReferenceType(AssociationChildReferenceType value) {
        this.referenceType = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link AssociationChildElementType }
     *     
     */
    public AssociationChildElementType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssociationChildElementType }
     *     
     */
    public void setType(AssociationChildElementType value) {
        this.type = value;
    }

}
