
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Used to update air segment information
 * 
 * <p>Java class for AirSegmentPartialUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AirSegmentPartialUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="ConfirmationUpdate" type="{http://webservices.sabre.com/pnrbuilder/v1_15}ConfirmationUpdate.PNRB"/>
 *         &lt;element name="ActionCodeUpdate" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AirActionCodeUpdate.PNRB"/>
 *         &lt;element name="ClassOfServiceUpdate" type="{http://webservices.sabre.com/pnrbuilder/v1_15}ClassOfServiceUpdate.PNRB"/>
 *       &lt;/choice>
 *       &lt;attribute name="id" use="required" type="{http://webservices.sabre.com/pnrbuilder/v1_15}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_15}OperationType.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirSegmentPartialUpdate.PNRB", propOrder = {
    "confirmationUpdate",
    "actionCodeUpdate",
    "classOfServiceUpdate"
})
public class AirSegmentPartialUpdatePNRB {

    @XmlElement(name = "ConfirmationUpdate")
    protected ConfirmationUpdatePNRB confirmationUpdate;
    @XmlElement(name = "ActionCodeUpdate")
    protected AirActionCodeUpdatePNRB actionCodeUpdate;
    @XmlElement(name = "ClassOfServiceUpdate")
    protected ClassOfServiceUpdatePNRB classOfServiceUpdate;
    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;

    /**
     * Gets the value of the confirmationUpdate property.
     * 
     * @return
     *     possible object is
     *     {@link ConfirmationUpdatePNRB }
     *     
     */
    public ConfirmationUpdatePNRB getConfirmationUpdate() {
        return confirmationUpdate;
    }

    /**
     * Sets the value of the confirmationUpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfirmationUpdatePNRB }
     *     
     */
    public void setConfirmationUpdate(ConfirmationUpdatePNRB value) {
        this.confirmationUpdate = value;
    }

    /**
     * Gets the value of the actionCodeUpdate property.
     * 
     * @return
     *     possible object is
     *     {@link AirActionCodeUpdatePNRB }
     *     
     */
    public AirActionCodeUpdatePNRB getActionCodeUpdate() {
        return actionCodeUpdate;
    }

    /**
     * Sets the value of the actionCodeUpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link AirActionCodeUpdatePNRB }
     *     
     */
    public void setActionCodeUpdate(AirActionCodeUpdatePNRB value) {
        this.actionCodeUpdate = value;
    }

    /**
     * Gets the value of the classOfServiceUpdate property.
     * 
     * @return
     *     possible object is
     *     {@link ClassOfServiceUpdatePNRB }
     *     
     */
    public ClassOfServiceUpdatePNRB getClassOfServiceUpdate() {
        return classOfServiceUpdate;
    }

    /**
     * Sets the value of the classOfServiceUpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClassOfServiceUpdatePNRB }
     *     
     */
    public void setClassOfServiceUpdate(ClassOfServiceUpdatePNRB value) {
        this.classOfServiceUpdate = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }

}
