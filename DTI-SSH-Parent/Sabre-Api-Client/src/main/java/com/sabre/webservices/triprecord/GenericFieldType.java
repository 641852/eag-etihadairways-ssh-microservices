
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GenericFieldType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GenericFieldType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FirstName"/>
 *     &lt;enumeration value="LastName"/>
 *     &lt;enumeration value="Email"/>
 *     &lt;enumeration value="ProfileID"/>
 *     &lt;enumeration value="ProfileDomain"/>
 *     &lt;enumeration value="ReservationID"/>
 *     &lt;enumeration value="ReservationDomain"/>
 *     &lt;enumeration value="FlightNumber"/>
 *     &lt;enumeration value="DepartureDate"/>
 *     &lt;enumeration value="ArrivalDate"/>
 *     &lt;enumeration value="DepartureAirport"/>
 *     &lt;enumeration value="ArrivalAirport"/>
 *     &lt;enumeration value="MarketingAirline"/>
 *     &lt;enumeration value="OperatingAirline"/>
 *     &lt;enumeration value="LoyaltyID"/>
 *     &lt;enumeration value="LoyaltyDomain"/>
 *     &lt;enumeration value="PhoneAreaCityCode"/>
 *     &lt;enumeration value="PhoneNumber"/>
 *     &lt;enumeration value="TripName"/>
 *     &lt;enumeration value="ComponentFirstName"/>
 *     &lt;enumeration value="ComponentLastName"/>
 *     &lt;enumeration value="ComponentLoyaltyID"/>
 *     &lt;enumeration value="ComponentLoyaltyDomain"/>
 *     &lt;enumeration value="ComponentPhoneAreaCityCode"/>
 *     &lt;enumeration value="ComponentPhoneNumber"/>
 *     &lt;enumeration value="ComponentEmail"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GenericFieldType")
@XmlEnum
public enum GenericFieldType {

    @XmlEnumValue("FirstName")
    FIRST_NAME("FirstName"),
    @XmlEnumValue("LastName")
    LAST_NAME("LastName"),
    @XmlEnumValue("Email")
    EMAIL("Email"),
    @XmlEnumValue("ProfileID")
    PROFILE_ID("ProfileID"),
    @XmlEnumValue("ProfileDomain")
    PROFILE_DOMAIN("ProfileDomain"),
    @XmlEnumValue("ReservationID")
    RESERVATION_ID("ReservationID"),
    @XmlEnumValue("ReservationDomain")
    RESERVATION_DOMAIN("ReservationDomain"),
    @XmlEnumValue("FlightNumber")
    FLIGHT_NUMBER("FlightNumber"),
    @XmlEnumValue("DepartureDate")
    DEPARTURE_DATE("DepartureDate"),
    @XmlEnumValue("ArrivalDate")
    ARRIVAL_DATE("ArrivalDate"),
    @XmlEnumValue("DepartureAirport")
    DEPARTURE_AIRPORT("DepartureAirport"),
    @XmlEnumValue("ArrivalAirport")
    ARRIVAL_AIRPORT("ArrivalAirport"),
    @XmlEnumValue("MarketingAirline")
    MARKETING_AIRLINE("MarketingAirline"),
    @XmlEnumValue("OperatingAirline")
    OPERATING_AIRLINE("OperatingAirline"),
    @XmlEnumValue("LoyaltyID")
    LOYALTY_ID("LoyaltyID"),
    @XmlEnumValue("LoyaltyDomain")
    LOYALTY_DOMAIN("LoyaltyDomain"),
    @XmlEnumValue("PhoneAreaCityCode")
    PHONE_AREA_CITY_CODE("PhoneAreaCityCode"),
    @XmlEnumValue("PhoneNumber")
    PHONE_NUMBER("PhoneNumber"),
    @XmlEnumValue("TripName")
    TRIP_NAME("TripName"),
    @XmlEnumValue("ComponentFirstName")
    COMPONENT_FIRST_NAME("ComponentFirstName"),
    @XmlEnumValue("ComponentLastName")
    COMPONENT_LAST_NAME("ComponentLastName"),
    @XmlEnumValue("ComponentLoyaltyID")
    COMPONENT_LOYALTY_ID("ComponentLoyaltyID"),
    @XmlEnumValue("ComponentLoyaltyDomain")
    COMPONENT_LOYALTY_DOMAIN("ComponentLoyaltyDomain"),
    @XmlEnumValue("ComponentPhoneAreaCityCode")
    COMPONENT_PHONE_AREA_CITY_CODE("ComponentPhoneAreaCityCode"),
    @XmlEnumValue("ComponentPhoneNumber")
    COMPONENT_PHONE_NUMBER("ComponentPhoneNumber"),
    @XmlEnumValue("ComponentEmail")
    COMPONENT_EMAIL("ComponentEmail");
    private final String value;

    GenericFieldType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GenericFieldType fromValue(String v) {
        for (GenericFieldType c: GenericFieldType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
