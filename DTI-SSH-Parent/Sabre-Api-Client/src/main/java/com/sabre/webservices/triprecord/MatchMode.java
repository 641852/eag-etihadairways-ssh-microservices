
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MatchMode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MatchMode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EXACT"/>
 *     &lt;enumeration value="START"/>
 *     &lt;enumeration value="END"/>
 *     &lt;enumeration value="ANYWHERE"/>
 *     &lt;enumeration value="SIMILAR"/>
 *     &lt;enumeration value="EXACT_NO_COMPRESS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MatchMode")
@XmlEnum
public enum MatchMode {

    EXACT,
    START,
    END,
    ANYWHERE,
    SIMILAR,
    EXACT_NO_COMPRESS;

    public String value() {
        return name();
    }

    public static MatchMode fromValue(String v) {
        return valueOf(v);
    }

}
