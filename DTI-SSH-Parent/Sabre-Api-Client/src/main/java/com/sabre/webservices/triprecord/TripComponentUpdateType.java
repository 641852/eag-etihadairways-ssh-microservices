
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TripComponentUpdateType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TripComponentUpdateType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ComponentID" type="{http://webservices.sabre.com/triprecord}ComponentIDType"/>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{http://webservices.sabre.com/triprecord}UpdateAttributes"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TripComponentUpdateType", propOrder = {
    "componentID"
})
@XmlSeeAlso({
    TripComponentType.class
})
public class TripComponentUpdateType {

    @XmlElement(name = "ComponentID", required = true)
    protected ComponentIDType componentID;
    @XmlAttribute(name = "SequenceNumber")
    protected Integer sequenceNumber;
    @XmlAttribute(name = "Action")
    protected UpdateActions action;

    /**
     * Gets the value of the componentID property.
     * 
     * @return
     *     possible object is
     *     {@link ComponentIDType }
     *     
     */
    public ComponentIDType getComponentID() {
        return componentID;
    }

    /**
     * Sets the value of the componentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComponentIDType }
     *     
     */
    public void setComponentID(ComponentIDType value) {
        this.componentID = value;
    }

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSequenceNumber(Integer value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateActions }
     *     
     */
    public UpdateActions getAction() {
        if (action == null) {
            return UpdateActions.NONE;
        } else {
            return action;
        }
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateActions }
     *     
     */
    public void setAction(UpdateActions value) {
        this.action = value;
    }

}
