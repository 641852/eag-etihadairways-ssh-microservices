
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for _PaymentCardType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="_PaymentCardType">
 *   &lt;restriction base="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString">
 *     &lt;enumeration value="AX"/>
 *     &lt;enumeration value="BC"/>
 *     &lt;enumeration value="BL"/>
 *     &lt;enumeration value="CB"/>
 *     &lt;enumeration value="DN"/>
 *     &lt;enumeration value="DS"/>
 *     &lt;enumeration value="EC"/>
 *     &lt;enumeration value="JC"/>
 *     &lt;enumeration value="MA"/>
 *     &lt;enumeration value="MC"/>
 *     &lt;enumeration value="TP"/>
 *     &lt;enumeration value="VI"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "_PaymentCardType")
@XmlEnum
public enum PaymentCardType {

    AX,
    BC,
    BL,
    CB,
    DN,
    DS,
    EC,
    JC,
    MA,
    MC,
    TP,
    VI;

    public String value() {
        return name();
    }

    public static PaymentCardType fromValue(String v) {
        return valueOf(v);
    }

}
