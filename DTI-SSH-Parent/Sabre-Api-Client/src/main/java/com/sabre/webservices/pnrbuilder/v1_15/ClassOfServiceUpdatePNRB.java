
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClassOfServiceUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClassOfServiceUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SegmentNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Numeric1to99"/>
 *         &lt;element name="DepartureCity" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="ArrivalCity" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="ClassOfService" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString"/>
 *       &lt;/sequence>
 *       &lt;attribute name="instantPurchase" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="journeyCarrierType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="isAvailabilityBreak" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="cat5RequiresRebook" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClassOfServiceUpdate.PNRB", propOrder = {
    "segmentNumber",
    "departureCity",
    "arrivalCity",
    "classOfService"
})
public class ClassOfServiceUpdatePNRB {

    @XmlElement(name = "SegmentNumber")
    protected int segmentNumber;
    @XmlElement(name = "DepartureCity")
    protected String departureCity;
    @XmlElement(name = "ArrivalCity")
    protected String arrivalCity;
    @XmlElement(name = "ClassOfService", required = true)
    protected String classOfService;
    @XmlAttribute(name = "instantPurchase")
    protected Boolean instantPurchase;
    @XmlAttribute(name = "journeyCarrierType")
    protected String journeyCarrierType;
    @XmlAttribute(name = "isAvailabilityBreak")
    protected Boolean isAvailabilityBreak;
    @XmlAttribute(name = "cat5RequiresRebook")
    protected Boolean cat5RequiresRebook;

    /**
     * Gets the value of the segmentNumber property.
     * 
     */
    public int getSegmentNumber() {
        return segmentNumber;
    }

    /**
     * Sets the value of the segmentNumber property.
     * 
     */
    public void setSegmentNumber(int value) {
        this.segmentNumber = value;
    }

    /**
     * Gets the value of the departureCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureCity() {
        return departureCity;
    }

    /**
     * Sets the value of the departureCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureCity(String value) {
        this.departureCity = value;
    }

    /**
     * Gets the value of the arrivalCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalCity() {
        return arrivalCity;
    }

    /**
     * Sets the value of the arrivalCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalCity(String value) {
        this.arrivalCity = value;
    }

    /**
     * Gets the value of the classOfService property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassOfService() {
        return classOfService;
    }

    /**
     * Sets the value of the classOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassOfService(String value) {
        this.classOfService = value;
    }

    /**
     * Gets the value of the instantPurchase property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInstantPurchase() {
        return instantPurchase;
    }

    /**
     * Sets the value of the instantPurchase property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInstantPurchase(Boolean value) {
        this.instantPurchase = value;
    }

    /**
     * Gets the value of the journeyCarrierType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJourneyCarrierType() {
        return journeyCarrierType;
    }

    /**
     * Sets the value of the journeyCarrierType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJourneyCarrierType(String value) {
        this.journeyCarrierType = value;
    }

    /**
     * Gets the value of the isAvailabilityBreak property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAvailabilityBreak() {
        return isAvailabilityBreak;
    }

    /**
     * Sets the value of the isAvailabilityBreak property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAvailabilityBreak(Boolean value) {
        this.isAvailabilityBreak = value;
    }

    /**
     * Gets the value of the cat5RequiresRebook property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCat5RequiresRebook() {
        return cat5RequiresRebook;
    }

    /**
     * Sets the value of the cat5RequiresRebook property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCat5RequiresRebook(Boolean value) {
        this.cat5RequiresRebook = value;
    }

}
