
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PshUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PshUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ParentId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChildId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChildType" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AssociationChildElementType"/>
 *         &lt;element name="Operation" type="{http://webservices.sabre.com/pnrbuilder/v1_15}OperationType.PNRB"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PshUpdate.PNRB", propOrder = {
    "parentId",
    "childId",
    "childType",
    "operation"
})
public class PshUpdatePNRB {

    @XmlElement(name = "ParentId", required = true)
    protected String parentId;
    @XmlElement(name = "ChildId", required = true)
    protected String childId;
    @XmlElement(name = "ChildType", required = true)
    protected AssociationChildElementType childType;
    @XmlElement(name = "Operation", required = true)
    protected OperationTypePNRB operation;

    /**
     * Gets the value of the parentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * Sets the value of the parentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentId(String value) {
        this.parentId = value;
    }

    /**
     * Gets the value of the childId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChildId() {
        return childId;
    }

    /**
     * Sets the value of the childId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChildId(String value) {
        this.childId = value;
    }

    /**
     * Gets the value of the childType property.
     * 
     * @return
     *     possible object is
     *     {@link AssociationChildElementType }
     *     
     */
    public AssociationChildElementType getChildType() {
        return childType;
    }

    /**
     * Sets the value of the childType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssociationChildElementType }
     *     
     */
    public void setChildType(AssociationChildElementType value) {
        this.childType = value;
    }

    /**
     * Gets the value of the operation property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOperation() {
        return operation;
    }

    /**
     * Sets the value of the operation property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOperation(OperationTypePNRB value) {
        this.operation = value;
    }

}
