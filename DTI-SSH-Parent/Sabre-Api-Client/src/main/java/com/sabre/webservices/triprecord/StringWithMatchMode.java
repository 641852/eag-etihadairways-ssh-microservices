
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for StringWithMatchMode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StringWithMatchMode">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://webservices.sabre.com/triprecord>string1to255">
 *       &lt;attribute name="MatchMode" type="{http://webservices.sabre.com/triprecord}MatchMode" default="EXACT" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StringWithMatchMode", propOrder = {
    "value"
})
public class StringWithMatchMode {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "MatchMode")
    protected MatchMode matchMode;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the matchMode property.
     * 
     * @return
     *     possible object is
     *     {@link MatchMode }
     *     
     */
    public MatchMode getMatchMode() {
        if (matchMode == null) {
            return MatchMode.EXACT;
        } else {
            return matchMode;
        }
    }

    /**
     * Sets the value of the matchMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link MatchMode }
     *     
     */
    public void setMatchMode(MatchMode value) {
        this.matchMode = value;
    }

}
