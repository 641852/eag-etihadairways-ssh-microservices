
package com.sabre.webservices.pnrbuilder.v1_15;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * When non-air segments are defined they will be added to
 *                 this sequence.
 *             
 * 
 * <p>Java class for Segments.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Segments.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;group ref="{http://webservices.sabre.com/pnrbuilder/v1_15}Segment.PNRB" maxOccurs="99" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="addARNKS" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Segments.PNRB", propOrder = {
    "segmentPNRB"
})
public class SegmentsPNRB {

    @XmlElements({
        @XmlElement(name = "AirSegmentBundledSell", type = AirSegmentBundledSellPNRB.class),
        @XmlElement(name = "AirSegment", type = AirSegmentPNRB.class),
        @XmlElement(name = "AirSegmentMultiLeg", type = AirSegmentMultiLegPNRB.class),
        @XmlElement(name = "AirSegmentOpen", type = AirSegmentOpenPNRB.class),
        @XmlElement(name = "AirSegmentARNK", type = AirSegmentARNKPNRB.class),
        @XmlElement(name = "AirSegmentSellFromBSGInventory", type = AirSegmentSellFromBSGInventoryPNRB.class)
    })
    protected List<Object> segmentPNRB;
    @XmlAttribute(name = "addARNKS")
    protected Boolean addARNKS;

    /**
     * Gets the value of the segmentPNRB property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the segmentPNRB property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSegmentPNRB().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirSegmentBundledSellPNRB }
     * {@link AirSegmentPNRB }
     * {@link AirSegmentMultiLegPNRB }
     * {@link AirSegmentOpenPNRB }
     * {@link AirSegmentARNKPNRB }
     * {@link AirSegmentSellFromBSGInventoryPNRB }
     * 
     * 
     */
    public List<Object> getSegmentPNRB() {
        if (segmentPNRB == null) {
            segmentPNRB = new ArrayList<Object>();
        }
        return this.segmentPNRB;
    }

    /**
     * Gets the value of the addARNKS property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAddARNKS() {
        return addARNKS;
    }

    /**
     * Sets the value of the addARNKS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAddARNKS(Boolean value) {
        this.addARNKS = value;
    }

}
