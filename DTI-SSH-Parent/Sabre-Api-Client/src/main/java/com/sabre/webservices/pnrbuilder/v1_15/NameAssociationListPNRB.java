
package com.sabre.webservices.pnrbuilder.v1_15;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NameAssociationList.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NameAssociationList.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NameAssociationTag" type="{http://webservices.sabre.com/pnrbuilder/v1_15}NameAssociationTag.PNRB" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NameAssociationList.PNRB", propOrder = {
    "nameAssociationTag"
})
public class NameAssociationListPNRB {

    @XmlElement(name = "NameAssociationTag")
    protected List<NameAssociationTagPNRB> nameAssociationTag;

    /**
     * Gets the value of the nameAssociationTag property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nameAssociationTag property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNameAssociationTag().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NameAssociationTagPNRB }
     * 
     * 
     */
    public List<NameAssociationTagPNRB> getNameAssociationTag() {
        if (nameAssociationTag == null) {
            nameAssociationTag = new ArrayList<NameAssociationTagPNRB>();
        }
        return this.nameAssociationTag;
    }

}
