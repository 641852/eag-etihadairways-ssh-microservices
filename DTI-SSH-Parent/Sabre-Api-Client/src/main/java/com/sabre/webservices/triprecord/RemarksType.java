
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RemarksType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RemarksType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="No Air,Car,Hotel,Cancelled Segments"/>
 *     &lt;enumeration value="Active Segments"/>
 *     &lt;enumeration value="Unknown"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RemarksType")
@XmlEnum
public enum RemarksType {

    @XmlEnumValue("No Air,Car,Hotel,Cancelled Segments")
    NO_AIR_CAR_HOTEL_CANCELLED_SEGMENTS("No Air,Car,Hotel,Cancelled Segments"),
    @XmlEnumValue("Active Segments")
    ACTIVE_SEGMENTS("Active Segments"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown");
    private final String value;

    RemarksType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RemarksType fromValue(String v) {
        for (RemarksType c: RemarksType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
