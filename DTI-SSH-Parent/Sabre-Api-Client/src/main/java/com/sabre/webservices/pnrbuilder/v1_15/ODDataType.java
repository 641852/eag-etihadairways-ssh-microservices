
package com.sabre.webservices.pnrbuilder.v1_15;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Bid Price and Fare Reference data - Pnr footprint
 * 
 * <p>Java class for ODDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ODDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OriginalFare" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="EffectiveBidPrice" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AdjustedFare" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AdjustedBidPrice" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FareClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LowestAvailFareClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PointOfCommencement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerScore" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Numeric0to99999" minOccurs="0"/>
 *         &lt;element name="FrequentFlyer" type="{http://webservices.sabre.com/pnrbuilder/v1_15}ODFrequentFlyerType" minOccurs="0"/>
 *         &lt;element name="Market" type="{http://webservices.sabre.com/pnrbuilder/v1_15}ODMarketType" minOccurs="0"/>
 *         &lt;element name="TripMarket" type="{http://webservices.sabre.com/pnrbuilder/v1_15}ODMarketType" minOccurs="0"/>
 *         &lt;element name="RetrievedFareMarket" type="{http://webservices.sabre.com/pnrbuilder/v1_15}ODMarketType" minOccurs="0"/>
 *         &lt;element name="FareTypeIndicators" type="{http://webservices.sabre.com/pnrbuilder/v1_15}ODFareTypeIndicatorsType" minOccurs="0"/>
 *         &lt;element name="EvaluationTypeIndicators" type="{http://webservices.sabre.com/pnrbuilder/v1_15}ODEvaluationTypeIndicatorsType" minOccurs="0"/>
 *         &lt;element name="ExceptionTypeIndicators" type="{http://webservices.sabre.com/pnrbuilder/v1_15}ODExceptionTypeIndicatorsType" minOccurs="0"/>
 *         &lt;element name="AdditionalFlags" type="{http://www.w3.org/2001/XMLSchema}unsignedLong" minOccurs="0"/>
 *         &lt;element name="AdditionalData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="uniqueId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ODDataType", propOrder = {
    "originalFare",
    "effectiveBidPrice",
    "adjustedFare",
    "adjustedBidPrice",
    "fareClass",
    "lowestAvailFareClass",
    "pointOfCommencement",
    "customerScore",
    "frequentFlyer",
    "market",
    "tripMarket",
    "retrievedFareMarket",
    "fareTypeIndicators",
    "evaluationTypeIndicators",
    "exceptionTypeIndicators",
    "additionalFlags",
    "additionalData"
})
public class ODDataType {

    @XmlElement(name = "OriginalFare")
    protected Integer originalFare;
    @XmlElement(name = "EffectiveBidPrice")
    protected Integer effectiveBidPrice;
    @XmlElement(name = "AdjustedFare")
    protected Integer adjustedFare;
    @XmlElement(name = "AdjustedBidPrice")
    protected Integer adjustedBidPrice;
    @XmlElement(name = "FareClass")
    protected String fareClass;
    @XmlElement(name = "LowestAvailFareClass")
    protected String lowestAvailFareClass;
    @XmlElement(name = "PointOfCommencement")
    protected String pointOfCommencement;
    @XmlElement(name = "CustomerScore")
    protected Integer customerScore;
    @XmlElement(name = "FrequentFlyer")
    protected ODFrequentFlyerType frequentFlyer;
    @XmlElement(name = "Market")
    protected ODMarketType market;
    @XmlElement(name = "TripMarket")
    protected ODMarketType tripMarket;
    @XmlElement(name = "RetrievedFareMarket")
    protected ODMarketType retrievedFareMarket;
    @XmlElement(name = "FareTypeIndicators")
    protected ODFareTypeIndicatorsType fareTypeIndicators;
    @XmlElement(name = "EvaluationTypeIndicators")
    protected ODEvaluationTypeIndicatorsType evaluationTypeIndicators;
    @XmlElement(name = "ExceptionTypeIndicators")
    protected ODExceptionTypeIndicatorsType exceptionTypeIndicators;
    @XmlElement(name = "AdditionalFlags")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger additionalFlags;
    @XmlElement(name = "AdditionalData")
    protected String additionalData;
    @XmlAttribute(name = "uniqueId")
    protected String uniqueId;

    /**
     * Gets the value of the originalFare property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOriginalFare() {
        return originalFare;
    }

    /**
     * Sets the value of the originalFare property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOriginalFare(Integer value) {
        this.originalFare = value;
    }

    /**
     * Gets the value of the effectiveBidPrice property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEffectiveBidPrice() {
        return effectiveBidPrice;
    }

    /**
     * Sets the value of the effectiveBidPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEffectiveBidPrice(Integer value) {
        this.effectiveBidPrice = value;
    }

    /**
     * Gets the value of the adjustedFare property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAdjustedFare() {
        return adjustedFare;
    }

    /**
     * Sets the value of the adjustedFare property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAdjustedFare(Integer value) {
        this.adjustedFare = value;
    }

    /**
     * Gets the value of the adjustedBidPrice property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAdjustedBidPrice() {
        return adjustedBidPrice;
    }

    /**
     * Sets the value of the adjustedBidPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAdjustedBidPrice(Integer value) {
        this.adjustedBidPrice = value;
    }

    /**
     * Gets the value of the fareClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareClass() {
        return fareClass;
    }

    /**
     * Sets the value of the fareClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareClass(String value) {
        this.fareClass = value;
    }

    /**
     * Gets the value of the lowestAvailFareClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLowestAvailFareClass() {
        return lowestAvailFareClass;
    }

    /**
     * Sets the value of the lowestAvailFareClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLowestAvailFareClass(String value) {
        this.lowestAvailFareClass = value;
    }

    /**
     * Gets the value of the pointOfCommencement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPointOfCommencement() {
        return pointOfCommencement;
    }

    /**
     * Sets the value of the pointOfCommencement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPointOfCommencement(String value) {
        this.pointOfCommencement = value;
    }

    /**
     * Gets the value of the customerScore property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCustomerScore() {
        return customerScore;
    }

    /**
     * Sets the value of the customerScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCustomerScore(Integer value) {
        this.customerScore = value;
    }

    /**
     * Gets the value of the frequentFlyer property.
     * 
     * @return
     *     possible object is
     *     {@link ODFrequentFlyerType }
     *     
     */
    public ODFrequentFlyerType getFrequentFlyer() {
        return frequentFlyer;
    }

    /**
     * Sets the value of the frequentFlyer property.
     * 
     * @param value
     *     allowed object is
     *     {@link ODFrequentFlyerType }
     *     
     */
    public void setFrequentFlyer(ODFrequentFlyerType value) {
        this.frequentFlyer = value;
    }

    /**
     * Gets the value of the market property.
     * 
     * @return
     *     possible object is
     *     {@link ODMarketType }
     *     
     */
    public ODMarketType getMarket() {
        return market;
    }

    /**
     * Sets the value of the market property.
     * 
     * @param value
     *     allowed object is
     *     {@link ODMarketType }
     *     
     */
    public void setMarket(ODMarketType value) {
        this.market = value;
    }

    /**
     * Gets the value of the tripMarket property.
     * 
     * @return
     *     possible object is
     *     {@link ODMarketType }
     *     
     */
    public ODMarketType getTripMarket() {
        return tripMarket;
    }

    /**
     * Sets the value of the tripMarket property.
     * 
     * @param value
     *     allowed object is
     *     {@link ODMarketType }
     *     
     */
    public void setTripMarket(ODMarketType value) {
        this.tripMarket = value;
    }

    /**
     * Gets the value of the retrievedFareMarket property.
     * 
     * @return
     *     possible object is
     *     {@link ODMarketType }
     *     
     */
    public ODMarketType getRetrievedFareMarket() {
        return retrievedFareMarket;
    }

    /**
     * Sets the value of the retrievedFareMarket property.
     * 
     * @param value
     *     allowed object is
     *     {@link ODMarketType }
     *     
     */
    public void setRetrievedFareMarket(ODMarketType value) {
        this.retrievedFareMarket = value;
    }

    /**
     * Gets the value of the fareTypeIndicators property.
     * 
     * @return
     *     possible object is
     *     {@link ODFareTypeIndicatorsType }
     *     
     */
    public ODFareTypeIndicatorsType getFareTypeIndicators() {
        return fareTypeIndicators;
    }

    /**
     * Sets the value of the fareTypeIndicators property.
     * 
     * @param value
     *     allowed object is
     *     {@link ODFareTypeIndicatorsType }
     *     
     */
    public void setFareTypeIndicators(ODFareTypeIndicatorsType value) {
        this.fareTypeIndicators = value;
    }

    /**
     * Gets the value of the evaluationTypeIndicators property.
     * 
     * @return
     *     possible object is
     *     {@link ODEvaluationTypeIndicatorsType }
     *     
     */
    public ODEvaluationTypeIndicatorsType getEvaluationTypeIndicators() {
        return evaluationTypeIndicators;
    }

    /**
     * Sets the value of the evaluationTypeIndicators property.
     * 
     * @param value
     *     allowed object is
     *     {@link ODEvaluationTypeIndicatorsType }
     *     
     */
    public void setEvaluationTypeIndicators(ODEvaluationTypeIndicatorsType value) {
        this.evaluationTypeIndicators = value;
    }

    /**
     * Gets the value of the exceptionTypeIndicators property.
     * 
     * @return
     *     possible object is
     *     {@link ODExceptionTypeIndicatorsType }
     *     
     */
    public ODExceptionTypeIndicatorsType getExceptionTypeIndicators() {
        return exceptionTypeIndicators;
    }

    /**
     * Sets the value of the exceptionTypeIndicators property.
     * 
     * @param value
     *     allowed object is
     *     {@link ODExceptionTypeIndicatorsType }
     *     
     */
    public void setExceptionTypeIndicators(ODExceptionTypeIndicatorsType value) {
        this.exceptionTypeIndicators = value;
    }

    /**
     * Gets the value of the additionalFlags property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAdditionalFlags() {
        return additionalFlags;
    }

    /**
     * Sets the value of the additionalFlags property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAdditionalFlags(BigInteger value) {
        this.additionalFlags = value;
    }

    /**
     * Gets the value of the additionalData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalData() {
        return additionalData;
    }

    /**
     * Sets the value of the additionalData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalData(String value) {
        this.additionalData = value;
    }

    /**
     * Gets the value of the uniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * Sets the value of the uniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueId(String value) {
        this.uniqueId = value;
    }

}
