
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DocumentType.PNRB.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DocumentType.PNRB">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="A"/>
 *     &lt;enumeration value="C"/>
 *     &lt;enumeration value="P"/>
 *     &lt;enumeration value="T"/>
 *     &lt;enumeration value="F"/>
 *     &lt;enumeration value="M"/>
 *     &lt;enumeration value="N"/>
 *     &lt;enumeration value="V"/>
 *     &lt;enumeration value="I"/>
 *     &lt;enumeration value="IN"/>
 *     &lt;enumeration value="PD"/>
 *     &lt;enumeration value="PM"/>
 *     &lt;enumeration value="PS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DocumentType.PNRB")
@XmlEnum
public enum DocumentTypePNRB {

    A,
    C,
    P,
    T,
    F,
    M,
    N,
    V,
    I,
    IN,
    PD,
    PM,
    PS;

    public String value() {
        return name();
    }

    public static DocumentTypePNRB fromValue(String v) {
        return valueOf(v);
    }

}
