
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PingServicesType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PingServicesType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PnrConn"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PingServicesType")
@XmlEnum
public enum PingServicesType {

    @XmlEnumValue("PnrConn")
    PNR_CONN("PnrConn");
    private final String value;

    PingServicesType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PingServicesType fromValue(String v) {
        for (PingServicesType c: PingServicesType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
