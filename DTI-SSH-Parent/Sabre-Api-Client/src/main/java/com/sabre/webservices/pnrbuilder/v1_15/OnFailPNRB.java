
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OnFail.PNRB.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OnFail.PNRB">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UPDATE_SESSION"/>
 *     &lt;enumeration value="UPDATE_AND_FILE"/>
 *     &lt;enumeration value="REJECT_ALL_UPDATES"/>
 *     &lt;enumeration value="UPDATE_SESSION_REJECT_FILE_REQUEST"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OnFail.PNRB")
@XmlEnum
public enum OnFailPNRB {

    UPDATE_SESSION,
    UPDATE_AND_FILE,
    REJECT_ALL_UPDATES,
    UPDATE_SESSION_REJECT_FILE_REQUEST;

    public String value() {
        return name();
    }

    public static OnFailPNRB fromValue(String v) {
        return valueOf(v);
    }

}
