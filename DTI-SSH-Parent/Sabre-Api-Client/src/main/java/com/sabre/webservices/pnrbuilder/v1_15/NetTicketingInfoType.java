
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NetTicketingInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NetTicketingInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NetFareDetails" type="{http://webservices.sabre.com/pnrbuilder/v1_15}FareDetailsType" minOccurs="0"/>
 *         &lt;element name="SellingFareDetails" type="{http://webservices.sabre.com/pnrbuilder/v1_15}FareDetailsType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="AccountCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CorporateId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NetTicketingInfoType", propOrder = {
    "netFareDetails",
    "sellingFareDetails"
})
public class NetTicketingInfoType {

    @XmlElement(name = "NetFareDetails")
    protected FareDetailsType netFareDetails;
    @XmlElement(name = "SellingFareDetails")
    protected FareDetailsType sellingFareDetails;
    @XmlAttribute(name = "AccountCode")
    protected String accountCode;
    @XmlAttribute(name = "CorporateId")
    protected String corporateId;

    /**
     * Gets the value of the netFareDetails property.
     * 
     * @return
     *     possible object is
     *     {@link FareDetailsType }
     *     
     */
    public FareDetailsType getNetFareDetails() {
        return netFareDetails;
    }

    /**
     * Sets the value of the netFareDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link FareDetailsType }
     *     
     */
    public void setNetFareDetails(FareDetailsType value) {
        this.netFareDetails = value;
    }

    /**
     * Gets the value of the sellingFareDetails property.
     * 
     * @return
     *     possible object is
     *     {@link FareDetailsType }
     *     
     */
    public FareDetailsType getSellingFareDetails() {
        return sellingFareDetails;
    }

    /**
     * Sets the value of the sellingFareDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link FareDetailsType }
     *     
     */
    public void setSellingFareDetails(FareDetailsType value) {
        this.sellingFareDetails = value;
    }

    /**
     * Gets the value of the accountCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCode() {
        return accountCode;
    }

    /**
     * Sets the value of the accountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCode(String value) {
        this.accountCode = value;
    }

    /**
     * Gets the value of the corporateId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorporateId() {
        return corporateId;
    }

    /**
     * Sets the value of the corporateId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorporateId(String value) {
        this.corporateId = value;
    }

}
