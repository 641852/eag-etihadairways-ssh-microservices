
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RemarkPartialUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RemarkPartialUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NameAssociationList" type="{http://webservices.sabre.com/pnrbuilder/v1_15}NameAssociationList.PNRB" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="RemarkTextUpdate">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;choice>
 *                     &lt;sequence>
 *                       &lt;element name="RemarkText" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString"/>
 *                       &lt;element name="OriginalRemarkText" type="{http://webservices.sabre.com/pnrbuilder/v1_15}NativeLanguageValueType"/>
 *                     &lt;/sequence>
 *                   &lt;/choice>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_15}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_15}OperationType.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemarkPartialUpdate.PNRB", propOrder = {
    "nameAssociationList",
    "remarkTextUpdate"
})
public class RemarkPartialUpdatePNRB {

    @XmlElement(name = "NameAssociationList")
    protected NameAssociationListPNRB nameAssociationList;
    @XmlElement(name = "RemarkTextUpdate")
    protected RemarkPartialUpdatePNRB.RemarkTextUpdate remarkTextUpdate;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;

    /**
     * Gets the value of the nameAssociationList property.
     * 
     * @return
     *     possible object is
     *     {@link NameAssociationListPNRB }
     *     
     */
    public NameAssociationListPNRB getNameAssociationList() {
        return nameAssociationList;
    }

    /**
     * Sets the value of the nameAssociationList property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameAssociationListPNRB }
     *     
     */
    public void setNameAssociationList(NameAssociationListPNRB value) {
        this.nameAssociationList = value;
    }

    /**
     * Gets the value of the remarkTextUpdate property.
     * 
     * @return
     *     possible object is
     *     {@link RemarkPartialUpdatePNRB.RemarkTextUpdate }
     *     
     */
    public RemarkPartialUpdatePNRB.RemarkTextUpdate getRemarkTextUpdate() {
        return remarkTextUpdate;
    }

    /**
     * Sets the value of the remarkTextUpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link RemarkPartialUpdatePNRB.RemarkTextUpdate }
     *     
     */
    public void setRemarkTextUpdate(RemarkPartialUpdatePNRB.RemarkTextUpdate value) {
        this.remarkTextUpdate = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;sequence>
     *           &lt;element name="RemarkText" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString"/>
     *           &lt;element name="OriginalRemarkText" type="{http://webservices.sabre.com/pnrbuilder/v1_15}NativeLanguageValueType"/>
     *         &lt;/sequence>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "remarkText",
        "originalRemarkText"
    })
    public static class RemarkTextUpdate {

        @XmlElement(name = "RemarkText")
        protected String remarkText;
        @XmlElement(name = "OriginalRemarkText")
        protected NativeLanguageValueType originalRemarkText;

        /**
         * Gets the value of the remarkText property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRemarkText() {
            return remarkText;
        }

        /**
         * Sets the value of the remarkText property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRemarkText(String value) {
            this.remarkText = value;
        }

        /**
         * Gets the value of the originalRemarkText property.
         * 
         * @return
         *     possible object is
         *     {@link NativeLanguageValueType }
         *     
         */
        public NativeLanguageValueType getOriginalRemarkText() {
            return originalRemarkText;
        }

        /**
         * Sets the value of the originalRemarkText property.
         * 
         * @param value
         *     allowed object is
         *     {@link NativeLanguageValueType }
         *     
         */
        public void setOriginalRemarkText(NativeLanguageValueType value) {
            this.originalRemarkText = value;
        }

    }

}
