
package com.sabre.webservices.pnrbuilder.v1_15;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for BookingDetails.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BookingDetails.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RecordLocator" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="CreationTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="SystemCreationTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CreationAgentID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UpdateTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PNRSequence" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Numeric0to99999" minOccurs="0"/>
 *         &lt;element name="FlightsRange" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="Start" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *                 &lt;attribute name="End" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ItineraryDate" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="Start" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *                 &lt;attribute name="End" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DivideSplitDetails" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;choice maxOccurs="unbounded" minOccurs="0">
 *                     &lt;element name="DividedRecord" type="{http://webservices.sabre.com/pnrbuilder/v1_15}DividedRemarkType"/>
 *                     &lt;element name="SplitFromRecord" type="{http://webservices.sabre.com/pnrbuilder/v1_15}SplitFromRemarkType"/>
 *                     &lt;element name="SplitToRecord" type="{http://webservices.sabre.com/pnrbuilder/v1_15}SplitToRemarkType"/>
 *                   &lt;/choice>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="EstimatedPurgeTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="UpdateToken" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookingDetails.PNRB", propOrder = {
    "header",
    "recordLocator",
    "creationTimestamp",
    "systemCreationTimestamp",
    "creationAgentID",
    "updateTimestamp",
    "pnrSequence",
    "flightsRange",
    "itineraryDate",
    "divideSplitDetails",
    "estimatedPurgeTimestamp",
    "updateToken"
})
public class BookingDetailsPNRB {

    @XmlElement(name = "Header")
    protected List<String> header;
    @XmlElement(name = "RecordLocator")
    protected String recordLocator;
    @XmlElement(name = "CreationTimestamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationTimestamp;
    @XmlElement(name = "SystemCreationTimestamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar systemCreationTimestamp;
    @XmlElement(name = "CreationAgentID")
    protected String creationAgentID;
    @XmlElement(name = "UpdateTimestamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar updateTimestamp;
    @XmlElement(name = "PNRSequence")
    protected Integer pnrSequence;
    @XmlElement(name = "FlightsRange")
    protected BookingDetailsPNRB.FlightsRange flightsRange;
    @XmlElement(name = "ItineraryDate")
    protected BookingDetailsPNRB.ItineraryDate itineraryDate;
    @XmlElement(name = "DivideSplitDetails")
    protected BookingDetailsPNRB.DivideSplitDetails divideSplitDetails;
    @XmlElement(name = "EstimatedPurgeTimestamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar estimatedPurgeTimestamp;
    @XmlElement(name = "UpdateToken")
    protected String updateToken;

    /**
     * Gets the value of the header property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the header property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHeader().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getHeader() {
        if (header == null) {
            header = new ArrayList<String>();
        }
        return this.header;
    }

    /**
     * Gets the value of the recordLocator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordLocator() {
        return recordLocator;
    }

    /**
     * Sets the value of the recordLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordLocator(String value) {
        this.recordLocator = value;
    }

    /**
     * Gets the value of the creationTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationTimestamp() {
        return creationTimestamp;
    }

    /**
     * Sets the value of the creationTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationTimestamp(XMLGregorianCalendar value) {
        this.creationTimestamp = value;
    }

    /**
     * Gets the value of the systemCreationTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSystemCreationTimestamp() {
        return systemCreationTimestamp;
    }

    /**
     * Sets the value of the systemCreationTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSystemCreationTimestamp(XMLGregorianCalendar value) {
        this.systemCreationTimestamp = value;
    }

    /**
     * Gets the value of the creationAgentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreationAgentID() {
        return creationAgentID;
    }

    /**
     * Sets the value of the creationAgentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreationAgentID(String value) {
        this.creationAgentID = value;
    }

    /**
     * Gets the value of the updateTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUpdateTimestamp() {
        return updateTimestamp;
    }

    /**
     * Sets the value of the updateTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUpdateTimestamp(XMLGregorianCalendar value) {
        this.updateTimestamp = value;
    }

    /**
     * Gets the value of the pnrSequence property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPNRSequence() {
        return pnrSequence;
    }

    /**
     * Sets the value of the pnrSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPNRSequence(Integer value) {
        this.pnrSequence = value;
    }

    /**
     * Gets the value of the flightsRange property.
     * 
     * @return
     *     possible object is
     *     {@link BookingDetailsPNRB.FlightsRange }
     *     
     */
    public BookingDetailsPNRB.FlightsRange getFlightsRange() {
        return flightsRange;
    }

    /**
     * Sets the value of the flightsRange property.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingDetailsPNRB.FlightsRange }
     *     
     */
    public void setFlightsRange(BookingDetailsPNRB.FlightsRange value) {
        this.flightsRange = value;
    }

    /**
     * Gets the value of the itineraryDate property.
     * 
     * @return
     *     possible object is
     *     {@link BookingDetailsPNRB.ItineraryDate }
     *     
     */
    public BookingDetailsPNRB.ItineraryDate getItineraryDate() {
        return itineraryDate;
    }

    /**
     * Sets the value of the itineraryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingDetailsPNRB.ItineraryDate }
     *     
     */
    public void setItineraryDate(BookingDetailsPNRB.ItineraryDate value) {
        this.itineraryDate = value;
    }

    /**
     * Gets the value of the divideSplitDetails property.
     * 
     * @return
     *     possible object is
     *     {@link BookingDetailsPNRB.DivideSplitDetails }
     *     
     */
    public BookingDetailsPNRB.DivideSplitDetails getDivideSplitDetails() {
        return divideSplitDetails;
    }

    /**
     * Sets the value of the divideSplitDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingDetailsPNRB.DivideSplitDetails }
     *     
     */
    public void setDivideSplitDetails(BookingDetailsPNRB.DivideSplitDetails value) {
        this.divideSplitDetails = value;
    }

    /**
     * Gets the value of the estimatedPurgeTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEstimatedPurgeTimestamp() {
        return estimatedPurgeTimestamp;
    }

    /**
     * Sets the value of the estimatedPurgeTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEstimatedPurgeTimestamp(XMLGregorianCalendar value) {
        this.estimatedPurgeTimestamp = value;
    }

    /**
     * Gets the value of the updateToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdateToken() {
        return updateToken;
    }

    /**
     * Sets the value of the updateToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdateToken(String value) {
        this.updateToken = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;choice maxOccurs="unbounded" minOccurs="0">
     *           &lt;element name="DividedRecord" type="{http://webservices.sabre.com/pnrbuilder/v1_15}DividedRemarkType"/>
     *           &lt;element name="SplitFromRecord" type="{http://webservices.sabre.com/pnrbuilder/v1_15}SplitFromRemarkType"/>
     *           &lt;element name="SplitToRecord" type="{http://webservices.sabre.com/pnrbuilder/v1_15}SplitToRemarkType"/>
     *         &lt;/choice>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dividedRecordOrSplitFromRecordOrSplitToRecord"
    })
    public static class DivideSplitDetails {

        @XmlElements({
            @XmlElement(name = "DividedRecord", type = DividedRemarkType.class),
            @XmlElement(name = "SplitFromRecord", type = SplitFromRemarkType.class),
            @XmlElement(name = "SplitToRecord", type = SplitToRemarkType.class)
        })
        protected List<BaseRemarkType> dividedRecordOrSplitFromRecordOrSplitToRecord;

        /**
         * Gets the value of the dividedRecordOrSplitFromRecordOrSplitToRecord property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the dividedRecordOrSplitFromRecordOrSplitToRecord property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDividedRecordOrSplitFromRecordOrSplitToRecord().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DividedRemarkType }
         * {@link SplitFromRemarkType }
         * {@link SplitToRemarkType }
         * 
         * 
         */
        public List<BaseRemarkType> getDividedRecordOrSplitFromRecordOrSplitToRecord() {
            if (dividedRecordOrSplitFromRecordOrSplitToRecord == null) {
                dividedRecordOrSplitFromRecordOrSplitToRecord = new ArrayList<BaseRemarkType>();
            }
            return this.dividedRecordOrSplitFromRecordOrSplitToRecord;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="Start" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
     *       &lt;attribute name="End" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class FlightsRange {

        @XmlAttribute(name = "Start")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar start;
        @XmlAttribute(name = "End")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar end;

        /**
         * Gets the value of the start property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getStart() {
            return start;
        }

        /**
         * Sets the value of the start property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setStart(XMLGregorianCalendar value) {
            this.start = value;
        }

        /**
         * Gets the value of the end property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getEnd() {
            return end;
        }

        /**
         * Sets the value of the end property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setEnd(XMLGregorianCalendar value) {
            this.end = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="Start" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
     *       &lt;attribute name="End" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ItineraryDate {

        @XmlAttribute(name = "Start")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar start;
        @XmlAttribute(name = "End")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar end;

        /**
         * Gets the value of the start property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getStart() {
            return start;
        }

        /**
         * Sets the value of the start property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setStart(XMLGregorianCalendar value) {
            this.start = value;
        }

        /**
         * Gets the value of the end property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getEnd() {
            return end;
        }

        /**
         * Sets the value of the end property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setEnd(XMLGregorianCalendar value) {
            this.end = value;
        }

    }

}
