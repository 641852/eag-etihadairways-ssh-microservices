
package com.sabre.services.res.or.v1_8;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.sabre.webservices.pnrbuilder.v1_15.OpenReservationElementHistoryType;


/**
 * <p>Java class for OpenReservationElementType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OpenReservationElementType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element ref="{http://services.sabre.com/res/or/v1_8}SocialMediaContact"/>
 *           &lt;element ref="{http://services.sabre.com/res/or/v1_8}AgencyFees"/>
 *           &lt;element ref="{http://services.sabre.com/res/or/v1_8}LangDetails"/>
 *           &lt;element ref="{http://services.sabre.com/res/or/v1_8}FormOfPayment"/>
 *           &lt;element ref="{http://services.sabre.com/res/or/v1_8}AncillaryProduct"/>
 *           &lt;element ref="{http://services.sabre.com/res/or/v1_8}InvoiceData"/>
 *           &lt;element ref="{http://services.sabre.com/res/or/v1_8}AccountingField"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="displayIndex" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="elementId" type="{http://services.sabre.com/res/or/v1_8}AssociationMatrixID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpenReservationElementType", propOrder = {
    "socialMediaContact",
    "agencyFees",
    "langDetails",
    "formOfPayment",
    "ancillaryProduct",
    "invoiceData",
    "accountingField"
})
@XmlSeeAlso({
    OpenReservationElementHistoryType.class
})
public class OpenReservationElementType {

    @XmlElement(name = "SocialMediaContact")
    protected SocialMediaContactType socialMediaContact;
    @XmlElement(name = "AgencyFees")
    protected AgencyFeesType agencyFees;
    @XmlElement(name = "LangDetails")
    protected LangDetailsType langDetails;
    @XmlElement(name = "FormOfPayment")
    protected FormOfPayment formOfPayment;
    @XmlElement(name = "AncillaryProduct")
    protected AncillaryProductObject ancillaryProduct;
    @XmlElement(name = "InvoiceData")
    protected InvoiceData invoiceData;
    @XmlElement(name = "AccountingField")
    protected AccountingField accountingField;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "type")
    protected String type;
    @XmlAttribute(name = "displayIndex")
    protected Integer displayIndex;
    @XmlAttribute(name = "elementId")
    protected String elementId;

    /**
     * Gets the value of the socialMediaContact property.
     * 
     * @return
     *     possible object is
     *     {@link SocialMediaContactType }
     *     
     */
    public SocialMediaContactType getSocialMediaContact() {
        return socialMediaContact;
    }

    /**
     * Sets the value of the socialMediaContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link SocialMediaContactType }
     *     
     */
    public void setSocialMediaContact(SocialMediaContactType value) {
        this.socialMediaContact = value;
    }

    /**
     * Gets the value of the agencyFees property.
     * 
     * @return
     *     possible object is
     *     {@link AgencyFeesType }
     *     
     */
    public AgencyFeesType getAgencyFees() {
        return agencyFees;
    }

    /**
     * Sets the value of the agencyFees property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgencyFeesType }
     *     
     */
    public void setAgencyFees(AgencyFeesType value) {
        this.agencyFees = value;
    }

    /**
     * Gets the value of the langDetails property.
     * 
     * @return
     *     possible object is
     *     {@link LangDetailsType }
     *     
     */
    public LangDetailsType getLangDetails() {
        return langDetails;
    }

    /**
     * Sets the value of the langDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link LangDetailsType }
     *     
     */
    public void setLangDetails(LangDetailsType value) {
        this.langDetails = value;
    }

    /**
     * Gets the value of the formOfPayment property.
     * 
     * @return
     *     possible object is
     *     {@link FormOfPayment }
     *     
     */
    public FormOfPayment getFormOfPayment() {
        return formOfPayment;
    }

    /**
     * Sets the value of the formOfPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormOfPayment }
     *     
     */
    public void setFormOfPayment(FormOfPayment value) {
        this.formOfPayment = value;
    }

    /**
     * Gets the value of the ancillaryProduct property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryProductObject }
     *     
     */
    public AncillaryProductObject getAncillaryProduct() {
        return ancillaryProduct;
    }

    /**
     * Sets the value of the ancillaryProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryProductObject }
     *     
     */
    public void setAncillaryProduct(AncillaryProductObject value) {
        this.ancillaryProduct = value;
    }

    /**
     * Gets the value of the invoiceData property.
     * 
     * @return
     *     possible object is
     *     {@link InvoiceData }
     *     
     */
    public InvoiceData getInvoiceData() {
        return invoiceData;
    }

    /**
     * Sets the value of the invoiceData property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvoiceData }
     *     
     */
    public void setInvoiceData(InvoiceData value) {
        this.invoiceData = value;
    }

    /**
     * Gets the value of the accountingField property.
     * 
     * @return
     *     possible object is
     *     {@link AccountingField }
     *     
     */
    public AccountingField getAccountingField() {
        return accountingField;
    }

    /**
     * Sets the value of the accountingField property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountingField }
     *     
     */
    public void setAccountingField(AccountingField value) {
        this.accountingField = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the displayIndex property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDisplayIndex() {
        return displayIndex;
    }

    /**
     * Sets the value of the displayIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDisplayIndex(Integer value) {
        this.displayIndex = value;
    }

    /**
     * Gets the value of the elementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElementId() {
        return elementId;
    }

    /**
     * Sets the value of the elementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElementId(String value) {
        this.elementId = value;
    }

}
