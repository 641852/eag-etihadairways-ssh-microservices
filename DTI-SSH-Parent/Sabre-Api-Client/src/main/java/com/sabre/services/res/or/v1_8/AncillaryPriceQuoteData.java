
package com.sabre.services.res.or.v1_8;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AncillaryPriceQuoteData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AncillaryPriceQuoteData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PriceQuoteId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProductFeeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExpirationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="LrecAssociation" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="PriceQuoteIdCompressed" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AncillaryPriceQuoteData", propOrder = {
    "priceQuoteId",
    "productFeeNumber",
    "expirationDate",
    "lrecAssociation"
})
public class AncillaryPriceQuoteData {

    @XmlElement(name = "PriceQuoteId")
    protected String priceQuoteId;
    @XmlElement(name = "ProductFeeNumber")
    protected String productFeeNumber;
    @XmlElement(name = "ExpirationDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDate;
    @XmlElement(name = "LrecAssociation")
    protected Integer lrecAssociation;
    @XmlAttribute(name = "PriceQuoteIdCompressed")
    protected Boolean priceQuoteIdCompressed;

    /**
     * Gets the value of the priceQuoteId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriceQuoteId() {
        return priceQuoteId;
    }

    /**
     * Sets the value of the priceQuoteId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriceQuoteId(String value) {
        this.priceQuoteId = value;
    }

    /**
     * Gets the value of the productFeeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductFeeNumber() {
        return productFeeNumber;
    }

    /**
     * Sets the value of the productFeeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductFeeNumber(String value) {
        this.productFeeNumber = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the lrecAssociation property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLrecAssociation() {
        return lrecAssociation;
    }

    /**
     * Sets the value of the lrecAssociation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLrecAssociation(Integer value) {
        this.lrecAssociation = value;
    }

    /**
     * Gets the value of the priceQuoteIdCompressed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPriceQuoteIdCompressed() {
        return priceQuoteIdCompressed;
    }

    /**
     * Sets the value of the priceQuoteIdCompressed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPriceQuoteIdCompressed(Boolean value) {
        this.priceQuoteIdCompressed = value;
    }

}
