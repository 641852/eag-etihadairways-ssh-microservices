/**
 * 
 */
package com.etihad.book.getreservationdata.schema;

import java.util.List;

/**
 * @author Danish
 *
 */
public class FlightDetails {

	private String originCity;

	private String originCountry;

	private String originAirportName;

	private String originAirportCode;

	private String destinationCity;

	private String destinationCountry;

	private String destinationAirportName;

	private String destinationAirportCode;

	private String flightStartDate;

	private String flightStartTime;

	private String flightEndTime;
	
	private String flightEndDate;

	private String durationDiffDays;

	private String flightDuration;

	private String flightNumber;

	private String flightClass;

	private String aircraft;

	private String operatedBy;

	private String layoverTime;

	private String departureTerminalName;

	private String arrivalTerminalName;

	private String operatingAirlineCode;

	private String operatingAirlineShortName;

	private String operatingFlightNumber;

	private String marketingAirlineCode;

	private String marketingFlightNumber;

	private String operatingClassOfService;

	private String marketingClassOfService;
	
	private String segment;
	
	private String legs;
	
	private String poc;
	
	
	
	/**
	 * @return the flightEndDate
	 */
	public String getFlightEndDate() {
		return flightEndDate;
	}

	/**
	 * @param flightEndDate the flightEndDate to set
	 */
	public void setFlightEndDate(String flightEndDate) {
		this.flightEndDate = flightEndDate;
	}

	/**
	 * @return the poc
	 */
	public String getPoc() {
		return poc;
	}

	/**
	 * @param poc the poc to set
	 */
	public void setPoc(String poc) {
		this.poc = poc;
	}

	/**
	 * @return the segment
	 */
	public String getSegment() {
		return segment;
	}

	/**
	 * @param segment the segment to set
	 */
	public void setSegment(String segment) {
		this.segment = segment;
	}

	/**
	 * @return the legs
	 */
	public String getLegs() {
		return legs;
	}

	/**
	 * @param legs the legs to set
	 */
	public void setLegs(String legs) {
		this.legs = legs;
	}

	private List<PassengerDetails> paxDetails;

	/**
	 * @return the departureTerminalName
	 */
	public String getDepartureTerminalName() {
		return departureTerminalName;
	}

	/**
	 * @param departureTerminalName
	 *            the departureTerminalName to set
	 */
	public void setDepartureTerminalName(String departureTerminalName) {
		this.departureTerminalName = departureTerminalName;
	}

	/**
	 * @return the arrivalTerminalName
	 */
	public String getArrivalTerminalName() {
		return arrivalTerminalName;
	}

	/**
	 * @param arrivalTerminalName
	 *            the arrivalTerminalName to set
	 */
	public void setArrivalTerminalName(String arrivalTerminalName) {
		this.arrivalTerminalName = arrivalTerminalName;
	}

	/**
	 * @return the operatingAirlineCode
	 */
	public String getOperatingAirlineCode() {
		return operatingAirlineCode;
	}

	/**
	 * @param operatingAirlineCode
	 *            the operatingAirlineCode to set
	 */
	public void setOperatingAirlineCode(String operatingAirlineCode) {
		this.operatingAirlineCode = operatingAirlineCode;
	}

	/**
	 * @return the operatingAirlineShortName
	 */
	public String getOperatingAirlineShortName() {
		return operatingAirlineShortName;
	}

	/**
	 * @param operatingAirlineShortName
	 *            the operatingAirlineShortName to set
	 */
	public void setOperatingAirlineShortName(String operatingAirlineShortName) {
		this.operatingAirlineShortName = operatingAirlineShortName;
	}

	/**
	 * @return the operatingFlightNumber
	 */
	public String getOperatingFlightNumber() {
		return operatingFlightNumber;
	}

	/**
	 * @param operatingFlightNumber
	 *            the operatingFlightNumber to set
	 */
	public void setOperatingFlightNumber(String operatingFlightNumber) {
		this.operatingFlightNumber = operatingFlightNumber;
	}

	/**
	 * @return the marketingAirlineCode
	 */
	public String getMarketingAirlineCode() {
		return marketingAirlineCode;
	}

	/**
	 * @param marketingAirlineCode
	 *            the marketingAirlineCode to set
	 */
	public void setMarketingAirlineCode(String marketingAirlineCode) {
		this.marketingAirlineCode = marketingAirlineCode;
	}

	/**
	 * @return the marketingFlightNumber
	 */
	public String getMarketingFlightNumber() {
		return marketingFlightNumber;
	}

	/**
	 * @param marketingFlightNumber
	 *            the marketingFlightNumber to set
	 */
	public void setMarketingFlightNumber(String marketingFlightNumber) {
		this.marketingFlightNumber = marketingFlightNumber;
	}

	/**
	 * @return the operatingClassOfService
	 */
	public String getOperatingClassOfService() {
		return operatingClassOfService;
	}

	/**
	 * @param operatingClassOfService
	 *            the operatingClassOfService to set
	 */
	public void setOperatingClassOfService(String operatingClassOfService) {
		this.operatingClassOfService = operatingClassOfService;
	}

	/**
	 * @return the marketingClassOfService
	 */
	public String getMarketingClassOfService() {
		return marketingClassOfService;
	}

	/**
	 * @param marketingClassOfService
	 *            the marketingClassOfService to set
	 */
	public void setMarketingClassOfService(String marketingClassOfService) {
		this.marketingClassOfService = marketingClassOfService;
	}

	/**
	 * @return the paxDetails
	 */
	public List<PassengerDetails> getPaxDetails() {
		return paxDetails;
	}

	/**
	 * @param paxDetails
	 *            the paxDetails to set
	 */
	public void setPaxDetails(List<PassengerDetails> paxDetails) {
		this.paxDetails = paxDetails;
	}

	/**
	 * @return the originCity
	 */
	public String getOriginCity() {
		return originCity;
	}

	/**
	 * @param originCity
	 *            the originCity to set
	 */
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}

	/**
	 * @return the originCountry
	 */
	public String getOriginCountry() {
		return originCountry;
	}

	/**
	 * @param originCountry
	 *            the originCountry to set
	 */
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	/**
	 * @return the originAirportName
	 */
	public String getOriginAirportName() {
		return originAirportName;
	}

	/**
	 * @param originAirportName
	 *            the originAirportName to set
	 */
	public void setOriginAirportName(String originAirportName) {
		this.originAirportName = originAirportName;
	}

	/**
	 * @return the originAirportCode
	 */
	public String getOriginAirportCode() {
		return originAirportCode;
	}

	/**
	 * @param originAirportCode
	 *            the originAirportCode to set
	 */
	public void setOriginAirportCode(String originAirportCode) {
		this.originAirportCode = originAirportCode;
	}

	/**
	 * @return the destinationCity
	 */
	public String getDestinationCity() {
		return destinationCity;
	}

	/**
	 * @param destinationCity
	 *            the destinationCity to set
	 */
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	/**
	 * @return the destinationCountry
	 */
	public String getDestinationCountry() {
		return destinationCountry;
	}

	/**
	 * @param destinationCountry
	 *            the destinationCountry to set
	 */
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	/**
	 * @return the destinationAirportName
	 */
	public String getDestinationAirportName() {
		return destinationAirportName;
	}

	/**
	 * @param destinationAirportName
	 *            the destinationAirportName to set
	 */
	public void setDestinationAirportName(String destinationAirportName) {
		this.destinationAirportName = destinationAirportName;
	}

	/**
	 * @return the destinationAirportCode
	 */
	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}

	/**
	 * @param destinationAirportCode
	 *            the destinationAirportCode to set
	 */
	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}

	/**
	 * @return the flightStartDate
	 */
	public String getFlightStartDate() {
		return flightStartDate;
	}

	/**
	 * @param flightStartDate
	 *            the flightStartDate to set
	 */
	public void setFlightStartDate(String flightStartDate) {
		this.flightStartDate = flightStartDate;
	}

	/**
	 * @return the flightStartTime
	 */
	public String getFlightStartTime() {
		return flightStartTime;
	}

	/**
	 * @param flightStartTime
	 *            the flightStartTime to set
	 */
	public void setFlightStartTime(String flightStartTime) {
		this.flightStartTime = flightStartTime;
	}

	/**
	 * @return the flightEndTime
	 */
	public String getFlightEndTime() {
		return flightEndTime;
	}

	/**
	 * @param flightEndTime
	 *            the flightEndTime to set
	 */
	public void setFlightEndTime(String flightEndTime) {
		this.flightEndTime = flightEndTime;
	}

	/**
	 * @return the durationDiffDays
	 */
	public String getDurationDiffDays() {
		return durationDiffDays;
	}

	/**
	 * @param durationDiffDays
	 *            the durationDiffDays to set
	 */
	public void setDurationDiffDays(String durationDiffDays) {
		this.durationDiffDays = durationDiffDays;
	}

	/**
	 * @return the flightDuration
	 */
	public String getFlightDuration() {
		return flightDuration;
	}

	/**
	 * @param flightDuration
	 *            the flightDuration to set
	 */
	public void setFlightDuration(String flightDuration) {
		this.flightDuration = flightDuration;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the flightClass
	 */
	public String getFlightClass() {
		return flightClass;
	}

	/**
	 * @param flightClass
	 *            the flightClass to set
	 */
	public void setFlightClass(String flightClass) {
		this.flightClass = flightClass;
	}

	/**
	 * @return the aircraft
	 */
	public String getAircraft() {
		return aircraft;
	}

	/**
	 * @param aircraft
	 *            the aircraft to set
	 */
	public void setAircraft(String aircraft) {
		this.aircraft = aircraft;
	}

	/**
	 * @return the operatedBy
	 */
	public String getOperatedBy() {
		return operatedBy;
	}

	/**
	 * @param operatedBy
	 *            the operatedBy to set
	 */
	public void setOperatedBy(String operatedBy) {
		this.operatedBy = operatedBy;
	}

	/**
	 * @return the layoverTime
	 */
	public String getLayoverTime() {
		return layoverTime;
	}

	/**
	 * @param layoverTime
	 *            the layoverTime to set
	 */
	public void setLayoverTime(String layoverTime) {
		this.layoverTime = layoverTime;
	}

}
