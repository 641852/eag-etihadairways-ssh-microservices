/**
 * 
 */
package com.etihad.book.getreservationdata.schema;

import java.util.List;

/**
 * @author 526837
 *
 */
public class PassengerDetails2 {

	private String firstName;

	private String lastName;

	private String gender;

	private String title;

	private List<Meal> meal;

	private String loyaltyNumber;

	private List<SpecialRequest> specialRequest;

	private String emailAddress;

	/**
	 * @return the meal
	 */
	public List<Meal> getMeal() {
		return meal;
	}

	/**
	 * @param meal the meal to set
	 */
	public void setMeal(List<Meal> meal) {
		this.meal = meal;
	}

	/**
	 * @return the specialRequest
	 */
	public List<SpecialRequest> getSpecialRequest() {
		return specialRequest;
	}

	/**
	 * @param specialRequest the specialRequest to set
	 */
	public void setSpecialRequest(List<SpecialRequest> specialRequest) {
		this.specialRequest = specialRequest;
	}

	/**
	 * @return the seatNumber
	 */
	public List<Seat> getSeatNumber() {
		return seatNumber;
	}

	/**
	 * @param seatNumber the seatNumber to set
	 */
	public void setSeatNumber(List<Seat> seatNumber) {
		this.seatNumber = seatNumber;
	}

	private String flightDate;

	private String flightNumber;

	private List<Seat> seatNumber;

	private String seatDescription;

	/**
	 * @return the flightDate
	 */
	public String getFlightDate() {
		return flightDate;
	}

	/**
	 * @param flightDate
	 *            the flightDate to set
	 */
	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}

	/**
	 * @return the seatDescription
	 */
	public String getSeatDescription() {
		return seatDescription;
	}

	/**
	 * @param seatDescription
	 *            the seatDescription to set
	 */
	public void setSeatDescription(String seatDescription) {
		this.seatDescription = seatDescription;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the loyaltyNumber
	 */
	public String getLoyaltyNumber() {
		return loyaltyNumber;
	}

	/**
	 * @param loyaltyNumber
	 *            the loyaltyNumber to set
	 */
	public void setLoyaltyNumber(String loyaltyNumber) {
		this.loyaltyNumber = loyaltyNumber;
	}

}
