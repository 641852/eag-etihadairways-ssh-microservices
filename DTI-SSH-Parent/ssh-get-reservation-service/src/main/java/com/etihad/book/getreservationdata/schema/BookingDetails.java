/**
 * 
 */
package com.etihad.book.getreservationdata.schema;

/**
 * @author 526837
 *
 */
public class BookingDetails {

	private String bookingReference;

	private String originCity;
	
	private String originCityCode;

	private String originCountry;

	private String destinationCity;

	private String destinationCountry;
	
	private String destinationCityCode;

	private String journeyStartDate;

	private String journeyEndDate;

	private String primaryPaxName;
	
	private String primaryPaxLastName;
	
	private String primaryPaxFirstName;
	
	private String primaryPaxTitle;
	
	private Boolean isRoundTrip;
	
	/**
	 * @return the destinationCityCode
	 */
	public String getDestinationCityCode() {
		return destinationCityCode;
	}

	/**
	 * @param destinationCityCode the destinationCityCode to set
	 */
	public void setDestinationCityCode(String destinationCityCode) {
		this.destinationCityCode = destinationCityCode;
	}

	/**
	 * @return the originCityCode
	 */
	public String getOriginCityCode() {
		return originCityCode;
	}

	/**
	 * @param originCityCode the originCityCode to set
	 */
	public void setOriginCityCode(String originCityCode) {
		this.originCityCode = originCityCode;
	}

	/**
	 * @return the isRoundTrip
	 */
	public Boolean getIsRoundTrip() {
		return isRoundTrip;
	}

	/**
	 * @param isRoundTrip the isRoundTrip to set
	 */
	public void setIsRoundTrip(Boolean isRoundTrip) {
		this.isRoundTrip = isRoundTrip;
	}

	/**
	 * @return the bookingReference
	 */
	public String getBookingReference() {
		return bookingReference;
	}

	/**
	 * @param bookingReference
	 *            the bookingReference to set
	 */
	public void setBookingReference(String bookingReference) {
		this.bookingReference = bookingReference;
	}

	/**
	 * @return the originCity
	 */
	public String getOriginCity() {
		return originCity;
	}

	/**
	 * @param originCity
	 *            the originCity to set
	 */
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}

	/**
	 * @return the originCountry
	 */
	public String getOriginCountry() {
		return originCountry;
	}

	/**
	 * @param originCountry
	 *            the originCountry to set
	 */
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	/**
	 * @return the destinationCity
	 */
	public String getDestinationCity() {
		return destinationCity;
	}

	/**
	 * @param destinationCity
	 *            the destinationCity to set
	 */
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	/**
	 * @return the destinationCountry
	 */
	public String getDestinationCountry() {
		return destinationCountry;
	}

	/**
	 * @param destinationCountry
	 *            the destinationCountry to set
	 */
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	/**
	 * @return the journeyStartDate
	 */
	public String getJourneyStartDate() {
		return journeyStartDate;
	}

	/**
	 * @param journeyStartDate
	 *            the journeyStartDate to set
	 */
	public void setJourneyStartDate(String journeyStartDate) {
		this.journeyStartDate = journeyStartDate;
	}

	/**
	 * @return the journeyEndDate
	 */
	public String getJourneyEndDate() {
		return journeyEndDate;
	}

	/**
	 * @param journeyEndDate
	 *            the journeyEndDate to set
	 */
	public void setJourneyEndDate(String journeyEndDate) {
		this.journeyEndDate = journeyEndDate;
	}

	/**
	 * @return the primaryPaxName
	 */
	public String getPrimaryPaxName() {
		return primaryPaxName;
	}

	/**
	 * @param primaryPaxName
	 *            the primaryPaxName to set
	 */
	public void setPrimaryPaxName(String primaryPaxName) {
		this.primaryPaxName = primaryPaxName;
	}

	/**
	 * @return the primaryPaxLastName
	 */
	public String getPrimaryPaxLastName() {
		return primaryPaxLastName;
	}

	/**
	 * @param primaryPaxLastName the primaryPaxLastName to set
	 */
	public void setPrimaryPaxLastName(String primaryPaxLastName) {
		this.primaryPaxLastName = primaryPaxLastName;
	}

	/**
	 * @return the primaryPaxFirstName
	 */
	public String getPrimaryPaxFirstName() {
		return primaryPaxFirstName;
	}

	/**
	 * @param primaryPaxFirstName the primaryPaxFirstName to set
	 */
	public void setPrimaryPaxFirstName(String primaryPaxFirstName) {
		this.primaryPaxFirstName = primaryPaxFirstName;
	}

	/**
	 * @return the primaryPaxTitle
	 */
	public String getPrimaryPaxTitle() {
		return primaryPaxTitle;
	}

	/**
	 * @param primaryPaxTitle the primaryPaxTitle to set
	 */
	public void setPrimaryPaxTitle(String primaryPaxTitle) {
		this.primaryPaxTitle = primaryPaxTitle;
	}

}
