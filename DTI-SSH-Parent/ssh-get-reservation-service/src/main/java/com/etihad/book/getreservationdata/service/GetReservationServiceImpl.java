package com.etihad.book.getreservationdata.service;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.etihad.book.getreservationdata.adaptor.GetReservationAdaptor;
import com.etihad.book.getreservationdata.adaptor.GetReservationFlightResponseAdaptor;
import com.etihad.book.getreservationdata.adaptor.GetReservationResponseAdaptor;
import com.etihad.book.getreservationdata.broker.GetReservationBroker;
import com.etihad.book.getreservationdata.broker.GetReservationMockBroker;
import com.etihad.book.getreservationdata.schema.GetReservationRequest;
import com.etihad.book.getreservationdata.schema.GetReservationResponse;
import com.etihad.book.getreservationdata.schema.ONDDataObj;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sabre.webservices.pnrbuilder.v1_17.GetReservationRQ;
import com.sabre.webservices.pnrbuilder.v1_17.GetReservationRS;

/**
 * @author CTS
 *
 */
@Service
public class GetReservationServiceImpl implements GetReservationService {

	@Autowired
	private GetReservationBroker getReservationBroker;

	@Autowired
	private GetReservationMockBroker getReservationMockBroker;

	@Autowired
	private GetReservationAdaptor getReservationAdaptor;
	
	@Autowired
	private GetReservationResponseAdaptor getReservationResponseAdaptor;
	
	@Autowired
	private GetReservationFlightResponseAdaptor getReservationFlightResponseAdaptor;
	
	@Autowired
	private Utils utils;
	
	@Autowired
	private Config config;
	
	public static final Map<String,String> referenceDataMap = new HashMap();
	
	public static final Map<String ,Map<String,ONDDataObj>> langOndMap = new HashMap<>();
	
	private static final String ENG_OND_CODE = "en-ae";
	private static final String ARA_OND_CODE = "ar-ae";



	public GetReservationResponse getReservationData(GetReservationRequest request) throws ApplicationException {

		GetReservationResponse response = null;
		GetReservationRQ soapRequest;
		GetReservationRS soapResponse;
		List<String> errorCodes = validateForGetReservationData(request);
		if (!errorCodes.isEmpty()) {
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		soapRequest = getReservationAdaptor.createSOAPRequestForGetReservationData(request);
		try {
			soapResponse = getReservationBroker.processGetReservationData(soapRequest,
					request.getSearchParameters().getBinarySecurityToken());
			
			//soapResponse = getReservationMockBroker.processGetReservationData();
		} catch (Exception e) {
			errorCodes.add("pnrretrieval.101");
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		response = getReservationFlightResponseAdaptor.processJSONResponseForGetReservationData(
				soapResponse,request.getSearchParameters().getLastName(),request.getSearchParameters().getLangCode());
		response.setMessages(utils.populateResponseHeader(errorCodes));
		return response;

	}

	/**
	 * This method validates the PNR enquiry request
	 * 
	 * @param request
	 * @return
	 */
	private List<String> validateForGetReservationData(GetReservationRequest request) {

		List<String> errorCodes = new ArrayList<>(5);
		if (null == request) {
			errorCodes.add("get-reservation.103");
		} else {
			if (null == request.getSearchParameters()) {
				errorCodes.add("get-reservation.104");
			} else {

				if (StringUtils.isEmpty(request.getSearchParameters().getPnr())) {
					errorCodes.add("get-reservation.105");
				}
			}
		}
		return errorCodes;
	}
	
	@PostConstruct
	public void loadStaticData()
	{
		String line = null;
		InputStream in = null;
        OutputStream out = null;
        BufferedReader br = null;
        try {
	            in = getClass().getResourceAsStream("/ReferenceData.csv"); 
	            File file = File.createTempFile("tempfile", ".tmp");
	            out = new FileOutputStream(file);
	            int read;
	            byte[] bytes = new byte[1024];
	
	            while ((read = in.read(bytes)) != -1) {
	                out.write(bytes, 0, read);
	            }
	    	    br = new BufferedReader(new FileReader(file));
    	    	while ((line = br.readLine()) != null) {
    	    		line = line.substring(1, line.length()-1);
    	            String[] data = line.split(","); 
    	            referenceDataMap.put(data[0], data[1]);
	    	    }
    	    	file.deleteOnExit();
        	}
        catch(Exception e)
        {
        	e.printStackTrace();
        }
        finally 
        {
    		if (in != null)
    		{
    			try 
    			{
    				in.close();
    			} catch (IOException e) 
    			{
    				e.printStackTrace();
    			}
    		}
    		if (out != null) 
    		{
    			try 
    			{
    				out.close();
    			} catch (IOException e) 
    			{
    				e.printStackTrace();
    			}
    		}
    		if (br != null) 
    		{
    			try 
    			{
    				br.close();
    			} catch (IOException e) 
    			{
    				e.printStackTrace();
    			}
    		}
        }
	    try {
        	Map<String,ONDDataObj> engDataMap = getONDData(config.getProperty("refData.ond.url"));
        	Map<String,ONDDataObj> araDataMap = getONDData(config.getProperty("refData.ond.arabic.url"));
        	
        	langOndMap.put(ENG_OND_CODE, engDataMap);
        	langOndMap.put(ARA_OND_CODE, araDataMap);
		} catch (Exception e) {
		}
	}  
	
	   private static Map<String,ONDDataObj> getONDData(String ondUrl) throws IOException {
	    	
	    	Map<String,ONDDataObj> dataMap = new HashMap<>();
	    	URL url;
	    	HttpURLConnection request = null;
			try {
				url = new URL(ondUrl);
				request = (HttpURLConnection) url.openConnection();
				request.connect();
			} catch (Exception e) {
			}

	        JsonParser jp = new JsonParser(); 
	        if( null != request)
	        {
		        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); 
		        JsonObject rootobj = root.getAsJsonObject();
		        JsonArray dataJ =	rootobj.getAsJsonArray("Data");
		        List<String> aprStr = new ArrayList();
		        for (JsonElement jsonElement : dataJ) {
		        	JsonObject objectData = jsonElement.getAsJsonObject();
		        	ONDDataObj dataObject = new ONDDataObj();
		        	dataObject.setCode(objectData.get("Code").getAsString());
		        	dataObject.setName(objectData.get("Name").getAsString());
		        	JsonArray aprData =	objectData.get("APR").getAsJsonArray();
		        	for (JsonElement jsonElement2 : aprData) {
		        		aprStr.add(jsonElement2.getAsString());
		        		dataObject.setApr(aprStr);
		        	}
		        	dataObject.setGaEnabled(objectData.get("GAEnabled").getAsBoolean());
		        	dataObject.setCityName(objectData.get("CityName").getAsString());
		        	dataObject.setCityCode(objectData.get("CityCode").getAsString());
		        	dataObject.setCountryCode(objectData.get("CountryCode").getAsString());
		        	dataObject.setCountryName(objectData.get("CountryName").getAsString());
		        	dataObject.setAirportName(objectData.get("AirportName").getAsString());
		        	dataObject.setUpdatedLocationName(objectData.get("UpdatedLocationName").getAsString());
					dataMap.put(dataObject.getCode(), dataObject);
		        }
	        }
			return dataMap;
	    }
	 }