/**
 * 
 */
package com.etihad.book.getreservationdata.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.getreservationdata.schema.GetReservationRequest;
import com.etihad.book.getreservationdata.schema.GetReservationResponse;
import com.etihad.book.getreservationdata.service.GetReservationServiceImpl;



/**
 * @author CTS
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/service/v1")
public class GetReservationController {
	

	@Autowired
	private GetReservationServiceImpl getReservationService;
	
	/**
	 * This method will be used to map the request to a specific URI and return the response 
	 * @return
	 * @throws ApplicationException 
	 */
	@CrossOrigin
	@RequestMapping(path="/get-reservation", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public GetReservationResponse getReservationData(@RequestBody GetReservationRequest request) throws ApplicationException  {
		return getReservationService.getReservationData(request);

	}


}
