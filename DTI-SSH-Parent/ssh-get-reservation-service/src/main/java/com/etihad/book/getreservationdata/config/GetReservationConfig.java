/**
 * 
 */
package com.etihad.book.getreservationdata.config;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

import com.etihad.book.getreservationdata.handler.LoggingHandler;


/**
 * @author CTS
 *
 */
@Configuration
public class GetReservationConfig {
	
	
	@Bean(name = "getReservationProxy")
    public JaxWsPortProxyFactoryBean createProxy() throws MalformedURLException {
        JaxWsPortProxyFactoryBean bean = new JaxWsPortProxyFactoryBean();
            bean.setServiceInterface(com.sabre.webservices.pnrbuilder.getres.GetReservationPortType.class);
            /*URL url = null;
            ClassLoader classLoader = GetReservationConfig.class.getClassLoader();
    		File file = new File(classLoader.getResource("GetReservation_1.17.0.wsdl").getFile());
    		url = file.toURI().toURL();*/
    		bean.setWsdlDocumentUrl(new URL("http://files.developer.sabre.com/wsdl/sabreXML1.0.00/pnrservices/GetReservation_1.17.0.wsdl"));
            bean.setNamespaceUri("http://webservices.sabre.com/pnrbuilder/getres");
            bean.setServiceName("GetReservationService");
            bean.setPortName("GetReservationPortType");
            bean.setEndpointAddress("https://sws-crt.cert.sabre.com/EY");
            bean.setLookupServiceOnStartup(false);
            bean.setHandlerResolver(handlerResolver());
        return bean;
    }
	
	 public HandlerResolver handlerResolver() {
		    return portInfo -> {
			    @SuppressWarnings("rawtypes")
				List<Handler> handlerChain = new ArrayList<>();
			    handlerChain.add(new LoggingHandler());
			    return handlerChain;
			};
		 }
}