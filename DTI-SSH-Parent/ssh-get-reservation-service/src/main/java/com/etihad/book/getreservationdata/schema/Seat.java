/**
 * 
 */
package com.etihad.book.getreservationdata.schema;

/**
 * @author 526837
 *
 */
public class Seat {
	
	private String seat;

	/**
	 * @return the seat
	 */
	public String getSeat() {
		return seat;
	}

	/**
	 * @param seat the seat to set
	 */
	public void setSeat(String seat) {
		this.seat = seat;
	}
	
	

}
