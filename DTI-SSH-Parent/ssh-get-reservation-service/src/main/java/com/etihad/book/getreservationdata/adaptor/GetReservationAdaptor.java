/**
 * 
 */
package com.etihad.book.getreservationdata.adaptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.etihad.book.getreservationdata.schema.BookingDetails;
import com.etihad.book.getreservationdata.schema.FlightDetails;
import com.etihad.book.getreservationdata.schema.GetReservationRequest;
import com.etihad.book.getreservationdata.schema.GetReservationResponse;
import com.etihad.book.getreservationdata.schema.PassengerDetails;
import com.sabre.services.res.or.v1_11.OpenReservationElementType;
import com.sabre.services.res.or.v1_11.SegmentAssociation;
import com.sabre.services.res.or.v1_11.SegmentAssociationTag;
import com.sabre.webservices.pnrbuilder.v1_17.APISRequestPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.AirType.MarriageGrp;
import com.sabre.webservices.pnrbuilder.v1_17.BookingDetailsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.BookingDetailsPNRB.FlightsRange;
import com.sabre.webservices.pnrbuilder.v1_17.DOCSEntryPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.FrequentFlyerPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.GenderDOCSEntryPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.GenericSpecialRequestPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.GetReservationRQ;
import com.sabre.webservices.pnrbuilder.v1_17.GetReservationRSType;
import com.sabre.webservices.pnrbuilder.v1_17.NameAssociatedSpecialRequestsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.ObjectFactory;
import com.sabre.webservices.pnrbuilder.v1_17.OpenReservationElementsType;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerReservationPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengersPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PreReservedSeatPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PreReservedSeatsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.ReservationPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SeatsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB.Air;
import com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB.Segment;
import com.sabre.webservices.pnrbuilder.v1_17.SpecialMealRequestPNRB;

/**
 * @author CTS
 *
 */
@Component
public class GetReservationAdaptor {

	@Autowired
	private Config config;

	@Autowired
	private Utils utils;

	Map<String, PassengerDetails> mapSeatWithPassenger = new HashMap<>();
	List<PassengerDetails> passengerDetailsList = new ArrayList<>();
	List<FlightDetails> flightDetailsList = new ArrayList<>();
	PassengerDetails paxDetails;
	FlightDetails flightDetails;
	GetReservationResponse response = new GetReservationResponse();
	BookingDetails bookingData = new BookingDetails();

	public GetReservationRQ createSOAPRequestForGetReservationData(GetReservationRequest request)
			throws ApplicationException {

		final ObjectFactory factory = new ObjectFactory();
		GetReservationRQ soapRequest = factory.createGetReservationRQ();
		soapRequest.setVersion("1.17.0");
		soapRequest.setLocator(request.getSearchParameters().getPnr());
		return soapRequest;
	}

	public GetReservationResponse processJSONResponseForGetReservationData(GetReservationRSType soapResponse) {

		if (null != soapResponse) {
			ReservationPNRB reservation = soapResponse.getReservation();

			if (null != reservation) {
				bookingData = setBookingDetails(reservation);
				Integer numberInParty = reservation.getNumberInParty();
				Integer numberInSegment = reservation.getNumberInSegment();
				Integer numberOfInfants = reservation.getNumberOfInfants();
				paxDetails = setPassengerDetails(reservation);
			}
			response.setBookingDetails(bookingData);
		}

		return response;

	}

	private PassengerDetails setPassengerDetails(ReservationPNRB reservation) {

		PassengerReservationPNRB passengerReservation = reservation.getPassengerReservation();

		if (null != passengerReservation) {

			PassengersPNRB passengers = passengerReservation.getPassengers();

			if (null != passengers) {
				setPassengers(passengers);
			}
			setJourneyDetails(passengerReservation);
		}
		return paxDetails;
	}

	private void setPassengers(PassengersPNRB passengers) {

		PassengerDetails paxDetails;
		List<PassengerPNRB> passengerList = passengers.getPassenger();
		if (null != passengerList) {
			for (PassengerPNRB passenger : passengerList) {
				if (null != passenger) {
					paxDetails = new PassengerDetails();
					paxDetails.setLastName(passenger.getLastName());
					paxDetails.setFirstName(passenger.getFirstName());
					NameAssociatedSpecialRequestsPNRB specialRequests = passenger.getSpecialRequests();
					if (null != specialRequests) {
						setSpecialRequests(specialRequests, paxDetails);
						setAPISRequests(specialRequests, paxDetails);
						setMeals(specialRequests, paxDetails);
					}
					List<FrequentFlyerPNRB> frequentFlyer = passenger.getFrequentFlyer();
					String gender = passenger.getGender();
					String passengerType = passenger.getPassengerType();
					// PhoneNumbersPNRB phoneNumbers =
					// passenger.getPhoneNumbers();
					SeatsPNRB seats = passenger.getSeats();
					if (null != seats) {
						setSeats(seats, paxDetails);
					}
					String title = passenger.getTitle();
					Boolean withInfant = passenger.isWithInfant();
					XMLGregorianCalendar dateOfBirth = passenger.getDateOfBirth();
				}
			}
		}

	}

	private void setSeats(SeatsPNRB seats, PassengerDetails paxDetails) {

		PreReservedSeatsPNRB preReservedSeats = seats.getPreReservedSeats();
		if (null != preReservedSeats) {
			List<PreReservedSeatPNRB> preReservedSeatList = preReservedSeats.getPreReservedSeat();
			for (PreReservedSeatPNRB preReservedSeat : preReservedSeatList) {
				if (null != preReservedSeat) {
					paxDetails.setSeatNumber(preReservedSeat.getSeatNumber());
					preReservedSeat.getSeatStatusCode();
					paxDetails.setSeatDescription(preReservedSeat.getSeatTypeCode());
					preReservedSeat.isSmokingPrefOfferedIndicator();
					mapSeatWithPassenger.put(preReservedSeat.getSeatNumber(), paxDetails);
				}
			}
		}
	}

	private void setMeals(NameAssociatedSpecialRequestsPNRB specialRequests, PassengerDetails paxDetails) {

		List<SpecialMealRequestPNRB> specialMealRequestList = specialRequests.getSpecialMealRequest();
		if (null != specialMealRequestList) {
			for (SpecialMealRequestPNRB specialMealRequest : specialMealRequestList) {
				if (null != specialMealRequest) {
					paxDetails.setMeal(specialMealRequest.getMealType());
					paxDetails.setFlightNumber(specialMealRequest.getFlightNumber());
					specialMealRequest.getVendorCode();
					paxDetails.setFlightDate(specialMealRequest.getFlightDate());
					specialMealRequest.getBoardCity();
					specialMealRequest.getActionCode();
					specialMealRequest.getOffCity();
					specialMealRequest.getNumberInParty();
				}
			}
		}
	}

	private void setAPISRequests(NameAssociatedSpecialRequestsPNRB specialRequests, PassengerDetails paxDetails) {

		List<APISRequestPNRB> apisRequestList = specialRequests.getAPISRequest();
		if (null != apisRequestList) {
			for (APISRequestPNRB apisRequest : apisRequestList) {

				if (null != apisRequest) {
					DOCSEntryPNRB docsEntry = apisRequest.getDOCSEntry();
					if (null != docsEntry) {
						XMLGregorianCalendar dateOfBirth = docsEntry.getDateOfBirth();
						GenderDOCSEntryPNRB gender = docsEntry.getGender();
						if (null != gender) {
							paxDetails.setGender(gender.value());
						}
						String actionCode = docsEntry.getActionCode();
						String countryOfIssue = docsEntry.getCountryOfIssue();
						String freeText = docsEntry.getFreeText();
						String vendorCode = docsEntry.getVendorCode();
						String surname = docsEntry.getSurname();
						String forename = docsEntry.getForename();
						String middleName = docsEntry.getMiddleName();
					}
				}
			}
		}
	}

	private void setSpecialRequests(NameAssociatedSpecialRequestsPNRB specialRequests, PassengerDetails paxDetails) {

		List<GenericSpecialRequestPNRB> genericSpecialRequestList = specialRequests.getGenericSpecialRequest();

		if (null != genericSpecialRequestList) {
			for (GenericSpecialRequestPNRB genericSpecialRequest : genericSpecialRequestList) {
				if (null != genericSpecialRequest) {
					genericSpecialRequest.getActionCode();
					genericSpecialRequest.getAirlineCode();
					genericSpecialRequest.getFreeText();
					genericSpecialRequest.getFullText();
				}
			}
		}
	}

	private BookingDetails setBookingDetails(ReservationPNRB reservation) {

		BookingDetailsPNRB bookingDetails = reservation.getBookingDetails();

		if (null != bookingDetails)
			bookingData.setBookingReference(bookingDetails.getRecordLocator());
		FlightsRange flightsRange = bookingDetails.getFlightsRange();
		if (null != flightsRange) {
			bookingData.setJourneyStartDate(utils.formatXMLDate(flightsRange.getStart()));
			// int timezone = flightsRange.getStart().getTimezone();
			bookingData.setJourneyEndDate(utils.formatXMLDate(flightsRange.getEnd()));
		}
		return bookingData;
	}

	private void setJourneyDetails(PassengerReservationPNRB passengerReservation) {

		SegmentTypePNRB segments = passengerReservation.getSegments();
		if (null != segments) {
			List<Segment> segmentList = segments.getSegment();
			if (null != segmentList && !segmentList.isEmpty()) {
				for (Segment segment : segmentList) {
					if (null != segment) {
						com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB.Segment.Air air = segment.getAir();
						if (null != air) {
							flightDetails = new FlightDetails();
							flightDetails.setOriginAirportCode(air.getDepartureAirport());
							air.getDepartureAirportCodeContext();
							flightDetails.setDestinationAirportCode(air.getArrivalAirport());
							air.getArrivalAirportCodeContext();
							flightDetails.setOperatedBy(air.getOperatingAirlineCode());
							flightDetails.setFlightNumber(air.getOperatingFlightNumber());
							flightDetails.setAircraft(air.getEquipmentType());
							air.getMarketingAirlineCode();
							flightDetails.setFlightNumber(air.getMarketingFlightNumber());
							flightDetails.setFlightClass(air.getOperatingClassOfService());
							air.getMarketingClassOfService();
							MarriageGrp marriageGrp = air.getMarriageGrp();
							if (null != marriageGrp) {
								marriageGrp.getInd();
								marriageGrp.getGroup();
								marriageGrp.getSequence();
							}

							flightDetails.setFlightStartDate(air.getDepartureDateTime());
							flightDetails.setFlightEndTime(air.getArrivalDateTime());
							flightDetails.setFlightNumber(air.getFlightNumber());
							flightDetails.setFlightClass(air.getClassOfService());
							air.getActionCode();
							air.getNumberInParty();

							SeatsPNRB seats = air.getSeats();
							if (null != seats) {
								passengerDetailsList = setFlightSeats(seats);
							}
							flightDetails.setPaxDetails(passengerDetailsList);
							flightDetailsList.add(flightDetails);
						}
					}
				}
			}
		}
		response.setFlightDetails(flightDetailsList);
	}

	private List<PassengerDetails> setFlightSeats(SeatsPNRB seats) {
		PreReservedSeatsPNRB preReservedSeats = seats.getPreReservedSeats();
		if (null != preReservedSeats) {
			List<PreReservedSeatPNRB> preReservedSeatList = preReservedSeats.getPreReservedSeat();
			if (null != preReservedSeatList) {
				for (PreReservedSeatPNRB preReservedSeat : preReservedSeatList) {
					if (null != preReservedSeat) {
						String seatNumber = preReservedSeat.getSeatNumber();
						PassengerDetails passengerDetails = mapSeatWithPassenger.get(preReservedSeat.getSeatNumber());
						passengerDetailsList.add(passengerDetails);
					}
				}
			}
		}
		return passengerDetailsList;
	}
}
