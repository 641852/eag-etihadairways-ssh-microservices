/**
 * 
 */
package com.etihad.book.getreservationdata.adaptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.util.Utils;
import com.etihad.book.getreservationdata.schema.BookingDetails;
import com.etihad.book.getreservationdata.schema.FlightDetails;
import com.etihad.book.getreservationdata.schema.GetReservationResponse;
import com.etihad.book.getreservationdata.schema.Meal;
import com.etihad.book.getreservationdata.schema.PassengerDetails;
import com.etihad.book.getreservationdata.schema.PassengerDetails2;
import com.etihad.book.getreservationdata.schema.Seat;
import com.etihad.book.getreservationdata.schema.SpecialRequest;
import com.sabre.webservices.pnrbuilder.v1_17.AirType.MarriageGrp;
import com.sabre.webservices.pnrbuilder.v1_17.BookingDetailsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.BookingDetailsPNRB.FlightsRange;
import com.sabre.webservices.pnrbuilder.v1_17.GenericSpecialRequestPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.GetReservationRSType;
import com.sabre.webservices.pnrbuilder.v1_17.NameAssociatedSpecialRequestsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerReservationPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengersPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PreReservedSeatPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PreReservedSeatsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.ReservationPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SeatsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB.Segment;
import com.sabre.webservices.pnrbuilder.v1_17.SpecialMealRequestPNRB;

/**
 * @author CTS
 *
 */
@Component
public class GetReservationResponseAdaptor {

	@Autowired
	private Config config;

	@Autowired
	private Utils utils;

	Map<String, PassengerDetails2> mapSeatWithPassenger = new TreeMap<>();
	List<PassengerDetails> passengerDetailsList = new ArrayList<>();
	List<FlightDetails> flightDetailsList = new ArrayList<>();
	PassengerDetails paxDetails;
	FlightDetails flightDetails;
	GetReservationResponse response = new GetReservationResponse();
	BookingDetails bookingData = new BookingDetails();
	List<PassengerDetails2> passengerDetailsList2 = new ArrayList<>();
	Map<String, List<SpecialMealRequestPNRB>> mapMealWithPassenger = new HashMap<>();
	Meal meal;
	List<Meal> mealsList = new ArrayList<>();

	public GetReservationResponse processJSONResponseForGetReservationData(GetReservationRSType soapResponse) {

		if (null != soapResponse) {
			ReservationPNRB reservation = soapResponse.getReservation();
			if (null != reservation) {
				bookingData = setBookingDetails(reservation);
				PassengerReservationPNRB passengerReservation = reservation.getPassengerReservation();
				if (null != passengerReservation) {
					PassengersPNRB passengers = passengerReservation.getPassengers();
					if (null != passengers) {
						List<PassengerPNRB> passengerList = passengers.getPassenger();
						if (null != passengerList && !passengerList.isEmpty()) {
							passengerDetailsList2 = setPassengersList(passengerList);
						}
					}
					setJourneyDetails(passengerReservation, passengerDetailsList2);
				}
			}
			response.setBookingDetails(bookingData);
			response.setPaxDetails(passengerDetailsList2);
		}
		return response;
	}

	private List<PassengerDetails2> setPassengersList(List<PassengerPNRB> passengerList) {

		List<PassengerDetails2> passengersDetailsList = new ArrayList<>();
		PassengerDetails2 paxDetails = null;
		List<SpecialRequest> specialRequestList;
		List<Meal> mealList;
		List<Seat> seatList;
		for (PassengerPNRB passenger : passengerList) {
			if (null != passenger) {
				paxDetails = new PassengerDetails2();
				paxDetails.setLastName(passenger.getLastName());
				paxDetails.setFirstName(passenger.getFirstName());
				paxDetails.setGender(passenger.getGender());
				NameAssociatedSpecialRequestsPNRB specialRequests = passenger.getSpecialRequests();
				if (null != specialRequests) {
					List<GenericSpecialRequestPNRB> genericSpecialRequestList = specialRequests
							.getGenericSpecialRequest();
					if (null != genericSpecialRequestList && !genericSpecialRequestList.isEmpty()) {
						specialRequestList = setSpecialRequestsList(genericSpecialRequestList);
						paxDetails.setSpecialRequest(specialRequestList);
					}
					List<SpecialMealRequestPNRB> specialMealRequestList = specialRequests.getSpecialMealRequest();
					if (null != specialMealRequestList && !specialMealRequestList.isEmpty()) {
						mealList = setMealsList(specialMealRequestList, paxDetails.getLastName());
						paxDetails.setMeal(mealList);
					}
				}
				SeatsPNRB seats = passenger.getSeats();
				if (null != seats) {
					PreReservedSeatsPNRB preReservedSeats = seats.getPreReservedSeats();
					if (null != preReservedSeats) {
						List<PreReservedSeatPNRB> preReservedSeatList = preReservedSeats.getPreReservedSeat();
						if (null != preReservedSeatList && !preReservedSeatList.isEmpty()) {
							seatList = setSeatsList(preReservedSeatList, paxDetails);
							paxDetails.setSeatNumber(seatList);
						}
					}
				}
				passengersDetailsList.add(paxDetails);
			}
		}
		return passengersDetailsList;
	}

	private List<Seat> setSeatsList(List<PreReservedSeatPNRB> preReservedSeatList, PassengerDetails2 paxDetails) {
		List<Seat> seatsList = new ArrayList<>();
		Seat seat;
		for (PreReservedSeatPNRB preReservedSeat : preReservedSeatList) {
			if (null != preReservedSeat) {
				seat = new Seat();
				seat.setSeat(preReservedSeat.getSeatNumber());
				seatsList.add(seat);
				mapSeatWithPassenger.put(preReservedSeat.getSeatNumber(), paxDetails);
			}
		}
		return seatsList;
	}

	private List<Meal> setMealsList(List<SpecialMealRequestPNRB> specialMealRequestList, String lastName) {

		for (SpecialMealRequestPNRB specialMealRequest : specialMealRequestList) {
			if (null != specialMealRequest) {
				meal = new Meal();
				meal.setMeal(specialMealRequest.getMealType());
				mealsList.add(meal);
				mapMealWithPassenger.put(lastName, specialMealRequestList);
			}
		}
		return mealsList;
	}

	private BookingDetails setBookingDetails(ReservationPNRB reservation) {

		BookingDetailsPNRB bookingDetails = reservation.getBookingDetails();
		if (null != bookingDetails) {
			bookingData.setBookingReference(bookingDetails.getRecordLocator());
			FlightsRange flightsRange = bookingDetails.getFlightsRange();
			if (null != flightsRange) {
				bookingData.setJourneyStartDate(utils.formatXMLDate(flightsRange.getStart()));
				bookingData.setJourneyEndDate(utils.formatXMLDate(flightsRange.getEnd()));
			}
		}
		return bookingData;
	}

	private List<SpecialRequest> setSpecialRequestsList(List<GenericSpecialRequestPNRB> genericSpecialRequestList) {
		List<SpecialRequest> specialRequestsList = new ArrayList<>();
		SpecialRequest specialRequest;
		for (GenericSpecialRequestPNRB genericSpecialRequest : genericSpecialRequestList) {
			if (null != genericSpecialRequest) {
				specialRequest = new SpecialRequest();
				specialRequest.setSpecialRequest(genericSpecialRequest.getCode());
				specialRequestsList.add(specialRequest);
			}
		}
		return specialRequestsList;
	}

	private void setJourneyDetails(PassengerReservationPNRB passengerReservation,
			List<PassengerDetails2> passengerDetailsList2) {

		SegmentTypePNRB segments = passengerReservation.getSegments();
		if (null != segments) {
			List<Segment> segmentList = segments.getSegment();
			if (null != segmentList && !segmentList.isEmpty()) {
				for (Segment segment : segmentList) {
					if (null != segment) {
						com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB.Segment.Air air = segment.getAir();
						if (null != air) {
							flightDetails = new FlightDetails();
							flightDetails.setOriginAirportCode(air.getDepartureAirport());
							air.getDepartureAirportCodeContext();
							flightDetails.setDestinationAirportCode(air.getArrivalAirport());
							air.getArrivalAirportCodeContext();
							flightDetails.setOperatedBy(air.getOperatingAirlineCode());
							flightDetails.setFlightNumber(air.getOperatingFlightNumber());
							flightDetails.setAircraft(air.getEquipmentType());
							air.getMarketingAirlineCode();
							flightDetails.setFlightNumber(air.getMarketingFlightNumber());
							flightDetails.setFlightClass(air.getOperatingClassOfService());
							air.getMarketingClassOfService();
							MarriageGrp marriageGrp = air.getMarriageGrp();
							if (null != marriageGrp) {
								marriageGrp.getInd();
								marriageGrp.getGroup();
								marriageGrp.getSequence();
							}

							flightDetails.setFlightStartDate(air.getDepartureDateTime());
							flightDetails.setFlightEndTime(air.getArrivalDateTime());
							flightDetails.setFlightNumber(air.getFlightNumber());
							flightDetails.setFlightClass(air.getClassOfService());
							air.getActionCode();
							air.getNumberInParty();

							SeatsPNRB seats = air.getSeats();
							if (null != seats) {
								PreReservedSeatsPNRB preReservedSeats = seats.getPreReservedSeats();
								if (null != preReservedSeats) {
									List<PreReservedSeatPNRB> preReservedSeatList = preReservedSeats
											.getPreReservedSeat();
									if (null != preReservedSeatList) {
										passengerDetailsList = setFlightSeats(preReservedSeatList,
												passengerDetailsList2, flightDetails);
									}
								}
							}
							flightDetails.setPaxDetails(passengerDetailsList);
							flightDetailsList.add(flightDetails);
						}
					}
				}
			}
		}
		mapSeatWithPassenger.clear();
		mapMealWithPassenger.clear();
		response.setFlightDetails(flightDetailsList);
	}

	private List<PassengerDetails> setFlightSeats(List<PreReservedSeatPNRB> preReservedSeatList,
			List<PassengerDetails2> paxDetails2List, FlightDetails flightDetails2) {
		List<PassengerDetails> passengerDetailsList = new ArrayList<>();
		PassengerDetails passengerDetails;
		for (PreReservedSeatPNRB preReservedSeat : preReservedSeatList) {
			if (null != preReservedSeat) {
				passengerDetails = new PassengerDetails();
				String seatNumber = preReservedSeat.getSeatNumber();
				PassengerDetails2 passengerDetails2 = getPassengerData(seatNumber);
				passengerDetails.setFirstName(passengerDetails2.getFirstName());
				passengerDetails.setLastName(passengerDetails2.getLastName());
				passengerDetails.setMeal(getMealsData(flightDetails2, passengerDetails.getLastName()));
				passengerDetailsList.add(passengerDetails);
				/*
				 * for (PassengerDetails2 paxDetails : paxDetails2List) {
				 * List<Seat> seatNumberList = paxDetails.getSeatNumber(); if
				 * (null != seatNumberList && seatNumberList.isEmpty()) { for
				 * (Seat seat : seatNumberList) { if
				 * (seat.getSeat().equals(seatNumber)) {
				 * passengerDetails.setFirstName(paxDetails.getFirstName());
				 * passengerDetailsList.add(passengerDetails); } break; } } }
				 */
			} /*
				 * else {
				 * passengerDetails.setFirstName(paxDetails.getFirstName());
				 * passengerDetailsList.add(passengerDetails); }
				 */
		}
		return passengerDetailsList;
	}

	private String getMealsData(FlightDetails flightDetails, String lastName) {
		String mealName = null;
		List<SpecialMealRequestPNRB> mappedMealsList = mapMealWithPassenger.get(lastName);
		for (SpecialMealRequestPNRB meal : mappedMealsList) {
			if (flightDetails.getFlightNumber().equals(meal.getFlightNumber())
					&& flightDetails.getFlightStartDate().substring(0, 10).equals(meal.getFlightDate())) {
				mealName = meal.getMealType();
				break;
			}
		}
		return mealName;
	}

	private PassengerDetails2 getPassengerData(String seatNumber) {
		PassengerDetails2 passengerDetails2 = mapSeatWithPassenger.get(seatNumber);
		return passengerDetails2;
	}
}
