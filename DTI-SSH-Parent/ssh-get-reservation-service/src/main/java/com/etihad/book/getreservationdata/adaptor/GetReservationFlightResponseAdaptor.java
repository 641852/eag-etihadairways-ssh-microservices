/**
 * 
 */
package com.etihad.book.getreservationdata.adaptor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.etihad.book.common.util.Utils;
import com.etihad.book.getreservationdata.schema.BookingDetails;
import com.etihad.book.getreservationdata.schema.FlightDetails;
import com.etihad.book.getreservationdata.schema.GetReservationResponse;
import com.etihad.book.getreservationdata.schema.ONDDataObj;
import com.etihad.book.getreservationdata.service.GetReservationServiceImpl;
import com.sabre.webservices.pnrbuilder.v1_17.BookingDetailsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.BookingDetailsPNRB.FlightsRange;
import com.sabre.webservices.pnrbuilder.v1_17.GetReservationRSType;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerReservationPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.ReservationPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB.Segment;

/**
 * @author CTS
 *
 */
@Component
public class GetReservationFlightResponseAdaptor {

	@Autowired
	private Utils utils;

	Map<String, String> flightLayoverMap = new HashMap<>();

	public GetReservationResponse processJSONResponseForGetReservationData(GetReservationRSType soapResponse,
			String lastName, String langCode) {

		GetReservationResponse response = new GetReservationResponse();
		BookingDetails bookingData = new BookingDetails();
		List<FlightDetails> flightDetailsList = new ArrayList<>();
		if (null != soapResponse) {
			ReservationPNRB reservation = soapResponse.getReservation();
			if (null != reservation) {
				PassengerReservationPNRB passengerReservation = reservation.getPassengerReservation();
				if (null != passengerReservation) {
					flightDetailsList = setJourneyDetails(passengerReservation, langCode);
				}
				bookingData = setBookingDetails(reservation, lastName, flightDetailsList, langCode);
			}
			response.setBookingDetails(bookingData);
			response.setFlightDetails(flightDetailsList);
		}
		return response;
	}

	private BookingDetails setBookingDetails(ReservationPNRB reservation, String lastName,
			List<FlightDetails> flightDetailsList, String langCode) {

		BookingDetails bookingData = new BookingDetails();
		BookingDetailsPNRB bookingDetails = reservation.getBookingDetails();
		if (null != bookingDetails) {
			bookingData.setBookingReference(bookingDetails.getRecordLocator());
			bookingData.setIsRoundTrip(checkRoundTrip(reservation));
			String originAirportCode = getOriginAirportCodeForBookingSummary(flightDetailsList);
			bookingData.setOriginCityCode(originAirportCode);
			Map<String, ONDDataObj> ondMap = GetReservationServiceImpl.langOndMap.get(langCode);
			ONDDataObj originDataObj = ondMap.get(originAirportCode);
			if (null != originDataObj) {
				bookingData.setOriginCity(originDataObj.getCityName());
				bookingData.setOriginCountry(originDataObj.getCountryName());
			}
			String departureAirportCode = getDestinationAirportCodeForBookingSummary(flightDetailsList,
					bookingData.getIsRoundTrip());
			bookingData.setDestinationCityCode(departureAirportCode);
			ONDDataObj destDataObj = ondMap.get(departureAirportCode);
			if (null != destDataObj) {
				bookingData.setDestinationCity(destDataObj.getCityName());
				bookingData.setDestinationCountry(destDataObj.getCountryName());
			}
			FlightsRange flightsRange = bookingDetails.getFlightsRange();
			if (null != flightsRange) {
				bookingData.setJourneyStartDate(flightsRange.getStart().toString());
				bookingData.setJourneyEndDate(flightsRange.getEnd().toString());
				PassengerPNRB passengerObj = getPrimaryPassengerDetails(reservation, lastName);
				if(!org.springframework.util.StringUtils.isEmpty(passengerObj)) {
					bookingData.setPrimaryPaxLastName(passengerObj.getLastName());
					bookingData.setPrimaryPaxFirstName(null != getPassengerFirstName(passengerObj.getFirstName())
							? getPassengerFirstName(passengerObj.getFirstName()) : passengerObj.getFirstName());
					bookingData.setPrimaryPaxTitle(null != passengerObj.getTitle() ? passengerObj.getTitle()
							: getPassengerTitle(passengerObj.getFirstName()));
				}
			}
		}
		return bookingData;
	}

	private String getPassengerFirstName(String firstName) {

		String name = null;

		Map<String, String> mapTitle = new HashMap<>();
		mapTitle.put("MR", "Mr.");
		mapTitle.put("MRS", "Mrs.");
		mapTitle.put("MS", "Ms.");
		mapTitle.put("MISS", "Miss");
		mapTitle.put("HE", "HE");
		mapTitle.put("HH", "HH");
		mapTitle.put("HRH", "HRH");
		mapTitle.put("DR", "Dr");
		mapTitle.put("PROF", "Prof");
		mapTitle.put("SHEK", "Sheikh");
		mapTitle.put("SHKA", "Shaikha");
		mapTitle.put("MSTR", "Master");

		for (String str : mapTitle.keySet()) {
			String firstNameUpper = firstName.toUpperCase();
			if (firstNameUpper.contains(str)) {
				int startIndex = firstNameUpper.indexOf(str);
				name = firstNameUpper.substring(0, startIndex - 1);
				break;
			}
		}
		return name;
	}

	private String getPassengerTitle(String primaryPaxFirstName) {

		String title = null;
		Map<String, String> mapTitle = new HashMap<>();
		mapTitle.put("MR", "Mr.");
		mapTitle.put("MRS", "Mrs.");
		mapTitle.put("MS", "Ms.");
		mapTitle.put("MISS", "Miss");
		mapTitle.put("HE", "HE");
		mapTitle.put("HH", "HH");
		mapTitle.put("HRH", "HRH");
		mapTitle.put("DR", "Dr");
		mapTitle.put("PROF", "Prof");
		mapTitle.put("SHEK", "Sheikh");
		mapTitle.put("SHKA", "Shaikha");
		mapTitle.put("MSTR", "Master");

		for (String str : mapTitle.keySet()) {
			String firstNameUpper = primaryPaxFirstName.toUpperCase();
			if (firstNameUpper.contains(str)) {
				int startIndex = firstNameUpper.indexOf(str);
				title = firstNameUpper.substring(startIndex, firstNameUpper.length());
				break;
			}
		}
		return mapTitle.get(title);
	}

	private String getDestinationAirportCodeForBookingSummary(List<FlightDetails> flightDetailsList,
			Boolean isRoundTrip) {
		String destinationAirportCode = null;
		if (isRoundTrip) {
			destinationAirportCode = flightDetailsList.get((flightDetailsList.size() / 2) - 1)
					.getDestinationAirportCode();
		} else {
			destinationAirportCode = flightDetailsList.get(flightDetailsList.size() - 1).getDestinationAirportCode();
		}

		return destinationAirportCode;
	}

	private String getOriginAirportCodeForBookingSummary(List<FlightDetails> flightDetailsList) {
		String originAirportCode = null;
		for (FlightDetails flightDetails : flightDetailsList) {
			originAirportCode = flightDetails.getPoc();
		}
		return originAirportCode;
	}

	private Boolean checkRoundTrip(ReservationPNRB reservation) {

		boolean isRoundTrip = false;
		if (null != reservation) {
			PassengerReservationPNRB passengerReservation = reservation.getPassengerReservation();
			if (null != passengerReservation) {
				SegmentTypePNRB segments = passengerReservation.getSegments();
				if (null != segments) {
					List<Segment> segmentList = segments.getSegment();
					String poc = segments.getPoc().getAirport();
					if (null != segmentList && !segmentList.isEmpty()) {
						for (Segment segment : segmentList) {
							if (null != segment) {
								com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB.Segment.Air air = segment
										.getAir();
								if (null != air) {
									if (poc.equals(air.getArrivalAirport())) {
										isRoundTrip = true;
									}
								}
							}
						}
					}
				}
			}
		}
		return isRoundTrip;
	}

	private PassengerPNRB getPrimaryPassengerDetails(ReservationPNRB reservation, String lastName) {
		List<PassengerPNRB> passenegerList = reservation.getPassengerReservation().getPassengers().getPassenger();

		PassengerPNRB passengerObj = new PassengerPNRB();
		if (null != passenegerList && !passenegerList.isEmpty()) {
			for (PassengerPNRB passengerPNRB : passenegerList) {
				String refNum = passengerPNRB.getReferenceNumber();
				if (StringUtils.isNotEmpty(lastName)
						&& lastName.equals(passengerPNRB.getLastName())) {
					passengerObj = passengerPNRB;
					break;
				}
			}
		}
		return passengerObj;
	}

	private List<FlightDetails> setJourneyDetails(PassengerReservationPNRB passengerReservation, String langCode) {

		FlightDetails flightDetails;
		List<FlightDetails> flightDetailsList = new ArrayList<>();
		SegmentTypePNRB segments = passengerReservation.getSegments();
		ONDDataObj originDataObj = null;
		ONDDataObj destDataObj = null;
		Map<String, ONDDataObj> ondMap = null;
		if (null != segments) {
			List<Segment> segmentList = segments.getSegment();
			if (null != segmentList && !segmentList.isEmpty()) {
				for (Segment segment : segmentList) {
					if (null != segment) {
						com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB.Segment.Air air = segment.getAir();
						if (null != air) {
							if (air.getActionCode().equals("WK")) {
								continue;
							}
							flightDetails = new FlightDetails();
							flightDetails.setPoc(segments.getPoc().getAirport());
							flightDetails.setOriginAirportCode(air.getDepartureAirport());
							ondMap = GetReservationServiceImpl.langOndMap.get(langCode);
							originDataObj = ondMap.get(air.getDepartureAirport());
							flightDetails.setOriginCity(originDataObj.getCityName());
							flightDetails.setOriginAirportName(originDataObj.getAirportName());
							flightDetails.setOriginCountry(originDataObj.getCountryName());
							flightDetails.setDestinationAirportCode(air.getArrivalAirport());
							destDataObj = ondMap.get(air.getArrivalAirport());
							flightDetails.setDestinationAirportName(destDataObj.getAirportName());
							flightDetails.setDestinationCity(destDataObj.getCityName());
							flightDetails.setDestinationCountry(destDataObj.getCountryName());
							flightDetails.setOperatedBy(air.getOperatingAirlineCode());
							String equipmentType = GetReservationServiceImpl.referenceDataMap
									.get(air.getEquipmentType());
							flightDetails.setAircraft(null != equipmentType ? equipmentType : air.getEquipmentType());
							flightDetails.setMarketingAirlineCode(air.getMarketingAirlineCode());
							flightDetails.setFlightNumber(air.getFlightNumber());
							String flightClass = GetReservationServiceImpl.referenceDataMap
									.get(air.getClassOfService());
							flightDetails.setFlightClass(null != flightClass ? flightClass : air.getClassOfService());
							flightDetails.setFlightStartDate(air.getDepartureDateTime().substring(0, 10));
							flightDetails.setFlightStartTime(
									air.getDepartureDateTime().substring(11, air.getDepartureDateTime().length()));
							flightDetails.setFlightEndDate(air.getArrivalDateTime().substring(0, 10));
							flightDetails.setFlightEndTime(
									air.getArrivalDateTime().substring(11, air.getArrivalDateTime().length()));
							flightDetails.setFlightNumber(air.getFlightNumber());
							flightDetails.setDepartureTerminalName(air.getDepartureTerminalName());
							flightDetails.setArrivalTerminalName(air.getArrivalTerminalName());
							flightDetails.setOperatingAirlineCode(air.getOperatingAirlineCode());
							flightDetails.setOperatingAirlineShortName(air.getOperatingAirlineShortName());
							flightDetails.setOperatingFlightNumber(air.getOperatingFlightNumber());
							flightDetails.setMarketingAirlineCode(air.getMarketingAirlineCode());
							flightDetails.setMarketingFlightNumber(air.getMarketingFlightNumber());
							flightDetails.setOperatingClassOfService(air.getOperatingClassOfService());
							flightDetails.setMarketingClassOfService(air.getMarketingClassOfService());
							flightDetails.setSegment(air.getMarriageGrp().getGroup());
							flightDetails.setLegs(air.getMarriageGrp().getSequence());
							flightDetails.setFlightDuration(calculateDuration(air.getArrivalDateTime(),
									air.getDepartureDateTime(), air.getArrivalAirport(), air.getDepartureAirport()));
							flightDetails.setDurationDiffDays(calculateDurationDaysDifference(air.getArrivalDateTime(),
									air.getDepartureDateTime()));
							flightDetails.setLayoverTime(calculateLayOver(segmentList, air.getDepartureDateTime()));
							flightDetailsList.add(flightDetails);
						}
					}
				}
			}
		}
		return flightDetailsList;
	}

	private String calculateDurationDaysDifference(String arrivalDateTime, String departureDateTime) {

		Integer daysDifference = (Integer.parseInt(arrivalDateTime.substring(8, 10)))
				- (Integer.parseInt(departureDateTime.substring(8, 10)));
		return daysDifference + "";
	}

	private String calculateDuration(String arrivalDateTime, String departureDateTime, String arrivalAirportCode,
			String departureAirportCode) {
		String changedArrivalDate = utils.changeTimezoneFrom(arrivalDateTime, "yyyy-MM-dd'T'HH:mm:ss",
				getTimeZoneForAirportCode(arrivalAirportCode));
		String changedDepartureDate = utils.changeTimezoneFrom(departureDateTime, "yyyy-MM-dd'T'HH:mm:ss",
				getTimeZoneForAirportCode(departureAirportCode));
		return differenceTime(changedArrivalDate, changedDepartureDate);
	}

	private String getTimeZoneForAirportCode(String airportCode) {
		return "Asia/Dubai";
	}

	private static String differenceTime(String changedArrivalDate, String changedDepartureDate) {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date date1 = null;
		Date date2 = null;
		try {
			date1 = format.parse(changedArrivalDate);
			date2 = format.parse(changedDepartureDate);
		} catch (ParseException e) {
			//
		}

		long difference = date1.getTime() - date2.getTime();

		return difference / (1000 * 60) + "";
	}

	private String calculateLayOver(List<Segment> segmentList, String departureDate) {

		String layover;

		flightLayoverMap = calculateLayOverFromMap(segmentList);
		layover = flightLayoverMap.get(departureDate);
		if (!StringUtils.isEmpty(layover)) {
			return layover;
		} else {
			return layover;
		}

	}

	private Map<String, String> calculateLayOverFromMap(List<Segment> segmentList) {

		String layOverTime = null;
		if (segmentList.size() > 1) {
			for (int i = 0; i < segmentList.size() - 1; i++) {
				Segment segment = segmentList.get(i);
				String gropup = segment.getAir().getMarriageGrp().getGroup();
				String arrivalAirport = segment.getAir().getArrivalAirport();
				String flightNumber = segment.getAir().getFlightNumber();
				Segment nextSegment = segmentList.get(i + 1);
				String nextGroup = nextSegment.getAir().getMarriageGrp().getGroup();
				String flightNumberNext = nextSegment.getAir().getFlightNumber();
				String departureAirport = nextSegment.getAir().getDepartureAirport();
				if (gropup.equals(nextGroup) && arrivalAirport.equals(departureAirport)) {
					if (!flightNumber.equals(flightNumberNext)
							&& (!segment.getAir().getMarriageGrp().getGroup().equals("0"))
							&& (!segment.getAir().getMarriageGrp().getSequence().equals("0"))) {
						layOverTime = calculateLayoverTime(segment.getAir().getArrivalDateTime(),
								nextSegment.getAir().getDepartureDateTime());
						flightLayoverMap.put(segment.getAir().getDepartureDateTime(), layOverTime);
					}
				}
			}
		}
		return flightLayoverMap;
	}

	private String calculateLayoverTime(String arrivalDateTime, String departureDateTime) {

		String str3 = arrivalDateTime.substring(11, arrivalDateTime.length());

		String str4 = departureDateTime.substring(11, departureDateTime.length());
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
		Date date1 = null;
		Date date2 = null;
		try {
			date1 = format.parse(str3);
			date2 = format.parse(str4);
		} catch (ParseException e) {
			//
		}

		long difference = date2.getTime() - date1.getTime();

		return difference / (1000 * 60) + "";
	}

}
