/**
 * 
 */
package com.etihad.book.getreservationdata.schema;

/**
 * @author 526837
 *
 */
public class SpecialRequest {
	
	private String specialRequest;

	/**
	 * @return the specialRequest
	 */
	public String getSpecialRequest() {
		return specialRequest;
	}

	/**
	 * @param specialRequest the specialRequest to set
	 */
	public void setSpecialRequest(String specialRequest) {
		this.specialRequest = specialRequest;
	}
	
	

}
