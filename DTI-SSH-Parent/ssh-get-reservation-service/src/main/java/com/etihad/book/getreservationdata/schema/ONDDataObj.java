package com.etihad.book.getreservationdata.schema;

import java.util.ArrayList;
import java.util.List;

/**
 * ONDDataObj class will be used for mapping OND data objects
 * 
 * @author anil kumar
 *
 */
public class ONDDataObj {

	private String code;
	private String name;
	private List<String> apr = new ArrayList<>();
	private boolean gaEnabled;
	private String cityName;
	private String cityCode;
	private String countryName;
	private String countryCode;
	private String airportName;
	private String updatedLocationName;
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the apr
	 */
	public List<String> getApr() {
		return apr;
	}
	/**
	 * @param apr the apr to set
	 */
	public void setApr(List<String> apr) {
		this.apr = apr;
	}
	/**
	 * @return the gaEnabled
	 */
	public boolean isGaEnabled() {
		return gaEnabled;
	}
	/**
	 * @param gaEnabled the gaEnabled to set
	 */
	public void setGaEnabled(boolean gaEnabled) {
		this.gaEnabled = gaEnabled;
	}
	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}
	/**
	 * @param cityName the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	/**
	 * @return the cityCode
	 */
	public String getCityCode() {
		return cityCode;
	}
	/**
	 * @param cityCode the cityCode to set
	 */
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}
	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	/**
	 * @return the cirportName
	 */
	public String getAirportName() {
		return airportName;
	}
	/**
	 * @param airportName the airportName to set
	 */
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
	/**
	 * @return the updatedLocationName
	 */
	public String getUpdatedLocationName() {
		return updatedLocationName;
	}
	/**
	 * @param updatedLocationName the updatedLocationName to set
	 */
	public void setUpdatedLocationName(String updatedLocationName) {
		this.updatedLocationName = updatedLocationName;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ONDDataObj [code=" + code + ", name=" + name + ", apr=" + apr + ", gaEnabled=" + gaEnabled
				+ ", cityName=" + cityName + ", cityCode=" + cityCode + ", countryName=" + countryName
				+ ", countryCode=" + countryCode + ", airportName=" + airportName + ", updatedLocationName="
				+ updatedLocationName + "]";
	}
	
		
}
