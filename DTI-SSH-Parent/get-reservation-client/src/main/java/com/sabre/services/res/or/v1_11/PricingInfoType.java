
package com.sabre.services.res.or.v1_11;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PricingInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PricingInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PriceSummary" type="{http://services.sabre.com/res/or/v1_11}PriceSummaryType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PriceProvider" type="{http://services.sabre.com/res/or/v1_11}PricingProviderType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PricingInfoType", propOrder = {
    "priceSummary",
    "priceProvider"
})
public class PricingInfoType {

    @XmlElement(name = "PriceSummary")
    protected List<PriceSummaryType> priceSummary;
    @XmlElement(name = "PriceProvider", required = true)
    protected PricingProviderType priceProvider;

    /**
     * Gets the value of the priceSummary property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the priceSummary property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPriceSummary().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PriceSummaryType }
     * 
     * 
     */
    public List<PriceSummaryType> getPriceSummary() {
        if (priceSummary == null) {
            priceSummary = new ArrayList<PriceSummaryType>();
        }
        return this.priceSummary;
    }

    /**
     * Gets the value of the priceProvider property.
     * 
     * @return
     *     possible object is
     *     {@link PricingProviderType }
     *     
     */
    public PricingProviderType getPriceProvider() {
        return priceProvider;
    }

    /**
     * Sets the value of the priceProvider property.
     * 
     * @param value
     *     allowed object is
     *     {@link PricingProviderType }
     *     
     */
    public void setPriceProvider(PricingProviderType value) {
        this.priceProvider = value;
    }

}
