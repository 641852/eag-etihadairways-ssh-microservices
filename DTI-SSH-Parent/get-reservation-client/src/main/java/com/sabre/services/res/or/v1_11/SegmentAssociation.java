
package com.sabre.services.res.or.v1_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SegmentAssociation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SegmentAssociation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice minOccurs="0">
 *         &lt;element name="AirSegment" type="{http://services.sabre.com/res/or/v1_11}SegmentAssociationTag"/>
 *       &lt;/choice>
 *       &lt;attGroup ref="{http://services.sabre.com/res/or/v1_11}SegmentAssociationsAttributes"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SegmentAssociation", propOrder = {
    "airSegment"
})
public class SegmentAssociation {

    @XmlElement(name = "AirSegment")
    protected SegmentAssociationTag airSegment;
    @XmlAttribute(name = "Id")
    protected String id;
    @XmlAttribute(name = "SegmentAssociationId")
    protected String segmentAssociationId;
    @XmlAttribute(name = "Sequence")
    protected String sequence;

    /**
     * Gets the value of the airSegment property.
     * 
     * @return
     *     possible object is
     *     {@link SegmentAssociationTag }
     *     
     */
    public SegmentAssociationTag getAirSegment() {
        return airSegment;
    }

    /**
     * Sets the value of the airSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link SegmentAssociationTag }
     *     
     */
    public void setAirSegment(SegmentAssociationTag value) {
        this.airSegment = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the segmentAssociationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmentAssociationId() {
        return segmentAssociationId;
    }

    /**
     * Sets the value of the segmentAssociationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmentAssociationId(String value) {
        this.segmentAssociationId = value;
    }

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequence() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequence(String value) {
        this.sequence = value;
    }

}
