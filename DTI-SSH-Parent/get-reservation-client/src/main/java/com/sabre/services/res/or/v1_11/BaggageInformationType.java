
package com.sabre.services.res.or.v1_11;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Information about baggage
 * 
 * <p>Java class for BaggageInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaggageInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Allowance" maxOccurs="99">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="passengerType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="pieces" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="weight" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="unit">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;enumeration value="kg"/>
 *                       &lt;enumeration value="lbs"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaggageInformationType", propOrder = {
    "allowance"
})
public class BaggageInformationType {

    @XmlElement(name = "Allowance", required = true)
    protected List<BaggageInformationType.Allowance> allowance;

    /**
     * Gets the value of the allowance property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allowance property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllowance().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BaggageInformationType.Allowance }
     * 
     * 
     */
    public List<BaggageInformationType.Allowance> getAllowance() {
        if (allowance == null) {
            allowance = new ArrayList<BaggageInformationType.Allowance>();
        }
        return this.allowance;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="passengerType" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="pieces" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="weight" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="unit">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;enumeration value="kg"/>
     *             &lt;enumeration value="lbs"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Allowance {

        @XmlAttribute(name = "passengerType")
        protected String passengerType;
        @XmlAttribute(name = "pieces")
        protected Integer pieces;
        @XmlAttribute(name = "weight")
        protected Integer weight;
        @XmlAttribute(name = "unit")
        protected String unit;

        /**
         * Gets the value of the passengerType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPassengerType() {
            return passengerType;
        }

        /**
         * Sets the value of the passengerType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPassengerType(String value) {
            this.passengerType = value;
        }

        /**
         * Gets the value of the pieces property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getPieces() {
            return pieces;
        }

        /**
         * Sets the value of the pieces property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setPieces(Integer value) {
            this.pieces = value;
        }

        /**
         * Gets the value of the weight property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getWeight() {
            return weight;
        }

        /**
         * Sets the value of the weight property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setWeight(Integer value) {
            this.weight = value;
        }

        /**
         * Gets the value of the unit property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnit() {
            return unit;
        }

        /**
         * Sets the value of the unit property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnit(String value) {
            this.unit = value;
        }

    }

}
