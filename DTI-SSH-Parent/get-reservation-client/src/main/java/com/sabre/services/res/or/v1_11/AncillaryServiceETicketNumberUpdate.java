
package com.sabre.services.res.or.v1_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AncillaryServiceETicketNumberUpdate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AncillaryServiceETicketNumberUpdate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ETicketNumber" type="{http://services.sabre.com/res/or/v1_11}CommonString" minOccurs="0"/>
 *         &lt;element name="ETicketCoupon" type="{http://services.sabre.com/res/or/v1_11}CommonString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AncillaryServiceETicketNumberUpdate", propOrder = {
    "eTicketNumber",
    "eTicketCoupon"
})
public class AncillaryServiceETicketNumberUpdate {

    @XmlElement(name = "ETicketNumber")
    protected String eTicketNumber;
    @XmlElement(name = "ETicketCoupon")
    protected String eTicketCoupon;

    /**
     * Gets the value of the eTicketNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getETicketNumber() {
        return eTicketNumber;
    }

    /**
     * Sets the value of the eTicketNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setETicketNumber(String value) {
        this.eTicketNumber = value;
    }

    /**
     * Gets the value of the eTicketCoupon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getETicketCoupon() {
        return eTicketCoupon;
    }

    /**
     * Sets the value of the eTicketCoupon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setETicketCoupon(String value) {
        this.eTicketCoupon = value;
    }

}
