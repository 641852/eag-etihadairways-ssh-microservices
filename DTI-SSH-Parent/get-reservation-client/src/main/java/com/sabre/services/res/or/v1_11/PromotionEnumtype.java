
package com.sabre.services.res.or.v1_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PromotionEnumtype.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PromotionEnumtype">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="C"/>
 *     &lt;enumeration value="L"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PromotionEnumtype")
@XmlEnum
public enum PromotionEnumtype {

    C,
    L;

    public String value() {
        return name();
    }

    public static PromotionEnumtype fromValue(String v) {
        return valueOf(v);
    }

}
