
package com.sabre.services.res.or.v1_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PriceSummaryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PriceSummaryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BasePrice" type="{http://services.sabre.com/res/or/v1_11}DecimalPrice" minOccurs="0"/>
 *         &lt;element name="EquivPrice" type="{http://services.sabre.com/res/or/v1_11}DecimalPrice" minOccurs="0"/>
 *         &lt;element name="TotalTaxes" type="{http://services.sabre.com/res/or/v1_11}TaxesType" minOccurs="0"/>
 *         &lt;element name="TotalPrice" type="{http://services.sabre.com/res/or/v1_11}DecimalPrice"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="refreshTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceSummaryType", propOrder = {
    "basePrice",
    "equivPrice",
    "totalTaxes",
    "totalPrice"
})
public class PriceSummaryType {

    @XmlElement(name = "BasePrice")
    protected DecimalPrice basePrice;
    @XmlElement(name = "EquivPrice")
    protected DecimalPrice equivPrice;
    @XmlElement(name = "TotalTaxes")
    protected TaxesType totalTaxes;
    @XmlElement(name = "TotalPrice", required = true)
    protected DecimalPrice totalPrice;
    @XmlAttribute(name = "id")
    protected Integer id;
    @XmlAttribute(name = "refreshTimestamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar refreshTimestamp;

    /**
     * Gets the value of the basePrice property.
     * 
     * @return
     *     possible object is
     *     {@link DecimalPrice }
     *     
     */
    public DecimalPrice getBasePrice() {
        return basePrice;
    }

    /**
     * Sets the value of the basePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link DecimalPrice }
     *     
     */
    public void setBasePrice(DecimalPrice value) {
        this.basePrice = value;
    }

    /**
     * Gets the value of the equivPrice property.
     * 
     * @return
     *     possible object is
     *     {@link DecimalPrice }
     *     
     */
    public DecimalPrice getEquivPrice() {
        return equivPrice;
    }

    /**
     * Sets the value of the equivPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link DecimalPrice }
     *     
     */
    public void setEquivPrice(DecimalPrice value) {
        this.equivPrice = value;
    }

    /**
     * Gets the value of the totalTaxes property.
     * 
     * @return
     *     possible object is
     *     {@link TaxesType }
     *     
     */
    public TaxesType getTotalTaxes() {
        return totalTaxes;
    }

    /**
     * Sets the value of the totalTaxes property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxesType }
     *     
     */
    public void setTotalTaxes(TaxesType value) {
        this.totalTaxes = value;
    }

    /**
     * Gets the value of the totalPrice property.
     * 
     * @return
     *     possible object is
     *     {@link DecimalPrice }
     *     
     */
    public DecimalPrice getTotalPrice() {
        return totalPrice;
    }

    /**
     * Sets the value of the totalPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link DecimalPrice }
     *     
     */
    public void setTotalPrice(DecimalPrice value) {
        this.totalPrice = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setId(Integer value) {
        this.id = value;
    }

    /**
     * Gets the value of the refreshTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRefreshTimestamp() {
        return refreshTimestamp;
    }

    /**
     * Sets the value of the refreshTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRefreshTimestamp(XMLGregorianCalendar value) {
        this.refreshTimestamp = value;
    }

}
