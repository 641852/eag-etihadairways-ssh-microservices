
package com.sabre.services.res.or.v1_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * Indicates preferences for choice of airline cabin for a given travel situation.
 * 
 * <p>Java class for CabinPrefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CabinPrefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="preferLevel" type="{http://services.sabre.com/res/or/v1_11}PreferLevelType" default="Preferred" />
 *       &lt;attribute name="cabin" type="{http://services.sabre.com/res/or/v1_11}CabinNameType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CabinPrefType")
public class CabinPrefType {

    @XmlAttribute(name = "preferLevel")
    protected PreferLevelType preferLevel;
    @XmlAttribute(name = "cabin")
    protected CabinNameType cabin;

    /**
     * Gets the value of the preferLevel property.
     * 
     * @return
     *     possible object is
     *     {@link PreferLevelType }
     *     
     */
    public PreferLevelType getPreferLevel() {
        if (preferLevel == null) {
            return PreferLevelType.PREFERRED;
        } else {
            return preferLevel;
        }
    }

    /**
     * Sets the value of the preferLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferLevelType }
     *     
     */
    public void setPreferLevel(PreferLevelType value) {
        this.preferLevel = value;
    }

    /**
     * Gets the value of the cabin property.
     * 
     * @return
     *     possible object is
     *     {@link CabinNameType }
     *     
     */
    public CabinNameType getCabin() {
        return cabin;
    }

    /**
     * Sets the value of the cabin property.
     * 
     * @param value
     *     allowed object is
     *     {@link CabinNameType }
     *     
     */
    public void setCabin(CabinNameType value) {
        this.cabin = value;
    }

}
