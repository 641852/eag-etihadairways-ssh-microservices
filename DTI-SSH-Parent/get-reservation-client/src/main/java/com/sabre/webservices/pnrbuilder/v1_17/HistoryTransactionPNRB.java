
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for HistoryTransaction.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HistoryTransaction.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Passengers" type="{http://webservices.sabre.com/pnrbuilder/v1_17}Passengers.PNRB" minOccurs="0"/>
 *         &lt;element name="Segments" type="{http://webservices.sabre.com/pnrbuilder/v1_17}SegmentType.PNRB" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="historySequence" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" />
 *       &lt;attribute name="historyDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HistoryTransaction.PNRB", propOrder = {
    "passengers",
    "segments"
})
public class HistoryTransactionPNRB {

    @XmlElement(name = "Passengers")
    protected PassengersPNRB passengers;
    @XmlElement(name = "Segments")
    protected SegmentTypePNRB segments;
    @XmlAttribute(name = "historySequence")
    protected String historySequence;
    @XmlAttribute(name = "historyDateTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar historyDateTime;

    /**
     * Gets the value of the passengers property.
     * 
     * @return
     *     possible object is
     *     {@link PassengersPNRB }
     *     
     */
    public PassengersPNRB getPassengers() {
        return passengers;
    }

    /**
     * Sets the value of the passengers property.
     * 
     * @param value
     *     allowed object is
     *     {@link PassengersPNRB }
     *     
     */
    public void setPassengers(PassengersPNRB value) {
        this.passengers = value;
    }

    /**
     * Gets the value of the segments property.
     * 
     * @return
     *     possible object is
     *     {@link SegmentTypePNRB }
     *     
     */
    public SegmentTypePNRB getSegments() {
        return segments;
    }

    /**
     * Sets the value of the segments property.
     * 
     * @param value
     *     allowed object is
     *     {@link SegmentTypePNRB }
     *     
     */
    public void setSegments(SegmentTypePNRB value) {
        this.segments = value;
    }

    /**
     * Gets the value of the historySequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHistorySequence() {
        return historySequence;
    }

    /**
     * Sets the value of the historySequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHistorySequence(String value) {
        this.historySequence = value;
    }

    /**
     * Gets the value of the historyDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHistoryDateTime() {
        return historyDateTime;
    }

    /**
     * Sets the value of the historyDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHistoryDateTime(XMLGregorianCalendar value) {
        this.historyDateTime = value;
    }

}
