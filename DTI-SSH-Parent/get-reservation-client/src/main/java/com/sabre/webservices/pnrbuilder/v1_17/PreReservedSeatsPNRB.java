
package com.sabre.webservices.pnrbuilder.v1_17;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PreReservedSeats.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PreReservedSeats.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PreReservedSeat" type="{http://webservices.sabre.com/pnrbuilder/v1_17}PreReservedSeat.PNRB" maxOccurs="999" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PreReservedSeats.PNRB", propOrder = {
    "preReservedSeat"
})
public class PreReservedSeatsPNRB {

    @XmlElement(name = "PreReservedSeat")
    protected List<PreReservedSeatPNRB> preReservedSeat;

    /**
     * Gets the value of the preReservedSeat property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the preReservedSeat property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPreReservedSeat().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PreReservedSeatPNRB }
     * 
     * 
     */
    public List<PreReservedSeatPNRB> getPreReservedSeat() {
        if (preReservedSeat == null) {
            preReservedSeat = new ArrayList<PreReservedSeatPNRB>();
        }
        return this.preReservedSeat;
    }

}
