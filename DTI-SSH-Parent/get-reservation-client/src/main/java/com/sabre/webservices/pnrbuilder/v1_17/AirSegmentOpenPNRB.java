
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AirSegmentOpen.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AirSegmentOpen.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatusCode" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="UnitsBooked" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="AirlineDesignator" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="ClassOfService" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="DepartureDate" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="DepartureTime" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="DayOfWeekIndicator" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="SegmentSpecialRequests" type="{http://webservices.sabre.com/pnrbuilder/v1_17}ItinerarySpecialRequests.PNRB" minOccurs="0"/>
 *         &lt;element name="PreReservedSeats" type="{http://webservices.sabre.com/pnrbuilder/v1_17}PreReservedSeats.PNRB" minOccurs="0"/>
 *         &lt;element name="FrequentFlyer" type="{http://webservices.sabre.com/pnrbuilder/v1_17}Loyalty.PNRB" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="segmentNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_17}Numeric0to99999" />
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_17}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_17}OperationType.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirSegmentOpen.PNRB", propOrder = {
    "statusCode",
    "unitsBooked",
    "airlineDesignator",
    "classOfService",
    "departureDate",
    "departureTime",
    "dayOfWeekIndicator",
    "segmentSpecialRequests",
    "preReservedSeats",
    "frequentFlyer"
})
public class AirSegmentOpenPNRB {

    @XmlElement(name = "StatusCode")
    protected String statusCode;
    @XmlElement(name = "UnitsBooked")
    protected String unitsBooked;
    @XmlElement(name = "AirlineDesignator")
    protected String airlineDesignator;
    @XmlElement(name = "ClassOfService")
    protected String classOfService;
    @XmlElement(name = "DepartureDate")
    protected String departureDate;
    @XmlElement(name = "DepartureTime")
    protected String departureTime;
    @XmlElement(name = "DayOfWeekIndicator")
    protected String dayOfWeekIndicator;
    @XmlElement(name = "SegmentSpecialRequests")
    protected ItinerarySpecialRequestsPNRB segmentSpecialRequests;
    @XmlElement(name = "PreReservedSeats")
    protected PreReservedSeatsPNRB preReservedSeats;
    @XmlElement(name = "FrequentFlyer")
    protected LoyaltyPNRB frequentFlyer;
    @XmlAttribute(name = "segmentNumber")
    protected Integer segmentNumber;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;

    /**
     * Gets the value of the statusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the unitsBooked property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitsBooked() {
        return unitsBooked;
    }

    /**
     * Sets the value of the unitsBooked property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitsBooked(String value) {
        this.unitsBooked = value;
    }

    /**
     * Gets the value of the airlineDesignator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirlineDesignator() {
        return airlineDesignator;
    }

    /**
     * Sets the value of the airlineDesignator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirlineDesignator(String value) {
        this.airlineDesignator = value;
    }

    /**
     * Gets the value of the classOfService property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassOfService() {
        return classOfService;
    }

    /**
     * Sets the value of the classOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassOfService(String value) {
        this.classOfService = value;
    }

    /**
     * Gets the value of the departureDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureDate() {
        return departureDate;
    }

    /**
     * Sets the value of the departureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureDate(String value) {
        this.departureDate = value;
    }

    /**
     * Gets the value of the departureTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureTime() {
        return departureTime;
    }

    /**
     * Sets the value of the departureTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureTime(String value) {
        this.departureTime = value;
    }

    /**
     * Gets the value of the dayOfWeekIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDayOfWeekIndicator() {
        return dayOfWeekIndicator;
    }

    /**
     * Sets the value of the dayOfWeekIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDayOfWeekIndicator(String value) {
        this.dayOfWeekIndicator = value;
    }

    /**
     * Gets the value of the segmentSpecialRequests property.
     * 
     * @return
     *     possible object is
     *     {@link ItinerarySpecialRequestsPNRB }
     *     
     */
    public ItinerarySpecialRequestsPNRB getSegmentSpecialRequests() {
        return segmentSpecialRequests;
    }

    /**
     * Sets the value of the segmentSpecialRequests property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItinerarySpecialRequestsPNRB }
     *     
     */
    public void setSegmentSpecialRequests(ItinerarySpecialRequestsPNRB value) {
        this.segmentSpecialRequests = value;
    }

    /**
     * Gets the value of the preReservedSeats property.
     * 
     * @return
     *     possible object is
     *     {@link PreReservedSeatsPNRB }
     *     
     */
    public PreReservedSeatsPNRB getPreReservedSeats() {
        return preReservedSeats;
    }

    /**
     * Sets the value of the preReservedSeats property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreReservedSeatsPNRB }
     *     
     */
    public void setPreReservedSeats(PreReservedSeatsPNRB value) {
        this.preReservedSeats = value;
    }

    /**
     * Gets the value of the frequentFlyer property.
     * 
     * @return
     *     possible object is
     *     {@link LoyaltyPNRB }
     *     
     */
    public LoyaltyPNRB getFrequentFlyer() {
        return frequentFlyer;
    }

    /**
     * Sets the value of the frequentFlyer property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoyaltyPNRB }
     *     
     */
    public void setFrequentFlyer(LoyaltyPNRB value) {
        this.frequentFlyer = value;
    }

    /**
     * Gets the value of the segmentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSegmentNumber() {
        return segmentNumber;
    }

    /**
     * Sets the value of the segmentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSegmentNumber(Integer value) {
        this.segmentNumber = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }

}
