
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BSGHistoryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BSGHistoryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="BSGReference" type="{http://webservices.sabre.com/pnrbuilder/v1_17}BSGRefHistoryType" minOccurs="0"/>
 *           &lt;element name="BSGCounter" type="{http://webservices.sabre.com/pnrbuilder/v1_17}BSGCounterHistoryType" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BSGHistoryType", propOrder = {
    "bsgReference",
    "bsgCounter"
})
public class BSGHistoryType {

    @XmlElement(name = "BSGReference")
    protected BSGRefHistoryType bsgReference;
    @XmlElement(name = "BSGCounter")
    protected BSGCounterHistoryType bsgCounter;

    /**
     * Gets the value of the bsgReference property.
     * 
     * @return
     *     possible object is
     *     {@link BSGRefHistoryType }
     *     
     */
    public BSGRefHistoryType getBSGReference() {
        return bsgReference;
    }

    /**
     * Sets the value of the bsgReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link BSGRefHistoryType }
     *     
     */
    public void setBSGReference(BSGRefHistoryType value) {
        this.bsgReference = value;
    }

    /**
     * Gets the value of the bsgCounter property.
     * 
     * @return
     *     possible object is
     *     {@link BSGCounterHistoryType }
     *     
     */
    public BSGCounterHistoryType getBSGCounter() {
        return bsgCounter;
    }

    /**
     * Sets the value of the bsgCounter property.
     * 
     * @param value
     *     allowed object is
     *     {@link BSGCounterHistoryType }
     *     
     */
    public void setBSGCounter(BSGCounterHistoryType value) {
        this.bsgCounter = value;
    }

}
