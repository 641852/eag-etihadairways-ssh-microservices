
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AirSegmentType.PNRB.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AirSegmentType.PNRB">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="REG"/>
 *     &lt;enumeration value="ARNK"/>
 *     &lt;enumeration value="OPEN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AirSegmentType.PNRB")
@XmlEnum
public enum AirSegmentTypePNRB {

    REG,
    ARNK,
    OPEN;

    public String value() {
        return name();
    }

    public static AirSegmentTypePNRB fromValue(String v) {
        return valueOf(v);
    }

}
