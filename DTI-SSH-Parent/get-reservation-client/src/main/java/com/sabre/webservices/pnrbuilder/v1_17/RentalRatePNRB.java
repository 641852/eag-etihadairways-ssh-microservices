
package com.sabre.webservices.pnrbuilder.v1_17;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Information on the rates associated with this vehicle. Rate information can include the
 *                 distance and the base rental cost, along with information on the various factors that may influence this
 *                 rate.
 *             
 * 
 * <p>Java class for RentalRate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RentalRate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RateCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateGuaranteedQuoted" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateChanged" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExtraMileageCharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MileageCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MileageDecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="MileageKilometersCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VehicleCharges" type="{http://webservices.sabre.com/pnrbuilder/v1_17}VehicleCharges.PNRB" minOccurs="0"/>
 *         &lt;element name="RateQualifierCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillingNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="BillingReference" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="PromotionalCoupon" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RentalRate.PNRB", propOrder = {
    "rateCode",
    "rateGuaranteedQuoted",
    "rateChanged",
    "extraMileageCharge",
    "mileageCurrencyCode",
    "mileageDecimalPlaces",
    "mileageKilometersCode",
    "vehicleCharges",
    "rateQualifierCode",
    "billingNumber",
    "billingReference",
    "promotionalCoupon"
})
public class RentalRatePNRB {

    @XmlElement(name = "RateCode")
    protected String rateCode;
    @XmlElement(name = "RateGuaranteedQuoted")
    protected String rateGuaranteedQuoted;
    @XmlElement(name = "RateChanged")
    protected String rateChanged;
    @XmlElement(name = "ExtraMileageCharge")
    protected BigDecimal extraMileageCharge;
    @XmlElement(name = "MileageCurrencyCode")
    protected String mileageCurrencyCode;
    @XmlElement(name = "MileageDecimalPlaces")
    protected BigInteger mileageDecimalPlaces;
    @XmlElement(name = "MileageKilometersCode")
    protected String mileageKilometersCode;
    @XmlElement(name = "VehicleCharges")
    protected VehicleChargesPNRB vehicleCharges;
    @XmlElement(name = "RateQualifierCode")
    protected String rateQualifierCode;
    @XmlElement(name = "BillingNumber")
    protected String billingNumber;
    @XmlElement(name = "BillingReference")
    protected String billingReference;
    @XmlElement(name = "PromotionalCoupon")
    protected String promotionalCoupon;

    /**
     * Gets the value of the rateCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateCode() {
        return rateCode;
    }

    /**
     * Sets the value of the rateCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateCode(String value) {
        this.rateCode = value;
    }

    /**
     * Gets the value of the rateGuaranteedQuoted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateGuaranteedQuoted() {
        return rateGuaranteedQuoted;
    }

    /**
     * Sets the value of the rateGuaranteedQuoted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateGuaranteedQuoted(String value) {
        this.rateGuaranteedQuoted = value;
    }

    /**
     * Gets the value of the rateChanged property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateChanged() {
        return rateChanged;
    }

    /**
     * Sets the value of the rateChanged property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateChanged(String value) {
        this.rateChanged = value;
    }

    /**
     * Gets the value of the extraMileageCharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExtraMileageCharge() {
        return extraMileageCharge;
    }

    /**
     * Sets the value of the extraMileageCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExtraMileageCharge(BigDecimal value) {
        this.extraMileageCharge = value;
    }

    /**
     * Gets the value of the mileageCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMileageCurrencyCode() {
        return mileageCurrencyCode;
    }

    /**
     * Sets the value of the mileageCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMileageCurrencyCode(String value) {
        this.mileageCurrencyCode = value;
    }

    /**
     * Gets the value of the mileageDecimalPlaces property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMileageDecimalPlaces() {
        return mileageDecimalPlaces;
    }

    /**
     * Sets the value of the mileageDecimalPlaces property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMileageDecimalPlaces(BigInteger value) {
        this.mileageDecimalPlaces = value;
    }

    /**
     * Gets the value of the mileageKilometersCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMileageKilometersCode() {
        return mileageKilometersCode;
    }

    /**
     * Sets the value of the mileageKilometersCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMileageKilometersCode(String value) {
        this.mileageKilometersCode = value;
    }

    /**
     * Gets the value of the vehicleCharges property.
     * 
     * @return
     *     possible object is
     *     {@link VehicleChargesPNRB }
     *     
     */
    public VehicleChargesPNRB getVehicleCharges() {
        return vehicleCharges;
    }

    /**
     * Sets the value of the vehicleCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleChargesPNRB }
     *     
     */
    public void setVehicleCharges(VehicleChargesPNRB value) {
        this.vehicleCharges = value;
    }

    /**
     * Gets the value of the rateQualifierCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateQualifierCode() {
        return rateQualifierCode;
    }

    /**
     * Sets the value of the rateQualifierCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateQualifierCode(String value) {
        this.rateQualifierCode = value;
    }

    /**
     * Gets the value of the billingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingNumber() {
        return billingNumber;
    }

    /**
     * Sets the value of the billingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingNumber(String value) {
        this.billingNumber = value;
    }

    /**
     * Gets the value of the billingReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingReference() {
        return billingReference;
    }

    /**
     * Sets the value of the billingReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingReference(String value) {
        this.billingReference = value;
    }

    /**
     * Gets the value of the promotionalCoupon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromotionalCoupon() {
        return promotionalCoupon;
    }

    /**
     * Sets the value of the promotionalCoupon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromotionalCoupon(String value) {
        this.promotionalCoupon = value;
    }

}
