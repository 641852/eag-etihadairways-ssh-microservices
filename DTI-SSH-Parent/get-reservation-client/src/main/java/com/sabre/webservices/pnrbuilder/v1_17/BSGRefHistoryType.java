
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BSGRefHistoryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BSGRefHistoryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HistoryAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BSGPNRRecordLocator" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="ActiveAssocPnr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CancelAssocPnr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BSGRefHistoryType", propOrder = {
    "historyAction",
    "bsgpnrRecordLocator",
    "activeAssocPnr",
    "cancelAssocPnr"
})
public class BSGRefHistoryType {

    @XmlElement(name = "HistoryAction")
    protected String historyAction;
    @XmlElement(name = "BSGPNRRecordLocator")
    protected String bsgpnrRecordLocator;
    @XmlElement(name = "ActiveAssocPnr")
    protected Boolean activeAssocPnr;
    @XmlElement(name = "CancelAssocPnr")
    protected Boolean cancelAssocPnr;

    /**
     * Gets the value of the historyAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHistoryAction() {
        return historyAction;
    }

    /**
     * Sets the value of the historyAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHistoryAction(String value) {
        this.historyAction = value;
    }

    /**
     * Gets the value of the bsgpnrRecordLocator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBSGPNRRecordLocator() {
        return bsgpnrRecordLocator;
    }

    /**
     * Sets the value of the bsgpnrRecordLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBSGPNRRecordLocator(String value) {
        this.bsgpnrRecordLocator = value;
    }

    /**
     * Gets the value of the activeAssocPnr property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isActiveAssocPnr() {
        return activeAssocPnr;
    }

    /**
     * Sets the value of the activeAssocPnr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setActiveAssocPnr(Boolean value) {
        this.activeAssocPnr = value;
    }

    /**
     * Gets the value of the cancelAssocPnr property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCancelAssocPnr() {
        return cancelAssocPnr;
    }

    /**
     * Sets the value of the cancelAssocPnr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCancelAssocPnr(Boolean value) {
        this.cancelAssocPnr = value;
    }

}
