
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubscriptionCardSegmentHistoryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubscriptionCardSegmentHistoryType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservices.sabre.com/pnrbuilder/v1_17}SegmentAssociationTag.PNRB">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubscriptionCardSegmentHistoryType")
public class SubscriptionCardSegmentHistoryType
    extends SegmentAssociationTagPNRB
{


}
