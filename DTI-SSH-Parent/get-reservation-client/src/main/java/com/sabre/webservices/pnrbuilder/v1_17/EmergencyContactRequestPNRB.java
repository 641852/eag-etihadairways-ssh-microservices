
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Can be name associated.
 * 
 *                 formats:
 * 
 *                 * add emergency information (GFAX AND AFAX)
 *                 4PCTC/(contact name)/(country code)(phone number)-(name
 *                 number) 4PCTC/PIERRE BOUVIER/US 817 555 0000-1.1
 * 
 *                 * add emergency information with free text (GFAX AND
 *                 AFAX) 4PCTC/(contact name)/(country code) (phone
 *                 number)/(free text)
 *                 4PCTC/MEIER THOMAS/DE 49 9131 45567/HOME
 * 
 *                 * indicate that passenger has refused to provide
 *                 emergency contact info (GFAX AND AFAX)
 * 
 *                 4PCTC//
 *             
 * 
 * <p>Java class for EmergencyContactRequest.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmergencyContactRequest.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContactName" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="CountryCode" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="PhoneNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="FreeText" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="Comment" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="PassengerRefusal" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="VendorCode" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_17}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="type" type="{http://webservices.sabre.com/pnrbuilder/v1_17}RequestType.PNRB" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_17}OperationType.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmergencyContactRequest.PNRB", propOrder = {
    "contactName",
    "countryCode",
    "phoneNumber",
    "freeText",
    "comment",
    "passengerRefusal",
    "vendorCode"
})
public class EmergencyContactRequestPNRB {

    @XmlElement(name = "ContactName")
    protected String contactName;
    @XmlElement(name = "CountryCode")
    protected String countryCode;
    @XmlElement(name = "PhoneNumber")
    protected String phoneNumber;
    @XmlElement(name = "FreeText")
    protected String freeText;
    @XmlElement(name = "Comment")
    protected String comment;
    @XmlElement(name = "PassengerRefusal")
    protected Boolean passengerRefusal;
    @XmlElement(name = "VendorCode")
    protected String vendorCode;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "type")
    protected RequestTypePNRB type;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;

    /**
     * Gets the value of the contactName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * Sets the value of the contactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactName(String value) {
        this.contactName = value;
    }

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the phoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets the value of the phoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber(String value) {
        this.phoneNumber = value;
    }

    /**
     * Gets the value of the freeText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreeText() {
        return freeText;
    }

    /**
     * Sets the value of the freeText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreeText(String value) {
        this.freeText = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

    /**
     * Gets the value of the passengerRefusal property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPassengerRefusal() {
        return passengerRefusal;
    }

    /**
     * Sets the value of the passengerRefusal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPassengerRefusal(Boolean value) {
        this.passengerRefusal = value;
    }

    /**
     * Gets the value of the vendorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorCode() {
        return vendorCode;
    }

    /**
     * Sets the value of the vendorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorCode(String value) {
        this.vendorCode = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link RequestTypePNRB }
     *     
     */
    public RequestTypePNRB getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestTypePNRB }
     *     
     */
    public void setType(RequestTypePNRB value) {
        this.type = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }

}
