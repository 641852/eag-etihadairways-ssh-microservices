
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReturnOptions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReturnOptions">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservices.sabre.com/pnrbuilder/v1_17}ReturnOptions.PNRB">
 *       &lt;attribute name="RetrievePNR" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="IncludeUpdateDetails" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="ReturnLocator" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReturnOptions")
public class ReturnOptions
    extends ReturnOptionsPNRB
{

    @XmlAttribute(name = "RetrievePNR")
    protected Boolean retrievePNR;
    @XmlAttribute(name = "IncludeUpdateDetails")
    protected Boolean includeUpdateDetails;
    @XmlAttribute(name = "ReturnLocator")
    protected Boolean returnLocator;

    /**
     * Gets the value of the retrievePNR property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isRetrievePNR() {
        if (retrievePNR == null) {
            return false;
        } else {
            return retrievePNR;
        }
    }

    /**
     * Sets the value of the retrievePNR property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetrievePNR(Boolean value) {
        this.retrievePNR = value;
    }

    /**
     * Gets the value of the includeUpdateDetails property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIncludeUpdateDetails() {
        if (includeUpdateDetails == null) {
            return false;
        } else {
            return includeUpdateDetails;
        }
    }

    /**
     * Sets the value of the includeUpdateDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeUpdateDetails(Boolean value) {
        this.includeUpdateDetails = value;
    }

    /**
     * Gets the value of the returnLocator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isReturnLocator() {
        if (returnLocator == null) {
            return false;
        } else {
            return returnLocator;
        }
    }

    /**
     * Sets the value of the returnLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnLocator(Boolean value) {
        this.returnLocator = value;
    }

}
