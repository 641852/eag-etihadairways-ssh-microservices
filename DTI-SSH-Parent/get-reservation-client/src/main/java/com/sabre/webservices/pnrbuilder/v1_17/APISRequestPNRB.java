
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Max line lengths
 * 
 *                 * generic DOCS/DOCO - 108 chars
 *                 after the first slash.
 *                 Alphas, numerics, slashses and spaces are
 *                 allowed.
 *                 *segment specific DOCS/DOCO - 91 chars after the first
 *                 slash
 *                 * DOCA is variable. It is the same as DOCS and
 *                 DOCO unless associated to more than one name. There is
 *                 one less char allowed for
 *                 each name to which the item is
 *                 associated.
 *                 * the AFAX DOCA (4DOCA) has a max length of 127 chars
 *             
 * 
 * <p>Java class for APISRequest.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="APISRequest.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DOCAEntry" type="{http://webservices.sabre.com/pnrbuilder/v1_17}DOCAEntry.PNRB" minOccurs="0"/>
 *         &lt;element name="DOCOEntry" type="{http://webservices.sabre.com/pnrbuilder/v1_17}DOCOEntry.PNRB" minOccurs="0"/>
 *         &lt;element name="DOCSEntry" type="{http://webservices.sabre.com/pnrbuilder/v1_17}DOCSEntry.PNRB" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "APISRequest.PNRB", propOrder = {
    "docaEntry",
    "docoEntry",
    "docsEntry"
})
public class APISRequestPNRB {

    @XmlElement(name = "DOCAEntry")
    protected DOCAEntryPNRB docaEntry;
    @XmlElement(name = "DOCOEntry")
    protected DOCOEntryPNRB docoEntry;
    @XmlElement(name = "DOCSEntry")
    protected DOCSEntryPNRB docsEntry;

    /**
     * Gets the value of the docaEntry property.
     * 
     * @return
     *     possible object is
     *     {@link DOCAEntryPNRB }
     *     
     */
    public DOCAEntryPNRB getDOCAEntry() {
        return docaEntry;
    }

    /**
     * Sets the value of the docaEntry property.
     * 
     * @param value
     *     allowed object is
     *     {@link DOCAEntryPNRB }
     *     
     */
    public void setDOCAEntry(DOCAEntryPNRB value) {
        this.docaEntry = value;
    }

    /**
     * Gets the value of the docoEntry property.
     * 
     * @return
     *     possible object is
     *     {@link DOCOEntryPNRB }
     *     
     */
    public DOCOEntryPNRB getDOCOEntry() {
        return docoEntry;
    }

    /**
     * Sets the value of the docoEntry property.
     * 
     * @param value
     *     allowed object is
     *     {@link DOCOEntryPNRB }
     *     
     */
    public void setDOCOEntry(DOCOEntryPNRB value) {
        this.docoEntry = value;
    }

    /**
     * Gets the value of the docsEntry property.
     * 
     * @return
     *     possible object is
     *     {@link DOCSEntryPNRB }
     *     
     */
    public DOCSEntryPNRB getDOCSEntry() {
        return docsEntry;
    }

    /**
     * Sets the value of the docsEntry property.
     * 
     * @param value
     *     allowed object is
     *     {@link DOCSEntryPNRB }
     *     
     */
    public void setDOCSEntry(DOCSEntryPNRB value) {
        this.docsEntry = value;
    }

}
