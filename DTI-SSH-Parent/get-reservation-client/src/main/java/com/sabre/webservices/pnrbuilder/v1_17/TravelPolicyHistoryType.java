
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TravelPolicyHistoryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TravelPolicyHistoryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HistoryAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TravelPolicyData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TravelPolicyHistoryType", propOrder = {
    "historyAction",
    "travelPolicyData"
})
public class TravelPolicyHistoryType {

    @XmlElement(name = "HistoryAction")
    protected String historyAction;
    @XmlElement(name = "TravelPolicyData")
    protected String travelPolicyData;

    /**
     * Gets the value of the historyAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHistoryAction() {
        return historyAction;
    }

    /**
     * Sets the value of the historyAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHistoryAction(String value) {
        this.historyAction = value;
    }

    /**
     * Gets the value of the travelPolicyData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTravelPolicyData() {
        return travelPolicyData;
    }

    /**
     * Sets the value of the travelPolicyData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTravelPolicyData(String value) {
        this.travelPolicyData = value;
    }

}
