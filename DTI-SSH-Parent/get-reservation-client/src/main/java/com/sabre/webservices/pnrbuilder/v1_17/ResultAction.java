
package com.sabre.webservices.pnrbuilder.v1_17;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResultAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResultAction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice minOccurs="0">
 *         &lt;element name="QueuePlaceAction" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="QueuePlacement" type="{http://webservices.sabre.com/pnrbuilder/v1_17}QueuePlacement" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="EvaluateOnly" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResultAction", propOrder = {
    "queuePlaceAction",
    "evaluateOnly"
})
public class ResultAction {

    @XmlElement(name = "QueuePlaceAction")
    protected ResultAction.QueuePlaceAction queuePlaceAction;
    @XmlElement(name = "EvaluateOnly")
    protected Boolean evaluateOnly;

    /**
     * Gets the value of the queuePlaceAction property.
     * 
     * @return
     *     possible object is
     *     {@link ResultAction.QueuePlaceAction }
     *     
     */
    public ResultAction.QueuePlaceAction getQueuePlaceAction() {
        return queuePlaceAction;
    }

    /**
     * Sets the value of the queuePlaceAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultAction.QueuePlaceAction }
     *     
     */
    public void setQueuePlaceAction(ResultAction.QueuePlaceAction value) {
        this.queuePlaceAction = value;
    }

    /**
     * Gets the value of the evaluateOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEvaluateOnly() {
        return evaluateOnly;
    }

    /**
     * Sets the value of the evaluateOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEvaluateOnly(Boolean value) {
        this.evaluateOnly = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="QueuePlacement" type="{http://webservices.sabre.com/pnrbuilder/v1_17}QueuePlacement" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "queuePlacement"
    })
    public static class QueuePlaceAction {

        @XmlElement(name = "QueuePlacement", required = true)
        protected List<QueuePlacement> queuePlacement;

        /**
         * Gets the value of the queuePlacement property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the queuePlacement property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getQueuePlacement().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueuePlacement }
         * 
         * 
         */
        public List<QueuePlacement> getQueuePlacement() {
            if (queuePlacement == null) {
                queuePlacement = new ArrayList<QueuePlacement>();
            }
            return this.queuePlacement;
        }

    }

}
