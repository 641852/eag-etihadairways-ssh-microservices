
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReservationEmail.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReservationEmail.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmailText" type="{http://webservices.sabre.com/pnrbuilder/v1_17}EmailLines.PNRB" minOccurs="0"/>
 *         &lt;element name="Subject" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="PseudoEmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_17}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="highPriority" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_17}OperationType.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReservationEmail.PNRB", propOrder = {
    "emailText",
    "subject",
    "pseudoEmailAddress"
})
public class ReservationEmailPNRB {

    @XmlElement(name = "EmailText")
    protected EmailLinesPNRB emailText;
    @XmlElement(name = "Subject")
    protected String subject;
    @XmlElement(name = "PseudoEmailAddress")
    protected String pseudoEmailAddress;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "highPriority")
    protected Boolean highPriority;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;

    /**
     * Gets the value of the emailText property.
     * 
     * @return
     *     possible object is
     *     {@link EmailLinesPNRB }
     *     
     */
    public EmailLinesPNRB getEmailText() {
        return emailText;
    }

    /**
     * Sets the value of the emailText property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailLinesPNRB }
     *     
     */
    public void setEmailText(EmailLinesPNRB value) {
        this.emailText = value;
    }

    /**
     * Gets the value of the subject property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Sets the value of the subject property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubject(String value) {
        this.subject = value;
    }

    /**
     * Gets the value of the pseudoEmailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPseudoEmailAddress() {
        return pseudoEmailAddress;
    }

    /**
     * Sets the value of the pseudoEmailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPseudoEmailAddress(String value) {
        this.pseudoEmailAddress = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the highPriority property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHighPriority() {
        return highPriority;
    }

    /**
     * Sets the value of the highPriority property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHighPriority(Boolean value) {
        this.highPriority = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }

}
