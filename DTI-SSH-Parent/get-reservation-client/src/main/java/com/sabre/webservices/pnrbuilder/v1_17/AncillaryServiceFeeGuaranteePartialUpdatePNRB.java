
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AncillaryServiceFeeGuaranteePartialUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AncillaryServiceFeeGuaranteePartialUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FeeGuaranteeIndicator" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AncillaryServiceFeeGuaranteePartialUpdate.PNRB", propOrder = {
    "feeGuaranteeIndicator"
})
public class AncillaryServiceFeeGuaranteePartialUpdatePNRB {

    @XmlElement(name = "FeeGuaranteeIndicator")
    protected String feeGuaranteeIndicator;

    /**
     * Gets the value of the feeGuaranteeIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeGuaranteeIndicator() {
        return feeGuaranteeIndicator;
    }

    /**
     * Sets the value of the feeGuaranteeIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeGuaranteeIndicator(String value) {
        this.feeGuaranteeIndicator = value;
    }

}
