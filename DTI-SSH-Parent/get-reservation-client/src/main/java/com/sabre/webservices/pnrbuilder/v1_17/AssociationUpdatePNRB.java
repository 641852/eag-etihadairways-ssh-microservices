
package com.sabre.webservices.pnrbuilder.v1_17;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssociationUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AssociationUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Parent" type="{http://webservices.sabre.com/pnrbuilder/v1_17}AssociationParent" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Child" type="{http://webservices.sabre.com/pnrbuilder/v1_17}AssociationChild" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_17}AssociationOperationType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssociationUpdate.PNRB", propOrder = {
    "parent",
    "child"
})
public class AssociationUpdatePNRB {

    @XmlElement(name = "Parent")
    protected List<AssociationParent> parent;
    @XmlElement(name = "Child")
    protected List<AssociationChild> child;
    @XmlAttribute(name = "op")
    protected AssociationOperationType op;

    /**
     * Gets the value of the parent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssociationParent }
     * 
     * 
     */
    public List<AssociationParent> getParent() {
        if (parent == null) {
            parent = new ArrayList<AssociationParent>();
        }
        return this.parent;
    }

    /**
     * Gets the value of the child property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the child property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChild().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssociationChild }
     * 
     * 
     */
    public List<AssociationChild> getChild() {
        if (child == null) {
            child = new ArrayList<AssociationChild>();
        }
        return this.child;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link AssociationOperationType }
     *     
     */
    public AssociationOperationType getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssociationOperationType }
     *     
     */
    public void setOp(AssociationOperationType value) {
        this.op = value;
    }

}
