
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SegmentSubset.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SegmentSubset">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ALL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SegmentSubset")
@XmlEnum
public enum SegmentSubset {

    ALL;

    public String value() {
        return name();
    }

    public static SegmentSubset fromValue(String v) {
        return valueOf(v);
    }

}
