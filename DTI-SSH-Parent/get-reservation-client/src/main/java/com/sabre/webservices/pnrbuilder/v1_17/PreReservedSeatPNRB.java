
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PreReservedSeat.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PreReservedSeat.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SeatNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString"/>
 *         &lt;element name="SmokingPrefOfferedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SeatTypeCode" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="BoardingPassIssueFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SeatStatusCode" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="IsAEAssociated" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsPREdit" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Cog" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CogFlight.PNRB" minOccurs="0"/>
 *         &lt;element name="CompartmentNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *         &lt;element name="BoardPoint" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CityCode" minOccurs="0"/>
 *         &lt;element name="OffPoint" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CityCode" minOccurs="0"/>
 *         &lt;element name="DepartureTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ArrivalTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_17}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_17}OperationType.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PreReservedSeat.PNRB", propOrder = {
    "seatNumber",
    "smokingPrefOfferedIndicator",
    "seatTypeCode",
    "boardingPassIssueFlag",
    "seatStatusCode",
    "isAEAssociated",
    "isPREdit",
    "cog",
    "compartmentNumber",
    "boardPoint",
    "offPoint",
    "departureTime",
    "arrivalTime"
})
public class PreReservedSeatPNRB {

    @XmlElement(name = "SeatNumber", required = true)
    protected String seatNumber;
    @XmlElement(name = "SmokingPrefOfferedIndicator")
    protected Boolean smokingPrefOfferedIndicator;
    @XmlElement(name = "SeatTypeCode")
    protected String seatTypeCode;
    @XmlElement(name = "BoardingPassIssueFlag")
    protected Boolean boardingPassIssueFlag;
    @XmlElement(name = "SeatStatusCode")
    protected String seatStatusCode;
    @XmlElement(name = "IsAEAssociated")
    protected Boolean isAEAssociated;
    @XmlElement(name = "IsPREdit")
    protected Boolean isPREdit;
    @XmlElement(name = "Cog")
    protected CogFlightPNRB cog;
    @XmlElement(name = "CompartmentNumber")
    protected String compartmentNumber;
    @XmlElement(name = "BoardPoint")
    protected String boardPoint;
    @XmlElement(name = "OffPoint")
    protected String offPoint;
    @XmlElement(name = "DepartureTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar departureTime;
    @XmlElement(name = "ArrivalTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar arrivalTime;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;

    /**
     * Gets the value of the seatNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatNumber() {
        return seatNumber;
    }

    /**
     * Sets the value of the seatNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatNumber(String value) {
        this.seatNumber = value;
    }

    /**
     * Gets the value of the smokingPrefOfferedIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSmokingPrefOfferedIndicator() {
        return smokingPrefOfferedIndicator;
    }

    /**
     * Sets the value of the smokingPrefOfferedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSmokingPrefOfferedIndicator(Boolean value) {
        this.smokingPrefOfferedIndicator = value;
    }

    /**
     * Gets the value of the seatTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatTypeCode() {
        return seatTypeCode;
    }

    /**
     * Sets the value of the seatTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatTypeCode(String value) {
        this.seatTypeCode = value;
    }

    /**
     * Gets the value of the boardingPassIssueFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBoardingPassIssueFlag() {
        return boardingPassIssueFlag;
    }

    /**
     * Sets the value of the boardingPassIssueFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBoardingPassIssueFlag(Boolean value) {
        this.boardingPassIssueFlag = value;
    }

    /**
     * Gets the value of the seatStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatStatusCode() {
        return seatStatusCode;
    }

    /**
     * Sets the value of the seatStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatStatusCode(String value) {
        this.seatStatusCode = value;
    }

    /**
     * Gets the value of the isAEAssociated property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAEAssociated() {
        return isAEAssociated;
    }

    /**
     * Sets the value of the isAEAssociated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAEAssociated(Boolean value) {
        this.isAEAssociated = value;
    }

    /**
     * Gets the value of the isPREdit property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPREdit() {
        return isPREdit;
    }

    /**
     * Sets the value of the isPREdit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPREdit(Boolean value) {
        this.isPREdit = value;
    }

    /**
     * Gets the value of the cog property.
     * 
     * @return
     *     possible object is
     *     {@link CogFlightPNRB }
     *     
     */
    public CogFlightPNRB getCog() {
        return cog;
    }

    /**
     * Sets the value of the cog property.
     * 
     * @param value
     *     allowed object is
     *     {@link CogFlightPNRB }
     *     
     */
    public void setCog(CogFlightPNRB value) {
        this.cog = value;
    }

    /**
     * Gets the value of the compartmentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompartmentNumber() {
        return compartmentNumber;
    }

    /**
     * Sets the value of the compartmentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompartmentNumber(String value) {
        this.compartmentNumber = value;
    }

    /**
     * Gets the value of the boardPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoardPoint() {
        return boardPoint;
    }

    /**
     * Sets the value of the boardPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoardPoint(String value) {
        this.boardPoint = value;
    }

    /**
     * Gets the value of the offPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffPoint() {
        return offPoint;
    }

    /**
     * Sets the value of the offPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffPoint(String value) {
        this.offPoint = value;
    }

    /**
     * Gets the value of the departureTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDepartureTime() {
        return departureTime;
    }

    /**
     * Sets the value of the departureTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDepartureTime(XMLGregorianCalendar value) {
        this.departureTime = value;
    }

    /**
     * Gets the value of the arrivalTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Sets the value of the arrivalTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setArrivalTime(XMLGregorianCalendar value) {
        this.arrivalTime = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }

}
