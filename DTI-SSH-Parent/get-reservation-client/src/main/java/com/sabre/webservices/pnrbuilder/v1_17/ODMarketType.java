
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ODMarketType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ODMarketType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="destination" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" />
 *       &lt;attribute name="destinationLevel" type="{http://webservices.sabre.com/pnrbuilder/v1_17}Numeric0to99999" />
 *       &lt;attribute name="origin" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" />
 *       &lt;attribute name="originLevel" type="{http://webservices.sabre.com/pnrbuilder/v1_17}Numeric0to99999" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ODMarketType")
public class ODMarketType {

    @XmlAttribute(name = "destination")
    protected String destination;
    @XmlAttribute(name = "destinationLevel")
    protected Integer destinationLevel;
    @XmlAttribute(name = "origin")
    protected String origin;
    @XmlAttribute(name = "originLevel")
    protected Integer originLevel;

    /**
     * Gets the value of the destination property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Sets the value of the destination property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestination(String value) {
        this.destination = value;
    }

    /**
     * Gets the value of the destinationLevel property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDestinationLevel() {
        return destinationLevel;
    }

    /**
     * Sets the value of the destinationLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDestinationLevel(Integer value) {
        this.destinationLevel = value;
    }

    /**
     * Gets the value of the origin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Sets the value of the origin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigin(String value) {
        this.origin = value;
    }

    /**
     * Gets the value of the originLevel property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOriginLevel() {
        return originLevel;
    }

    /**
     * Sets the value of the originLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOriginLevel(Integer value) {
        this.originLevel = value;
    }

}
