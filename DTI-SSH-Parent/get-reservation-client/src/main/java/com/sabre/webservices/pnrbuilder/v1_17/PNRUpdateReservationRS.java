
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PNRUpdateReservationRS complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PNRUpdateReservationRS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Reservation" type="{http://webservices.sabre.com/pnrbuilder/v1_17}Reservation.PNRB" minOccurs="0"/>
 *         &lt;element name="POS" type="{http://webservices.sabre.com/pnrbuilder/v1_17}POS_Type.PNRB" minOccurs="0"/>
 *         &lt;element name="RecordLocator" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PNRUpdateReservationRS", propOrder = {
    "reservation",
    "pos",
    "recordLocator"
})
public class PNRUpdateReservationRS {

    @XmlElement(name = "Reservation")
    protected ReservationPNRB reservation;
    @XmlElement(name = "POS")
    protected POSTypePNRB pos;
    @XmlElement(name = "RecordLocator")
    protected String recordLocator;

    /**
     * Gets the value of the reservation property.
     * 
     * @return
     *     possible object is
     *     {@link ReservationPNRB }
     *     
     */
    public ReservationPNRB getReservation() {
        return reservation;
    }

    /**
     * Sets the value of the reservation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReservationPNRB }
     *     
     */
    public void setReservation(ReservationPNRB value) {
        this.reservation = value;
    }

    /**
     * Gets the value of the pos property.
     * 
     * @return
     *     possible object is
     *     {@link POSTypePNRB }
     *     
     */
    public POSTypePNRB getPOS() {
        return pos;
    }

    /**
     * Sets the value of the pos property.
     * 
     * @param value
     *     allowed object is
     *     {@link POSTypePNRB }
     *     
     */
    public void setPOS(POSTypePNRB value) {
        this.pos = value;
    }

    /**
     * Gets the value of the recordLocator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordLocator() {
        return recordLocator;
    }

    /**
     * Sets the value of the recordLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordLocator(String value) {
        this.recordLocator = value;
    }

}
