
package com.sabre.webservices.pnrbuilder.v1_17;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PricingAndTaxesPartialUpdateType.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PricingAndTaxesPartialUpdateType.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OriginalBasePrice" type="{http://webservices.sabre.com/pnrbuilder/v1_17}AncillaryPrice.PNRB" minOccurs="0"/>
 *         &lt;element name="EquivalentPrice" type="{http://webservices.sabre.com/pnrbuilder/v1_17}AncillaryPrice.PNRB" minOccurs="0"/>
 *         &lt;element name="TTLPrice" type="{http://webservices.sabre.com/pnrbuilder/v1_17}AncillaryPrice.PNRB" minOccurs="0"/>
 *         &lt;element name="Taxes" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Tax" type="{http://webservices.sabre.com/pnrbuilder/v1_17}AncillaryTax.PNRB" maxOccurs="99" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PricingAndTaxesPartialUpdateType.PNRB", propOrder = {
    "originalBasePrice",
    "equivalentPrice",
    "ttlPrice",
    "taxes"
})
public class PricingAndTaxesPartialUpdateTypePNRB {

    @XmlElement(name = "OriginalBasePrice")
    protected AncillaryPricePNRB originalBasePrice;
    @XmlElement(name = "EquivalentPrice")
    protected AncillaryPricePNRB equivalentPrice;
    @XmlElement(name = "TTLPrice")
    protected AncillaryPricePNRB ttlPrice;
    @XmlElement(name = "Taxes")
    protected PricingAndTaxesPartialUpdateTypePNRB.Taxes taxes;

    /**
     * Gets the value of the originalBasePrice property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public AncillaryPricePNRB getOriginalBasePrice() {
        return originalBasePrice;
    }

    /**
     * Sets the value of the originalBasePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public void setOriginalBasePrice(AncillaryPricePNRB value) {
        this.originalBasePrice = value;
    }

    /**
     * Gets the value of the equivalentPrice property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public AncillaryPricePNRB getEquivalentPrice() {
        return equivalentPrice;
    }

    /**
     * Sets the value of the equivalentPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public void setEquivalentPrice(AncillaryPricePNRB value) {
        this.equivalentPrice = value;
    }

    /**
     * Gets the value of the ttlPrice property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public AncillaryPricePNRB getTTLPrice() {
        return ttlPrice;
    }

    /**
     * Sets the value of the ttlPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public void setTTLPrice(AncillaryPricePNRB value) {
        this.ttlPrice = value;
    }

    /**
     * Gets the value of the taxes property.
     * 
     * @return
     *     possible object is
     *     {@link PricingAndTaxesPartialUpdateTypePNRB.Taxes }
     *     
     */
    public PricingAndTaxesPartialUpdateTypePNRB.Taxes getTaxes() {
        return taxes;
    }

    /**
     * Sets the value of the taxes property.
     * 
     * @param value
     *     allowed object is
     *     {@link PricingAndTaxesPartialUpdateTypePNRB.Taxes }
     *     
     */
    public void setTaxes(PricingAndTaxesPartialUpdateTypePNRB.Taxes value) {
        this.taxes = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Tax" type="{http://webservices.sabre.com/pnrbuilder/v1_17}AncillaryTax.PNRB" maxOccurs="99" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tax"
    })
    public static class Taxes {

        @XmlElement(name = "Tax")
        protected List<AncillaryTaxPNRB> tax;

        /**
         * Gets the value of the tax property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the tax property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTax().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AncillaryTaxPNRB }
         * 
         * 
         */
        public List<AncillaryTaxPNRB> getTax() {
            if (tax == null) {
                tax = new ArrayList<AncillaryTaxPNRB>();
            }
            return this.tax;
        }

    }

}
