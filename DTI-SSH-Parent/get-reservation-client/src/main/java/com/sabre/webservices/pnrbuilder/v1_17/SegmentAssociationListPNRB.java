
package com.sabre.webservices.pnrbuilder.v1_17;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SegmentAssociationList.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SegmentAssociationList.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SegmentAssociationTag" type="{http://webservices.sabre.com/pnrbuilder/v1_17}SegmentAssociationTag.PNRB" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SegmentAssociationList.PNRB", propOrder = {
    "segmentAssociationTag"
})
public class SegmentAssociationListPNRB {

    @XmlElement(name = "SegmentAssociationTag")
    protected List<SegmentAssociationTagPNRB> segmentAssociationTag;

    /**
     * Gets the value of the segmentAssociationTag property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the segmentAssociationTag property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSegmentAssociationTag().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SegmentAssociationTagPNRB }
     * 
     * 
     */
    public List<SegmentAssociationTagPNRB> getSegmentAssociationTag() {
        if (segmentAssociationTag == null) {
            segmentAssociationTag = new ArrayList<SegmentAssociationTagPNRB>();
        }
        return this.segmentAssociationTag;
    }

}
