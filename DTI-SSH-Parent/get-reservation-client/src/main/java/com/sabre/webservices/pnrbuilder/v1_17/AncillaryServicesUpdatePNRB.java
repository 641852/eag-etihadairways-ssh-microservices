
package com.sabre.webservices.pnrbuilder.v1_17;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AncillaryServicesUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AncillaryServicesUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NameAssociationList" type="{http://webservices.sabre.com/pnrbuilder/v1_17}NameAssociationList.PNRB" minOccurs="0"/>
 *         &lt;element name="SegmentAssociationList" type="{http://webservices.sabre.com/pnrbuilder/v1_17}SegmentAssociationList.PNRB" minOccurs="0"/>
 *         &lt;group ref="{http://webservices.sabre.com/pnrbuilder/v1_17}AncillaryServicesDataGroup.PNRB" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_17}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_17}OperationType.PNRB" />
 *       &lt;attribute name="elementId" type="{http://webservices.sabre.com/pnrbuilder/v1_17}AssociationMatrixID.PNRB" />
 *       &lt;attribute name="UpdateId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AncillaryServicesUpdate.PNRB", propOrder = {
    "nameAssociationList",
    "segmentAssociationList",
    "commercialName",
    "rficCode",
    "rficSubcode",
    "ssrCode",
    "productTextDetails",
    "productText",
    "owningCarrierCode",
    "ssimCode",
    "bookingIndicator",
    "vendor",
    "emdType",
    "emdNumber",
    "emdCoupon",
    "serviceFeeLineNumber",
    "displayOnlyIndicator",
    "consumptionIndicator",
    "presentTo",
    "atCity",
    "endorsements",
    "stationIndicator",
    "serviceCity",
    "serviceCityDestination",
    "serviceType",
    "ruleSet",
    "originalPrice",
    "quantity",
    "segmentNumber",
    "equivalentPrice",
    "ttlPrice",
    "portionOfTravelIndicator",
    "originalBasePrice",
    "refundIndicator",
    "commisionIndicator",
    "interlineIndicator",
    "feeApplicationIndicator",
    "passengerTypeCode",
    "boardPoint",
    "offPoint",
    "taxesIncluded",
    "taxes",
    "softMatchIndicator",
    "noChargeNotAvailIndicator",
    "ticketCouponNumberAssociation",
    "simultaneousTicketIndicator",
    "formOfRefund",
    "feeNotGuaranteedIndicator",
    "fqtvTierLevel",
    "tourCodeSHC",
    "travelDateEffective",
    "latestTravelDatePermitted",
    "purchaseByDate",
    "totalOriginalBasePrice",
    "totalEquivalentPrice",
    "totalTTLPrice",
    "totalTaxes",
    "taxExemptIndicator",
    "bagWeight",
    "statusIndicator",
    "numberOfItems",
    "actionCode",
    "segmentIndicator",
    "frequentFlyerTier",
    "refundFormIndicator",
    "fareGuaranteedIndicator",
    "serviceChargeIndicator",
    "advancePurchaseIndicator",
    "bookingSource",
    "taxIndicator",
    "ticketingIndicator",
    "feeWaiveReason",
    "fulfillmentType",
    "aaPayOriginalSeat",
    "pdcSeat",
    "equipmentType",
    "aaPayOptionalStatus",
    "firstTravelDate",
    "lastTravelDate",
    "ttyConfirmationTimestamp",
    "purchaseTimestamp",
    "brandedFareId",
    "groupCode",
    "tourCode",
    "emdPaperIndicator",
    "seatRequestTransactionID",
    "ticketUsedForEMDPricing",
    "emdConsummedAtIssuance",
    "paperDocRequired",
    "taxExemption",
    "acsCount",
    "netAmount",
    "priceQuoteDesignator",
    "priceMismatchAction",
    "segment",
    "travelPortions"
})
public class AncillaryServicesUpdatePNRB {

    @XmlElement(name = "NameAssociationList")
    protected NameAssociationListPNRB nameAssociationList;
    @XmlElement(name = "SegmentAssociationList")
    protected SegmentAssociationListPNRB segmentAssociationList;
    @XmlElement(name = "CommercialName")
    protected String commercialName;
    @XmlElement(name = "RficCode")
    protected String rficCode;
    @XmlElement(name = "RficSubcode")
    protected String rficSubcode;
    @XmlElement(name = "SSRCode")
    protected String ssrCode;
    @XmlElement(name = "ProductTextDetails")
    protected AncillaryServicesUpdatePNRB.ProductTextDetails productTextDetails;
    @XmlElement(name = "ProductText")
    protected String productText;
    @XmlElement(name = "OwningCarrierCode")
    protected String owningCarrierCode;
    @XmlElement(name = "SsimCode")
    protected String ssimCode;
    @XmlElement(name = "BookingIndicator")
    protected String bookingIndicator;
    @XmlElement(name = "Vendor")
    protected String vendor;
    @XmlElement(name = "EMDType")
    protected String emdType;
    @XmlElement(name = "EMDNumber")
    protected String emdNumber;
    @XmlElement(name = "EMDCoupon")
    protected String emdCoupon;
    @XmlElement(name = "ServiceFeeLineNumber")
    protected String serviceFeeLineNumber;
    @XmlElement(name = "DisplayOnlyIndicator")
    protected Boolean displayOnlyIndicator;
    @XmlElement(name = "ConsumptionIndicator")
    protected String consumptionIndicator;
    @XmlElement(name = "PresentTo")
    protected String presentTo;
    @XmlElement(name = "AtCity")
    protected String atCity;
    @XmlElement(name = "Endorsements")
    protected String endorsements;
    @XmlElement(name = "StationIndicator")
    protected String stationIndicator;
    @XmlElement(name = "ServiceCity")
    protected String serviceCity;
    @XmlElement(name = "ServiceCityDestination")
    protected String serviceCityDestination;
    @XmlElement(name = "ServiceType")
    protected String serviceType;
    @XmlElement(name = "RuleSet")
    protected AncillaryServicesUpdatePNRB.RuleSet ruleSet;
    @XmlElement(name = "OriginalPrice")
    protected AncillaryServicesUpdatePNRB.OriginalPrice originalPrice;
    @XmlElement(name = "Quantity")
    protected BigInteger quantity;
    @XmlElement(name = "SegmentNumber", type = Byte.class)
    protected List<Byte> segmentNumber;
    @XmlElement(name = "EquivalentPrice")
    protected AncillaryPricePNRB equivalentPrice;
    @XmlElement(name = "TTLPrice")
    protected AncillaryPricePNRB ttlPrice;
    @XmlElement(name = "PortionOfTravelIndicator")
    protected String portionOfTravelIndicator;
    @XmlElement(name = "OriginalBasePrice")
    protected AncillaryPricePNRB originalBasePrice;
    @XmlElement(name = "RefundIndicator")
    protected String refundIndicator;
    @XmlElement(name = "CommisionIndicator")
    protected String commisionIndicator;
    @XmlElement(name = "InterlineIndicator")
    protected String interlineIndicator;
    @XmlElement(name = "FeeApplicationIndicator")
    protected String feeApplicationIndicator;
    @XmlElement(name = "PassengerTypeCode")
    protected String passengerTypeCode;
    @XmlElement(name = "BoardPoint")
    protected String boardPoint;
    @XmlElement(name = "OffPoint")
    protected String offPoint;
    @XmlElement(name = "TaxesIncluded")
    protected Boolean taxesIncluded;
    @XmlElement(name = "Taxes")
    protected AncillaryServicesUpdatePNRB.Taxes taxes;
    @XmlElement(name = "SoftMatchIndicator")
    protected Boolean softMatchIndicator;
    @XmlElement(name = "NoChargeNotAvailIndicator")
    protected String noChargeNotAvailIndicator;
    @XmlElement(name = "TicketCouponNumberAssociation")
    protected List<AncillaryServicesUpdatePNRB.TicketCouponNumberAssociation> ticketCouponNumberAssociation;
    @XmlElement(name = "SimultaneousTicketIndicator")
    protected String simultaneousTicketIndicator;
    @XmlElement(name = "FormOfRefund")
    protected String formOfRefund;
    @XmlElement(name = "FeeNotGuaranteedIndicator")
    protected Boolean feeNotGuaranteedIndicator;
    @XmlElement(name = "FQTVTierLevel")
    protected Short fqtvTierLevel;
    @XmlElement(name = "TourCodeSHC")
    protected String tourCodeSHC;
    @XmlElement(name = "TravelDateEffective")
    protected String travelDateEffective;
    @XmlElement(name = "LatestTravelDatePermitted")
    protected String latestTravelDatePermitted;
    @XmlElement(name = "PurchaseByDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar purchaseByDate;
    @XmlElement(name = "TotalOriginalBasePrice")
    protected AncillaryPricePNRB totalOriginalBasePrice;
    @XmlElement(name = "TotalEquivalentPrice")
    protected AncillaryPricePNRB totalEquivalentPrice;
    @XmlElement(name = "TotalTTLPrice")
    protected AncillaryPricePNRB totalTTLPrice;
    @XmlElement(name = "TotalTaxes")
    protected AncillaryServicesUpdatePNRB.TotalTaxes totalTaxes;
    @XmlElement(name = "TaxExemptIndicator")
    protected String taxExemptIndicator;
    @XmlElement(name = "BagWeight")
    protected AncillaryServicesUpdatePNRB.BagWeight bagWeight;
    @XmlElement(name = "StatusIndicator")
    protected BigInteger statusIndicator;
    @XmlElement(name = "NumberOfItems")
    protected String numberOfItems;
    @XmlElement(name = "ActionCode")
    protected String actionCode;
    @XmlElement(name = "SegmentIndicator")
    protected String segmentIndicator;
    @XmlElement(name = "FrequentFlyerTier")
    protected String frequentFlyerTier;
    @XmlElement(name = "RefundFormIndicator")
    protected String refundFormIndicator;
    @XmlElement(name = "FareGuaranteedIndicator")
    protected String fareGuaranteedIndicator;
    @XmlElement(name = "ServiceChargeIndicator")
    protected String serviceChargeIndicator;
    @XmlElement(name = "AdvancePurchaseIndicator")
    protected String advancePurchaseIndicator;
    @XmlElement(name = "BookingSource")
    protected String bookingSource;
    @XmlElement(name = "TaxIndicator")
    protected String taxIndicator;
    @XmlElement(name = "TicketingIndicator")
    protected String ticketingIndicator;
    @XmlElement(name = "FeeWaiveReason")
    protected String feeWaiveReason;
    @XmlElement(name = "FulfillmentType")
    protected String fulfillmentType;
    @XmlElement(name = "AaPayOriginalSeat")
    protected String aaPayOriginalSeat;
    @XmlElement(name = "PdcSeat")
    protected String pdcSeat;
    @XmlElement(name = "EquipmentType")
    protected String equipmentType;
    @XmlElement(name = "AaPayOptionalStatus")
    protected String aaPayOptionalStatus;
    @XmlElement(name = "FirstTravelDate")
    protected String firstTravelDate;
    @XmlElement(name = "LastTravelDate")
    protected String lastTravelDate;
    @XmlElement(name = "TTYConfirmationTimestamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ttyConfirmationTimestamp;
    @XmlElement(name = "PurchaseTimestamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar purchaseTimestamp;
    @XmlElement(name = "BrandedFareId")
    protected String brandedFareId;
    @XmlElement(name = "GroupCode")
    protected String groupCode;
    @XmlElement(name = "TourCode")
    protected String tourCode;
    @XmlElement(name = "EmdPaperIndicator")
    protected String emdPaperIndicator;
    @XmlElement(name = "SeatRequestTransactionID")
    protected String seatRequestTransactionID;
    @XmlElement(name = "TicketUsedForEMDPricing")
    protected String ticketUsedForEMDPricing;
    @XmlElement(name = "EMDConsummedAtIssuance")
    protected String emdConsummedAtIssuance;
    @XmlElement(name = "PaperDocRequired")
    protected String paperDocRequired;
    @XmlElement(name = "TaxExemption")
    @XmlSchemaType(name = "string")
    protected IndicatorType taxExemption;
    @XmlElement(name = "ACSCount")
    protected BigInteger acsCount;
    @XmlElement(name = "NetAmount")
    protected AncillaryPricePNRB netAmount;
    @XmlElement(name = "PriceQuoteDesignator")
    protected BigInteger priceQuoteDesignator;
    @XmlElement(name = "PriceMismatchAction")
    @XmlSchemaType(name = "string")
    protected PriceMismatchActionType priceMismatchAction;
    @XmlElement(name = "Segment")
    protected SegmentOrTravelPortionType segment;
    @XmlElement(name = "TravelPortions")
    protected AncillaryServicesUpdatePNRB.TravelPortions travelPortions;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;
    @XmlAttribute(name = "elementId")
    protected String elementId;
    @XmlAttribute(name = "UpdateId")
    protected String updateId;

    /**
     * Gets the value of the nameAssociationList property.
     * 
     * @return
     *     possible object is
     *     {@link NameAssociationListPNRB }
     *     
     */
    public NameAssociationListPNRB getNameAssociationList() {
        return nameAssociationList;
    }

    /**
     * Sets the value of the nameAssociationList property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameAssociationListPNRB }
     *     
     */
    public void setNameAssociationList(NameAssociationListPNRB value) {
        this.nameAssociationList = value;
    }

    /**
     * Gets the value of the segmentAssociationList property.
     * 
     * @return
     *     possible object is
     *     {@link SegmentAssociationListPNRB }
     *     
     */
    public SegmentAssociationListPNRB getSegmentAssociationList() {
        return segmentAssociationList;
    }

    /**
     * Sets the value of the segmentAssociationList property.
     * 
     * @param value
     *     allowed object is
     *     {@link SegmentAssociationListPNRB }
     *     
     */
    public void setSegmentAssociationList(SegmentAssociationListPNRB value) {
        this.segmentAssociationList = value;
    }

    /**
     * Gets the value of the commercialName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommercialName() {
        return commercialName;
    }

    /**
     * Sets the value of the commercialName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommercialName(String value) {
        this.commercialName = value;
    }

    /**
     * Gets the value of the rficCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRficCode() {
        return rficCode;
    }

    /**
     * Sets the value of the rficCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRficCode(String value) {
        this.rficCode = value;
    }

    /**
     * Gets the value of the rficSubcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRficSubcode() {
        return rficSubcode;
    }

    /**
     * Sets the value of the rficSubcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRficSubcode(String value) {
        this.rficSubcode = value;
    }

    /**
     * Gets the value of the ssrCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSRCode() {
        return ssrCode;
    }

    /**
     * Sets the value of the ssrCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSRCode(String value) {
        this.ssrCode = value;
    }

    /**
     * Gets the value of the productTextDetails property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryServicesUpdatePNRB.ProductTextDetails }
     *     
     */
    public AncillaryServicesUpdatePNRB.ProductTextDetails getProductTextDetails() {
        return productTextDetails;
    }

    /**
     * Sets the value of the productTextDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryServicesUpdatePNRB.ProductTextDetails }
     *     
     */
    public void setProductTextDetails(AncillaryServicesUpdatePNRB.ProductTextDetails value) {
        this.productTextDetails = value;
    }

    /**
     * Gets the value of the productText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductText() {
        return productText;
    }

    /**
     * Sets the value of the productText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductText(String value) {
        this.productText = value;
    }

    /**
     * Gets the value of the owningCarrierCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwningCarrierCode() {
        return owningCarrierCode;
    }

    /**
     * Sets the value of the owningCarrierCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwningCarrierCode(String value) {
        this.owningCarrierCode = value;
    }

    /**
     * Gets the value of the ssimCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSsimCode() {
        return ssimCode;
    }

    /**
     * Sets the value of the ssimCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSsimCode(String value) {
        this.ssimCode = value;
    }

    /**
     * Gets the value of the bookingIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingIndicator() {
        return bookingIndicator;
    }

    /**
     * Sets the value of the bookingIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingIndicator(String value) {
        this.bookingIndicator = value;
    }

    /**
     * Gets the value of the vendor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendor() {
        return vendor;
    }

    /**
     * Sets the value of the vendor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendor(String value) {
        this.vendor = value;
    }

    /**
     * Gets the value of the emdType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMDType() {
        return emdType;
    }

    /**
     * Sets the value of the emdType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMDType(String value) {
        this.emdType = value;
    }

    /**
     * Gets the value of the emdNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMDNumber() {
        return emdNumber;
    }

    /**
     * Sets the value of the emdNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMDNumber(String value) {
        this.emdNumber = value;
    }

    /**
     * Gets the value of the emdCoupon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMDCoupon() {
        return emdCoupon;
    }

    /**
     * Sets the value of the emdCoupon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMDCoupon(String value) {
        this.emdCoupon = value;
    }

    /**
     * Gets the value of the serviceFeeLineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceFeeLineNumber() {
        return serviceFeeLineNumber;
    }

    /**
     * Sets the value of the serviceFeeLineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceFeeLineNumber(String value) {
        this.serviceFeeLineNumber = value;
    }

    /**
     * Gets the value of the displayOnlyIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisplayOnlyIndicator() {
        return displayOnlyIndicator;
    }

    /**
     * Sets the value of the displayOnlyIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayOnlyIndicator(Boolean value) {
        this.displayOnlyIndicator = value;
    }

    /**
     * Gets the value of the consumptionIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumptionIndicator() {
        return consumptionIndicator;
    }

    /**
     * Sets the value of the consumptionIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumptionIndicator(String value) {
        this.consumptionIndicator = value;
    }

    /**
     * Gets the value of the presentTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPresentTo() {
        return presentTo;
    }

    /**
     * Sets the value of the presentTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPresentTo(String value) {
        this.presentTo = value;
    }

    /**
     * Gets the value of the atCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtCity() {
        return atCity;
    }

    /**
     * Sets the value of the atCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtCity(String value) {
        this.atCity = value;
    }

    /**
     * Gets the value of the endorsements property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndorsements() {
        return endorsements;
    }

    /**
     * Sets the value of the endorsements property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndorsements(String value) {
        this.endorsements = value;
    }

    /**
     * Gets the value of the stationIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStationIndicator() {
        return stationIndicator;
    }

    /**
     * Sets the value of the stationIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStationIndicator(String value) {
        this.stationIndicator = value;
    }

    /**
     * Gets the value of the serviceCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceCity() {
        return serviceCity;
    }

    /**
     * Sets the value of the serviceCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceCity(String value) {
        this.serviceCity = value;
    }

    /**
     * Gets the value of the serviceCityDestination property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceCityDestination() {
        return serviceCityDestination;
    }

    /**
     * Sets the value of the serviceCityDestination property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceCityDestination(String value) {
        this.serviceCityDestination = value;
    }

    /**
     * Gets the value of the serviceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * Sets the value of the serviceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceType(String value) {
        this.serviceType = value;
    }

    /**
     * Gets the value of the ruleSet property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryServicesUpdatePNRB.RuleSet }
     *     
     */
    public AncillaryServicesUpdatePNRB.RuleSet getRuleSet() {
        return ruleSet;
    }

    /**
     * Sets the value of the ruleSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryServicesUpdatePNRB.RuleSet }
     *     
     */
    public void setRuleSet(AncillaryServicesUpdatePNRB.RuleSet value) {
        this.ruleSet = value;
    }

    /**
     * Gets the value of the originalPrice property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryServicesUpdatePNRB.OriginalPrice }
     *     
     */
    public AncillaryServicesUpdatePNRB.OriginalPrice getOriginalPrice() {
        return originalPrice;
    }

    /**
     * Sets the value of the originalPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryServicesUpdatePNRB.OriginalPrice }
     *     
     */
    public void setOriginalPrice(AncillaryServicesUpdatePNRB.OriginalPrice value) {
        this.originalPrice = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantity(BigInteger value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the segmentNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the segmentNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSegmentNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Byte }
     * 
     * 
     */
    public List<Byte> getSegmentNumber() {
        if (segmentNumber == null) {
            segmentNumber = new ArrayList<Byte>();
        }
        return this.segmentNumber;
    }

    /**
     * Gets the value of the equivalentPrice property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public AncillaryPricePNRB getEquivalentPrice() {
        return equivalentPrice;
    }

    /**
     * Sets the value of the equivalentPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public void setEquivalentPrice(AncillaryPricePNRB value) {
        this.equivalentPrice = value;
    }

    /**
     * Gets the value of the ttlPrice property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public AncillaryPricePNRB getTTLPrice() {
        return ttlPrice;
    }

    /**
     * Sets the value of the ttlPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public void setTTLPrice(AncillaryPricePNRB value) {
        this.ttlPrice = value;
    }

    /**
     * Gets the value of the portionOfTravelIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortionOfTravelIndicator() {
        return portionOfTravelIndicator;
    }

    /**
     * Sets the value of the portionOfTravelIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortionOfTravelIndicator(String value) {
        this.portionOfTravelIndicator = value;
    }

    /**
     * Gets the value of the originalBasePrice property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public AncillaryPricePNRB getOriginalBasePrice() {
        return originalBasePrice;
    }

    /**
     * Sets the value of the originalBasePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public void setOriginalBasePrice(AncillaryPricePNRB value) {
        this.originalBasePrice = value;
    }

    /**
     * Gets the value of the refundIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundIndicator() {
        return refundIndicator;
    }

    /**
     * Sets the value of the refundIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundIndicator(String value) {
        this.refundIndicator = value;
    }

    /**
     * Gets the value of the commisionIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommisionIndicator() {
        return commisionIndicator;
    }

    /**
     * Sets the value of the commisionIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommisionIndicator(String value) {
        this.commisionIndicator = value;
    }

    /**
     * Gets the value of the interlineIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterlineIndicator() {
        return interlineIndicator;
    }

    /**
     * Sets the value of the interlineIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterlineIndicator(String value) {
        this.interlineIndicator = value;
    }

    /**
     * Gets the value of the feeApplicationIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeApplicationIndicator() {
        return feeApplicationIndicator;
    }

    /**
     * Sets the value of the feeApplicationIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeApplicationIndicator(String value) {
        this.feeApplicationIndicator = value;
    }

    /**
     * Gets the value of the passengerTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassengerTypeCode() {
        return passengerTypeCode;
    }

    /**
     * Sets the value of the passengerTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassengerTypeCode(String value) {
        this.passengerTypeCode = value;
    }

    /**
     * Gets the value of the boardPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoardPoint() {
        return boardPoint;
    }

    /**
     * Sets the value of the boardPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoardPoint(String value) {
        this.boardPoint = value;
    }

    /**
     * Gets the value of the offPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffPoint() {
        return offPoint;
    }

    /**
     * Sets the value of the offPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffPoint(String value) {
        this.offPoint = value;
    }

    /**
     * Gets the value of the taxesIncluded property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTaxesIncluded() {
        return taxesIncluded;
    }

    /**
     * Sets the value of the taxesIncluded property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxesIncluded(Boolean value) {
        this.taxesIncluded = value;
    }

    /**
     * Gets the value of the taxes property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryServicesUpdatePNRB.Taxes }
     *     
     */
    public AncillaryServicesUpdatePNRB.Taxes getTaxes() {
        return taxes;
    }

    /**
     * Sets the value of the taxes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryServicesUpdatePNRB.Taxes }
     *     
     */
    public void setTaxes(AncillaryServicesUpdatePNRB.Taxes value) {
        this.taxes = value;
    }

    /**
     * Gets the value of the softMatchIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSoftMatchIndicator() {
        return softMatchIndicator;
    }

    /**
     * Sets the value of the softMatchIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSoftMatchIndicator(Boolean value) {
        this.softMatchIndicator = value;
    }

    /**
     * Gets the value of the noChargeNotAvailIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoChargeNotAvailIndicator() {
        return noChargeNotAvailIndicator;
    }

    /**
     * Sets the value of the noChargeNotAvailIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoChargeNotAvailIndicator(String value) {
        this.noChargeNotAvailIndicator = value;
    }

    /**
     * Gets the value of the ticketCouponNumberAssociation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ticketCouponNumberAssociation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTicketCouponNumberAssociation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AncillaryServicesUpdatePNRB.TicketCouponNumberAssociation }
     * 
     * 
     */
    public List<AncillaryServicesUpdatePNRB.TicketCouponNumberAssociation> getTicketCouponNumberAssociation() {
        if (ticketCouponNumberAssociation == null) {
            ticketCouponNumberAssociation = new ArrayList<AncillaryServicesUpdatePNRB.TicketCouponNumberAssociation>();
        }
        return this.ticketCouponNumberAssociation;
    }

    /**
     * Gets the value of the simultaneousTicketIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSimultaneousTicketIndicator() {
        return simultaneousTicketIndicator;
    }

    /**
     * Sets the value of the simultaneousTicketIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSimultaneousTicketIndicator(String value) {
        this.simultaneousTicketIndicator = value;
    }

    /**
     * Gets the value of the formOfRefund property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormOfRefund() {
        return formOfRefund;
    }

    /**
     * Sets the value of the formOfRefund property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormOfRefund(String value) {
        this.formOfRefund = value;
    }

    /**
     * Gets the value of the feeNotGuaranteedIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFeeNotGuaranteedIndicator() {
        return feeNotGuaranteedIndicator;
    }

    /**
     * Sets the value of the feeNotGuaranteedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFeeNotGuaranteedIndicator(Boolean value) {
        this.feeNotGuaranteedIndicator = value;
    }

    /**
     * Gets the value of the fqtvTierLevel property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getFQTVTierLevel() {
        return fqtvTierLevel;
    }

    /**
     * Sets the value of the fqtvTierLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setFQTVTierLevel(Short value) {
        this.fqtvTierLevel = value;
    }

    /**
     * Gets the value of the tourCodeSHC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTourCodeSHC() {
        return tourCodeSHC;
    }

    /**
     * Sets the value of the tourCodeSHC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTourCodeSHC(String value) {
        this.tourCodeSHC = value;
    }

    /**
     * Gets the value of the travelDateEffective property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTravelDateEffective() {
        return travelDateEffective;
    }

    /**
     * Sets the value of the travelDateEffective property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTravelDateEffective(String value) {
        this.travelDateEffective = value;
    }

    /**
     * Gets the value of the latestTravelDatePermitted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLatestTravelDatePermitted() {
        return latestTravelDatePermitted;
    }

    /**
     * Sets the value of the latestTravelDatePermitted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLatestTravelDatePermitted(String value) {
        this.latestTravelDatePermitted = value;
    }

    /**
     * Gets the value of the purchaseByDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPurchaseByDate() {
        return purchaseByDate;
    }

    /**
     * Sets the value of the purchaseByDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPurchaseByDate(XMLGregorianCalendar value) {
        this.purchaseByDate = value;
    }

    /**
     * Gets the value of the totalOriginalBasePrice property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public AncillaryPricePNRB getTotalOriginalBasePrice() {
        return totalOriginalBasePrice;
    }

    /**
     * Sets the value of the totalOriginalBasePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public void setTotalOriginalBasePrice(AncillaryPricePNRB value) {
        this.totalOriginalBasePrice = value;
    }

    /**
     * Gets the value of the totalEquivalentPrice property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public AncillaryPricePNRB getTotalEquivalentPrice() {
        return totalEquivalentPrice;
    }

    /**
     * Sets the value of the totalEquivalentPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public void setTotalEquivalentPrice(AncillaryPricePNRB value) {
        this.totalEquivalentPrice = value;
    }

    /**
     * Gets the value of the totalTTLPrice property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public AncillaryPricePNRB getTotalTTLPrice() {
        return totalTTLPrice;
    }

    /**
     * Sets the value of the totalTTLPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public void setTotalTTLPrice(AncillaryPricePNRB value) {
        this.totalTTLPrice = value;
    }

    /**
     * Gets the value of the totalTaxes property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryServicesUpdatePNRB.TotalTaxes }
     *     
     */
    public AncillaryServicesUpdatePNRB.TotalTaxes getTotalTaxes() {
        return totalTaxes;
    }

    /**
     * Sets the value of the totalTaxes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryServicesUpdatePNRB.TotalTaxes }
     *     
     */
    public void setTotalTaxes(AncillaryServicesUpdatePNRB.TotalTaxes value) {
        this.totalTaxes = value;
    }

    /**
     * Gets the value of the taxExemptIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxExemptIndicator() {
        return taxExemptIndicator;
    }

    /**
     * Sets the value of the taxExemptIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxExemptIndicator(String value) {
        this.taxExemptIndicator = value;
    }

    /**
     * Gets the value of the bagWeight property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryServicesUpdatePNRB.BagWeight }
     *     
     */
    public AncillaryServicesUpdatePNRB.BagWeight getBagWeight() {
        return bagWeight;
    }

    /**
     * Sets the value of the bagWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryServicesUpdatePNRB.BagWeight }
     *     
     */
    public void setBagWeight(AncillaryServicesUpdatePNRB.BagWeight value) {
        this.bagWeight = value;
    }

    /**
     * Gets the value of the statusIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStatusIndicator() {
        return statusIndicator;
    }

    /**
     * Sets the value of the statusIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStatusIndicator(BigInteger value) {
        this.statusIndicator = value;
    }

    /**
     * Gets the value of the numberOfItems property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberOfItems() {
        return numberOfItems;
    }

    /**
     * Sets the value of the numberOfItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberOfItems(String value) {
        this.numberOfItems = value;
    }

    /**
     * Gets the value of the actionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Sets the value of the actionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

    /**
     * Gets the value of the segmentIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmentIndicator() {
        return segmentIndicator;
    }

    /**
     * Sets the value of the segmentIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmentIndicator(String value) {
        this.segmentIndicator = value;
    }

    /**
     * Gets the value of the frequentFlyerTier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrequentFlyerTier() {
        return frequentFlyerTier;
    }

    /**
     * Sets the value of the frequentFlyerTier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrequentFlyerTier(String value) {
        this.frequentFlyerTier = value;
    }

    /**
     * Gets the value of the refundFormIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundFormIndicator() {
        return refundFormIndicator;
    }

    /**
     * Sets the value of the refundFormIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundFormIndicator(String value) {
        this.refundFormIndicator = value;
    }

    /**
     * Gets the value of the fareGuaranteedIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareGuaranteedIndicator() {
        return fareGuaranteedIndicator;
    }

    /**
     * Sets the value of the fareGuaranteedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareGuaranteedIndicator(String value) {
        this.fareGuaranteedIndicator = value;
    }

    /**
     * Gets the value of the serviceChargeIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceChargeIndicator() {
        return serviceChargeIndicator;
    }

    /**
     * Sets the value of the serviceChargeIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceChargeIndicator(String value) {
        this.serviceChargeIndicator = value;
    }

    /**
     * Gets the value of the advancePurchaseIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvancePurchaseIndicator() {
        return advancePurchaseIndicator;
    }

    /**
     * Sets the value of the advancePurchaseIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvancePurchaseIndicator(String value) {
        this.advancePurchaseIndicator = value;
    }

    /**
     * Gets the value of the bookingSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingSource() {
        return bookingSource;
    }

    /**
     * Sets the value of the bookingSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingSource(String value) {
        this.bookingSource = value;
    }

    /**
     * Gets the value of the taxIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxIndicator() {
        return taxIndicator;
    }

    /**
     * Sets the value of the taxIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxIndicator(String value) {
        this.taxIndicator = value;
    }

    /**
     * Gets the value of the ticketingIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketingIndicator() {
        return ticketingIndicator;
    }

    /**
     * Sets the value of the ticketingIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketingIndicator(String value) {
        this.ticketingIndicator = value;
    }

    /**
     * Gets the value of the feeWaiveReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeWaiveReason() {
        return feeWaiveReason;
    }

    /**
     * Sets the value of the feeWaiveReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeWaiveReason(String value) {
        this.feeWaiveReason = value;
    }

    /**
     * Gets the value of the fulfillmentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfillmentType() {
        return fulfillmentType;
    }

    /**
     * Sets the value of the fulfillmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfillmentType(String value) {
        this.fulfillmentType = value;
    }

    /**
     * Gets the value of the aaPayOriginalSeat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAaPayOriginalSeat() {
        return aaPayOriginalSeat;
    }

    /**
     * Sets the value of the aaPayOriginalSeat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAaPayOriginalSeat(String value) {
        this.aaPayOriginalSeat = value;
    }

    /**
     * Gets the value of the pdcSeat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPdcSeat() {
        return pdcSeat;
    }

    /**
     * Sets the value of the pdcSeat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPdcSeat(String value) {
        this.pdcSeat = value;
    }

    /**
     * Gets the value of the equipmentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentType() {
        return equipmentType;
    }

    /**
     * Sets the value of the equipmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentType(String value) {
        this.equipmentType = value;
    }

    /**
     * Gets the value of the aaPayOptionalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAaPayOptionalStatus() {
        return aaPayOptionalStatus;
    }

    /**
     * Sets the value of the aaPayOptionalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAaPayOptionalStatus(String value) {
        this.aaPayOptionalStatus = value;
    }

    /**
     * Gets the value of the firstTravelDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstTravelDate() {
        return firstTravelDate;
    }

    /**
     * Sets the value of the firstTravelDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstTravelDate(String value) {
        this.firstTravelDate = value;
    }

    /**
     * Gets the value of the lastTravelDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastTravelDate() {
        return lastTravelDate;
    }

    /**
     * Sets the value of the lastTravelDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastTravelDate(String value) {
        this.lastTravelDate = value;
    }

    /**
     * Gets the value of the ttyConfirmationTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTTYConfirmationTimestamp() {
        return ttyConfirmationTimestamp;
    }

    /**
     * Sets the value of the ttyConfirmationTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTTYConfirmationTimestamp(XMLGregorianCalendar value) {
        this.ttyConfirmationTimestamp = value;
    }

    /**
     * Gets the value of the purchaseTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPurchaseTimestamp() {
        return purchaseTimestamp;
    }

    /**
     * Sets the value of the purchaseTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPurchaseTimestamp(XMLGregorianCalendar value) {
        this.purchaseTimestamp = value;
    }

    /**
     * Gets the value of the brandedFareId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandedFareId() {
        return brandedFareId;
    }

    /**
     * Sets the value of the brandedFareId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandedFareId(String value) {
        this.brandedFareId = value;
    }

    /**
     * Gets the value of the groupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupCode() {
        return groupCode;
    }

    /**
     * Sets the value of the groupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupCode(String value) {
        this.groupCode = value;
    }

    /**
     * Gets the value of the tourCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTourCode() {
        return tourCode;
    }

    /**
     * Sets the value of the tourCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTourCode(String value) {
        this.tourCode = value;
    }

    /**
     * Gets the value of the emdPaperIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmdPaperIndicator() {
        return emdPaperIndicator;
    }

    /**
     * Sets the value of the emdPaperIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmdPaperIndicator(String value) {
        this.emdPaperIndicator = value;
    }

    /**
     * Gets the value of the seatRequestTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatRequestTransactionID() {
        return seatRequestTransactionID;
    }

    /**
     * Sets the value of the seatRequestTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatRequestTransactionID(String value) {
        this.seatRequestTransactionID = value;
    }

    /**
     * Gets the value of the ticketUsedForEMDPricing property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketUsedForEMDPricing() {
        return ticketUsedForEMDPricing;
    }

    /**
     * Sets the value of the ticketUsedForEMDPricing property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketUsedForEMDPricing(String value) {
        this.ticketUsedForEMDPricing = value;
    }

    /**
     * Gets the value of the emdConsummedAtIssuance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMDConsummedAtIssuance() {
        return emdConsummedAtIssuance;
    }

    /**
     * Sets the value of the emdConsummedAtIssuance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMDConsummedAtIssuance(String value) {
        this.emdConsummedAtIssuance = value;
    }

    /**
     * Gets the value of the paperDocRequired property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaperDocRequired() {
        return paperDocRequired;
    }

    /**
     * Sets the value of the paperDocRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaperDocRequired(String value) {
        this.paperDocRequired = value;
    }

    /**
     * Gets the value of the taxExemption property.
     * 
     * @return
     *     possible object is
     *     {@link IndicatorType }
     *     
     */
    public IndicatorType getTaxExemption() {
        return taxExemption;
    }

    /**
     * Sets the value of the taxExemption property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndicatorType }
     *     
     */
    public void setTaxExemption(IndicatorType value) {
        this.taxExemption = value;
    }

    /**
     * Gets the value of the acsCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getACSCount() {
        return acsCount;
    }

    /**
     * Sets the value of the acsCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setACSCount(BigInteger value) {
        this.acsCount = value;
    }

    /**
     * Gets the value of the netAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public AncillaryPricePNRB getNetAmount() {
        return netAmount;
    }

    /**
     * Sets the value of the netAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryPricePNRB }
     *     
     */
    public void setNetAmount(AncillaryPricePNRB value) {
        this.netAmount = value;
    }

    /**
     * Gets the value of the priceQuoteDesignator property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPriceQuoteDesignator() {
        return priceQuoteDesignator;
    }

    /**
     * Sets the value of the priceQuoteDesignator property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPriceQuoteDesignator(BigInteger value) {
        this.priceQuoteDesignator = value;
    }

    /**
     * Gets the value of the priceMismatchAction property.
     * 
     * @return
     *     possible object is
     *     {@link PriceMismatchActionType }
     *     
     */
    public PriceMismatchActionType getPriceMismatchAction() {
        return priceMismatchAction;
    }

    /**
     * Sets the value of the priceMismatchAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceMismatchActionType }
     *     
     */
    public void setPriceMismatchAction(PriceMismatchActionType value) {
        this.priceMismatchAction = value;
    }

    /**
     * Gets the value of the segment property.
     * 
     * @return
     *     possible object is
     *     {@link SegmentOrTravelPortionType }
     *     
     */
    public SegmentOrTravelPortionType getSegment() {
        return segment;
    }

    /**
     * Sets the value of the segment property.
     * 
     * @param value
     *     allowed object is
     *     {@link SegmentOrTravelPortionType }
     *     
     */
    public void setSegment(SegmentOrTravelPortionType value) {
        this.segment = value;
    }

    /**
     * Gets the value of the travelPortions property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryServicesUpdatePNRB.TravelPortions }
     *     
     */
    public AncillaryServicesUpdatePNRB.TravelPortions getTravelPortions() {
        return travelPortions;
    }

    /**
     * Sets the value of the travelPortions property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryServicesUpdatePNRB.TravelPortions }
     *     
     */
    public void setTravelPortions(AncillaryServicesUpdatePNRB.TravelPortions value) {
        this.travelPortions = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }

    /**
     * Gets the value of the elementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElementId() {
        return elementId;
    }

    /**
     * Sets the value of the elementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElementId(String value) {
        this.elementId = value;
    }

    /**
     * Gets the value of the updateId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdateId() {
        return updateId;
    }

    /**
     * Sets the value of the updateId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdateId(String value) {
        this.updateId = value;
    }


    /**
     * Used to hold the weight of the Bag for baggage ancillary types
     * 
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>integer">
     *       &lt;attribute name="Unit" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class BagWeight {

        @XmlValue
        protected BigInteger value;
        @XmlAttribute(name = "Unit")
        protected String unit;

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setValue(BigInteger value) {
            this.value = value;
        }

        /**
         * Gets the value of the unit property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnit() {
            return unit;
        }

        /**
         * Sets the value of the unit property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnit(String value) {
            this.unit = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>decimal">
     *       &lt;attribute name="WaiverCode" use="required" type="{http://webservices.sabre.com/pnrbuilder/v1_17}StringLength1to10" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class OriginalPrice {

        @XmlValue
        protected BigDecimal value;
        @XmlAttribute(name = "WaiverCode", required = true)
        protected String waiverCode;

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setValue(BigDecimal value) {
            this.value = value;
        }

        /**
         * Gets the value of the waiverCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWaiverCode() {
            return waiverCode;
        }

        /**
         * Sets the value of the waiverCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWaiverCode(String value) {
            this.waiverCode = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ProductTextDetailsItem" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;attribute name="ItemName" use="required" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" />
     *                 &lt;attribute name="ItemValue" use="required" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "productTextDetailsItem"
    })
    public static class ProductTextDetails {

        @XmlElement(name = "ProductTextDetailsItem")
        protected List<AncillaryServicesUpdatePNRB.ProductTextDetails.ProductTextDetailsItem> productTextDetailsItem;

        /**
         * Gets the value of the productTextDetailsItem property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the productTextDetailsItem property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProductTextDetailsItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AncillaryServicesUpdatePNRB.ProductTextDetails.ProductTextDetailsItem }
         * 
         * 
         */
        public List<AncillaryServicesUpdatePNRB.ProductTextDetails.ProductTextDetailsItem> getProductTextDetailsItem() {
            if (productTextDetailsItem == null) {
                productTextDetailsItem = new ArrayList<AncillaryServicesUpdatePNRB.ProductTextDetails.ProductTextDetailsItem>();
            }
            return this.productTextDetailsItem;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;attribute name="ItemName" use="required" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" />
         *       &lt;attribute name="ItemValue" use="required" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class ProductTextDetailsItem {

            @XmlAttribute(name = "ItemName", required = true)
            protected String itemName;
            @XmlAttribute(name = "ItemValue", required = true)
            protected String itemValue;

            /**
             * Gets the value of the itemName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getItemName() {
                return itemName;
            }

            /**
             * Sets the value of the itemName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setItemName(String value) {
                this.itemName = value;
            }

            /**
             * Gets the value of the itemValue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getItemValue() {
                return itemValue;
            }

            /**
             * Sets the value of the itemValue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setItemValue(String value) {
                this.itemValue = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RuleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Deal" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;choice>
     *                     &lt;element name="Percentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                     &lt;element name="Amount" minOccurs="0">
     *                       &lt;complexType>
     *                         &lt;simpleContent>
     *                           &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>decimal">
     *                             &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;/extension>
     *                         &lt;/simpleContent>
     *                       &lt;/complexType>
     *                     &lt;/element>
     *                   &lt;/choice>
     *                 &lt;/sequence>
     *                 &lt;attribute name="type" use="required">
     *                   &lt;simpleType>
     *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                       &lt;enumeration value="DISCOUNT"/>
     *                       &lt;enumeration value="RISE"/>
     *                       &lt;enumeration value="OVERRIDE"/>
     *                     &lt;/restriction>
     *                   &lt;/simpleType>
     *                 &lt;/attribute>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attribute name="RuleId" type="{http://www.w3.org/2001/XMLSchema}unsignedLong" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ruleName",
        "deal"
    })
    public static class RuleSet {

        @XmlElement(name = "RuleName")
        protected String ruleName;
        @XmlElement(name = "Deal")
        protected AncillaryServicesUpdatePNRB.RuleSet.Deal deal;
        @XmlAttribute(name = "RuleId")
        @XmlSchemaType(name = "unsignedLong")
        protected BigInteger ruleId;

        /**
         * Gets the value of the ruleName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRuleName() {
            return ruleName;
        }

        /**
         * Sets the value of the ruleName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRuleName(String value) {
            this.ruleName = value;
        }

        /**
         * Gets the value of the deal property.
         * 
         * @return
         *     possible object is
         *     {@link AncillaryServicesUpdatePNRB.RuleSet.Deal }
         *     
         */
        public AncillaryServicesUpdatePNRB.RuleSet.Deal getDeal() {
            return deal;
        }

        /**
         * Sets the value of the deal property.
         * 
         * @param value
         *     allowed object is
         *     {@link AncillaryServicesUpdatePNRB.RuleSet.Deal }
         *     
         */
        public void setDeal(AncillaryServicesUpdatePNRB.RuleSet.Deal value) {
            this.deal = value;
        }

        /**
         * Gets the value of the ruleId property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getRuleId() {
            return ruleId;
        }

        /**
         * Sets the value of the ruleId property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setRuleId(BigInteger value) {
            this.ruleId = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;choice>
         *           &lt;element name="Percentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *           &lt;element name="Amount" minOccurs="0">
         *             &lt;complexType>
         *               &lt;simpleContent>
         *                 &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>decimal">
         *                   &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;/extension>
         *               &lt;/simpleContent>
         *             &lt;/complexType>
         *           &lt;/element>
         *         &lt;/choice>
         *       &lt;/sequence>
         *       &lt;attribute name="type" use="required">
         *         &lt;simpleType>
         *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *             &lt;enumeration value="DISCOUNT"/>
         *             &lt;enumeration value="RISE"/>
         *             &lt;enumeration value="OVERRIDE"/>
         *           &lt;/restriction>
         *         &lt;/simpleType>
         *       &lt;/attribute>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "percentage",
            "amount"
        })
        public static class Deal {

            @XmlElement(name = "Percentage")
            protected BigDecimal percentage;
            @XmlElement(name = "Amount")
            protected AncillaryServicesUpdatePNRB.RuleSet.Deal.Amount amount;
            @XmlAttribute(name = "type", required = true)
            protected String type;

            /**
             * Gets the value of the percentage property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getPercentage() {
                return percentage;
            }

            /**
             * Sets the value of the percentage property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setPercentage(BigDecimal value) {
                this.percentage = value;
            }

            /**
             * Gets the value of the amount property.
             * 
             * @return
             *     possible object is
             *     {@link AncillaryServicesUpdatePNRB.RuleSet.Deal.Amount }
             *     
             */
            public AncillaryServicesUpdatePNRB.RuleSet.Deal.Amount getAmount() {
                return amount;
            }

            /**
             * Sets the value of the amount property.
             * 
             * @param value
             *     allowed object is
             *     {@link AncillaryServicesUpdatePNRB.RuleSet.Deal.Amount }
             *     
             */
            public void setAmount(AncillaryServicesUpdatePNRB.RuleSet.Deal.Amount value) {
                this.amount = value;
            }

            /**
             * Gets the value of the type property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getType() {
                return type;
            }

            /**
             * Sets the value of the type property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setType(String value) {
                this.type = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;simpleContent>
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>decimal">
             *       &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/extension>
             *   &lt;/simpleContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class Amount {

                @XmlValue
                protected BigDecimal value;
                @XmlAttribute(name = "currency")
                protected String currency;

                /**
                 * Gets the value of the value property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getValue() {
                    return value;
                }

                /**
                 * Sets the value of the value property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setValue(BigDecimal value) {
                    this.value = value;
                }

                /**
                 * Gets the value of the currency property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrency() {
                    return currency;
                }

                /**
                 * Sets the value of the currency property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrency(String value) {
                    this.currency = value;
                }

            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Tax" type="{http://webservices.sabre.com/pnrbuilder/v1_17}AncillaryTax.PNRB" maxOccurs="99" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tax"
    })
    public static class Taxes {

        @XmlElement(name = "Tax")
        protected List<AncillaryTaxPNRB> tax;

        /**
         * Gets the value of the tax property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the tax property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTax().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AncillaryTaxPNRB }
         * 
         * 
         */
        public List<AncillaryTaxPNRB> getTax() {
            if (tax == null) {
                tax = new ArrayList<AncillaryTaxPNRB>();
            }
            return this.tax;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TicketCouponNumber" type="{http://www.w3.org/2001/XMLSchema}short" maxOccurs="16" minOccurs="0"/>
     *         &lt;element name="TicketReferenceNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ticketCouponNumber",
        "ticketReferenceNumber"
    })
    public static class TicketCouponNumberAssociation {

        @XmlElement(name = "TicketCouponNumber", type = Short.class)
        protected List<Short> ticketCouponNumber;
        @XmlElement(name = "TicketReferenceNumber")
        protected String ticketReferenceNumber;

        /**
         * Gets the value of the ticketCouponNumber property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the ticketCouponNumber property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTicketCouponNumber().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Short }
         * 
         * 
         */
        public List<Short> getTicketCouponNumber() {
            if (ticketCouponNumber == null) {
                ticketCouponNumber = new ArrayList<Short>();
            }
            return this.ticketCouponNumber;
        }

        /**
         * Gets the value of the ticketReferenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTicketReferenceNumber() {
            return ticketReferenceNumber;
        }

        /**
         * Sets the value of the ticketReferenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTicketReferenceNumber(String value) {
            this.ticketReferenceNumber = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Tax" type="{http://webservices.sabre.com/pnrbuilder/v1_17}AncillaryTax.PNRB" maxOccurs="99" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tax"
    })
    public static class TotalTaxes {

        @XmlElement(name = "Tax")
        protected List<AncillaryTaxPNRB> tax;

        /**
         * Gets the value of the tax property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the tax property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTax().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AncillaryTaxPNRB }
         * 
         * 
         */
        public List<AncillaryTaxPNRB> getTax() {
            if (tax == null) {
                tax = new ArrayList<AncillaryTaxPNRB>();
            }
            return this.tax;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TravelPortion" type="{http://webservices.sabre.com/pnrbuilder/v1_17}SegmentOrTravelPortionType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "travelPortion"
    })
    public static class TravelPortions {

        @XmlElement(name = "TravelPortion")
        protected List<SegmentOrTravelPortionType> travelPortion;

        /**
         * Gets the value of the travelPortion property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the travelPortion property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTravelPortion().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SegmentOrTravelPortionType }
         * 
         * 
         */
        public List<SegmentOrTravelPortionType> getTravelPortion() {
            if (travelPortion == null) {
                travelPortion = new ArrayList<SegmentOrTravelPortionType>();
            }
            return this.travelPortion;
        }

    }

}
