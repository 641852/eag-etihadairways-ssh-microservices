
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Locator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SnapshotID" type="{http://webservices.sabre.com/pnrbuilder/v1_17}UUID" minOccurs="0"/>
 *         &lt;element name="RequestType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreateDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="ReturnOptions" type="{http://webservices.sabre.com/pnrbuilder/v1_17}ReturnOptions.PNRB" minOccurs="0"/>
 *         &lt;element name="POS" type="{http://webservices.sabre.com/pnrbuilder/v1_17}POS_Type.PNRB" minOccurs="0"/>
 *         &lt;element name="Poc" type="{http://webservices.sabre.com/pnrbuilder/v1_17}Poc" minOccurs="0"/>
 *         &lt;element name="RawContent" type="{http://webservices.sabre.com/pnrbuilder/v1_17}RawContentType" minOccurs="0"/>
 *         &lt;element name="ClientContext" type="{http://webservices.sabre.com/pnrbuilder/v1_17}ClientContext" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Version" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="EchoToken" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "locator",
    "snapshotID",
    "requestType",
    "createDate",
    "returnOptions",
    "pos",
    "poc",
    "rawContent",
    "clientContext"
})
@XmlRootElement(name = "GetReservationRQ")
public class GetReservationRQ {

    @XmlElement(name = "Locator")
    protected String locator;
    @XmlElement(name = "SnapshotID")
    protected String snapshotID;
    @XmlElement(name = "RequestType")
    protected String requestType;
    @XmlElement(name = "CreateDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar createDate;
    @XmlElement(name = "ReturnOptions")
    protected ReturnOptionsPNRB returnOptions;
    @XmlElement(name = "POS")
    protected POSTypePNRB pos;
    @XmlElement(name = "Poc")
    protected Poc poc;
    @XmlElement(name = "RawContent")
    protected RawContentType rawContent;
    @XmlElement(name = "ClientContext")
    protected ClientContext clientContext;
    @XmlAttribute(name = "Version")
    protected String version;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;

    /**
     * Gets the value of the locator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocator() {
        return locator;
    }

    /**
     * Sets the value of the locator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocator(String value) {
        this.locator = value;
    }

    /**
     * Gets the value of the snapshotID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSnapshotID() {
        return snapshotID;
    }

    /**
     * Sets the value of the snapshotID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSnapshotID(String value) {
        this.snapshotID = value;
    }

    /**
     * Gets the value of the requestType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestType() {
        return requestType;
    }

    /**
     * Sets the value of the requestType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestType(String value) {
        this.requestType = value;
    }

    /**
     * Gets the value of the createDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }

    /**
     * Sets the value of the createDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDate(XMLGregorianCalendar value) {
        this.createDate = value;
    }

    /**
     * Gets the value of the returnOptions property.
     * 
     * @return
     *     possible object is
     *     {@link ReturnOptionsPNRB }
     *     
     */
    public ReturnOptionsPNRB getReturnOptions() {
        return returnOptions;
    }

    /**
     * Sets the value of the returnOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReturnOptionsPNRB }
     *     
     */
    public void setReturnOptions(ReturnOptionsPNRB value) {
        this.returnOptions = value;
    }

    /**
     * Gets the value of the pos property.
     * 
     * @return
     *     possible object is
     *     {@link POSTypePNRB }
     *     
     */
    public POSTypePNRB getPOS() {
        return pos;
    }

    /**
     * Sets the value of the pos property.
     * 
     * @param value
     *     allowed object is
     *     {@link POSTypePNRB }
     *     
     */
    public void setPOS(POSTypePNRB value) {
        this.pos = value;
    }

    /**
     * Gets the value of the poc property.
     * 
     * @return
     *     possible object is
     *     {@link Poc }
     *     
     */
    public Poc getPoc() {
        return poc;
    }

    /**
     * Sets the value of the poc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Poc }
     *     
     */
    public void setPoc(Poc value) {
        this.poc = value;
    }

    /**
     * Gets the value of the rawContent property.
     * 
     * @return
     *     possible object is
     *     {@link RawContentType }
     *     
     */
    public RawContentType getRawContent() {
        return rawContent;
    }

    /**
     * Sets the value of the rawContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link RawContentType }
     *     
     */
    public void setRawContent(RawContentType value) {
        this.rawContent = value;
    }

    /**
     * Gets the value of the clientContext property.
     * 
     * @return
     *     possible object is
     *     {@link ClientContext }
     *     
     */
    public ClientContext getClientContext() {
        return clientContext;
    }

    /**
     * Sets the value of the clientContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClientContext }
     *     
     */
    public void setClientContext(ClientContext value) {
        this.clientContext = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the echoToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Sets the value of the echoToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

}
