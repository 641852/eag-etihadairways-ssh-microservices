
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.sabre.services.res.or.v1_11.OpenReservationElementType;


/**
 * <p>Java class for OpenReservationElementHistoryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OpenReservationElementHistoryType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://services.sabre.com/res/or/v1_11}OpenReservationElementType">
 *       &lt;sequence>
 *         &lt;element name="HistoryAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HistoryTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="OpenReservationElementId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpenReservationElementHistoryType", propOrder = {
    "historyAction",
    "historyTimestamp"
})
public class OpenReservationElementHistoryType
    extends OpenReservationElementType
{

    @XmlElement(name = "HistoryAction")
    protected String historyAction;
    @XmlElement(name = "HistoryTimestamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar historyTimestamp;
    @XmlAttribute(name = "OpenReservationElementId")
    protected String openReservationElementId;

    /**
     * Gets the value of the historyAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHistoryAction() {
        return historyAction;
    }

    /**
     * Sets the value of the historyAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHistoryAction(String value) {
        this.historyAction = value;
    }

    /**
     * Gets the value of the historyTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHistoryTimestamp() {
        return historyTimestamp;
    }

    /**
     * Sets the value of the historyTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHistoryTimestamp(XMLGregorianCalendar value) {
        this.historyTimestamp = value;
    }

    /**
     * Gets the value of the openReservationElementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpenReservationElementId() {
        return openReservationElementId;
    }

    /**
     * Sets the value of the openReservationElementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpenReservationElementId(String value) {
        this.openReservationElementId = value;
    }

}
