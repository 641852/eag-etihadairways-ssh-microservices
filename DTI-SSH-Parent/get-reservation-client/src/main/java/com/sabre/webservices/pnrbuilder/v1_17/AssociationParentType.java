
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssociationParentType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AssociationParentType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NameNumber"/>
 *     &lt;enumeration value="NameSeqId"/>
 *     &lt;enumeration value="FOP"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AssociationParentType")
@XmlEnum
public enum AssociationParentType {

    @XmlEnumValue("NameNumber")
    NAME_NUMBER("NameNumber"),
    @XmlEnumValue("NameSeqId")
    NAME_SEQ_ID("NameSeqId"),
    FOP("FOP");
    private final String value;

    AssociationParentType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AssociationParentType fromValue(String v) {
        for (AssociationParentType c: AssociationParentType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
