
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PassengerNameUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PassengerNameUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TravelerName" type="{http://webservices.sabre.com/pnrbuilder/v1_17}TravelerName" minOccurs="0"/>
 *         &lt;element name="NameAssociationTag" type="{http://webservices.sabre.com/pnrbuilder/v1_17}NameAssociationTag.PNRB" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_17}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_17}OperationType.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PassengerNameUpdate.PNRB", propOrder = {
    "travelerName",
    "nameAssociationTag"
})
public class PassengerNameUpdatePNRB {

    @XmlElement(name = "TravelerName")
    protected TravelerName travelerName;
    @XmlElement(name = "NameAssociationTag")
    protected NameAssociationTagPNRB nameAssociationTag;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;

    /**
     * Gets the value of the travelerName property.
     * 
     * @return
     *     possible object is
     *     {@link TravelerName }
     *     
     */
    public TravelerName getTravelerName() {
        return travelerName;
    }

    /**
     * Sets the value of the travelerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link TravelerName }
     *     
     */
    public void setTravelerName(TravelerName value) {
        this.travelerName = value;
    }

    /**
     * Gets the value of the nameAssociationTag property.
     * 
     * @return
     *     possible object is
     *     {@link NameAssociationTagPNRB }
     *     
     */
    public NameAssociationTagPNRB getNameAssociationTag() {
        return nameAssociationTag;
    }

    /**
     * Sets the value of the nameAssociationTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameAssociationTagPNRB }
     *     
     */
    public void setNameAssociationTag(NameAssociationTagPNRB value) {
        this.nameAssociationTag = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }

}
