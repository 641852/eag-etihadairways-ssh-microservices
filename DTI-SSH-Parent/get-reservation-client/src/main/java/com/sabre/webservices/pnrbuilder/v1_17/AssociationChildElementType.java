
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssociationChildElementType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AssociationChildElementType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Remark"/>
 *     &lt;enumeration value="PhoneNumber"/>
 *     &lt;enumeration value="Ticket"/>
 *     &lt;enumeration value="AccountingLine"/>
 *     &lt;enumeration value="FOP"/>
 *     &lt;enumeration value="FopUsageType"/>
 *     &lt;enumeration value="FopTripType"/>
 *     &lt;enumeration value="AccountingField"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AssociationChildElementType")
@XmlEnum
public enum AssociationChildElementType {

    @XmlEnumValue("Remark")
    REMARK("Remark"),
    @XmlEnumValue("PhoneNumber")
    PHONE_NUMBER("PhoneNumber"),
    @XmlEnumValue("Ticket")
    TICKET("Ticket"),
    @XmlEnumValue("AccountingLine")
    ACCOUNTING_LINE("AccountingLine"),
    FOP("FOP"),
    @XmlEnumValue("FopUsageType")
    FOP_USAGE_TYPE("FopUsageType"),
    @XmlEnumValue("FopTripType")
    FOP_TRIP_TYPE("FopTripType"),
    @XmlEnumValue("AccountingField")
    ACCOUNTING_FIELD("AccountingField");
    private final String value;

    AssociationChildElementType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AssociationChildElementType fromValue(String v) {
        for (AssociationChildElementType c: AssociationChildElementType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
