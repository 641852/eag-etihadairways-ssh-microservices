
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FrequentTravelerNameSpecificType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FrequentTravelerNameSpecificType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservices.sabre.com/pnrbuilder/v1_17}FlightBasicData">
 *       &lt;sequence>
 *         &lt;element name="FrequentTravelerAirline" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FrequentTravelerNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FreeText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SSRPreference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SeatPreference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PassengerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FrequentTravelerNameSpecificType", propOrder = {
    "frequentTravelerAirline",
    "frequentTravelerNumber",
    "freeText",
    "ssrPreference",
    "seatPreference",
    "passengerName"
})
public class FrequentTravelerNameSpecificType
    extends FlightBasicData
{

    @XmlElement(name = "FrequentTravelerAirline")
    protected String frequentTravelerAirline;
    @XmlElement(name = "FrequentTravelerNumber")
    protected String frequentTravelerNumber;
    @XmlElement(name = "FreeText")
    protected String freeText;
    @XmlElement(name = "SSRPreference")
    protected String ssrPreference;
    @XmlElement(name = "SeatPreference")
    protected String seatPreference;
    @XmlElement(name = "PassengerName")
    protected String passengerName;

    /**
     * Gets the value of the frequentTravelerAirline property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrequentTravelerAirline() {
        return frequentTravelerAirline;
    }

    /**
     * Sets the value of the frequentTravelerAirline property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrequentTravelerAirline(String value) {
        this.frequentTravelerAirline = value;
    }

    /**
     * Gets the value of the frequentTravelerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrequentTravelerNumber() {
        return frequentTravelerNumber;
    }

    /**
     * Sets the value of the frequentTravelerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrequentTravelerNumber(String value) {
        this.frequentTravelerNumber = value;
    }

    /**
     * Gets the value of the freeText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreeText() {
        return freeText;
    }

    /**
     * Sets the value of the freeText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreeText(String value) {
        this.freeText = value;
    }

    /**
     * Gets the value of the ssrPreference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSRPreference() {
        return ssrPreference;
    }

    /**
     * Sets the value of the ssrPreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSRPreference(String value) {
        this.ssrPreference = value;
    }

    /**
     * Gets the value of the seatPreference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatPreference() {
        return seatPreference;
    }

    /**
     * Sets the value of the seatPreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatPreference(String value) {
        this.seatPreference = value;
    }

    /**
     * Gets the value of the passengerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassengerName() {
        return passengerName;
    }

    /**
     * Sets the value of the passengerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassengerName(String value) {
        this.passengerName = value;
    }

}
