
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RequestorID.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestorID.PNRB">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservices.sabre.com/pnrbuilder/v1_17}UniqueID_Type.PNRB">
 *       &lt;attribute name="MessagePassword" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestorID.PNRB")
public class RequestorIDPNRB
    extends UniqueIDTypePNRB
{

    @XmlAttribute(name = "MessagePassword")
    protected String messagePassword;

    /**
     * Gets the value of the messagePassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessagePassword() {
        return messagePassword;
    }

    /**
     * Sets the value of the messagePassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessagePassword(String value) {
        this.messagePassword = value;
    }

}
