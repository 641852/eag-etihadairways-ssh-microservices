
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Transaction.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Transaction.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="StatelessTransaction" type="{http://webservices.sabre.com/pnrbuilder/v1_17}StatelessTransaction.PNRB" minOccurs="0"/>
 *         &lt;element name="StatefulTransaction" type="{http://webservices.sabre.com/pnrbuilder/v1_17}StatefulTransaction.PNRB" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Transaction.PNRB", propOrder = {
    "statelessTransaction",
    "statefulTransaction"
})
public class TransactionPNRB {

    @XmlElement(name = "StatelessTransaction")
    protected StatelessTransactionPNRB statelessTransaction;
    @XmlElement(name = "StatefulTransaction")
    protected StatefulTransactionPNRB statefulTransaction;

    /**
     * Gets the value of the statelessTransaction property.
     * 
     * @return
     *     possible object is
     *     {@link StatelessTransactionPNRB }
     *     
     */
    public StatelessTransactionPNRB getStatelessTransaction() {
        return statelessTransaction;
    }

    /**
     * Sets the value of the statelessTransaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatelessTransactionPNRB }
     *     
     */
    public void setStatelessTransaction(StatelessTransactionPNRB value) {
        this.statelessTransaction = value;
    }

    /**
     * Gets the value of the statefulTransaction property.
     * 
     * @return
     *     possible object is
     *     {@link StatefulTransactionPNRB }
     *     
     */
    public StatefulTransactionPNRB getStatefulTransaction() {
        return statefulTransaction;
    }

    /**
     * Sets the value of the statefulTransaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatefulTransactionPNRB }
     *     
     */
    public void setStatefulTransaction(StatefulTransactionPNRB value) {
        this.statefulTransaction = value;
    }

}
