
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for RequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://webservices.sabre.com/pnrbuilder/v1_17>RequestEnumerationType">
 *       &lt;attribute name="commitTransaction" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="initialIgnore" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestType", propOrder = {
    "value"
})
public class RequestType {

    @XmlValue
    protected RequestEnumerationType value;
    @XmlAttribute(name = "commitTransaction")
    protected Boolean commitTransaction;
    @XmlAttribute(name = "initialIgnore")
    protected Boolean initialIgnore;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link RequestEnumerationType }
     *     
     */
    public RequestEnumerationType getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestEnumerationType }
     *     
     */
    public void setValue(RequestEnumerationType value) {
        this.value = value;
    }

    /**
     * Gets the value of the commitTransaction property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCommitTransaction() {
        return commitTransaction;
    }

    /**
     * Sets the value of the commitTransaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCommitTransaction(Boolean value) {
        this.commitTransaction = value;
    }

    /**
     * Gets the value of the initialIgnore property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInitialIgnore() {
        return initialIgnore;
    }

    /**
     * Sets the value of the initialIgnore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInitialIgnore(Boolean value) {
        this.initialIgnore = value;
    }

}
