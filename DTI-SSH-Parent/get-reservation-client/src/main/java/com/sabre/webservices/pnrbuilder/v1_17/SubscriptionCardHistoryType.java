
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubscriptionCardHistoryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubscriptionCardHistoryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HistoryAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FreeText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="SubscriptionCardBasic" type="{http://webservices.sabre.com/pnrbuilder/v1_17}SubscriptionCardBasicHistoryType" minOccurs="0"/>
 *           &lt;element name="SubscriptionCardSegment" type="{http://webservices.sabre.com/pnrbuilder/v1_17}SubscriptionCardSegmentHistoryType" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubscriptionCardHistoryType", propOrder = {
    "historyAction",
    "cardNumber",
    "freeText",
    "subscriptionCardBasic",
    "subscriptionCardSegment"
})
public class SubscriptionCardHistoryType {

    @XmlElement(name = "HistoryAction")
    protected String historyAction;
    @XmlElement(name = "CardNumber")
    protected String cardNumber;
    @XmlElement(name = "FreeText")
    protected String freeText;
    @XmlElement(name = "SubscriptionCardBasic")
    protected SubscriptionCardBasicHistoryType subscriptionCardBasic;
    @XmlElement(name = "SubscriptionCardSegment")
    protected SubscriptionCardSegmentHistoryType subscriptionCardSegment;

    /**
     * Gets the value of the historyAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHistoryAction() {
        return historyAction;
    }

    /**
     * Sets the value of the historyAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHistoryAction(String value) {
        this.historyAction = value;
    }

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the freeText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreeText() {
        return freeText;
    }

    /**
     * Sets the value of the freeText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreeText(String value) {
        this.freeText = value;
    }

    /**
     * Gets the value of the subscriptionCardBasic property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriptionCardBasicHistoryType }
     *     
     */
    public SubscriptionCardBasicHistoryType getSubscriptionCardBasic() {
        return subscriptionCardBasic;
    }

    /**
     * Sets the value of the subscriptionCardBasic property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriptionCardBasicHistoryType }
     *     
     */
    public void setSubscriptionCardBasic(SubscriptionCardBasicHistoryType value) {
        this.subscriptionCardBasic = value;
    }

    /**
     * Gets the value of the subscriptionCardSegment property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriptionCardSegmentHistoryType }
     *     
     */
    public SubscriptionCardSegmentHistoryType getSubscriptionCardSegment() {
        return subscriptionCardSegment;
    }

    /**
     * Sets the value of the subscriptionCardSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriptionCardSegmentHistoryType }
     *     
     */
    public void setSubscriptionCardSegment(SubscriptionCardSegmentHistoryType value) {
        this.subscriptionCardSegment = value;
    }

}
