
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GenderDOCS_Entry.PNRB.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GenderDOCS_Entry.PNRB">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="M"/>
 *     &lt;enumeration value="F"/>
 *     &lt;enumeration value="MI"/>
 *     &lt;enumeration value="FI"/>
 *     &lt;enumeration value="U"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GenderDOCS_Entry.PNRB")
@XmlEnum
public enum GenderDOCSEntryPNRB {

    M,
    F,
    MI,
    FI,
    U;

    public String value() {
        return name();
    }

    public static GenderDOCSEntryPNRB fromValue(String v) {
        return valueOf(v);
    }

}
