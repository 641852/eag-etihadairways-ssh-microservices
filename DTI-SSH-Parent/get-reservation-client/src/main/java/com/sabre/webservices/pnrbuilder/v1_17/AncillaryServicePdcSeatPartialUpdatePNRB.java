
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AncillaryServicePdcSeatPartialUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AncillaryServicePdcSeatPartialUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PdcSeat" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AncillaryServicePdcSeatPartialUpdate.PNRB", propOrder = {
    "pdcSeat"
})
public class AncillaryServicePdcSeatPartialUpdatePNRB {

    @XmlElement(name = "PdcSeat", required = true)
    protected String pdcSeat;

    /**
     * Gets the value of the pdcSeat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPdcSeat() {
        return pdcSeat;
    }

    /**
     * Sets the value of the pdcSeat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPdcSeat(String value) {
        this.pdcSeat = value;
    }

}
