
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Each remark line can contain a max of 70 chars
 * 
 *                 The
 *                 entire remarks field can contain a max of 32,767
 *                 chars.
 * 
 *                 Remarks
 *                 formats:
 * 
 *                 * add remark
 * 
 *                 5(free text) 5JASON AGE 6YRS
 * 
 *                 * add more than one
 *                 line of remark in one entry
 * 
 *                 5(free text)�5(free text) 5CHECK
 *                 PASSPORTS�5CHECK
 *                 ADDRESS FIELD
 * 
 *                 * add historical remarks to store info
 *                 in history
 * 
 *                 5H-(free text) 5H-FARE QUOTED 690.00
 * 
 *                 * enter alpha coded
 *                 remarks
 * 
 *                 5(letter)�(free text) 5H�HOTEL NEEDED IN SWITZERLAND
 * 
 *                 5C�CALL
 *                 AND MAKE CAR RESERVATIONS ON DEC 23
 * 
 *                 * add corporate identification
 *                 number
 * 
 *                 5C-CORP(corporate #) 5C-CORPD14569812
 * 
 *                 * enter hidden remark
 * 
 *                 5HR-(free text) 5HR-XYZ CORP PSGRS RECIEVE FOLLOWING
 *                 DISCOUNTS 5HR-25
 *                 PERCENT OFF ALL CAR RENTALS
 * 
 *                 NOTE: max 70 chars after the 5. Total
 *                 size of complete
 *                 remarks field in any PNR is 32.762. To enter
 *                 multiple
 *                 lines use the �
 * 
 *                 * modify basic remark
 * 
 *                 5(line #)�(new text)
 *                 59�YOU MUST BRING YOUR PHOTO ID
 * 
 *                 * modify an invoice remark
 * 
 *                 5(line
 *                 #)�.(new text) 52�.THANKS
 * 
 *                 * modify an itinerary remark
 * 
 *                 5(line #)��(new
 *                 text) 59��HAVE A GOOD TRIP
 * 
 *                 * delete remarks
 * 
 *                 5(line #)� 53�
 * 
 *                 *delete a
 *                 range of remarks lines
 * 
 *                 5(line #)-(line #)� 59-12�
 * 
 *                 * delete specific
 *                 remarks lines
 * 
 *                 5(line #),(line #)� 59,12�
 * 
 *                 NOTE: When you delete
 *                 specific remarks lines, you can
 *                 delete a max of 5 per entry.
 * 
 *                 * insert
 *                 a new remark after specified remarks line
 * 
 *                 5(line # new text is to
 *                 follow)/(free text) 52/PLEASE
 *                 ALLOW EXTRA TIME DUE TO AIRPORT
 *                 CONSTRUCTION
 * 
 *                 * insert a new remark as the first remark in a PNR
 * 
 *                 50/(free text) 50/HAVE ADVISED ALL DOCS
 * 
 *                 * move and insert specific
 *                 remarks
 * 
 *                 5(remark line you want to insert after)//(remark line to
 *                 be
 *                 moved),(remark line to be moved) 57//10,2
 * 
 *                 NOTE: the above moves
 *                 remarks 10 and 2 and inserts them
 *                 after remark 7
 * 
 *                 * move and insert a
 *                 range of remarks
 * 
 *                 5(remark line you want to insert after)//(range of
 *                 remarks) 57//3-5
 * 
 *                 * combine a range of remarks and specific remarks
 *                 lines
 * 
 *                 5(remark line you want to insert after)//(range of
 *                 remarks),(specific remark line) 57//3-5,9
 * 
 *                 * reverse order of remarks
 *                 - example from 1 through 10
 * 
 *                 50//(remark lines in descending order)
 *                 50//10,9,8,7,6,5,4,3,2,1
 *             
 * 
 * <p>Java class for Remark.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Remark.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RemarkLines" type="{http://webservices.sabre.com/pnrbuilder/v1_17}RemarkLines.PNRB" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="index" type="{http://webservices.sabre.com/pnrbuilder/v1_17}Numeric0to99999" />
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_17}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="type" type="{http://webservices.sabre.com/pnrbuilder/v1_17}RemarkType.PNRB" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_17}OperationType.PNRB" />
 *       &lt;attribute name="code">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;pattern value="[A-Z]"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="segmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="elementId" type="{http://webservices.sabre.com/pnrbuilder/v1_17}AssociationMatrixID.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Remark.PNRB", propOrder = {
    "remarkLines"
})
public class RemarkPNRB {

    @XmlElement(name = "RemarkLines")
    protected RemarkLinesPNRB remarkLines;
    @XmlAttribute(name = "index")
    protected Integer index;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "type")
    protected RemarkTypePNRB type;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;
    @XmlAttribute(name = "code")
    protected String code;
    @XmlAttribute(name = "segmentNumber")
    protected String segmentNumber;
    @XmlAttribute(name = "elementId")
    protected String elementId;

    /**
     * Gets the value of the remarkLines property.
     * 
     * @return
     *     possible object is
     *     {@link RemarkLinesPNRB }
     *     
     */
    public RemarkLinesPNRB getRemarkLines() {
        return remarkLines;
    }

    /**
     * Sets the value of the remarkLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link RemarkLinesPNRB }
     *     
     */
    public void setRemarkLines(RemarkLinesPNRB value) {
        this.remarkLines = value;
    }

    /**
     * Gets the value of the index property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIndex() {
        return index;
    }

    /**
     * Sets the value of the index property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIndex(Integer value) {
        this.index = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link RemarkTypePNRB }
     *     
     */
    public RemarkTypePNRB getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link RemarkTypePNRB }
     *     
     */
    public void setType(RemarkTypePNRB value) {
        this.type = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the segmentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmentNumber() {
        return segmentNumber;
    }

    /**
     * Sets the value of the segmentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmentNumber(String value) {
        this.segmentNumber = value;
    }

    /**
     * Gets the value of the elementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElementId() {
        return elementId;
    }

    /**
     * Sets the value of the elementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElementId(String value) {
        this.elementId = value;
    }

}
