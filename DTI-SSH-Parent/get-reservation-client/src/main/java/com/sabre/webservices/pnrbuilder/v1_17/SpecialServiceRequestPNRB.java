
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SpecialServiceRequest.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SpecialServiceRequest.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NameAssociationList" type="{http://webservices.sabre.com/pnrbuilder/v1_17}NameAssociationList.PNRB" minOccurs="0"/>
 *         &lt;element name="SegmentAssociationList" type="{http://webservices.sabre.com/pnrbuilder/v1_17}SegmentAssociationList.PNRB" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="SpecialServiceRequestText" type="{http://webservices.sabre.com/pnrbuilder/v1_17}CommonString" minOccurs="0"/>
 *           &lt;element name="SpecialService" type="{http://webservices.sabre.com/pnrbuilder/v1_17}SpecialService.PNRB" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_17}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="type" type="{http://webservices.sabre.com/pnrbuilder/v1_17}FactType.PNRB" default="H" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpecialServiceRequest.PNRB", propOrder = {
    "nameAssociationList",
    "segmentAssociationList",
    "specialServiceRequestText",
    "specialService"
})
public class SpecialServiceRequestPNRB {

    @XmlElement(name = "NameAssociationList")
    protected NameAssociationListPNRB nameAssociationList;
    @XmlElement(name = "SegmentAssociationList")
    protected SegmentAssociationListPNRB segmentAssociationList;
    @XmlElement(name = "SpecialServiceRequestText")
    protected String specialServiceRequestText;
    @XmlElement(name = "SpecialService")
    protected SpecialServicePNRB specialService;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "type")
    protected FactTypePNRB type;

    /**
     * Gets the value of the nameAssociationList property.
     * 
     * @return
     *     possible object is
     *     {@link NameAssociationListPNRB }
     *     
     */
    public NameAssociationListPNRB getNameAssociationList() {
        return nameAssociationList;
    }

    /**
     * Sets the value of the nameAssociationList property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameAssociationListPNRB }
     *     
     */
    public void setNameAssociationList(NameAssociationListPNRB value) {
        this.nameAssociationList = value;
    }

    /**
     * Gets the value of the segmentAssociationList property.
     * 
     * @return
     *     possible object is
     *     {@link SegmentAssociationListPNRB }
     *     
     */
    public SegmentAssociationListPNRB getSegmentAssociationList() {
        return segmentAssociationList;
    }

    /**
     * Sets the value of the segmentAssociationList property.
     * 
     * @param value
     *     allowed object is
     *     {@link SegmentAssociationListPNRB }
     *     
     */
    public void setSegmentAssociationList(SegmentAssociationListPNRB value) {
        this.segmentAssociationList = value;
    }

    /**
     * Gets the value of the specialServiceRequestText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialServiceRequestText() {
        return specialServiceRequestText;
    }

    /**
     * Sets the value of the specialServiceRequestText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialServiceRequestText(String value) {
        this.specialServiceRequestText = value;
    }

    /**
     * Gets the value of the specialService property.
     * 
     * @return
     *     possible object is
     *     {@link SpecialServicePNRB }
     *     
     */
    public SpecialServicePNRB getSpecialService() {
        return specialService;
    }

    /**
     * Sets the value of the specialService property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecialServicePNRB }
     *     
     */
    public void setSpecialService(SpecialServicePNRB value) {
        this.specialService = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link FactTypePNRB }
     *     
     */
    public FactTypePNRB getType() {
        if (type == null) {
            return FactTypePNRB.H;
        } else {
            return type;
        }
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link FactTypePNRB }
     *     
     */
    public void setType(FactTypePNRB value) {
        this.type = value;
    }

}
