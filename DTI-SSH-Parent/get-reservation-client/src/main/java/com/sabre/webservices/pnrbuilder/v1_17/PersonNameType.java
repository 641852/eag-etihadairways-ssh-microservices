
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PersonNameType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PersonNameType">
 *   &lt;restriction base="{http://webservices.sabre.com/pnrbuilder/v1_17}StringLength0to32">
 *     &lt;enumeration value="Former"/>
 *     &lt;enumeration value="Nickname"/>
 *     &lt;enumeration value="Alternate"/>
 *     &lt;enumeration value="Maiden"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PersonNameType")
@XmlEnum
public enum PersonNameType {

    @XmlEnumValue("Former")
    FORMER("Former"),
    @XmlEnumValue("Nickname")
    NICKNAME("Nickname"),
    @XmlEnumValue("Alternate")
    ALTERNATE("Alternate"),
    @XmlEnumValue("Maiden")
    MAIDEN("Maiden");
    private final String value;

    PersonNameType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PersonNameType fromValue(String v) {
        for (PersonNameType c: PersonNameType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
