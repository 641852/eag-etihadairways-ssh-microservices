
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AncillaryServiceSeatRequestTransactionIDUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AncillaryServiceSeatRequestTransactionIDUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SeatRequestTransactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AncillaryServiceSeatRequestTransactionIDUpdate.PNRB", propOrder = {
    "seatRequestTransactionID"
})
public class AncillaryServiceSeatRequestTransactionIDUpdatePNRB {

    @XmlElement(name = "SeatRequestTransactionID", required = true)
    protected String seatRequestTransactionID;

    /**
     * Gets the value of the seatRequestTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatRequestTransactionID() {
        return seatRequestTransactionID;
    }

    /**
     * Sets the value of the seatRequestTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatRequestTransactionID(String value) {
        this.seatRequestTransactionID = value;
    }

}
