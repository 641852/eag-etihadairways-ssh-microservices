
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PriceMismatchActionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PriceMismatchActionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="REJECT"/>
 *     &lt;enumeration value="ACCEPT_ANY_PRICE"/>
 *     &lt;enumeration value="ACCEPT_LOWER_PRICE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PriceMismatchActionType")
@XmlEnum
public enum PriceMismatchActionType {

    REJECT,
    ACCEPT_ANY_PRICE,
    ACCEPT_LOWER_PRICE;

    public String value() {
        return name();
    }

    public static PriceMismatchActionType fromValue(String v) {
        return valueOf(v);
    }

}
