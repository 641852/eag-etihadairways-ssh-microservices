/**
 * 
 */
package com.etihad.book.utilities;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

/**
 * @author 375669
 *
 */
public class RestUtils {

	public static String path;
	
	
	/**
	 * Sets base URI
	 * @param baseURI
	 */
	public static void setBaseURI(String baseURI){
		
		RestAssured.baseURI = baseURI;
		
	}
	
	/**
	 * Sets base Path
	 * @param basePath
	 */
	public static void setBasePath(String basePath){
		
		RestAssured.basePath = basePath;
	}
	
	
	
	/**
	 * Reset base URI to null after each text execution
	 * @param baseURI
	 */
	public static void resetBaseURI(String baseURI){
		
		RestAssured.baseURI = null;
	}
	
	
	
	/**
	 * Reset base Path to null after each test execution
	 * @param basePath
	 */
	public static void resetBasePath(String basePath){
		
		RestAssured.basePath = null;
	}
	
	
	/**
	 * Sets Content type, content type should be set as JSON or XML
	 * @param Type
	 */
	public static void setContentType(ContentType Type){
		
		RestAssured.given().contentType(Type);
	}
	
	
	public static void createSearchQueryPath(String searchTerm,
			String jsonPath,String param, 
			String paramValue){
		
		path = searchTerm + "/" + jsonPath + "?" + param + "=" + paramValue;
		
	}
	
	/**
	 * 
	 * @return Response of the API
	 */
	public static Response getResponse(){
		
		return RestAssured.get(path);
	}
	
	
	/**
	 * Returns JsonPath Object. First converts API's response to String
	 * And then send this string formatted Json response to JsonPath class and return the JsonPath
	 * @param res
	 * @return
	 */
	public static JsonPath getJsonPath(Response res){
		
		String json = res.asString();
		return new JsonPath(json);
		
	}
	
	
}
